-- Angle Indicator Module
---@diagnostic disable: lowercase-global

local local_player = entity.get_local_player()
local screen_size = {client.screen_size()}
local center, center2 = screen_size[1] / 2, screen_size[2] / 2
local module = {}
local white = {255, 255, 255, 255}
local yaw, yaw2, AbsRotation, angle = 0, 0, 0, 0

local function k(l)
    l = (l % 360 + 360) % 360
    return l > 180 and l - 360 or l
end

function module.on_setup_command(m)
    if m.chokedcommands == 0 then
        yaw = m.yaw
    elseif m.chokedcommands == 1 then
        yaw2 = m.yaw - yaw
    end
end

function module.on_run_command()
    local_player = entity.get_local_player()
    camera = {client.camera_angles()}
    _, AbsRotation = entity.get_prop(local_player, "m_angAbsRotation")
    yaw2 = yaw2 < 1 and yaw2 > 0.0001 and math.floor(yaw2, 1) or yaw2
    if camera[2] ~= nil and AbsRotation ~= nil and 60 < math.floor(math.abs(k(camera[2] - (AbsRotation + yaw2)))) then
        yaw2 = yaw2 * -1
    end
    white = {0, 102, 255, 255}
    angle = math.floor(k(camera[2] - AbsRotation - 120 + yaw2))
end

function module.on_render()
    if not entity.is_alive(local_player) then
        return
    end
    renderer.circle_outline(center, center2, white[1], white[2], white[3], white[4], 180, 20 + angle, 0.05, 10)
end

return module
