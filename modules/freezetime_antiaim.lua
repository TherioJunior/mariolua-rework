---@diagnostic disable: need-check-nil
-- Freezetime Antiaim Module
---@diagnostic disable: lowercase-global

local ffi = require("ffi")
local module = {}
local g = ffi.typeof("void***")
local h =
    client.create_interface("client_panorama.dll", "VClientEntityList003") or
    error("VClientEntityList003 wasnt found", 2)
local i = ffi.cast(g, h) or error("rawientitylist is nil", 2)
local k = ffi.cast("void*(__thiscall*)(void*, int)", i[0][3]) or error("get_client_entity is nil", 2)

local function l(m)
    if m == nil then
        return
    end

    local n = ffi.cast("void***", k(i, m))
    local o = ffi.cast("char*", n) + 0x3914
    local p = ffi.cast("struct c_animstate_freeze_aa_mariolua**", o)[0]

    return p
end

function round(u)
    return u >= 0 and math.floor(u + 0.5) or math.ceil(u - 0.5)
end

local z = {
    enabled = ui.reference("aa", "anti-aimbot angles", "enabled"),
    pitch = ui.reference("aa", "anti-aimbot angles", "pitch"),
    yaw_base = ui.reference("aa", "anti-aimbot angles", "yaw base"),
    yaw = {ui.reference("aa", "anti-aimbot angles", "yaw")},
    jitter = {ui.reference("aa", "anti-aimbot angles", "yaw jitter")},
    body_yaw = {ui.reference("aa", "anti-aimbot angles", "body yaw")},
    freestanding_body_yaw = ui.reference("aa", "anti-aimbot angles", "freestanding body yaw"),
    freestanding = {ui.reference("aa", "anti-aimbot angles", "freestanding")},
    fake = ui.reference("aa", "anti-aimbot angles", "fake yaw limit"),
    edge = ui.reference("aa", "anti-aimbot angles", "edge yaw"),
    onshot_aa = {ui.reference("AA", "Other", "On shot anti-aim")},
    slow_motion = {ui.reference("AA", "other", "slow motion")},
    fakelag = {
        amount = ui.reference("AA", "fake lag", "amount"),
        limit = ui.reference("AA", "fake lag", "limit"),
        variance = ui.reference("AA", "fake lag", "variance")
    }
}

local function E(F)
    while F > 180 do
        F = F - 360
    end

    while F < -180 do
        F = F + 360
    end

    return F
end

local function round(B, O)
    local P = 10 ^ (O or 0)

    return math.floor(B * P + 0.5) / P
end

local function Q(R)
    return round(R / globals.tickinterval())
end

local function W(m)
    local X = entity.get_prop(m, "m_fFlags")
    return bit.band(X, bit.lshift(1, 0)) == 1
end

local Y = 0
local Z = false

local function _(a0)
    local a1 = globals.curtime()
    local a2 = l(entity.get_local_player())
    if not W(entity.get_local_player()) then
        return false
    end

    if a2.m_flSpeed2D > 0.1 then
        Y = a1 + 0.22
    end

    if Q(Y) - 4 < Q(a1) and Z then
        a0.allow_send_packet = true
        Z = false
    end

    if Y < a1 then
        Y = a1 + 1.1
        Z = true
        return true
    end

    return false
end

local function a3(a0)
    if ui.get(z.pitch) == "Off" then
        return
    end

    if ui.get(z.pitch) == "Down" or ui.get(z.pitch) == "Default" then
        a0.pitch = 89
    elseif ui.get(z.pitch) == "Up" then
        a0.pitch = -89
    elseif ui.get(z.pitch) == "Random" then
        a0.pitch = math.random(-89, 89)
    end
end

local a4 = 0
local a5 = 0

local function a6(a0)
    if ui.get(z.yaw[1]) == "Off" then
        return
    end

    if ui.get(z.yaw[1]) == "180" then
        a0.yaw = a0.yaw + 180 + ui.get(z.yaw[2])
    elseif ui.get(z.yaw[1]) == "Spin" then
        if ui.get(z.yaw[2]) > 0 then
            a0.yaw = a0.yaw + a4
            a4 = a4 + ui.get(z.yaw[2])
        elseif ui.get(z.yaw[2]) < 0 then
            a0.yaw = a0.yaw - a5
            a5 = a5 + math.abs(ui.get(z.yaw[2]))
        end
    end
end

local a7 = false

local function a8(a0)
    if ui.get(z.jitter[1]) == "Off" then
        return
    end

    if ui.get(z.jitter[1]) == "Offset" then
        if a7 then
            a0.yaw = a0.yaw + ui.get(z.jitter[2])
        else
            a0.yaw = a0.yaw - ui.get(z.jitter[2])
        end

        a7 = not a7
    elseif ui.get(z.jitter[1]) == "Center" then
        if ui.get(z.jitter[2]) > 0 then
            a0.yaw = a0.yaw + a4
            a4 = a4 + ui.get(z.jitter[2])
        elseif ui.get(z.jitter[2]) < 0 then
            a0.yaw = a0.yaw - a5
            a5 = a5 + math.abs(ui.get(z.jitter[2]))
        end
    elseif ui.get(z.jitter[1]) == "Random" then
        a0.yaw = math.random(-ui.get(z.jitter[2]), ui.get(z.jitter[2]))
    end
end

local a9 = false

local function aa(a0)
    local ab = a0.chokedcommands ~= 0

    if ui.get(z.body_yaw[1]) == "Off" or ab then
        return
    end

    local ac = false

    if ui.get(z.body_yaw[1]) == "Static" then
        a0.allow_send_packet = ac
        a0.yaw = a0.yaw - ui.get(z.body_yaw[2])
    elseif ui.get(z.body_yaw[1]) == "Opposite" then
        a0.allow_send_packet = ac
        a0.yaw = ui.get(z.body_yaw[2])
    elseif ui.get(z.body_yaw[1]) == "Jitter" then
        a0.allow_send_packet = ac
        if a9 then
            a0.yaw = a0.yaw - ui.get(z.body_yaw[2])
        else
            a0.yaw = a0.yaw + ui.get(z.body_yaw[2])
        end

        a9 = not a9
    end
end

function module.on_setup_command(event)
    local EyeAngles = {entity.get_prop(entity.get_local_player(), "m_angEyeAngles")}
    local AbsRotation = {entity.get_prop(entity.get_local_player(), "m_angAbsRotation")}
    local af = E(AbsRotation[2] - EyeAngles[2]) > 0 and -1 or 1
    local FreezePeriod = entity.get_prop(entity.get_game_rules(), "m_bFreezePeriod")
    entity.set_prop(entity.get_game_rules(), "m_bFreezePeriod", 1)
    local ActiveWeapon = entity.get_prop(entity.get_local_player(), "m_hActiveWeapon")
    local NextPrimaryAttack = entity.get_prop(ActiveWeapon, "m_flNextPrimaryAttack")

    if FreezePeriod == 1 then
        if event.in_use == 1 or event.in_attack == 1 and globals.curtime() >= NextPrimaryAttack or not ui.get(z.enabled) then
            return
        end

        if _(event) then
            event.allow_send_packet = false
            event.yaw = E(EyeAngles[2] + 60 * af)
        else
            event.allow_send_packet = true
        end

        a3(event)
        a6(event)
        a8(event)
        aa(event)
    end
end

return module
