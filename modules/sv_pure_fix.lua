-- sv_pure fix Module
---@diagnostic disable: lowercase-global
local ffi = require("ffi")
local module = {}
local internal_disable_sv_pure = ui.reference("MISC", "Miscellaneous", "Disable sv_pure")
local interface = ffi.cast("int*", client.create_interface("filesystem_stdio.dll", "VFileSystem017") or error("VFileSystem017 not found"))
local state = false

function module.on_paint_ui()
    if entity.get_local_player() == nil and not state then
        state = true
        ui.set(internal_disable_sv_pure, false)
        client.delay_call(
            1,
            function()
                ui.set(internal_disable_sv_pure, true)
                set_files_is_checked_porperly()
            end
        )
    elseif state and entity.get_local_player() ~= nil then
        state = false
    end
end

function set_files_is_checked_porperly()
    interface[56] = 1
    client.delay_call(0.04, set_files_is_checked_porperly)
end

return module
