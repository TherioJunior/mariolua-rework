-- Anti Bruteforce Module
local yaw_mode, yaw_amount = ui.reference("AA", "Anti-aimbot angles", "Yaw")
local bodyyaw_mode, bodyyaw_amount = ui.reference("AA", "Anti-aimbot angles", "Body Yaw")
local invert = false
local inf = math.huge
local module = {
    enable_anti_bruteforce = false,
    anti_bruteforce_timeout = 0,
    default_yaw_offset = 0,
    max_yaw_delta = 0,
    yaw_step = 0,
    default_body_yaw_offset = 0,
    max_body_yaw_delta = 0,
    body_yaw_step = 0,
    vector = nil,
    log = nil
}
local function h(i, j)
    local k = 10 ^ (j or 0)
    return math.floor(i * k + 0.5) / k
end
local function normalize_yaw(yaw)
    while yaw > 180 do
        yaw = yaw - 360
    end
    while yaw < -180 do
        yaw = yaw + 360
    end
    return yaw
end
local function q(r, s)
    local t, u = r.x - s.x, r.y - s.y
    return math.sqrt(t * t + u * u)
end
function module.on_bullet_impact(v)
    local w = entity.get_local_player()
    local x = client.userid_to_entindex(v.userid)
    if x ~= w and entity.is_enemy(x) and entity.is_alive(w) then
        local y = module.vector(entity.hitbox_position(x, 0))
        local z = module.vector(v.x, v.y, v.z)
        local A = module.vector(client.eye_position())
        local B = A:closest_ray_point(y, z)
        local C = q(B, A)
        if C <= 250 then
            local D = normalize_yaw(A:angle_to(B)[2])
            local E =
                h(module.max_yaw_delta / module.yaw_step, 0.1) * client.random_int(1, module.yaw_step) *
                (client.random_int(0, 1) * 2 - 1)
            local F = normalize_yaw(module.default_yaw_offset + E)
            local G =
                h(module.max_body_yaw_delta / module.body_yaw_step, 0.1) * client.random_int(1, module.body_yaw_step) *
                (client.random_int(0, 1) * 2 - 1)
            local H = normalize_yaw(module.default_body_yaw_offset + G)
            ui.set(yaw_mode, "180")
            ui.set(yaw_amount, F)
            ui.set(bodyyaw_mode, "Static")
            ui.set(bodyyaw_amount, H)
            inf = globals.curtime()
            invert = true
        end
    end
end
function module.on_run_command()
    if globals.curtime() - inf > module.anti_bruteforce_timeout and invert then
        ui.set(yaw_mode, "180")
        ui.set(yaw_amount, module.default_yaw_offset)
        ui.set(bodyyaw_mode, "Static")
        ui.set(bodyyaw_amount, module.default_body_yaw_offset)
        invert = false
    end
end
return module
