-- Antiaim Module
---@diagnostic disable: lowercase-global

local ffi = require("ffi")
local antiaim = {}
local aa_settings = {}
local cache = {}
local mariolua = {}
local cmath = {}
local centity = {}
local cangle = {}
local crender = {}
local k = {}
local refs = {}
local bindings = {}
local vector = {}
local function Angle()
end
local function Vector3()
end
local local_player = entity.get_local_player()
function antiaim.set_vars(r)
    mariolua = r.mariolua or mariolua
    cache = r.cache or cache
    bindings = r.bindings or bindings
    refs = r.refs or refs
    cmath = r.cmath or cmath
    ctable = r.ctable or ctable
    centity = r.centity or centity
    k = r.cstring or k
    crender = r.crender or crender
    cangle = r.cangle or cangle
    Angle = r.Angle or Angle
    Vector3 = r.Vector3 or Vector3
    vector = r.vector or vector
end
local function s()
    local t = entity.get_players(true)
    if #t ~= 0 then
        local u, v, w = client.eye_position()
        local x, y = client.camera_angles()
        cache.closest_enemy = nil
        cache.closest_distance = 999999999
        for z = 1, #t do
            local A = t[z]
            local B, C, D = entity.hitbox_position(A, 0)
            local E = B - u
            local F = C - v
            local G = D - w
            local H = math.atan2(F, E) * 180 / math.pi
            local I = -(math.atan2(G, math.sqrt(math.pow(E, 2) + math.pow(F, 2))) * 180 / math.pi)
            local J = math.abs(y % 360 - H % 360) % 360
            local K = math.abs(x - I) % 360
            if J > 180 then
                J = 360 - J
            end
            local L = math.sqrt(math.pow(J, 2) + math.pow(K, 2))
            if cache.closest_distance > L then
                cache.closest_distance = L
                cache.closest_enemy = A
            end
        end
        if cache.closest_enemy ~= nil then
            return cache.closest_enemy, cache.closest_distance
        end
    end
    return nil, nil
end
aa_settings = {}
aa_settings.settings = {}
function aa_settings.Enable(M)
    if not M then
        return ui.get(cache.ref.aa.enabled)
    end
    if aa_settings.settings.enable ~= M then
        aa_settings.settings.enable = M
        ui.set(cache.ref.aa.enabled, M)
    end
end
function aa_settings.BodyYaw(M)
    if not M then
        return ui.get(cache.ref.aa.body_yaw[1])
    end
    if aa_settings.settings.body_yaw ~= M then
        aa_settings.settings.body_yaw = M
        ui.set(cache.ref.aa.body_yaw[1], M)
    end
end
function aa_settings.BodyYawValue(M)
    if not M then
        return ui.get(cache.ref.aa.body_yaw[2])
    end
    if aa_settings.settings.body_yaw_value ~= M then
        aa_settings.settings.body_yaw_value = M
        ui.set(cache.ref.aa.body_yaw[2], M)
    end
end
function aa_settings.YawBase(M)
    if not M then
        return ui.get(cache.ref.aa.yaw_base)
    end
    if aa_settings.settings.yaw_base ~= M then
        aa_settings.settings.yaw_base = M
        ui.set(cache.ref.aa.yaw_base, M)
    end
end
function aa_settings.YawJitter(M)
    if not M then
        return ui.get(cache.ref.aa.jitter[1])
    end
    if aa_settings.settings.jitter ~= M then
        aa_settings.settings.jitter = M
        ui.set(cache.ref.aa.jitter[1], M)
    end
end
function aa_settings.YawJitterValue(M)
    if not M then
        return ui.get(cache.ref.aa.jitter[2])
    end
    if aa_settings.settings.jitter_value ~= M then
        aa_settings.settings.jitter_value = M
        ui.set(cache.ref.aa.jitter[2], M)
    end
end
function aa_settings.FreestandingBodyYaw(M)
    if not M then
        return ui.get(cache.ref.aa.freestanding_body_yaw)
    end
    if aa_settings.settings.freestanding_body_yaw ~= M then
        aa_settings.settings.freestanding_body_yaw = M
        ui.set(cache.ref.aa.freestanding_body_yaw, M)
    end
end
function aa_settings.Yaw(M)
    local N, O = ui.reference("AA", "Anti-aimbot angles", "Yaw")
    if not M then
        return ui.get(N)
    end
    if aa_settings.settings.yaw ~= M then
        aa_settings.settings.yaw = M
        ui.set(N, M)
    end
end
function aa_settings.YawValue(M)
    local O, P = ui.reference("AA", "Anti-aimbot angles", "Yaw")
    if not M then
        return ui.get(P)
    end
    if aa_settings.settings.yaw_value ~= M then
        aa_settings.settings.yaw_value = M
        ui.set(P, M)
    end
end
function aa_settings.Edge(M)
    if not M then
        return ui.get(cache.ref.aa.edge)
    end
    if aa_settings.settings.edge ~= M then
        aa_settings.settings.edge = M
        ui.set(cache.ref.aa.edge, M)
    end
end
function aa_settings.Freestanding(M)
    if not M then
        return ui.get(cache.ref.aa.freestanding[1])
    end
    if aa_settings.settings.freestanding ~= M then
        aa_settings.settings.freestanding = M
        ui.set(cache.ref.aa.freestanding[1], M)
    end
end
function aa_settings.FreestandingBind(M)
    if not M then
        return ui.get(cache.ref.aa.freestanding[2])
    end
    if aa_settings.settings.freestandingbinsd ~= M then
        aa_settings.settings.freestandingbinsd = M
        ui.set(cache.ref.aa.freestanding[2], M)
    end
end
function aa_settings.Pitch(M)
    if not M then
        return ui.get(cache.ref.aa.pitch)
    end
    if aa_settings.settings.pitch ~= M then
        aa_settings.settings.pitch = M
        ui.set(cache.ref.aa.pitch, M)
    end
end
function aa_settings.Fake(M)
    if not M then
        return ui.get(cache.ref.aa.fake)
    end
    if aa_settings.settings.fake ~= M then
        aa_settings.settings.fake = M
        ui.set(cache.ref.aa.fake, M)
    end
end
antiaim.freestanding = {}
antiaim.freestanding.data = {side = 1, last_side = 0, last_hit = 0, hit_side = 0}
antiaim.fake_yaw = 0
antiaim.body_yaw_value = 60
antiaim.pich = "Off"
antiaim.yaw = "Off"
antiaim.yaw_value = 0
antiaim.lowerbody_yaw_target = "Eye Yaw"
antiaim.fakelag_limit = 14
antiaim.fakelag_limit_backup = false
function antiaim.Freestanding()
    if not refs.antiaim_freestanding:GetItemName() == "Skeet Freestanding" then
        return
    end
    local Q, R = "left", "right"
    local q = entity.get_local_player()
    if not q or entity.get_prop(q, "m_lifeState") ~= 0 then
        return
    end
    local S = globals.curtime()
    if antiaim.freestanding.data.hit_side ~= 0 and S - antiaim.freestanding.data.last_hit > 5 then
        antiaim.freestanding.data.last_side = 0
        antiaim.freestanding.data.last_hit = 0
        antiaim.freestanding.data.hit_side = 0
    end
    local T = "Hide real"
    local eyeposX, eyeposY, eyeposZ = client.eye_position()
    local _, camera_angles = client.camera_angles()
    local U = refs.antiaim_state:GetItemName()
    local V, W = nil, nil;
    local vecOrigin = Vector3(entity.get_prop(q, "m_vecOrigin"))
    if U == "Rage" then
        V, W = s()
    end
    local Y = {left = 0, right = 0}
    local Z
    for z = camera_angles - 90, camera_angles + 90, 10 do
        if z ~= camera_angles then
            local _ = math.rad(z)
            local a0, a1, a2 = eyeposX + 256 * math.cos(_), eyeposY + 256 * math.sin(_), eyeposZ
            local a3 = client.trace_line(q, eyeposX, eyeposY, eyeposZ, a0, a1, a2)
            if a3 == nil then
                return
            end
            local a4 = z < camera_angles and Q or R
            Y[a4] = Y[a4] + a3
        end
    end
    antiaim.freestanding.data.side = Y.left < Y.right and 1 or 2
    if antiaim.freestanding.data.side == antiaim.freestanding.data.last_side then
        return
    end
    antiaim.freestanding.data.last_side = antiaim.freestanding.data.side
    if antiaim.freestanding.data.hit_side ~= 0 then
        antiaim.freestanding.data.side = antiaim.freestanding.data.hit_side == 1 and 2 or 1
    end
    local a5 = refs.antiaim_body:GetValue()
    if V ~= nil and U == "Rage" then
        vecOrigin = Vector3(entity.get_origin(q))
        local a6 = Vector3(entity.get_origin(V))
        local a7 = cangle.CalcRelativeAngle(vecOrigin.x, vecOrigin.y, a6.x, a6.y)
        local a8 =
            T == "Hide real" and (antiaim.freestanding.data.side == 1 and 60 or -60) or
            (antiaim.freestanding.data.side == 1 and -60 or 60)
        local O, a9 = client.camera_angles()
        local aa = cangle.NormalizeAsYaw(a7 + a8 * -1 - a9)
        antiaim.yaw_value = aa
    end
    antiaim.body_yaw_value =
        T == "Hide real" and (antiaim.freestanding.data.side == 1 and a5 or -a5) or
        (antiaim.freestanding.data.side == 1 and -a5 or a5)
end
function antiaim.SetAntiAim(settings_table)
    aa_settings.Enable(settings_table.enabled or aa_settings.Enable())
    aa_settings.BodyYaw(settings_table.body_yaw or aa_settings.BodyYaw())
    aa_settings.BodyYawValue(settings_table.body_yaw_value or aa_settings.BodyYawValue())
    aa_settings.YawBase(settings_table.yaw_base or aa_settings.YawBase())
    aa_settings.YawJitter(settings_table.jitter or aa_settings.YawJitter())
    aa_settings.YawJitterValue(settings_table.jitter_value or aa_settings.YawJitterValue())
    aa_settings.Freestanding(settings_table.fa or aa_settings.Freestanding())
    aa_settings.FreestandingBodyYaw(settings_table.fa_body_yaw or aa_settings.FreestandingBodyYaw())
    aa_settings.FreestandingBind(settings_table.fa_bind)
    aa_settings.Yaw(settings_table.yaw or aa_settings.Yaw())
    aa_settings.YawValue(settings_table.yaw_value or aa_settings.YawValue())
    aa_settings.Edge(settings_table.edge or aa_settings.Edge())
    aa_settings.Pitch(settings_table.pitch or aa_settings.Pitch())
    aa_settings.Fake(settings_table.fake or aa_settings.Fake())
end
function antiaim.SetLegitAntiAim()
    aa_settings.Pitch("Off")
    aa_settings.Yaw("Off")
    aa_settings.YawValue("0")
    aa_settings.YawBase("Local View")
    aa_settings.YawJitter("Off")
    aa_settings.Edge(false)
    aa_settings.Freestanding("-")
    aa_settings.FreestandingBind("On hotkey")
end
function antiaim.SetRageAntiAim()
    aa_settings.Pitch("Down")
    aa_settings.Yaw("180")
    aa_settings.YawValue("0")
end
function antiaim.SetMarioLuaFreestanding()
    local aa_body_mode = refs.antiaim_body_mode:GetItemName()
    aa_settings.Enable(antiaim.exception_met or false)
    local camera_angles = {client.camera_angles()}
    local yaw = cache.indicator.real_yaw < 1 and cache.indicator.real_yaw > 0.0001 and math.floor(cache.indicator.real_yaw, 1) or cache.indicator.real_yaw
    if camera_angles[2] ~= nil and ad ~= nil and 60 < math.floor(math.abs(cangle.normalize_yaw(camera_angles[2] - yaw))) then
        yaw = yaw * -1
    end
    aa_settings.BodyYawValue(antiaim.body_yaw_value)
    aa_settings.BodyYaw(aa_body_mode)
    aa_settings.FreestandingBodyYaw(false)
    aa_settings.Fake(antiaim.fake_yaw)
    antiaim.state = "on"
end
function antiaim.SetSkeetFreestanding(ab)
    local aa_body_mode = refs.antiaim_body_mode:GetItemName()
    aa_settings.Enable(antiaim.exception_met)
    aa_settings.BodyYaw(aa_body_mode)
    if antiaim.pich == "Down" and antiaim.yaw == "180" then
        aa_settings.BodyYawValue(antiaim.body_yaw_value * -1)
    else
        aa_settings.BodyYawValue(antiaim.body_yaw_value)
    end
    aa_settings.FreestandingBodyYaw(true)
    aa_settings.Fake(antiaim.fake_yaw)
    antiaim.state = "on"
end
function antiaim.SetManualAntiAim()
    local aa_body_mode = refs.antiaim_body_mode:GetItemName()
    aa_settings.Enable(antiaim.exception_met)
    aa_settings.BodyYaw(aa_body_mode)
    aa_settings.BodyYawValue(antiaim.body_yaw_value)
    aa_settings.FreestandingBodyYaw(false)
    aa_settings.Fake(antiaim.fake_yaw)
    antiaim.state = "on"
end
function antiaim.GetFakeJitter()
    local aa_fake = refs.antiaim_fake:GetValue()
    if not refs.antiaim_fake_jitter_enable:GetValue() then
        antiaim.fake_yaw = aa_fake
        return
    end
    local aa_fake_jitter = refs.antiaim_fake_jitter:GetValue()
    local aa_fake_jitter_speed = cmath.round(refs.antiaim_fake_jitter_speed:GetValue())
    if aa_fake == aa_fake_jitter then
        antiaim.fake_yaw = aa_fake
        return
    end
    client.delay_call(
        aa_fake_jitter_speed / 1000,
        function()
            if antiaim.fake_yaw == aa_fake_jitter then
                antiaim.fake_yaw = aa_fake
            elseif antiaim.fake_yaw == aa_fake then
                antiaim.fake_yaw = aa_fake_jitter
            else
                antiaim.fake_yaw = aa_fake
            end
        end
    )
end
function antiaim.exceptions()
    if
        not ui.get(cache.ref.rage.fakeduck) and
            (refs.antiaim_exception_enable:GetValue() and refs.antiaim_enable:GetValue() and local_player ~= nil and
                entity.is_alive(local_player))
     then
        local aj = ffi.cast("void***", bindings.get_net_channel_info(bindings.ivengineclient))
        local ak = ffi.cast("get_avg_loss_t", aj[0][11])
        local al = ffi.cast("get_avg_choke_t", aj[0][12])
        local am = ak(aj, 1)
        local an = al(aj, 1)
        antiaim.exception_toggle = true
        local ao = function()
            return math.floor(client.latency() * 1000 + 0.5)
        end
        if am >= refs.antiaim_exception_loss:GetValue() and refs.antiaim_exception_loss:GetValue() ~= 100 then
            antiaim.exception_met = false
        elseif an >= refs.antiaim_exception_choke:GetValue() and refs.antiaim_exception_choke:GetValue() ~= 100 then
            antiaim.exception_met = false
        elseif
            cache.AccumulateFps() <= refs.antiaim_exception_fps:GetValue() and
                refs.antiaim_exception_fps:GetValue() ~= 100
         then
            antiaim.exception_met = false
        elseif ao() >= refs.antiaim_exception_ping:GetValue() and refs.antiaim_exception_ping:GetValue() ~= 1000 then
            antiaim.exception_met = false
        elseif antiaim.exception_met ~= true then
            antiaim.exception_met = true
        else
            antiaim.exception_met = false
        end
        aa_settings.Enable(antiaim.exception_met)
    elseif not refs.antiaim_exception_enable:GetValue() and antiaim.exception_toggle then
        antiaim.exception_met = true
        antiaim.exception_toggle = false
    end
end
local manual_invert = false
function antiaim.handle()
    if refs.antiaim_enable:GetValue() then
        local aq = refs.antiaim_fake:GetValue()
        local ar = refs.antiaim_body:GetValue()
        local ab = refs.antiaim_body_mode:GetItemName()
        local U = refs.antiaim_state:GetItemName()
        local as = refs.antiaim_freestanding:GetItemName()
        antiaim.body_yaw_mode = refs.antiaim_body_mode:GetItemName()
        if U == "Legit" then
            antiaim.SetAntiAim(
                {
                    enabled = true,
                    yaw_base = "Local View",
                    body_yaw = ab,
                    body_yaw_value = ar,
                    fa = false,
                    fa_bind = "On hotkey",
                    yaw = "Off",
                    yaw_value = "0",
                    edge = false,
                    pitch = "Off",
                    fake = aq,
                    lby = "Opposite"
                }
            )
        elseif U == "Rage" then
            antiaim.SetAntiAim(
                {
                    enabled = true,
                    yaw_base = "Local View",
                    body_yaw = ab,
                    body_yaw_value = ar,
                    yaw = "180",
                    yaw_value = "0",
                    pitch = "Down",
                    fake = aq
                }
            )
        end
        if refs.antiaim_lby_mode:GetItemName() ~= "Match Real" and antiaim.fakelag_limit_backup then
            ui.set(cache.ref.fakelag_limit, antiaim.fakelag_limit)
            antiaim.fakelag_limit_backup = false
        end
        if refs.antiaim_lby_mode:GetItemName() == "Local View" then
            antiaim.lowerbody_yaw_target = "Eye Yaw"
        elseif refs.antiaim_lby_mode:GetItemName() == "Opposite" then
            antiaim.lowerbody_yaw_target = "Opposite"
        elseif refs.antiaim_lby_mode:GetItemName() == "Match Real" then
            antiaim.lowerbody_yaw_target = "Off"
            if not antiaim.fakelag_limit_backup then
                antiaim.fakelag_limit = ui.get(cache.ref.fakelag_limit)
                antiaim.fakelag_limit_backup = true
            end
            ui.set(cache.ref.fakelag_limit, 2)
        elseif refs.antiaim_lby_mode:GetItemName() == "Sway" then
            antiaim.lowerbody_yaw_target = "Sway"
        end
        if refs.antiaim_switch_key:IsKeyToggle() and not manual_invert then
            manual_invert = true
            antiaim.body_yaw_value = antiaim.body_yaw_value * -1
        elseif not refs.antiaim_switch_key:IsKeyToggle() and manual_invert then
            manual_invert = false
            antiaim.body_yaw_value = antiaim.body_yaw_value * -1
        end
        if refs.antiaim_off_key:IsKeyToggle() then
            refs.antiaim_off_key.bSet = true
            antiaim.SetAntiAim(
                {
                    enabled = false,
                    body_yaw = "Off",
                    body_yaw_value = "0",
                    jitter = "Off",
                    jitter_value = "0",
                    fa = false,
                    yaw = "Off",
                    yaw_value = "0",
                    edge = false,
                    pitch = "Off",
                    fake = "0",
                    lby = "Off"
                }
            )
            antiaim.state = "off"
        else
            refs.antiaim_off_key.bSet = false
        end
        antiaim.GetFakeJitter()
        antiaim.exceptions()
        if not refs.antiaim_off_key.bSet then
            if as == "MarioLua Freestanding" then
                antiaim.Freestanding()
                antiaim.SetMarioLuaFreestanding()
            elseif as == "Skeet Freestanding" then
                antiaim.SetSkeetFreestanding()
            elseif as == "Manual" then
                antiaim.SetManualAntiAim()
            end
        end
    elseif antiaim.state == "on" then
        aa_settings.Enable(false)
        antiaim.state = "off"
    end
end
return {
    antiaim = antiaim,
    cache = cache,
    bindings = bindings,
    refs = refs,
    cmath = cmath,
    centity = centity,
    crender = crender,
    cangle = cangle,
    Angle = Angle,
    Vector3 = Vector3,
    vector = vector,
    mariolua = mariolua
}
