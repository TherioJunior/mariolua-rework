-- Resolver Module
---@diagnostic disable: lowercase-global, need-check-nil

local resolver = {}
local cache = {resolver = {}}
local mariolua = {}
local cmath = {}
local centity = {}
local cangle = {}
local crender = {}
local h = {}
local i = {}
local refs = {}
local bindings = {}
local vector = {}
local m = {}
local function Angle()
end
local function Vector3()
end
local p = entity.get_local_player()

function resolver.get_vars()
    return {
        resolver = resolver,
        cache = cache,
        bindings = bindings,
        refs = refs,
        cmath = cmath,
        centity = centity,
        crender = crender,
        cangle = cangle,
        Angle = Angle,
        Vector3 = Vector3,
        vector = vector,
        mariolua = mariolua,
        cstring = h,
        plylist = m,
        cweapon = i
    }
end

function resolver.set_vars(q)
    mariolua = q.mariolua or mariolua
    cache = q.cache or cache
    bindings = q.bindings or bindings
    refs = q.refs or refs
    cmath = q.cmath or cmath
    centity = q.centity or centity
    h = q.cstring or h
    crender = q.crender or crender
    cangle = q.cangle or cangle
    Angle = q.Angle or Angle
    Vector3 = q.Vector3 or Vector3
    vector = q.vector or vector
    m = q.plylist or m
    i = q.cweapon or i
end

resolver.indicator = {
    x = {},
    y = {},
    color = {},
    arrow = {}
}

local r = {
    ["arrow_1_l"] = "4peE",
    ["arrow_1_r"] = "4pa6",
    ["arrow_2_l"] = "4q+H",
    ["arrow_2_r"] = "4q+I",
    ["arrow_3_l"] = "4q6Y",
    ["arrow_3_r"] = "4q6a",
    ["arrow_4"] = "4oCS"
}

function resolver.IsActive()
    return refs.resolver_enable:GetValue() and not resolver.force_off
end

local s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
local function base64_decode(u)
    local u = string.gsub(u, "[^" .. s .. "=]", "")
    return u:gsub(
        ".",
        function(v)
            if v == "=" then
                return ""
            end
            local w, x = "", s:find(v) - 1
            for y = 6, 1, -1 do
                w = w .. (x % 2 ^ y - x % 2 ^ (y - 1) > 0 and "1" or "0")
            end
            return w
        end
    ):gsub(
        "%d%d%d?%d?%d?%d?%d?%d?",
        function(v)
            if #v ~= 8 then
                return ""
            end
            local z = 0
            for y = 1, 8 do
                z = z + (v:sub(y, y) == "1" and 2 ^ (8 - y) or 0)
            end
            return string.char(z)
        end
    )
end

local A = {
    ["left_1"] = base64_decode(r["arrow_1_l"]),
    ["right_1"] = base64_decode(r["arrow_1_r"]),
    ["left_2"] = base64_decode(r["arrow_2_l"]),
    ["right_2"] = base64_decode(r["arrow_2_r"]),
    ["left_3"] = base64_decode(r["arrow_3_l"]),
    ["right_3"] = base64_decode(r["arrow_3_r"]),
    ["4"] = base64_decode(r["arrow_4"])
}

function resolver.handle_resolver_indicator(ent)
    if
        not refs.resolver_draw_side_indicator:GetValue() or not resolver.IsActive() or not entity.is_alive(p) or
            not m.GetForceBodyYawCheckbox(ent)
     then
        return
    end

    local B =
        vector.get_FOV(
        Vector3(0, entity.get_prop(ent, "m_angEyeAngles[1]"), 0),
        Vector3(entity.get_prop(ent, "m_vecOrigin")),
        Vector3(entity.get_prop(p, "m_vecAbsOrigin"))
    )
    local C = B >= 0 and B <= 90 and true or false
    local D = m.GetPlayerBodyYaw(ent)
    local E = 1 + math.sin(math.abs(-math.pi + globals.realtime() * 1 / 0.5 % (math.pi * 2))) * 170

    if C and D > 0 or not C and D < 0 then
        resolver.indicator.color[ent] = {r = 124, g = 195, b = 13, a = E}
        resolver.indicator.arrow[ent] = A["left_2"]
    elseif C and D < 0 or not C and D > 0 then
        resolver.indicator.color[ent] = {r = 250, g = 120, b = 170, a = E}
        resolver.indicator.arrow[ent] = A["right_2"]
    end
end

function resolver.render_resolver_indicator()
    if not refs.resolver_draw_side_indicator:GetValue() or not resolver.IsActive() or not entity.is_alive(p) then
        return
    end

    local F = entity.get_players(true)

    for y = 1, #F do
        local ent = F[y]
        if not m.GetForceBodyYawCheckbox(ent) then
            return
        end
        local G, H, I = entity.hitbox_position(ent, 1)
        resolver.indicator.x[ent], resolver.indicator.y[ent] = renderer.world_to_screen(G, H, I)
        if resolver.indicator.color[ent] and resolver.indicator.x[ent] and resolver.indicator.y[ent] then
            local J = resolver.indicator.color[ent]
            renderer.text(
                resolver.indicator.x[ent],
                resolver.indicator.y[ent],
                J.r,
                J.g,
                J.b,
                J.a,
                "c+",
                0,
                resolver.indicator.arrow[ent]
            )
        end
    end
end

resolver.hit = {
    ["Gamesense"] = {
        ["count"] = 0,
        ["headshot"] = 0
    },
    ["MarioLua"] = {
        ["count"] = 0,
        ["headshot"] = 0
    }
}
resolver.missed = {
    ["Gamesense"] = 0,
    ["MarioLua"] = 0
}
resolver.data = {}
resolver.info = {}
resolver.force_off = false
resolver.data_off = false

function resolver.full_reset()
    resolver.hit = {
        ["Gamesense"] = {
            ["count"] = 0,
            ["headshot"] = 0
        },
        ["MarioLua"] = {
            ["count"] = 0,
            ["headshot"] = 0
        }
    }
    resolver.missed = {
        ["Gamesense"] = 0,
        ["MarioLua"] = 0
    }
    resolver.data = {}
    resolver.info = {}
    resolver.force_off = false
    resolver.data_off = false
    mariolua.log("[Resolver] Full reseted!")
end

function resolver.set_current_resolver(ent)
    resolver.data[ent] = resolver.data[ent] or resolver.set_round_data()
    resolver.info[ent] = resolver.info[ent] or resolver.set_match_data()
    if ui.get(cache.ref.rage.resolver_ref) then
        if resolver.IsActive() then
            if not m.IsCorrectionActive(ent) then
                resolver.info[ent].current =
                    refs.resolver_modules:IsEnabled("Desync Detection") and "MarioLua (no correction active)" or
                    "Gamesense (no correction active)"
            else
                resolver.info[ent].current = "MarioLua"
            end
        else
            resolver.info[ent].current =
                not m.IsCorrectionActive(ent) and "Gamesense (no correction active)" or "Gamesense"
        end
    else
        resolver.info[ent].current = "None"
    end
end

function resolver.set_round_data()
    return {
        logic = {},
        ["slowwalk_limit"] = false,
        ["bruteforce"] = {["active"] = false, ["hit"] = 0, ["miss"] = 0, ["set_yaw"] = nil},
        antiaim = {["is_backwards"] = false, ["extended"] = false, ["eye_yaw"] = false},
        switch = {},
        low_delta_switch = {},
        ["animlayer"] = {
            ["switched"] = false,
            ["switch_state"] = false,
            ["switch_count"] = 0,
            ["is_desync"] = false,
            ["eye_yaw"] = false,
            ["fake_yaw"] = 60
        },
        props = {
            ["is_fakeduck"] = false,
            ["is_slowwalking"] = false,
            ["m_flChokedPackets"] = 0,
            ["m_flLowerBodyYawTarget"] = 0,
            ["m_flLowerBodyYawMoving"] = 0,
            ["m_flLowerBodyYawStanding"] = 0,
            ["m_flLowerBodyFakeDuck"] = 0,
            ["m_flLowerBodyYawSequenceAct"] = 0,
            ["m_angEyeAngles"] = Angle(0, 0, 0),
            ["velocity"] = {},
            ["m_flVelocity2D"] = 0,
            ["lowdelta"] = false,
            ["highdelta"] = false,
            ["correction"] = nil,
            ["should_resolve"] = false,
            ["pref_packet"] = 0,
            ["bad_packets"] = 0,
            ["clean_packets"] = 0,
            ["m_flEyeYaw"] = 0,
            ["m_flGoalFeetYaw"] = 0,
            ["m_flGoalFeetDelta"] = 0,
            ["m_flCurrentFeetYaw"] = 0
        },
        record = {abs_yaw_set = false, latest_bullet_impact = 0, old_simulation_time = 0, latest_shot_yaw = 0},
        is_onshot = false,
        cur_hit_pattern = -1,
        fakeduck = false,
        force_zero = false,
        rec_sw_time = 0,
        sw_vel = {},
        lby_next_think = 0,
        lby_side = "",
        lby_yaw = 0,
        yaw_dif = 0,
        resolved_yaw = 0,
        last_side = 0,
        side = 0,
        last_hit = 0,
        hit_side = 0,
        misses = 0,
        hits = 0,
        slowwalk_misses = 0,
        missed_body_yaw = nil,
        dmg_left = 0,
        dmg_right = 0,
        at_pitch = 0,
        at_yaw = 0
    }
end

function resolver.set_match_data()
    return {
        logic = {side = ""},
        current = "",
        module = "",
        ["modules"] = {
            ["trace"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["trace2"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["fraction"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["fraction2"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["predict"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["edge"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["edge2"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["damage"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["damage2"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["position"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["simple"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["onshot"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["condition"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["lby"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100},
            ["animlayer"] = {["side"] = "", ["angle"] = 0, ["success"] = 1, ["failed"] = 1, ["accuracy"] = 100}
        },
        ["low_delta"] = {["count"] = 0, ["active"] = false, ["yaw"] = nil},
        ["bruteforce"] = {
            ["count"] = 0,
            ["hit_yaw"] = nil,
            ["missed_yaw"] = nil,
            ["fucked_up"] = false,
            ["reset_brute_timer"] = false
        },
        ["hit_yaws_t"] = {},
        ["missed_yaws_t"] = {},
        ["average_hit_yaw"] = 0,
        ["average_missed_yaw"] = 0,
        ["hit_info"] = {},
        ["miss_info"] = {},
        miss_streak = 0,
        fake_limit = 60,
        misses = 0,
        hits = 0,
        hit_yaw = 60,
        force_off = false,
        inversed = false,
        slowwalk_inversed = false,
        real = "Hide",
        missed_body_yaw = nil,
        hide_miss = 0,
        show_miss = 0,
        hide_hit = 0,
        show_hit = 0,
        state = ""
    }
end

function resolver.update_resolver_stats()
    resolver.print_resolver_stats = {
        [1] = "------------[Resolver Stats]------------",
        [2] = "[Gamesense] Missed: " ..
            resolver.missed["Gamesense"] ..
                " | Hit: " ..
                    resolver.hit["Gamesense"]["count"] .. " | Headshots: " .. resolver.hit["Gamesense"]["headshot"],
        [3] = "[MarioLua] Missed: " ..
            resolver.missed["MarioLua"] ..
                " | Hit: " ..
                    resolver.hit["MarioLua"]["count"] .. " | Headshots: " .. resolver.hit["MarioLua"]["headshot"],
        [4] = "-------------------------------------------"
    }
end

function resolver.OffResolverAll()
    local F = entity.get_players(true)
    for y = 1, #F do
        local ent = F[y]
        if resolver.info[ent] ~= nil then
            resolver.info[ent].current = "Gamesense"
            resolver.info[ent].module = ""
        end
        client.update_player_list()
        m.SetPlayerBodyYaw(ent, 0)
        m.SetForceBodyYawCheckbox(ent, false)
        mariolua.log("[Resolver] Set " .. entity.get_player_name(ent) .. "'s' off.")
    end
end

function resolver.OffResolver(ent)
    if resolver.info[ent] ~= nil then
        resolver.info[ent].current = "Gamesense"
        resolver.info[ent].module = ""
    end
    m.SetPlayerBodyYaw(ent, 0)
    m.SetForceBodyYawCheckbox(ent, false)
    mariolua.log("[Resolver] Set " .. entity.get_player_name(ent) .. "'s' off.")
end

function resolver.add_hit_info(ent, q)
    table.insert(resolver.info[ent]["hit_info"], q)
end

function resolver.add_miss_info(ent, q)
    table.insert(resolver.info[ent]["miss_info"], q)
end

function resolver.match_info_pattern(ent)
    if not refs.resolver_modules:IsEnabled("Pattern") then
        resolver.data[ent].cur_hit_pattern = -1
        return
    end
    for y = 1, #resolver.info[ent]["hit_info"] do
        local K = resolver.info[ent]["hit_info"][y]
        if
            K.slowwalk and resolver.data[ent].props["is_slowwalking"] or
                K.mode == resolver.data[ent].antiaim["mode"] and K.yaw_base == resolver.data[ent].antiaim["yaw_base"] or
                K.fakeduck and resolver.data[ent].props["is_fakeduck"]
         then
            resolver.data[ent].cur_hit_pattern = y
            local D = resolver.data[ent].logic.side == "left" and K.body_yaw or -K.body_yaw
            resolver.data[ent].resolved_yaw = D
            m.SetForceBodyYawCheckbox(ent, true)
            m.SetPlayerBodyYaw(ent, D)
            mariolua.log(
                "[Resolver] Found matching pattern for " ..
                    entity.get_player_name(ent) .. ". Trying body yaw " .. D .. "�."
            )
        end
    end
end

function resolver.should_log(ent)
    local L = resolver.data[ent]["condition_active"]
    return not L
end

cache.brute_pos_offsets = {Vector3(0, 0, 15), Vector3(0, 15, 0), Vector3(15, 0, 0)}
function resolver.render_brute_pos()
    if resolver.data[ent].brute_pos[1] == nil then
        return
    end
    local M, N =
        renderer.world_to_screen(
        resolver.data[ent].brute_pos[1],
        resolver.data[ent].brute_pos[2],
        resolver.data[ent].brute_pos[3]
    )
    if M ~= nil then
        if not true then
            local O, P = renderer.world_to_screen(resolver.data[ent].brute_pos + cache.brute_pos_offsets[1])
            if O ~= nil then
                renderer.line(M, N, O, P, 255, 0, 0, 180)
            end
            local Q, R = renderer.world_to_screen(resolver.data[ent].brute_pos + cache.brute_pos_offsets[2])
            if Q ~= nil then
                renderer.line(M, N, Q, R, 255, 0, 0, 180)
            end
            local S, T = renderer.world_to_screen(resolver.data[ent].brute_pos - cache.brute_pos_offsets[2])
            if S ~= nil then
                renderer.line(M, N, S, T, 255, 0, 0, 180)
            end
            local U, V = renderer.world_to_screen(resolver.data[ent].brute_pos + cache.brute_pos_offsets[3])
            if U ~= nil then
                renderer.line(M, N, U, V, 255, 0, 0, 180)
            end
            local W, X = renderer.world_to_screen(resolver.data[ent].brute_pos - cache.brute_pos_offsets[3])
            if W ~= nil then
                renderer.line(M, N, W, X, 255, 0, 0, 180)
            end
        end
    end
end

function resolver.is_brute_pos(ent)
    if resolver.data[ent].misses < 1 then
        return
    end
    local Y = Vector3(entity.get_prop(ent, "m_vecOrigin"))
    if Y.x == nil then
        return
    end
    local Z = vector.distance_2d(Y.x, Y.y, resolver.data[ent].brute_pos[1], resolver.data[ent].brute_pos[2])
    if Z >= 5 then
        resolver.data[ent]["bruteforce"]["active"] = false
    else
        resolver.data[ent]["bruteforce"]["active"] = true
    end
end

function resolver.log_bruteforce_pos(ent)
    local Y = Vector3(entity.get_prop(ent, "m_vecOrigin"))
    if Y.x == nil then
        return
    end
    resolver.data[ent].brute_pos = {Y.x, Y.y, Y.z}
end

function resolver.reset_bruteforce_on_dormancy(ent)
    if not resolver.data[ent] or not entity.is_dormant(ent) then
        return
    end
    if not resolver.data[ent]["bruteforce"]["active"] then
        return
    end
    resolver.data[ent].reset_brute_timer = true
    client.delay_call(
        5,
        function()
            if not resolver.data[ent] or not entity.is_dormant(ent) then
                return
            end
            if not resolver.data[ent]["bruteforce"]["active"] then
                return
            end
            resolver.data[ent]["bruteforce"]["fucked_up"] = false
            resolver.data[ent]["bruteforce"]["active"] = false
            resolver.data[ent].reset_brute_timer = false
            mariolua.log("[Resolver] Reset bruteforce on " .. entity.get_player_name(ent) .. " due to long dormancy.")
        end
    )
end

cache.bruteforce_low_delta = {40, 30, 20, 10}
cache.bruteforce_yaw = {60, 45, 30, 20, 10}

function resolver.bruteforce(ent, D)
    if resolver.info[ent]["low_delta"]["active"] or not resolver.data[ent]["bruteforce"]["active"] then
        return
    end
    if resolver.data[ent]["bruteforce"]["miss"] == 1 and resolver.info[ent]["bruteforce"]["count"] > 1 then
        resolver.info[ent]["bruteforce"]["count"] = resolver.info[ent]["bruteforce"]["count"] - 1
    end
    local _ = resolver.info[ent]["bruteforce"]["count"]
    local a0 = refs.resolver_yawlimit:GetValue()
    local a1 = cmath.IsNumberNegative(D)
    resolver.data[ent].switch = {D * -1, a1 and 45 or -45, a1 and 30 or -30, a1 and 20 or -20, a1 and 10 or -10, 0}
    resolver.data[ent]["bruteforce"]["set_yaw"] = resolver.data[ent].switch[_]
    mariolua.log(
        "[Resolver] Bruteforced " .. entity.get_player_name(ent) .. "'s yaw to " .. resolver.data[ent].switch[_] .. "�."
    )
    m.SetPlayerBodyYaw(ent, cmath.clamp(resolver.data[ent].switch[_ % #resolver.data[ent].switch], -a0, a0))
    if resolver.info[ent]["bruteforce"]["count"] >= #resolver.data[ent].switch then
        resolver.info[ent]["bruteforce"]["count"] = 1
        resolver.info[ent]["bruteforce"]["fucked_up"] = true
    end
    if resolver.data[ent].switch[resolver.info[ent]["bruteforce"]["count"]] == 0 then
        resolver.info[ent]["bruteforce"]["count"] = 1
    end
end

function resolver.bruteforce_lowdelta(ent, D)
    if
        not refs.resolver_modules:IsEnabled("Low Delta") and
            (not resolver.info[ent]["bruteforce"]["fucked_up"] and not refs.resolver_force_lowdelta:GetValue())
     then
        return
    end
    if resolver.data[ent]["bruteforce"]["miss"] == 1 and resolver.info[ent]["bruteforce"]["count"] > 1 then
        resolver.info[ent]["bruteforce"]["count"] = resolver.info[ent]["bruteforce"]["count"] - 1
    end
    local a0 = refs.resolver_yawlimit:GetValue()
    local a1 = cmath.IsNumberNegative(D)
    resolver.data[ent].low_delta_switch = {D * -1, a1 and 30 or -30, a1 and 20 or -20, a1 and 10 or -10, 0}
    resolver.data[ent]["bruteforce"]["set_yaw"] = resolver.data[ent].switch[resolver.info[ent]["bruteforce"]["count"]]
    mariolua.log(
        "[Resolver] *Low-Delta* Bruteforced '" ..
            entity.get_player_name(ent) ..
                "'s yaw to " .. resolver.data[ent].low_delta_switch[resolver.info[ent]["bruteforce"]["count"]] .. "."
    )
    m.SetPlayerBodyYaw(
        ent,
        cmath.clamp(
            resolver.data[ent].low_delta_switch[
                resolver.info[ent]["bruteforce"]["count"] % #resolver.data[ent].low_delta_switch
            ],
            -a0,
            a0
        )
    )
    if resolver.info[ent]["bruteforce"]["count"] >= #resolver.data[ent].low_delta_switch then
        resolver.info[ent]["bruteforce"]["count"] = 1
    end
    if resolver.data[ent].low_delta_switch[resolver.info[ent]["bruteforce"]["count"]] == 0 then
        resolver.info[ent]["bruteforce"]["count"] = 1
    end
end

function resolver.bruteforce_handler(ent)
    if not refs.resolver_modules:IsEnabled("Bruteforce") then
        return
    end
    resolver.data[ent]["bruteforce"]["active"] = true
    resolver.info[ent]["bruteforce"]["count"] = resolver.info[ent]["bruteforce"]["count"] + 1
    local D = m.GetPlayerBodyYaw(ent)
    if resolver.info[ent]["bruteforce"]["count"] <= 1 then
        if resolver.info[ent]["bruteforce"]["missed_yaw"] ~= nil then
            D = resolver.info[ent]["bruteforce"]["missed_yaw"]
            if math.abs(D) == 0 then
                D = 60
            end
            nearest_idx, nearest_value =
                cmath.nearest_value(
                (resolver.info[ent]["low_delta"]["active"] or refs.resolver_force_lowdelta:GetValue()) and
                    cache.bruteforce_low_delta or
                    cache.bruteforce_yaw,
                math.abs(D)
            )
            resolver.info[ent]["bruteforce"]["missed_yaw"] = nil
            resolver.info[ent]["bruteforce"]["count"] = nearest_idx + 1
        else
            D = m.GetPlayerBodyYaw(ent)
            nearest_idx, nearest_value =
                cmath.nearest_value(
                (resolver.info[ent]["low_delta"]["active"] or refs.resolver_force_lowdelta:GetValue()) and
                    cache.bruteforce_low_delta or
                    cache.bruteforce_yaw,
                math.abs(D)
            )
            resolver.info[ent]["bruteforce"]["count"] = nearest_idx
        end
    end
    resolver.bruteforce(ent, D)
    if resolver.info[ent]["bruteforce"]["count"] >= #resolver.data[ent].switch then
        resolver.info[ent]["bruteforce"]["count"] = 1
        resolver.info[ent]["bruteforce"]["fucked_up"] = true
    end
end

function resolver.low_delta(ent, a2)
    if not refs.resolver_modules:IsEnabled("Low Delta") or not resolver.info[ent]["low_delta"]["active"] then
        return
    end
    if a2 == "miss" then
        if resolver.data[ent].misses >= 1 and resolver.data[ent].hits == 0 then
            resolver.info[ent]["low_delta"]["count"] = resolver.info[ent]["low_delta"]["count"] + 1
        end
        if resolver.data[ent].misses == 1 and resolver.info[ent]["low_delta"]["count"] - 1 > 1 then
            resolver.info[ent]["low_delta"]["count"] = resolver.info[ent]["low_delta"]["count"] - 1
        end
        local a3 = resolver.info[ent]["low_delta"]["count"] - 1
        local D = m.GetPlayerBodyYaw(ent)
        local a0 = refs.resolver_yawlimit:GetValue()
        local a1 = cmath.IsNumberNegative(D)
        if a3 == 1 then
            resolver.info[ent]["low_delta"]["active"] = true
            resolver.info[ent]["low_delta"]["yaw"] = a1 and 30 or -30
            mariolua.log(
                "[Resolver] Low-delta[" ..
                    a3 ..
                        "] detected on target ['" ..
                            entity.get_player_name(ent) ..
                                "']. Trying fake yaw '" .. resolver.info[ent]["low_delta"]["yaw"] .. "°'"
            )
            m.SetPlayerBodyYaw(ent, cmath.clamp(resolver.info[ent]["low_delta"]["yaw"], -a0, a0))
        elseif a3 == 2 then
            resolver.info[ent]["low_delta"]["yaw"] = a1 and 20 or -20
            mariolua.log(
                "[Resolver] Low-delta[" ..
                    a3 ..
                        "] target ['" ..
                            entity.get_player_name(ent) ..
                                "']. Trying fake yaw '" .. resolver.info[ent]["low_delta"]["yaw"] .. "°'"
            )
            m.SetPlayerBodyYaw(ent, cmath.clamp(resolver.info[ent]["low_delta"]["yaw"], -a0, a0))
        elseif a3 == 3 then
            resolver.info[ent]["low_delta"]["yaw"] = a1 and 10 or -10
            mariolua.log(
                "[Resolver] Low-delta[" ..
                    a3 ..
                        "] target ['" ..
                            entity.get_player_name(ent) ..
                                "']. Trying fake yaw '" .. resolver.info[ent]["low_delta"]["yaw"] .. "°'"
            )
            m.SetPlayerBodyYaw(ent, cmath.clamp(resolver.info[ent]["low_delta"]["yaw"], -a0, a0))
        elseif a3 == 4 then
            resolver.info[ent]["low_delta"]["count"] = 0
            resolver.info[ent]["low_delta"]["active"] = false
            resolver.info[ent]["low_delta"]["yaw"] = nil
            m.SetPlayerBodyYaw(ent, cmath.clamp(a1 and 60 or -60, -a0, a0))
            mariolua.log(
                "[Resolver] Low-delta[" ..
                    a3 ..
                        "] target ['" ..
                            entity.get_player_name(ent) ..
                                "']. Possible wrong detection. Reseting low-delta data and body yaw to " ..
                                    cmath.clamp(a1 and 60 or -60, -a0, a0) .. "�"
            )
        end
    elseif a2 == "hit" then
        if math.abs(resolver.info[ent].hit_yaw) <= 30 and not resolver.info[ent]["low_delta"]["active"] then
            resolver.info[ent]["low_delta"]["active"] = true
            resolver.info[ent]["low_delta"]["count"] = 2
        elseif math.abs(resolver.info[ent].hit_yaw) > 30 and resolver.info[ent]["low_delta"]["active"] then
            resolver.info[ent]["low_delta"]["active"] = false
            resolver.info[ent]["low_delta"]["count"] = 0
        end
        if
            not resolver.info[ent]["low_delta"]["active"] and
                (resolver.info[ent].hits == 2 and resolver.info[ent]["low_delta"]["count"] < 2 and
                    resolver.info[ent].misses == 0)
         then
            resolver.info[ent]["low_delta"]["count"] = 0
        end
        if not resolver.info[ent]["low_delta"]["active"] and resolver.info[ent]["low_delta"]["count"] > 0 then
            resolver.info[ent]["low_delta"]["count"] = 0
            mariolua.log(
                "[Resolver] Hit target ['" .. entity.get_player_name(ent) .. "']. Setting low delta count to 0�"
            )
        end
    end
end

function resolver.set_module_stats(ent, a4)
    local state = a4 and "failed" or "success"
    local a5 = resolver.info[ent].logic.side
    for a6, a7 in pairs(resolver.info[ent]["modules"]) do
        a7[state] = a7["side"] == a5 and a7[state] + 1 or a7[state]
    end
end

function resolver.slowwalk_miss(ent)
    if resolver.data[ent].props["is_slowwalking"] then
        resolver.info[ent].slowwalk_inversed = not resolver.info[ent].slowwalk_inversed
    elseif resolver.info[ent].misses == 2 then
        resolver.info[ent].inversed = true
    elseif resolver.info[ent].misses == 4 then
        resolver.info[ent].inversed = false
        resolver.info[ent].misses = 0
    end
end

function resolver.handle_miss_streak(ent)
    if resolver.data[ent].misses == 1 and resolver.data[ent].hits == 0 and not resolver.data[ent].record.abs_yaw_set then
        resolver.info[ent].miss_streak = resolver.info[ent].miss_streak + 1
    end
    if resolver.info[ent].miss_streak == 3 then
        resolver.info[ent].real = "Show"
        mariolua.log("[Resolver] " .. entity.get_player_name(ent) .. " is maybe peeking with real.")
    elseif resolver.info[ent].miss_streak >= 6 then
        resolver.info[ent].real = "Hide"
        mariolua.log("[Resolver] " .. entity.get_player_name(ent) .. " is not peeking with real.")
    end
end

cache.hitgroup_names = {
    "generic",
    "head",
    "chest",
    "stomach",
    "left arm",
    "right arm",
    "left leg",
    "right leg",
    "neck",
    "?",
    "gear"
}
cache.shot_data = {}

function resolver.aim_fire(a8)
    cache.shot_data[a8.id] = {
        state = state or "unknown",
        target = a8.target or "",
        hitgroup = a8.hitgroup or "",
        target_hitgroup = a8.hitgroup or ""
    }
end

function resolver.aim_hit(a8)
    local a9 = a8.id
    cache.shot_data[a9].state = "hit"
    cache.shot_data[a9].hitgroup = a8.hitgroup
    local ent = a8.target
    local aa = cache.hitgroup_names[a8.hitgroup + 1] or "?"
    local ab = cache.hitgroup_names[cache.shot_data[a9].target_hitgroup + 1] or "?"
    resolver.data[ent] = resolver.data[ent] or resolver.set_round_data()
    resolver.info[ent] = resolver.info[ent] or resolver.set_match_data()
    resolver.set_current_resolver(ent)
    local ac = {
        ["hit"] = string.format(
            "Hit %s in the %s for %d damage (%d health remaining)",
            entity.get_player_name(a8.target),
            aa,
            a8.damage,
            entity.get_prop(a8.target, "m_iHealth")
        ),
        ["wrong_hit"] = string.format('Missed target hitgroup "%s"  and hit "%s"', ab, aa),
        ["resolver_hit"] = ""
    }

    if resolver.info[ent].current == "Gamesense" or resolver.info[ent].current == "MarioLua" then
        ac["resolver_hit"] =
            string.format(
            "%s Resolver hit %s in the %s for %d damage (%d health remaining)",
            resolver.info[ent].current,
            entity.get_player_name(a8.target),
            aa,
            a8.damage,
            entity.get_prop(a8.target, "m_iHealth")
        )
        resolver.hit[resolver.info[ent].current]["count"] = resolver.hit[resolver.info[ent].current]["count"] + 1
        if a8.hitgroup == 1 then
            resolver.hit[resolver.info[ent].current]["headshot"] =
                resolver.hit[resolver.info[ent].current]["headshot"] + 1
        end
        if resolver.info[ent].current == "MarioLua" then
            resolver.info[ent].hits = resolver.info[ent].hits + 1
            resolver.data[ent].hits = resolver.data[ent].hits + 1
        end
        if ab == aa and resolver.info[ent].current == "MarioLua" then
            resolver.set_module_stats(ent, false)
            resolver.info[ent].hit_yaw = m.GetPlayerBodyYaw(ent)
            resolver.data[ent].resolved_yaw = m.GetPlayerBodyYaw(ent)
            table.insert(resolver.info[ent]["hit_yaws_t"], math.abs(resolver.info[ent].hit_yaw))
            resolver.low_delta(ent, "hit")
        end
        resolver.data[ent]["bruteforce"]["hit"] =
            resolver.data[ent]["bruteforce"]["active"] and resolver.data[ent]["bruteforce"]["hit"] + 1 or
            resolver.data[ent]["bruteforce"]["hit"]
        resolver.info[ent]["bruteforce"]["hit_yaw"] =
            resolver.data[ent]["bruteforce"]["active"] and m.GetPlayerBodyYaw(ent) or
            resolver.info[ent]["bruteforce"]["hit_yaw"]
        if refs.resolver_log_hit:GetValue() then
            mariolua.PrintInChat(ac["resolver_hit"])
            if ab ~= aa then
                mariolua.PrintInChat(ac["wrong_hit"])
                mariolua.print(ac["wrong_hit"])
            end
        end

        mariolua.print(ac["resolver_hit"])
        resolver.update_resolver_stats()
    else
        if refs.resolver_log_hit:GetValue() then
            mariolua.PrintInChat(ac["hit"])
        end

        if ab ~= aa then
            mariolua.print(ac["hit"])
        end
    end
end

function resolver.aim_miss(a8)
    local ent = a8.target
    local ad = a8.reason == "?" and "resolver" or a8.reason
    local ae = {}
    ae["others"] = "Missed shot at " .. entity.get_player_name(ent) .. " due to " .. ad .. "."
    if ad == "resolver" then
        resolver.data[ent] = resolver.data[ent] or resolver.set_round_data()
        resolver.info[ent] = resolver.info[ent] or resolver.set_match_data()
        resolver.set_current_resolver(ent)
        resolver.info[ent].missed_body_yaw = m.GetPlayerBodyYaw(ent)
        ae["resolver"] =
            "Missed shot at " ..
            entity.get_player_name(ent) ..
                " due to " ..
                    resolver.info[ent].current .. " Resolver." .. " [" .. resolver.data[ent].misses + 1 .. "x]"
        if resolver.info[ent].current:match("MarioLua") or resolver.info[ent].current:match("Gamesense") then
            resolver.missed[resolver.info[ent].current:match("MarioLua") and "MarioLua" or "Gamesense"] =
                resolver.missed[resolver.info[ent].current:match("MarioLua") and "MarioLua" or "Gamesense"] + 1
            resolver.data[ent].misses = resolver.data[ent].misses + 1
            resolver.info[ent].misses = resolver.info[ent].misses + 1
            resolver.set_module_stats(ent, true)
            if
                resolver.IsActive() and refs.resolver_modules:IsEnabled("Desync Detection") and
                    not resolver.data[ent].props["should_resolve"]
             then
                resolver.data[ent].props["should_resolve"] = true
                resolver.data[ent].props["correction"] = true
                m.SetForceBodyYawCheckbox(ent, true)
                m.SetCorrectionActive(ent, true)
            end
            if resolver.info[ent].current:match("MarioLua") then
                table.insert(resolver.info[ent]["missed_yaws_t"], math.abs(resolver.info[ent].missed_body_yaw))
                resolver.data[ent]["bruteforce"]["miss"] =
                    resolver.data[ent]["bruteforce"]["active"] and resolver.data[ent]["bruteforce"]["miss"] + 1 or
                    resolver.data[ent]["bruteforce"]["miss"]
                resolver.bruteforce_handler(ent)
                resolver.low_delta(ent, "miss")
            end
            if resolver.data[ent].props["is_slowwalking"] then
                resolver.data[ent].slowwalk_misses = resolver.data[ent].slowwalk_misses + 1
            end
            resolver.update_resolver_stats()
        end
        if refs.resolver_log_miss:GetValue() then
            mariolua.PrintInChat(ae["resolver"])
            mariolua.print(ae["resolver"])
        end
        mariolua.log(
            "[Resolver] Missed shot player " ..
                entity.get_player_name(ent) ..
                    " at yaw: " .. m.GetPlayerBodyYaw(ent) .. "� [" .. resolver.data[ent].misses + 1 .. "x]"
        )
    elseif refs.resolver_log_miss:GetValue() then
        mariolua.PrintInChat(ae["others"])
        mariolua.print(ae["others"])
    end
end

cache.t_last_layer_info = {}
cache.m_flPrevAccelerating = {}
cache.m_flPrevSlowingDown = {}
cache.m_flPrevVelocity2D = {}
cache.m_flPrevCycle = {}
cache.m_flPrevWeight = {}
cache.m_flPrevCycle6 = {}
cache.m_flPrevWeight6 = {}
cache.layer_control = {}
cache.layer_control_table = {
    ["is_active"] = "",
    ["override"] = true,
    ["basic"] = {["value"] = 0},
    ["onshot"] = {["shot_fired"] = false},
    ["velocity"] = {}
}
resolver.records = {}
resolver.record_max_ticks = 8
resolver.record_old_tickcount = {}

function resolver.get_player_records(ent)
    if ent ~= nil and resolver.records[ent] ~= nil then
        return resolver.records[ent]
    end
end

function resolver.reset(ent)
    resolver.records[ent] = {}
end

function resolver.update_player_record_data(ent)
    local af = resolver.records[ent]
    local ag = entity.get_prop(ent, "m_flSimulationTime")
    if af == nil then
        resolver.records[ent] = {}
        af = resolver.records[ent]
    end
    resolver.record_old_tickcount[ent] = resolver.record_old_tickcount[ent] or globals.tickcount()
    if ag > 0 and (#af == 0 or #af > 0 and af[1].simulation_time ~= ag) then
        local ah = centity.get_animlayer(ent, 3)
        local ai = centity.get_animlayer(ent, 6)
        local aj = centity.get_animlayer(ent, 12)
        local ak = {
            [3] = {m_flCycle = ah.m_flCycle, m_flWeight = ah.m_flWeight, m_flPlaybackRate = ah.m_flPlaybackRate},
            [6] = {m_flCycle = ai.m_flCycle, m_flWeight = ai.m_flWeight, m_flPlaybackRate = ai.m_flPlaybackRate},
            [12] = {m_flCycle = aj.m_flCycle, m_flWeight = aj.m_flWeight, m_flPlaybackRate = aj.m_flPlaybackRate}
        }
        local al = {
            animlayer = ak,
            m_flVelocity2D = {resolver.data[ent].props["m_flVelocity2D"]},
            m_angEyeAngles = {resolver.data[ent].props["m_angEyeAngles"]},
            m_flLowerBodyYawTarget = resolver.data[ent].props["m_flLowerBodyYawTarget"],
            m_flLowerBodyDelta = resolver.data[ent].props["m_flLowerBodyDelta"],
            m_flLowerBodyYawMoving = resolver.data[ent].props["m_flLowerBodyYawMoving"],
            m_flLowerBodyYawStanding = resolver.data[ent].props["m_flLowerBodyYawStanding"],
            m_flEyeYaw = resolver.data[ent].props["m_flEyeYaw"],
            m_flGoalFeetYaw = resolver.data[ent].props["m_flGoalFeetYaw"],
            m_flGoalFeetDelta = resolver.data[ent].props["m_flGoalFeetDelta"],
            m_flCurrentFeetYaw = resolver.data[ent].props["m_flCurrentFeetYaw"],
            origin = {entity.get_prop(ent, "m_vecOrigin")},
            m_vecMins = {entity.get_prop(ent, "m_vecMins")},
            m_vecMaxs = {entity.get_prop(ent, "m_vecMaxs")},
            simulation_time = entity.get_prop(ent, "m_flSimulationTime"),
            on_ground = entity.get_prop(ent, "m_vecVelocity[2]") ^ 2 > 0,
            m_iTickCount = globals.tickcount()
        }

        for y = resolver.record_max_ticks, 2, -1 do
            resolver.records[ent][y] = resolver.records[ent][y - 1]
        end
        resolver.record_old_tickcount[ent] = globals.tickcount()
        resolver.records[ent][1] = al
    end
end

function resolver.handle_record(ent)
    resolver.update_player_record_data(ent)
end

function resolver.log_layer_t(ent, ak, y)
    if y ~= 6 then
        return
    end
    local am =
        "[Resolver] Anim layer " ..
        entity.get_player_name(ent) ..
            "\nLayer " ..
                y ..
                    "\nm_flWeight " ..
                        ak.m_flWeight ..
                            "\nm_flPrevWeight " ..
                                cache.m_flPrevWeight[ent] ..
                                    "\nm_flCycle " ..
                                        ak.m_flCycle ..
                                            "\nm_flPrevCycle " ..
                                                cache.m_flPrevCycle[ent] ..
                                                    "\nvelocity2d " .. cmath.no_decimals(velocity2d)
    mariolua.log(am)
end
cache.animlayer_c = {}
cache.animlayer = {}
function resolver.animlayer_pref(ent, an)
    if ent == nil then
        return
    end

    local ak = centity.get_animlayer(ent, an)
    local ao = centity.animstate(ent)
    local ap = math.abs(ao.m_iLastClientSideAnimationUpdateFramecount - ao.m_iLastClientSideAnimationUpdateFramecount)
    local aq = {
        m_flCycle = 1,
        m_flWeight = 1,
        m_flPlaybackRate = 1
    }
    local ar = {
        m_flPrevCycle = ak.m_flCycle,
        m_flCycle = ak.m_flCycle,
        m_flClientCycle = {0, 0, 0},
        m_flPrevWeight = ak.m_flWeight,
        m_flWeight = ak.m_flWeight,
        m_flClientWeight = {0, 0, 0},
        m_flPrevPlaybackRate = ak.m_flPlaybackRate,
        m_flPlaybackRate = ak.m_flPlaybackRate,
        m_flClientPlaybackRate = {0, 0, 0},
        m_flWeightAdvanced = ak.m_flWeightDeltaRate * ap + ak.m_flWeight
    }

    cache.animlayer_c[ent] = cache.animlayer_c[ent] or {}
    cache.animlayer_c[ent][an] = cache.animlayer_c[ent][an] or aq
    cache.animlayer[ent] = cache.animlayer[ent] or {}
    cache.animlayer[ent][an] = cache.animlayer[ent][an] or ar

    if ak.m_flCycle ~= cache.animlayer[ent][an].m_flCycle then
        cache.animlayer[ent][an].m_flPrevCycle = cache.animlayer[ent][an].m_flCycle
        cache.animlayer[ent][an].m_flCycle = ak.m_flCycle
        cache.animlayer[ent][an].m_flClientCycle[cache.animlayer_c[ent][an].m_flCycle] = ak.m_flCycle
        if cache.animlayer_c[ent][an].m_flCycle == 3 then
            cache.animlayer_c[ent][an].m_flCycle = 1
        else
            cache.animlayer_c[ent][an].m_flCycle = cache.animlayer_c[ent][an].m_flCycle + 1
        end
    end

    if ak.m_flWeight ~= cache.animlayer[ent][an].m_flWeight then
        cache.animlayer[ent][an].m_flPrevWeight = cache.animlayer[ent][an].m_flWeight
        cache.animlayer[ent][an].m_flWeight = ak.m_flWeight
        cache.animlayer[ent][an].m_flClientWeight[cache.animlayer_c[ent][an].m_flWeight] = ak.m_flWeight
        if cache.animlayer_c[ent][an].m_flWeight == 3 then
            cache.animlayer_c[ent][an].m_flWeight = 1
        else
            cache.animlayer_c[ent][an].m_flWeight = cache.animlayer_c[ent][an].m_flWeight + 1
        end
    end

    if ak.m_flPlaybackRate ~= cache.animlayer[ent][an].m_flPlaybackRate then
        cache.animlayer[ent][an].m_flPrevPlaybackRate = cache.animlayer[ent][an].m_flPlaybackRate
        cache.animlayer[ent][an].m_flPlaybackRate = ak.m_flPlaybackRate
        cache.animlayer[ent][an].m_flClientPlaybackRate[cache.animlayer_c[ent][an].m_flPlaybackRate] =
            ak.m_flPlaybackRate
        if cache.animlayer_c[ent][an].m_flPlaybackRate == 3 then
            cache.animlayer_c[ent][an].m_flPlaybackRate = 1
        else
            cache.animlayer_c[ent][an].m_flPlaybackRate = cache.animlayer_c[ent][an].m_flPlaybackRate + 1
        end
    end
end

cache.layer_log_t = {}
function resolver.record_animlayers(ent, v, as)
    if ent == nil or cache.animlayer[ent] == nil then
        return
    end
    local ah, at = centity.get_animlayer(ent, 3)
    local ai = centity.get_animlayer(ent, 6)
    local aj = centity.get_animlayer(ent, 12)
    local ao = centity.animstate(ent)
    local ap = math.abs(ao.m_iLastClientSideAnimationUpdateFramecount - ao.m_iLastClientSideAnimationUpdateFramecount)
    local au = ah.m_flWeightDeltaRate * ap + ah.m_flWeight
    local av = ai.m_flWeightDeltaRate * ap + ai.m_flWeight
    local aw = aj.m_flWeightDeltaRate * ap + aj.m_flWeight
    local ax = resolver.get_player_records(ent)
    local ay = {
        "\n",
        "[Layer 3]",
        string.format("     m_flWeightAdvanced: %f ", au),
        string.format("     m_flWeightDeltaRate: %f ", ah.m_flWeightDeltaRate),
        string.format("     m_flWeight: %f ", ah.m_flWeight),
        string.format("     m_flPrevWeight: %f ", cache.animlayer[ent][3].m_flPrevWeight),
        string.format("     m_flCycle: %f ", ah.m_flCycle),
        string.format("     m_flPrevCycle: %f ", cache.animlayer[ent][3].m_flPrevCycle),
        string.format("     m_flPlaybackRate: %f ", ah.m_flPlaybackRate),
        string.format("     m_flPrevPlaybackRate: %f ", cache.animlayer[ent][3].m_flPrevPlaybackRate),
        string.format(
            "     m_flWeight                 1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[3].m_flWeight,
            ax[2].animlayer[3].m_flWeight,
            ax[3].animlayer[3].m_flWeight
        ),
        string.format(
            "     m_flCycle                  1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[3].m_flCycle,
            ax[2].animlayer[3].m_flCycle,
            ax[3].animlayer[3].m_flCycle
        ),
        string.format(
            "     m_flPlaybackRate           1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[3].m_flPlaybackRate,
            ax[2].animlayer[3].m_flPlaybackRate,
            ax[3].animlayer[3].m_flPlaybackRate
        ),
        string.format("     Sequence Activity: %i ", at),
        "\n",
        "[Layer 6]",
        string.format("     m_flWeightAdvanced: %f ", av),
        string.format("     m_flWeightDeltaRate: %f ", ai.m_flPlaybackRate - av),
        string.format("     m_flWeight: %f ", ai.m_flWeight),
        string.format("     m_flPrevWeight: %f ", cache.animlayer[ent][6].m_flPrevWeight),
        string.format("     m_flCycle: %f ", ai.m_flCycle),
        string.format("     m_flPrevCycle: %f ", cache.animlayer[ent][6].m_flPrevCycle),
        string.format("     m_flPlaybackRate: %f ", ai.m_flPlaybackRate),
        string.format(
            "     m_flWeight                 1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[6].m_flWeight,
            ax[2].animlayer[6].m_flWeight,
            ax[3].animlayer[6].m_flWeight
        ),
        string.format(
            "     m_flCycle                  1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[6].m_flCycle,
            ax[2].animlayer[6].m_flCycle,
            ax[3].animlayer[6].m_flCycle
        ),
        string.format(
            "     m_flPlaybackRate           1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[6].m_flPlaybackRate,
            ax[2].animlayer[6].m_flPlaybackRate,
            ax[3].animlayer[6].m_flPlaybackRate
        ),
        string.format("     m_flPrevPlaybackRate: %f ", cache.animlayer[ent][6].m_flPrevPlaybackRate),
        "\n",
        "[Layer 12]",
        string.format("     m_flWeightAdvanced: %f ", aw),
        string.format("     m_flWeightDeltaRate: %f ", aj.m_flWeightDeltaRate),
        string.format("     m_flWeight: %f ", aj.m_flWeight),
        string.format("     m_flPrevWeight: %f ", cache.animlayer[ent][12].m_flPrevWeight),
        string.format("     m_flCycle: %f ", aj.m_flCycle),
        string.format("     m_flPrevCycle: %f ", cache.animlayer[ent][12].m_flPrevCycle),
        string.format("     m_flPlaybackRate: %f ", aj.m_flPlaybackRate),
        string.format(
            "     m_flWeight                 1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[12].m_flWeight,
            ax[2].animlayer[12].m_flWeight,
            ax[3].animlayer[12].m_flWeight
        ),
        string.format(
            "     m_flCycle                  1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[12].m_flCycle,
            ax[2].animlayer[12].m_flCycle,
            ax[3].animlayer[12].m_flCycle
        ),
        string.format(
            "     m_flPlaybackRate           1: %f,           2: %f,           3: %f ",
            ax[1].animlayer[12].m_flPlaybackRate,
            ax[2].animlayer[12].m_flPlaybackRate,
            ax[3].animlayer[12].m_flPlaybackRate
        ),
        string.format("     m_flPrevPlaybackRate: %f ", cache.animlayer[ent][12].m_flPrevPlaybackRate),
        "----------------------------------------------------------\n",
        "\n"
    }

    local az = entity.get_player_name(ent)
    cache.layer_log_t[az] = cache.layer_log_t[az] or {}
    cache.layer_log_t[az][1] = cache.layer_log_t[az][1] or ay
    cache.layer_log_t[az][1] = ay
    cache.draw_ply_info(cache.layer_log_t[az], ent == p and 0 or v - 350, ent == p and 360 or as)
end

cache.highest_weight = {
    [0] = 0,
    [1] = 0.004,
    [2] = 0.004,
    [3] = 0.004,
    [4] = 0.004,
    [5] = 0.004,
    [6] = 0.004,
    [7] = 0.004,
    [8] = 0.004,
    [9] = 0.004,
    [10] = 0.004,
    [11] = 0.004,
    [12] = 0.004,
    [13] = 0.004,
    [14] = 0.004,
    [15] = 0.004,
    [16] = 0.004,
    [17] = 0.004,
    [18] = 0.004,
    [19] = 0.004,
    [20] = 0.008,
    [21] = 0.024,
    [22] = 0.024,
    [23] = 024,
    [24] = 024,
    [25] = 024,
    [26] = 024,
    [27] = 024,
    [28] = 024,
    [29] = 024,
    [30] = 024,
    [31] = 024,
    [32] = 0.09,
    [33] = 0.141,
    [34] = 141,
    [35] = 0.141,
    [36] = 141,
    [37] = 0.141,
    [38] = 141,
    [39] = 0.141,
    [40] = 0.314,
    [41] = 0.314,
    [42] = 0.314,
    [43] = 0.314,
    [44] = 0.314,
    [45] = 0.141,
    [46] = 0.314,
    [47] = 0.373,
    [48] = 0.345,
    [49] = 0.392,
    [50] = 0.4,
    [51] = 0.4,
    [52] = 0.4,
    [53] = 0.424,
    [54] = 0.424,
    [55] = 0.424,
    [56] = 0.447,
    [57] = 0.447,
    [58] = 0.447,
    [59] = 0.447,
    [60] = 0.478,
    [61] = 0.463,
    [62] = 0.494,
    [63] = 0.478,
    [64] = 0.478,
    [65] = 0.478,
    [66] = 0.478,
    [67] = 0.478,
    [68] = 0.541,
    [69] = 0.541,
    [70] = 0.565,
    [71] = 0.596,
    [72] = 0.604,
    [73] = 0.651,
    [74] = 0.651,
    [75] = 0.651,
    [76] = 0.596,
    [77] = 0.592,
    [78] = 0.604,
    [79] = 0.682,
    [80] = 0.835,
    [81] = 0.835,
    [82] = 0.835,
    [83] = 0.835,
    [84] = 0.835,
    [85] = 0.835,
    [86] = 0.835,
    [87] = 0.835,
    [88] = 0.835,
    [89] = 0.835,
    [90] = 0.827,
    [91] = 0.827,
    [92] = 0.827,
    [93] = 0.827,
    [94] = 0.969,
    [95] = 0.969,
    [96] = 0.969,
    [97] = 0.969,
    [98] = 0.969,
    [99] = 0.969,
    [100] = 0.969,
    [101] = 0.969,
    [102] = 0.969,
    [103] = 0.969,
    [104] = 0.969,
    [105] = 0.969,
    [106] = 0.969,
    [107] = 0.969,
    [108] = 0.969,
    [109] = 0.969,
    [110] = 0.969,
    [111] = 0.969,
    [112] = 0.969,
    [113] = 0.969,
    [114] = 0.969,
    [115] = 0.969,
    [116] = 0.969,
    [117] = 0.969,
    [118] = 0.969,
    [119] = 0.969,
    [120] = 0.969,
    [121] = 0.969,
    [122] = 0.969,
    [123] = 0.969,
    [124] = 0.969,
    [125] = 0.969,
    [126] = 0.969,
    [127] = 0.969,
    [128] = 0.969,
    [129] = 0.969,
    [130] = 0.969,
    [131] = 0.969,
    [132] = 0.969,
    [133] = 0.969,
    [134] = 0.969,
    [135] = 0.937,
    [136] = 0.937,
    [137] = 0.933,
    [138] = 0.933,
    [139] = 0.937,
    [140] = 0.937,
    [141] = 0.937,
    [143] = 0.933,
    [144] = 0.929,
    [145] = 0.937,
    [146] = 0.933,
    [147] = 0.933,
    [148] = 0.933,
    [149] = 0.937,
    [150] = 0.933,
    [151] = 0.922,
    [152] = 0.922,
    [153] = 0.933,
    [154] = 0.929,
    [155] = 0.922,
    [156] = 0.914,
    [157] = 0.922,
    [158] = 0.922,
    [159] = 0.929,
    [160] = 0.933,
    [161] = 0.933,
    [162] = 0.933,
    [163] = 0.933,
    [164] = 0.933,
    [165] = 0.914,
    [166] = 0.933,
    [167] = 0.933,
    [168] = 0.925,
    [169] = 0.929,
    [170] = 0.933,
    [171] = 0.922,
    [172] = 0.914,
    [173] = 0.933,
    [174] = 0.925,
    [175] = 0.918,
    [176] = 0.933,
    [177] = 0.918,
    [178] = 0.933,
    [179] = 0.933,
    [180] = 0.925,
    [181] = 0.933,
    [182] = 0.918,
    [183] = 0.918,
    [184] = 0.922,
    [185] = 0.929,
    [186] = 0.933,
    [187] = 0.91,
    [188] = 0.922,
    [189] = 0.91,
    [190] = 0.914,
    [191] = 0.914,
    [192] = 0.933,
    [193] = 0.914,
    [194] = 0.929,
    [195] = 0.914,
    [196] = 0.918,
    [197] = 0.929,
    [198] = 0.91,
    [199] = 0.914,
    [200] = 0.918,
    [201] = 0.918,
    [202] = 0.91,
    [203] = 0.922,
    [204] = 0.922,
    [205] = 0.929,
    [206] = 0.929,
    [207] = 0.929,
    [208] = 0.914,
    [209] = 0.914,
    [210] = 0.925,
    [211] = 0.929,
    [212] = 0.914,
    [213] = 0.914,
    [214] = 0.929,
    [215] = 0.914,
    [216] = 0.918,
    [217] = 0.91,
    [218] = 0.914,
    [219] = 0.922,
    [220] = 0.914,
    [221] = 0.914,
    [222] = 0.925,
    [223] = 0.914,
    [224] = 0.914,
    [225] = 0.91,
    [226] = 0.914,
    [227] = 0.914,
    [228] = 0.922,
    [229] = 0.925,
    [230] = 0.914,
    [231] = 0.914,
    [232] = 0.91,
    [233] = 0.925,
    [234] = 0.925,
    [235] = 0.914,
    [236] = 0.918,
    [237] = 0.914,
    [238] = 0.918,
    [239] = 0.922,
    [240] = 0.914,
    [241] = 0.914,
    [242] = 0.914,
    [243] = 0.922,
    [244] = 0.914,
    [245] = 0.91,
    [246] = 0.918,
    [247] = 0.914,
    [248] = 0.914,
    [249] = 0.918,
    [250] = 0.918
}
cache.lowest_weight = {
    [0] = 0,
    [1] = 0.004,
    [2] = 0.004,
    [3] = 0.004,
    [4] = 0.004,
    [6] = 0.008,
    [7] = 0.008,
    [8] = 0.008,
    [9] = 0.008,
    [10] = 0.008,
    [11] = 0.008,
    [12] = 0.008,
    [13] = 0.008,
    [14] = 0.008,
    [15] = 0.008,
    [16] = 0.008,
    [17] = 0.008,
    [18] = 0.039,
    [19] = 0.039,
    [20] = 0.039,
    [21] = 0.039,
    [22] = 0.039,
    [23] = 0.039,
    [24] = 0.039,
    [25] = 0.043,
    [26] = 0.043,
    [27] = 0.043,
    [28] = 0.043,
    [29] = 0.043,
    [30] = 0.043,
    [31] = 0.043,
    [32] = 0.043,
    [33] = 0.043,
    [34] = 0.063,
    [35] = 0.063,
    [36] = 0.063,
    [37] = 0.063,
    [38] = 0.063,
    [39] = 0.063,
    [40] = 0.063,
    [41] = 0.063,
    [42] = 0.063,
    [43] = 0.063,
    [44] = 0.063,
    [45] = 0.063,
    [46] = 0.063,
    [47] = 0.098,
    [48] = 0.098,
    [49] = 0.098,
    [50] = 0.098,
    [51] = 0.098,
    [52] = 0.098,
    [53] = 0.098,
    [54] = 0.098,
    [55] = 0.098,
    [56] = 0.098,
    [57] = 0.098,
    [58] = 0.098,
    [59] = 0.118,
    [60] = 0.118,
    [61] = 0.118,
    [62] = 0.141,
    [63] = 0.141,
    [64] = 0.141,
    [65] = 0.141,
    [66] = 0.141,
    [67] = 0.141,
    [68] = 0.141,
    [69] = 0.157,
    [70] = 0.157,
    [71] = 0.192,
    [72] = 0.153,
    [73] = 0.153,
    [74] = 0.153,
    [75] = 0.153,
    [76] = 0.153,
    [77] = 0.212,
    [78] = 0.212,
    [79] = 0.212,
    [80] = 0.212,
    [81] = 0.212,
    [82] = 0.212,
    [83] = 0.212,
    [84] = 0.212,
    [85] = 0.212,
    [86] = 0.212,
    [87] = 0.290,
    [88] = 0.290,
    [89] = 0.318,
    [90] = 0.318,
    [91] = 0.318,
    [92] = 0.318,
    [93] = 0.318,
    [94] = 0.318,
    [95] = 0.318,
    [96] = 0.318,
    [97] = 0.318,
    [98] = 0.318,
    [99] = 0.388,
    [100] = 0.376,
    [101] = 0.376,
    [102] = 0.573,
    [103] = 0.569,
    [104] = 0.569,
    [105] = 0.616,
    [106] = 0.616,
    [107] = 0.682,
    [108] = 0.682,
    [109] = 0.682,
    [110] = 0.733,
    [111] = 0.733,
    [112] = 0.733,
    [113] = 0.824,
    [115] = 0.855,
    [116] = 0.855,
    [117] = 0.855,
    [118] = 0.855,
    [119] = 0.900,
    [120] = 0.900,
    [121] = 0.900,
    [122] = 0.900,
    [123] = 0.900,
    [124] = 0.900,
    [125] = 0.900,
    [126] = 0.900,
    [127] = 0.900,
    [128] = 0.900,
    [129] = 0.900,
    [130] = 0.900,
    [131] = 0.900,
    [132] = 0.900,
    [133] = 0.900,
    [134] = 0.900,
    [135] = 0.900,
    [136] = 0.900,
    [137] = 0.900,
    [138] = 0.900,
    [139] = 0.900,
    [140] = 0.900,
    [141] = 0.900,
    [142] = 0.900,
    [143] = 0.900,
    [144] = 0.900,
    [145] = 0.900,
    [146] = 0.900,
    [147] = 0.900,
    [148] = 0.900,
    [149] = 0.900,
    [150] = 0.900,
    [151] = 0.900,
    [152] = 0.900,
    [153] = 0.900,
    [154] = 0.900,
    [155] = 0.900,
    [156] = 0.900,
    [157] = 0.900,
    [158] = 0.900,
    [159] = 0.900,
    [160] = 0.900,
    [161] = 0.900,
    [162] = 0.900,
    [163] = 0.900,
    [164] = 0.900,
    [165] = 0.900,
    [166] = 0.900,
    [167] = 0.900,
    [168] = 0.900,
    [169] = 0.900,
    [170] = 0.900,
    [171] = 0.900,
    [172] = 0.900,
    [173] = 0.900,
    [174] = 0.900,
    [175] = 0.900,
    [176] = 0.900,
    [177] = 0.900,
    [178] = 0.900,
    [179] = 0.900,
    [180] = 0.900,
    [181] = 0.900,
    [182] = 0.900,
    [183] = 0.900,
    [184] = 0.900,
    [185] = 0.900,
    [186] = 0.900,
    [187] = 0.900,
    [188] = 0.900,
    [189] = 0.900,
    [190] = 0.900,
    [191] = 0.900,
    [192] = 0.900,
    [193] = 0.900,
    [194] = 0.900,
    [195] = 0.900,
    [196] = 0.900,
    [197] = 0.900,
    [198] = 0.900,
    [199] = 0.900,
    [200] = 0.900,
    [201] = 0.900,
    [202] = 0.900,
    [203] = 0.900,
    [204] = 0.900,
    [205] = 0.900,
    [206] = 0.900,
    [207] = 0.900,
    [208] = 0.900,
    [209] = 0.900,
    [210] = 0.900,
    [211] = 0.900,
    [212] = 0.900,
    [213] = 0.900,
    [214] = 0.900,
    [215] = 0.900,
    [216] = 0.900,
    [217] = 0.900,
    [218] = 0.900,
    [219] = 0.900,
    [220] = 0.900,
    [221] = 0.900,
    [222] = 0.900,
    [223] = 0.900,
    [224] = 0.900,
    [225] = 0.900,
    [226] = 0.900,
    [227] = 0.900,
    [228] = 0.900,
    [229] = 0.900,
    [230] = 0.900,
    [231] = 0.900,
    [232] = 0.900,
    [233] = 0.900,
    [234] = 0.900,
    [235] = 0.900,
    [236] = 0.900,
    [237] = 0.900,
    [238] = 0.900,
    [239] = 0.900,
    [240] = 0.900,
    [241] = 0.900,
    [242] = 0.900,
    [243] = 0.900,
    [244] = 0.900,
    [245] = 0.900,
    [246] = 0.900,
    [247] = 0.900,
    [248] = 0.900,
    [249] = 0.900,
    [250] = 0.900
}
cache.highest_switch_values = {
    [0] = 0,
    [1] = 0.004,
    [2] = 0.004,
    [3] = 0.004,
    [4] = 0.004,
    [5] = 0.004,
    [6] = 0.004,
    [7] = 0.004,
    [8] = 0.004,
    [9] = 0.004,
    [10] = 0.004,
    [11] = 0.004,
    [12] = 0.004,
    [13] = 0.004,
    [14] = 0.004,
    [15] = 0.004,
    [16] = 0.004,
    [17] = 0.004,
    [18] = 0.004,
    [19] = 0.004,
    [20] = 0.008,
    [21] = 0.024,
    [22] = 0.024,
    [23] = 0.024,
    [24] = 0.024,
    [25] = 0.070,
    [26] = 0.070,
    [27] = 0.070,
    [28] = 0.090,
    [29] = 0.090,
    [30] = 0.090,
    [31] = 0.090,
    [32] = 0.090,
    [33] = 0.141,
    [34] = 0.141,
    [35] = 0.141,
    [36] = 0.141,
    [37] = 0.141,
    [38] = 0.141,
    [39] = 0.141,
    [40] = 0.141,
    [41] = 0.141,
    [42] = 0.141,
    [43] = 0.314,
    [44] = 0.314,
    [45] = 0.314,
    [46] = 0.314,
    [47] = 0.373,
    [48] = 0.373,
    [49] = 0.392,
    [50] = 0.400,
    [51] = 0.400,
    [52] = 0.400,
    [53] = 0.424,
    [54] = 0.424,
    [55] = 0.424,
    [56] = 0.447,
    [57] = 0.447,
    [58] = 0.447,
    [59] = 0.447,
    [60] = 0.478,
    [61] = 0.478,
    [62] = 0.494,
    [63] = 0.494,
    [64] = 0.494,
    [65] = 0.494,
    [66] = 0.494,
    [67] = 0.494,
    [68] = 0.541,
    [69] = 0.541,
    [70] = 0.565,
    [71] = 0.596,
    [72] = 0.604,
    [73] = 0.651,
    [74] = 0.651,
    [75] = 0.651,
    [76] = 0.651,
    [77] = 0.651,
    [78] = 0.651,
    [79] = 0.682,
    [80] = 0.835,
    [81] = 0.835,
    [82] = 0.835,
    [83] = 0.835,
    [84] = 0.835,
    [85] = 0.835,
    [86] = 0.835,
    [87] = 0.835,
    [88] = 0.835,
    [89] = 0.835,
    [90] = 0.827,
    [91] = 0.827,
    [92] = 0.827,
    [93] = 0.827,
    [94] = 0.969,
    [95] = 1.00,
    [96] = 1.00,
    [97] = 1.00,
    [98] = 1.00,
    [99] = 1.00,
    [100] = 1.00,
    [101] = 1.00,
    [102] = 1.00,
    [103] = 1.00,
    [104] = 1.00,
    [105] = 1.00,
    [106] = 1.00,
    [107] = 1.00,
    [108] = 1.00,
    [109] = 1.00,
    [110] = 1.00,
    [111] = 1.00,
    [112] = 1.00,
    [113] = 1.00,
    [114] = 1.00,
    [115] = 1.00,
    [116] = 1.00,
    [117] = 1.00,
    [118] = 1.00,
    [119] = 1.00,
    [120] = 1.00,
    [121] = 1.00,
    [122] = 1.00,
    [123] = 1.00,
    [124] = 1.00,
    [125] = 1.00,
    [126] = 1.00,
    [127] = 1.00,
    [128] = 1.00,
    [129] = 1,
    [130] = 1,
    [131] = 1,
    [132] = 1,
    [133] = 1,
    [134] = 1,
    [135] = 1,
    [136] = 1,
    [137] = 1,
    [138] = 1,
    [139] = 1,
    [140] = 1,
    [141] = 1,
    [142] = 1,
    [143] = 1,
    [144] = 1,
    [145] = 1,
    [146] = 1,
    [147] = 1,
    [148] = 1,
    [149] = 1,
    [150] = 1,
    [151] = 1,
    [152] = 1,
    [153] = 1,
    [154] = 1,
    [155] = 1,
    [156] = 1,
    [157] = 1,
    [158] = 1,
    [159] = 1,
    [160] = 1,
    [161] = 1,
    [162] = 1,
    [163] = 1,
    [164] = 1,
    [165] = 1,
    [166] = 1,
    [167] = 1,
    [168] = 1,
    [169] = 1,
    [170] = 1,
    [171] = 1,
    [172] = 1,
    [173] = 1,
    [174] = 1,
    [175] = 1,
    [176] = 1,
    [177] = 1,
    [178] = 1,
    [179] = 1,
    [180] = 1,
    [181] = 1,
    [182] = 1,
    [183] = 1,
    [184] = 1,
    [185] = 1,
    [186] = 1,
    [187] = 1,
    [188] = 1,
    [189] = 1,
    [190] = 1,
    [191] = 1,
    [192] = 1,
    [193] = 1,
    [194] = 1,
    [195] = 1,
    [196] = 1,
    [197] = 1,
    [198] = 1,
    [199] = 1,
    [200] = 1,
    [201] = 1,
    [202] = 1,
    [203] = 1,
    [204] = 1,
    [205] = 1,
    [206] = 1,
    [207] = 1,
    [208] = 1,
    [209] = 1,
    [210] = 1,
    [211] = 1,
    [212] = 1,
    [213] = 1,
    [214] = 1,
    [215] = 1,
    [216] = 1,
    [217] = 1,
    [218] = 1,
    [219] = 1,
    [220] = 1,
    [221] = 1,
    [222] = 1,
    [223] = 1,
    [224] = 1,
    [225] = 1,
    [226] = 1,
    [227] = 1,
    [228] = 1,
    [229] = 1,
    [230] = 1,
    [231] = 1,
    [232] = 1,
    [233] = 1,
    [234] = 1,
    [235] = 1,
    [236] = 1,
    [237] = 1,
    [238] = 1,
    [239] = 1,
    [240] = 1,
    [241] = 1,
    [242] = 1,
    [243] = 1,
    [244] = 1,
    [245] = 1,
    [246] = 1,
    [247] = 1,
    [248] = 1,
    [249] = 1,
    [250] = 1
}
cache.value_change = {}
cache.val_change_t = {
    s_table = {},
    val_on_velocity = {},
    last_values = 0,
    last_true = 0,
    repeats = {}
}

function resolver.get_changed_val(ent, ak)
    local aA = {entity.get_prop(ent, "m_vecVelocity")}
    local velocity2d = math.abs(math.sqrt(aA[1] ^ 2 + aA[2] ^ 2))
    local aB = cmath.no_decimals(velocity2d)
    local aC = cmath.no_decimals(ak.m_flWeight * 1000)
    if velocity2d < 20 then
        return
    end
    cache.value_change[ent] = cache.value_change[ent] or cache.val_change_t
    cache.value_change[ent].last_true = cache.value_change[ent].last_true or -1
    cache.value_change[ent].last_values = cache.value_change[ent].last_values or aC
    if
        cmath.between(
            cache.highest_weight[cmath.clamp(cmath.no_decimals(velocity2d), 0, 250)],
            cache.highest_switch_values[cmath.clamp(cmath.no_decimals(velocity2d), 0, 250)]
        )
     then
        cache.value_change[ent].repeats[aC] = cache.value_change[ent].repeats[aC] or 0
        if aC == cache.value_change[ent].last_values then
            cache.value_change[ent].repeats[aC] = cache.value_change[ent].repeats[aC] + 1
        elseif
            cache.value_change[ent].repeats[aC] <= 1 and
                not cmath.between(cache.value_change[ent].last_values, aC - 150, aC + 150) and
                not cmath.between(aC, cache.value_change[ent].last_true - 20, cache.value_change[ent].last_true + 20)
         then
            cache.value_change[ent].last_true = aC
            return true
        else
            cache.value_change[ent].repeats[aC] = 0
        end
    end
    cache.value_change[ent].last_values = aC
end
function resolver.anim_layer_control(ent, an)
    cache.layer_control[ent] = cache.layer_control[ent] or cache.layer_control_table
    local ak = centity.get_animlayer(ent, an)
    if cache.layer_control[ent].is_active == "" or "basic" then
        if
            cmath.between(ak.m_flWeight, 0.50, 0.60) and
                cmath.between(cache.animlayer[ent][an].m_flPrevWeight, 0.60, 0.69) or
                cache.layer_control[ent].override
         then
            if not cache.layer_control[ent].override then
                cache.layer_control[ent].override = true
                cache.layer_control[ent].is_active = "basic"
                cache.layer_control[ent]["basic"].value = cmath.no_decimals(ak.m_flWeight * 10)
            end
            if
                cmath.no_decimals(ak.m_flWeight * 10) < cache.layer_control[ent]["basic"].value - 1 or
                    cmath.no_decimals(ak.m_flWeight * 10) > cache.layer_control[ent]["basic"].value or
                    cache.layer_control[ent]["basic"].value < 1
             then
                cache.layer_control[ent].override = false
                cache.layer_control[ent]["basic"].value = 0
                cache.layer_control[ent].is_active = ""
            elseif cmath.no_decimals(ak.m_flWeight * 10) == cache.layer_control[ent]["basic"].value then
            elseif cmath.no_decimals(ak.m_flWeight * 10) == cache.layer_control[ent]["basic"].value - 1 then
                cache.layer_control[ent]["basic"].value = cache.layer_control[ent]["basic"].value - 1
            end
        end
    end
    if cache.layer_control[ent].is_active == "" or "onshot" then
        if cache.layer_control[ent]["onshot"].shot_fired then
            if
                ak.m_flWeight == 0 and ak.m_flCycle == 0 and cache.animlayer[ent][an].m_flPrevWeight == 0 and
                    cache.animlayer[ent][an].m_flPrevCycle == 0 and
                    cache.layer_control[ent].override
             then
                cache.layer_control[ent].override = false
                cache.layer_control[ent].is_active = ""
                cache.layer_control[ent]["onshot"].shot_fired = false
                mariolua.log(
                    "[Resolver] Layer control for " .. entity.get_player_name(ent) .. " 'on shot' is no longer active "
                )
            elseif not cache.layer_control[ent].override then
                cache.layer_control[ent].override = true
                cache.layer_control[ent].is_active = "onshot"
            end
        end
    end
    if cache.layer_control[ent].is_active == "" or "velocity" then
        if cache.m_flPrevSlowingDown[ent] and cmath.between(velocity2d, 20, 100) then
            cache.layer_control[ent].override = true
            cache.layer_control[ent].is_active = "velocity"
        elseif cache.layer_control[ent].override then
            cache.layer_control[ent].override = false
            cache.layer_control[ent].is_active = ""
        end
    end
end
function resolver.balance_adjust(ent)
    local ak = centity.get_animlayer(ent, 3)
    if ak.m_flWeight == 1 and cmath.between(ak.m_flCycle, 0.0, 0.7) then
        return true
    end
    return false
end
function resolver.anim_layer_switch(ent)
    if cache.layer_control[ent].override then
        return false
    end
    local ao = centity.animstate(ent)
    local ah = centity.get_animlayer(ent, 3)
    local ai = centity.get_animlayer(ent, 6)
    local ax = resolver.get_player_records(ent)
    local aD, aE = ax[1], ax[2]
    local aF = cangle.normalize_as_yaw(aD.m_flGoalFeetYaw - aE.m_flGoalFeetYaw)
    local aG = math.abs(aD.m_flEyeYaw) - math.abs(aE.m_flEyeYaw)
    local aH = math.abs(aD.m_flLowerBodyDelta) - math.abs(aE.m_flLowerBodyDelta)
    local aI = math.abs(aD.m_flCurrentFeetYaw) - math.abs(aE.m_flCurrentFeetYaw)
    if
        not ao.m_bInHitGroundAnimation and ao.m_flTimeSinceStoppedMoving > 0.2 and
            (ax[1].animlayer[6].m_flWeight == 0.003922 and ax[2].animlayer[6].m_flWeight == 0.003922 and
                ax[3].animlayer[6].m_flWeight == 0 and
                ax[4].animlayer[6].m_flWeight == 0 or
                ax[1].animlayer[6].m_flWeight == 0 and ax[2].animlayer[6].m_flWeight == 0 and
                    ax[3].animlayer[6].m_flWeight > 0.001 and
                    ax[4].animlayer[6].m_flWeight > 0.001 or
                ai.m_flWeight ~= 0 and
                    math.floor(ai.m_flPlaybackRate * 1000000) ~=
                        math.floor(ax[2].animlayer[6].m_flPlaybackRate * 1000000))
     then
        return true
    elseif
        resolver.data[ent].props["m_flVelocity2D"] < 1.01 and
            (cmath.between(ah.m_flWeight, 0.42, 0.44) and
                cmath.between(cache.animlayer[ent][3].m_flPrevWeight, 0.60, 0.66) or
                ah.m_flWeight == 0 and cmath.between(cache.animlayer[ent][3].m_flPrevWeight, 0.30, 0.33) and
                    ah.m_flCycle == 0.037 or
                resolver.get_changed_val(ent, ah) or
                ah.m_flWeight == 0 and ah.m_flCycle == 0 and
                    (cmath.between(cache.animlayer[ent][3].m_flPrevWeight, 0.043, 0.064) and
                        cmath.between(cache.animlayer[ent][3].m_flPrevCycle, 0.01, 0.05)) or
                ah.m_flWeight == 0 and ah.m_flCycle == 0 and
                    (cmath.between(cache.animlayer[ent][3].m_flPrevWeight, 0.30, 0.44) and
                        cmath.between(cache.animlayer[ent][3].m_flPrevCycle, 0.01, 0.05)))
     then
        return true
    elseif
        not ao.m_bInHitGroundAnimation and resolver.data[ent].props["m_flVelocity2D"] > 1.1 and
            (cmath.between(ai.m_flWeight, 0.42, 0.61) and
                cmath.between(cache.animlayer[ent][6].m_flPrevWeight, 0.60, 0.66) or
                resolver.get_changed_val(ent, ai) or
                ai.m_flWeight == 0 and cmath.between(cache.animlayer[ent][6].m_flPrevWeight, 0.30, 0.33) or
                cmath.between(ai.m_flWeight, 0.50, 0.60) and
                    cmath.between(cache.animlayer[ent][6].m_flPrevWeight, 0.28, 0.36) or
                ai.m_flWeight >= 0.94 and cmath.between(cache.animlayer[ent][6].m_flPrevWeight, 0.90, 0.92) or
                cache.animlayer[ent][6].m_flPrevWeight == 0 and
                    cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.54, 0.87) and
                    (cmath.between(ai.m_flWeight, 0.0037, 0.0042) and cmath.between(ai.m_flCycle, 0.43, 0.48)) or
                cmath.between(ai.m_flWeight, 0.0037, 0.0042) and ai.m_flCycle >= 0.43 and
                    (cache.animlayer[ent][6].m_flPrevWeight == 0 and
                        cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.43, 0.48)))
     then
        return true
    else
        return false
    end
end
cache.resolver_yaw_pattern_count = {}
cache.resolver_last_yaw_diff = {}
cache.resolver_pattern_yaw = {}
function resolver.get_delta_size(ent)
    if cache.layer_control[ent].override or not refs.resolver_modules:IsEnabled("Animlayer Low Delta") then
        return
    end
    local ah, aJ = centity.get_animlayer(ent, 3)
    local ai = centity.get_animlayer(ent, 6)
    local ax = resolver.get_player_records(ent)
    local aD, aE = ax[1], ax[2]
    if (aD == nil or aE == nil) then
        return
    end
    local aF = cangle.normalize_as_yaw(aD.m_flGoalFeetYaw - aE.m_flGoalFeetYaw)
    local aG = math.abs(aD.m_flEyeYaw) - math.abs(aE.m_flEyeYaw)
    local aH = math.abs(aD.m_flLowerBodyDelta) - math.abs(aE.m_flLowerBodyDelta)
    local aI = math.abs(aD.m_flCurrentFeetYaw) - math.abs(aE.m_flCurrentFeetYaw)
    cache.resolver_pattern_yaw[ent] = cache.resolver_pattern_yaw[ent] or 0
    cache.resolver_yaw_pattern_count[ent] = cache.resolver_yaw_pattern_count[ent] or 0
    cache.resolver_last_yaw_diff[ent] = cache.resolver_last_yaw_diff[ent] or 0
    if math.abs(aF) > 0 and (aD.m_flVelocity2D[1] < 1.01 and aE.m_flVelocity2D[1] < 1.01) then
        local aK = resolver.data[ent]["animlayer"]["eye_yaw"] and 1 or 3
        if math.abs(aF) == cache.resolver_last_yaw_diff[ent] then
            cache.resolver_yaw_pattern_count[ent] = cache.resolver_yaw_pattern_count[ent] + 1
        elseif cache.resolver_yaw_pattern_count[ent] >= aK then
            cache.resolver_pattern_yaw[ent] = math.abs(aF)
            cache.resolver_yaw_pattern_count[ent] = 0
        else
            cache.resolver_yaw_pattern_count[ent] = 0
        end
        cache.resolver_last_yaw_diff[ent] = math.abs(aF)
    end
    if resolver.data[ent].props["m_flVelocity2D"] <= 1.01 then
        if not resolver.data[ent]["animlayer"]["eye_yaw"] then
            if resolver.data[ent].props["highdelta"] then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 60
            elseif cmath.between(ai.m_flCycle, 0.500090, 0.500100) and resolver.data[ent].props["lowdelta"] then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 45
                mariolua.log(
                    "[Resolver] Anim layer | " ..
                        entity.get_player_name(ent) .. ", using lower than 45� fake yaw desync."
                )
            elseif
                (cmath.between(ai.m_flCycle, 0.350050, 0.350150) and
                    cmath.between(cache.animlayer[ent][6].m_flPrefCycle, 0.500050, 0.591000) or
                    cmath.between(ai.m_flCycle, 0.350050, 0.591000) and
                        cmath.between(cache.animlayer[ent][6].m_flPrefCycle, 0.350050, 0.350150) or
                    cmath.between(cache.resolver_pattern_yaw[ent], 10.78, 10.82)) and
                    resolver.data[ent].props["lowdelta"]
             then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 26
                mariolua.log(
                    "[Resolver] Anim layer | " ..
                        entity.get_player_name(ent) .. ", using lower than 26� fake yaw desync."
                )
            elseif cmath.between(cache.resolver_pattern_yaw[ent], 9.0, 9.4) and resolver.data[ent].props["lowdelta"] then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 5
                mariolua.log("[Resolver] Anim layer | " .. entity.get_player_name(ent) .. ", using 5� fake yaw desync.")
            elseif
                (math.abs(resolver.data[ent].props["m_flGoalFeetDelta"], 40, 43) or
                    math.abs(resolver.data[ent].props["m_flGoalFeetDelta"], 30, 35)) and
                    resolver.data[ent].props["lowdelta"]
             then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 45
                mariolua.log(
                    "[Resolver] Anim layer | " ..
                        entity.get_player_name(ent) .. ", using lower than 55� fake yaw desync."
                )
            end
        elseif resolver.data[ent]["animlayer"]["eye_yaw"] then
            if resolver.data[ent].props["highdelta"] then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 60
            elseif
                cmath.between(ai.m_flCycle, 0.500090, 0.500100) and
                    cmath.between(cache.resolver_pattern_yaw[ent], 103.7, 103.8) and
                    resolver.data[ent].props["lowdelta"]
             then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 45
                mariolua.log(
                    "[Resolver] Anim layer | " ..
                        entity.get_player_name(ent) .. ", using lower than 45� fake yaw desync."
                )
            elseif
                (cmath.between(ai.m_flCycle, 0.350050, 0.350150) and
                    cmath.between(cache.animlayer[ent][6].m_flPrefCycle, 0.500050, 0.591000) or
                    cmath.between(ai.m_flCycle, 0.350050, 0.591000) and
                        cmath.between(cache.animlayer[ent][6].m_flPrefCycle, 0.350050, 0.350150)) and
                    cmath.between(cache.resolver_pattern_yaw[ent], 2.700, 2.999) and
                    resolver.data[ent].props["lowdelta"]
             then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 26
                mariolua.log(
                    "[Resolver] Anim layer | " ..
                        entity.get_player_name(ent) .. ", using lower than 26� fake yaw desync."
                )
            elseif
                (math.abs(resolver.data[ent].props["m_flGoalFeetDelta"], 40, 43) or
                    math.abs(resolver.data[ent].props["m_flGoalFeetDelta"], 30, 35) or
                    cmath.between(cache.resolver_pattern_yaw[ent], 103.7, 103.8)) and
                    resolver.data[ent].props["lowdelta"]
             then
                resolver.data[ent]["animlayer"]["fake_yaw"] = 45
                mariolua.log(
                    "[Resolver] Anim layer | " ..
                        entity.get_player_name(ent) .. ", using lower than 55� fake yaw desync."
                )
            end
        end
        if
            ah.m_flWeightDeltaRate == 0 and ah.m_flWeight == 0 and
                (cmath.between(ah.m_flCycle, 0.00, 0.02) or cmath.between(ah.m_flCycle, 0.998, 1) or
                    cmath.between(ah.m_flCycle, 0.5548, 0.5558))
         then
            resolver.data[ent]["animlayer"]["eye_yaw"] = true
            resolver.data[ent]["animlayer"]["is_desync"] = true
            resolver.data[ent].antiaim["eye_yaw"] = true
            mariolua.log("[Resolver] Anim layer | " .. entity.get_player_name(ent) .. ", is using eye yaw desync")
            if
                cmath.between(ai.m_flWeight, 0.003, 0.020) and cmath.between(ai.m_flCycle, 0, 1) and
                    ai.m_flPlaybackRate * 1000 > 0.000 or
                    cmath.between(cache.resolver_pattern_yaw[ent], 109.5, 109.8)
             then
                resolver.data[ent].props["highdelta"] = true
                resolver.data[ent].props["lowdelta"] = false
                mariolua.log(
                    "[Resolver] Anim layer | " .. entity.get_player_name(ent) .. ", using 60� fake yaw desync."
                )
            elseif
                not resolver.balance_adjust(ent) and not cmath.between(ai.m_flWeight, 0.003, 0.004) and
                    ai.m_flPlaybackRate * 1000 > 0.000
             then
                resolver.data[ent].props["highdelta"] = false
                resolver.data[ent].props["lowdelta"] = true
                mariolua.log(
                    "[Resolver] Anim layer | " ..
                        entity.get_player_name(ent) .. ", using lower than 60� fake yaw desync."
                )
            else
                resolver.data[ent].props["highdelta"] = false
                resolver.data[ent].props["lowdelta"] = false
            end
        elseif resolver.data[ent]["animlayer"]["eye_yaw"] then
            mariolua.log("[Resolver] Anim layer | " .. entity.get_player_name(ent) .. ", is not using eye yaw desync")
            resolver.data[ent]["animlayer"]["eye_yaw"] = false
            resolver.data[ent].antiaim["eye_yaw"] = false
        end
        if
            (cmath.between(ah.m_flWeight, 0.042, 0.044) and cmath.between(ah.m_flCycle, 0.005, 0.020) or
                cache.animlayer[ent][3].m_flPrevCycle > 0.92 and ah.m_flCycle > 0.92 and ah.m_flWeight == 0 and
                    cmath.between(ah.m_flWeightDeltaRate, 2.68, 2.69) or
                cmath.between(cache.resolver_pattern_yaw[ent], 108.0, 108.5)) and
                not resolver.data[ent]["animlayer"]["eye_yaw"]
         then
            if not resolver.data[ent].props["lowdelta"] then
                mariolua.log(
                    "[Resolver] Anim layer | " ..
                        entity.get_player_name(ent) .. ", using less than 60� fake yaw desync."
                )
                mariolua.log("[Resolver] Anim layer | " .. entity.get_player_name(ent) .. ", extended desync detected.")
            end
            resolver.data[ent].antiaim["extended"] = true
            resolver.data[ent].props["highdelta"] = false
            resolver.data[ent].props["lowdelta"] = true
            resolver.data[ent]["animlayer"]["is_desync"] = true
        elseif
            ah.m_flWeight == 0 and ah.m_flCycle == 0 or
                (cmath.between(ai.m_flWeight, 0.0039, 0.004) or
                    cmath.between(cache.resolver_pattern_yaw[ent], 111.0, 111.6)) and
                    (cmath.between(ah.m_flWeightDeltaRate, 2.68, 2.69) and
                        not resolver.data[ent]["animlayer"]["eye_yaw"])
         then
            if not resolver.data[ent].props["highdelta"] then
                mariolua.log(
                    "[Resolver] Anim layer | " .. entity.get_player_name(ent) .. ", using 60� fake yaw desync."
                )
                mariolua.log("[Resolver] Anim layer | " .. entity.get_player_name(ent) .. ", extended desync detected.")
            end
            resolver.data[ent].antiaim["extended"] = true
            resolver.data[ent].props["highdelta"] = true
            resolver.data[ent].props["lowdelta"] = false
            resolver.data[ent]["animlayer"]["is_desync"] = true
        elseif
            ah.m_flWeight == 0 and cmath.between(ah.m_flCycle, 0, 1) and ah.m_flWeightDeltaRate == 0 and
                not resolver.data[ent]["animlayer"]["eye_yaw"]
         then
            if resolver.data[ent]["animlayer"]["is_desync"] then
                mariolua.log("[Resolver] Anim layer | " .. entity.get_player_name(ent) .. ", no desync?")
            end
            resolver.data[ent].antiaim["extended"] = false
            resolver.data[ent]["animlayer"]["is_desync"] = false
            resolver.data[ent].props["highdelta"] = false
            resolver.data[ent].props["lowdelta"] = false
        elseif
            cmath.between(ah.m_flWeight, 0.042, 0.044) and cmath.between(ah.m_flCycle, 0.012, 0.013) and
                not resolver.data[ent]["animlayer"]["eye_yaw"]
         then
            resolver.data[ent]["animlayer"]["is_desync"] = true
        end
    end
end

cache.last_delta1 = {}
cache.last_delta2 = {}
cache.last_velocity = {}
cache.last_velocity2 = {}
cache.last_delta_t = {}
cache.last_delta_t2 = {}

function resolver.animlayer_get_side(ent)
    if not refs.resolver_modules:IsEnabled("Animlayer Side") then
        resolver.info[ent]["modules"]["animlayer"]["side"] = ""
        resolver.info[ent]["modules"]["animlayer"]["angle"] = 0
        return
    end
    local ao = centity.animstate(ent)
    local ah, _ = centity.get_animlayer(ent, 3)
    local ai = centity.get_animlayer(ent, 6)
    local ax = resolver.get_player_records(ent)
    if not ax[#ax] then
        return
    end
    if (not ax[3]) then
        return
    end
    resolver.data[ent].old_body_yaw = resolver.data[ent].old_body_yaw or 0
    resolver.data[ent].old_goalfeet = resolver.data[ent].old_goalfeet or 0
    resolver.data[ent].old_lby = resolver.data[ent].old_lby or 0
    resolver.data[ent].old_eye_yaw = resolver.data[ent].old_eye_yaw or resolver.data[ent].props["m_flEyeYaw"]
    resolver.data[ent].old_cycle = resolver.data[ent].old_cycle or 0
    resolver.data[ent].old_weight = resolver.data[ent].old_weight or 0
    resolver.data[ent].old_playback_rate = resolver.data[ent].old_playback_rate or 0
    resolver.data[ent].prev_goalfeet = resolver.data[ent].prev_goalfeet or 0
    resolver.data[ent].prev_eye_yaw = resolver.data[ent].prev_eye_yaw or resolver.data[ent].props["m_flEyeYaw"]
    local aY = resolver.data[ent].old_lby > 0 and true or false
    local aZ = resolver.data[ent].props["m_flLowerBodyDelta"] > 0 and true or false
    local a_ =
        aZ and aY and
        cmath.between(
            resolver.data[ent].old_lby,
            resolver.data[ent].props["m_flLowerBodyDelta"] - 2,
            resolver.data[ent].props["m_flLowerBodyDelta"] + 2
        ) or
        (not aZ and not aY and
            cmath.between(
                math.abs(resolver.data[ent].old_lby),
                math.abs(resolver.data[ent].props["m_flLowerBodyDelta"]) - 2,
                math.abs(resolver.data[ent].props["m_flLowerBodyDelta"]) + 2
            ) or
            false)
    local b0 = resolver.data[ent].prev_goalfeet > 0 and true or false
    local b1 = resolver.data[ent].props["m_flGoalFeetDelta"] > 0 and true or false
    local b2 = ax[3].m_flGoalFeetDelta > 0 and true or false
    local b3 = ax[3].m_flGoalFeetDelta > 0 and true or false
    local b4 = resolver.data[ent].prev_goalfeet
    local b5 = resolver.data[ent].prev_eye_yaw or resolver.data[ent].props["m_flEyeYaw"]
    resolver.data[ent].prev_eye_yaw =
        resolver.data[ent].prev_eye_yaw ~= resolver.data[ent].props["m_flEyeYaw"] and
        resolver.data[ent].props["m_flEyeYaw"] or
        resolver.data[ent].prev_eye_yaw
    if resolver.data[ent].props["m_flVelocity2D"] < 1.1 then
        if
            ax[1].m_flEyeYaw == ax[3].m_flEyeYaw and ax[1].m_flEyeYaw == resolver.data[ent].props["m_flEyeYaw"] and
                (resolver.data[ent].old_goalfeet ~= resolver.data[ent].props["m_flGoalFeetDelta"] or
                    resolver.data[ent].prev_goalfeet ~= resolver.data[ent].props["m_flGoalFeetDelta"]) and
                not a_
         then
            if resolver.info[ent]["modules"]["animlayer"]["side"] ~= "left" then
                local b6 = false
                if
                    resolver.data[ent].props["m_flGoalFeetDelta"] > 0 and resolver.data[ent].prev_goalfeet > 0 or
                        resolver.data[ent].props["m_flGoalFeetDelta"] < 0 and resolver.data[ent].prev_goalfeet < 0
                 then
                    if
                        math.abs(resolver.data[ent].props["m_flGoalFeetDelta"]) >
                            math.abs(resolver.data[ent].prev_goalfeet)
                     then
                        b6 = true
                    elseif
                        math.abs(resolver.data[ent].props["m_flGoalFeetDelta"]) <
                            math.abs(resolver.data[ent].prev_goalfeet)
                     then
                        b6 = false
                    end
                else
                    b6 = false
                end
                local b7 = ""
                if
                    (cmath.between(ai.m_flCycle, 0.50001, 0.50013) or b6) and
                        (ax[1].m_flGoalFeetDelta > ax[3].m_flGoalFeetDelta and
                            (cmath.IsNumberNegative(ax[1].m_flGoalFeetDelta) and
                                cmath.IsNumberNegative(ax[3].m_flGoalFeetDelta) or
                                not cmath.IsNumberNegative(ax[1].m_flGoalFeetDelta) and
                                    not cmath.IsNumberNegative(ax[3].m_flGoalFeetDelta)) or
                            resolver.data[ent].props["m_flLowerBodyDelta"] > 0 and
                                resolver.data[ent].props["m_flGoalFeetDelta"] > 0 or
                            math.abs(resolver.data[ent].props["m_flGoalFeetDelta"]) > math.abs(ax[3].m_flGoalFeetDelta))
                 then
                    b7 = "left"
                else
                    if
                        not (ax[1].animlayer[6].m_flCycle < ax[2].animlayer[6].m_flCycle and
                            ax[2].animlayer[6].m_flCycle < ax[3].animlayer[6].m_flCycle and
                            ax[3].animlayer[6].m_flCycle < ax[4].animlayer[6].m_flCycle and
                            ax[4].animlayer[6].m_flCycle < ax[5].animlayer[6].m_flCycle and
                            ax[5].animlayer[6].m_flCycle < ax[6].animlayer[6].m_flCycle and
                            ax[6].animlayer[6].m_flCycle < ax[7].animlayer[6].m_flCycle and
                            ax[7].animlayer[6].m_flCycle < ax[8].animlayer[6].m_flCycle) and
                            (cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.5900, 0.5903) and
                                (cmath.between(ai.m_flCycle, 0.470100, 0.470220) or
                                    cmath.between(ai.m_flCycle, 0.250000, 0.250300))) or
                            cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.430100, 0.430210) and
                                cmath.between(ai.m_flCycle, 0.250000, 0.250300) or
                            cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.250000, 0.250300) and
                                cmath.between(ai.m_flCycle, 0.350000, 0.350170) or
                            cmath.between(ai.m_flCycle, 0.250000, 0.250300) or
                            cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.50001, 0.50013) and
                                not (cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.350000, 0.350170) and
                                    cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.5900, 0.5903)) and
                                (cmath.between(ai.m_flCycle, 0.430100, 0.430210) or
                                    cmath.between(ai.m_flCycle, 0.470100, 0.470220))
                     then
                        b7 = "left"
                    else
                        b7 = "right"
                    end
                    if
                        resolver.data[ent]["animlayer"]["eye_yaw"] or
                            cmath.between(ah.m_flWeight, 0.42, 0.44) and
                                cmath.between(cache.animlayer[ent][3].m_flPrevWeight, 0.60, 0.66)
                     then
                        if
                            cmath.between(cache.resolver_pattern_yaw[ent], 109, 110) or
                                cmath.between(ai.m_flPlaybackRate * 1000, 0.060, 0.065) or
                                cmath.between(ai.m_flPlaybackRate * 1000, 0.0579, 0.0590)
                         then
                            b7 = "left"
                        else
                            b7 = "right"
                        end
                    end
                end
                resolver.info[ent]["modules"]["animlayer"]["side"] =
                    b7 == "left" and "left" or resolver.info[ent]["modules"]["animlayer"]["side"]
                if b7 == "left" then
                    resolver.data[ent].old_cycle = ai.m_flCycle
                    resolver.data[ent].old_weight = ai.m_flWeight
                    resolver.data[ent].old_playback_rate = ai.m_flPlaybackRate
                    resolver.data[ent].old_body_yaw = m.GetPlayerBodyYaw(ent)
                    resolver.data[ent].old_lby = resolver.data[ent].props["m_flLowerBodyDelta"]
                    resolver.data[ent].old_eye_yaw = resolver.data[ent].props["m_flEyeYaw"]
                    resolver.data[ent].old_goalfeet = resolver.data[ent].props["m_flGoalFeetDelta"]
                    resolver.data[ent].prev_eye_yaw = resolver.data[ent].props["m_flEyeYaw"]
                    return
                end
            end
            if resolver.info[ent]["modules"]["animlayer"]["side"] ~= "right" then
                local b6 = false
                if
                    resolver.data[ent].props["m_flGoalFeetDelta"] > 0 and resolver.data[ent].prev_goalfeet > 0 or
                        resolver.data[ent].props["m_flGoalFeetDelta"] < 0 and resolver.data[ent].prev_goalfeet < 0
                 then
                    if
                        math.abs(resolver.data[ent].props["m_flGoalFeetDelta"]) <
                            math.abs(resolver.data[ent].prev_goalfeet)
                     then
                        b6 = true
                    elseif
                        math.abs(resolver.data[ent].props["m_flGoalFeetDelta"]) >
                            math.abs(resolver.data[ent].prev_goalfeet)
                     then
                        b6 = false
                    end
                else
                    b6 = false
                end
                local b7 = ""
                if
                    (cmath.between(ai.m_flCycle, 0.50001, 0.50013) or b6) and
                        (ax[1].m_flGoalFeetDelta < ax[3].m_flGoalFeetDelta and
                            (cmath.IsNumberNegative(ax[1].m_flGoalFeetDelta) and
                                cmath.IsNumberNegative(ax[3].m_flGoalFeetDelta) or
                                not cmath.IsNumberNegative(ax[1].m_flGoalFeetDelta) and
                                    not cmath.IsNumberNegative(ax[3].m_flGoalFeetDelta)) or
                            resolver.data[ent].props["m_flLowerBodyDelta"] < 0 and
                                resolver.data[ent].props["m_flGoalFeetDelta"] < 0 or
                            b6 or
                            math.abs(resolver.data[ent].props["m_flGoalFeetDelta"]) < math.abs(ax[3].m_flGoalFeetDelta))
                 then
                    b7 = "right"
                else
                    if
                        not (ax[1].animlayer[6].m_flCycle < ax[2].animlayer[6].m_flCycle and
                            ax[2].animlayer[6].m_flCycle < ax[3].animlayer[6].m_flCycle and
                            ax[3].animlayer[6].m_flCycle < ax[4].animlayer[6].m_flCycle and
                            ax[4].animlayer[6].m_flCycle < ax[5].animlayer[6].m_flCycle and
                            ax[5].animlayer[6].m_flCycle < ax[6].animlayer[6].m_flCycle and
                            ax[6].animlayer[6].m_flCycle < ax[7].animlayer[6].m_flCycle and
                            ax[7].animlayer[6].m_flCycle < ax[8].animlayer[6].m_flCycle) and
                            (not (cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.50001, 0.50013) and
                                cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.350000, 0.350170) and
                                cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.5900, 0.5903)) and
                                (cmath.between(ai.m_flCycle, 0.430100, 0.430210) or
                                    cmath.between(ai.m_flCycle, 0.470100, 0.470220))) or
                            cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.250000, 0.250300) and
                                cmath.between(ai.m_flCycle, 0.5900, 0.5903) or
                            cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.350000, 0.350170) and
                                cmath.between(ai.m_flCycle, 0.250000, 0.250300) or
                            cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.50001, 0.50013) and
                                not (cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.430100, 0.430210) and
                                    cmath.between(cache.animlayer[ent][6].m_flPrevCycle, 0.470100, 0.470220)) and
                                (cmath.between(ai.m_flCycle, 0.350000, 0.350170) or
                                    cmath.between(ai.m_flCycle, 0.5900, 0.5903))
                     then
                        b7 = "right"
                    else
                        b7 = "left"
                    end
                    if
                        resolver.data[ent]["animlayer"]["eye_yaw"] or
                            cmath.between(ah.m_flWeight, 0.42, 0.44) and
                                cmath.between(cache.animlayer[ent][3].m_flPrevWeight, 0.60, 0.66)
                     then
                        if
                            cmath.between(cache.resolver_pattern_yaw[ent], 103, 104) or
                                cmath.between(ai.m_flPlaybackRate * 1000, 0.0539, 0.0550) or
                                cmath.between(ai.m_flPlaybackRate * 1000, 0.0649, 0.0660)
                         then
                            b7 = "right"
                        else
                            b7 = "left"
                        end
                    end
                end
                resolver.info[ent]["modules"]["animlayer"]["side"] =
                    b7 == "right" and "right" or resolver.info[ent]["modules"]["animlayer"]["side"]
                if b7 == "right" then
                    resolver.data[ent].old_cycle = ai.m_flCycle
                    resolver.data[ent].old_weight = ai.m_flWeight
                    resolver.data[ent].old_playback_rate = ai.m_flPlaybackRate
                    resolver.data[ent].old_body_yaw = m.GetPlayerBodyYaw(ent)
                    resolver.data[ent].old_lby = resolver.data[ent].props["m_flLowerBodyDelta"]
                    resolver.data[ent].old_eye_yaw = resolver.data[ent].props["m_flEyeYaw"]
                    resolver.data[ent].old_goalfeet = resolver.data[ent].props["m_flGoalFeetDelta"]
                    resolver.data[ent].prev_eye_yaw = resolver.data[ent].props["m_flEyeYaw"]
                    return
                end
            end
        end
    end
end
function resolver.lby_controller(ent)
    local b8 = globals.curtime()
    local an = centity.get_animlayer(ent, 3)
    if an.m_pOwner ~= nil then
        if resolver.data[ent].props["m_flChokedPackets"] == 0 then
            resolver.data[ent].lby_can_update =
                centity.on_ground(ent) and resolver.data[ent].props["m_flVelocity2D"] <= 1.0
        end
        if not resolver.data[ent].lby_can_update then
            resolver.data[ent].lby_next_think = b8 + 0.22
        elseif resolver.balance_adjust(ent) then
            if an.m_flWeight >= 0.0 and an.m_flCycle <= 0.070000 then
                if resolver.data[ent].lby_next_think < b8 then
                    resolver.data[ent].lby_next_think = b8 + 1.1
                end
            elseif an.m_flWeight == 0 and an.m_flCycle <= 0.070000 then
                resolver.data[ent].lby_can_update = false
            end
        end
    end
end
function resolver.lowerbody(ent)
    if not refs.resolver_modules:IsEnabled("Lby") then
        resolver.info[ent]["modules"]["lby"]["side"] = ""
        resolver.info[ent]["modules"]["lby"]["angle"] = 0
        return
    end
    local b9, ba = "right", "left"
    local ak, aJ = centity.get_animlayer(ent, 3)
    if resolver.data[ent].props["m_flVelocity2D"] == 0 and aJ == 979 then
        resolver.data[ent].props["m_flLowerBodyYawSequenceAct"] = resolver.data[ent].props["m_flLowerBodyDelta"]
        if cmath.between(resolver.data[ent].props["m_flLowerBodyYawSequenceAct"], -30, 30) then
            resolver.info[ent]["modules"]["lby"]["side"] = resolver.info[ent]["modules"]["lby"]["side"] or ""
        else
            resolver.info[ent]["modules"]["lby"]["side"] =
                resolver.data[ent].props["m_flLowerBodyYawSequenceAct"] > 0 and ba or b9
        end
    end
end
function resolver.max_wep_speed(ent)
    local bb = entity.get_prop(ent, "m_hActiveWeapon")
    if bb ~= nil then
        local bc = entity.get_prop(bb, "m_iItemDefinitionIndex")
        if bc ~= nil then
            bc = bit.band(bc, 0xFFFF)
            local maxspeed = maxspeed or 250
            if i[bc] == nil then
                return
            end
            if i[bc].name == "Knife" then
                maxspeed = 250
            else
                maxspeed = i[bc].max_speed
            end
            local bd = i[bc].console_name
            local be = entity.get_prop(ent, "m_bIsScoped") == 1
            if bd == "weapon_scar20" or bd == "weapon_g3sg1" then
                if be then
                    maxspeed = maxspeed - 95
                end
            else
                if bd == "weapon_awp" then
                    if be then
                        maxspeed = maxspeed - 100
                    end
                end
            end
            maxspeed = maxspeed / 100 * 33
            return maxspeed
        end
    end
end
resolver.lp_is_slowwalking = false
function resolver.is_slow_walking(ent)
    local bf = resolver.max_wep_speed(ent) or 250
    local bg, bh = entity.get_prop(ent, "m_vecVelocity")
    local bi = math.floor(math.min(10000, math.sqrt(bg * bg + bh * bh) + 0.5))
    if math.abs(bi) >= 1 and math.abs(math.abs(bi) - bf) < 34 then
        if ent == p then
            if not resolver.lp_is_slowwalking then
                client.delay_call(
                    1,
                    function()
                        local bf = resolver.max_wep_speed(p) or 250
                        local bg, bh = entity.get_prop(p, "m_vecVelocity")
                        local bi = math.floor(math.min(10000, math.sqrt(bg * bg + bh * bh) + 0.5))
                        if math.abs(bi) > 0 and math.abs(math.abs(bi) - bf) < 34 then
                            resolver.lp_is_slowwalking = true
                        end
                    end
                )
            else
                resolver.lp_is_slowwalking = true
            end
        else
            if not resolver.data[ent].props["is_slowwalking"] then
                client.delay_call(
                    0.5,
                    function()
                        if entity.is_alive(ent) then
                            local bf = resolver.max_wep_speed(ent) or 250
                            local bg, bh = entity.get_prop(ent, "m_vecVelocity")
                            local bi = math.floor(math.min(10000, math.sqrt(bg * bg + bh * bh) + 0.5))
                            if math.abs(bi) > 0 and math.abs(math.abs(bi) - bf) < 34 then
                                if resolver.data[ent] ~= nil then
                                    resolver.data[ent].props["is_slowwalking"] = true
                                end
                            elseif resolver.data[ent] ~= nil then
                                resolver.data[ent].props["is_slowwalking"] = false
                                resolver.data[ent]["animlayer"]["switch_count"] = 0
                                resolver.data[ent]["animlayer"]["switch_state"] = false
                                resolver.data[ent]["animlayer"]["switched"] = false
                            end
                        end
                    end
                )
            else
                resolver.data[ent].props["is_slowwalking"] = true
            end
        end
    elseif resolver.data[ent].props["is_slowwalking"] and entity.is_alive(ent) then
        client.delay_call(
            1,
            function()
                if ent == nil and p == nil then
                    return
                end
                if not entity.is_alive(ent) or resolver.data[ent] == nil then
                    return
                end
                if ent == p then
                    resolver.lp_is_slowwalking = false
                elseif resolver.data[ent].props["m_flVelocity2D"] < 30 then
                    resolver.data[ent].props["is_slowwalking"] = false
                end
            end
        )
    end
end
function resolver.IsBreakingLBY(ent)
    local bj = entity.get_prop(ent, "m_flPoseParameter", 11) or 0
    return math.abs(bj - 0.5) > 0.302
end
function resolver.condition(ent)
    if not refs.resolver_modules:IsEnabled("Condition") then
        resolver.data[ent].force_zero = false
        return
    end
    local bg, bh = entity.get_prop(ent, "m_vecVelocity")
    local bi = math.floor(math.min(10000, math.sqrt(bg * bg + bh * bh) + 0.5))
    local bf = resolver.max_wep_speed(ent) or 250
    if resolver.info[ent]["low_delta"]["active"] then
        if resolver.data[ent].props["is_slowwalking"] then
            resolver.data[ent]["condition_active"] = true
            resolver.data[ent].force_zero = true
            mariolua.log("[Resolver] Condition forced " .. entity.get_player_name(ent) .. "'s yaw to 0. Slowwalking")
        elseif math.abs(bi) > 200 and not resolver.data[ent].props["highdelta"] then
            resolver.data[ent]["condition_active"] = true
            resolver.data[ent].force_zero = true
            mariolua.log("[Resolver] Condition forced " .. entity.get_player_name(ent) .. "'s yaw to 0. Velocity > 200")
        end
    elseif resolver.data[ent].force_zero then
        resolver.data[ent]["condition_active"] = false
        resolver.data[ent].force_zero = false
    end
    if refs.resolver_modules:IsEnabled("Condition") then
        local bk = cmath.clamp(cangle.GetMaxFeetYaw(ent), 0, resolver.info[ent].fake_limit)
        local bg, bh = entity.get_prop(ent, "m_vecVelocity")
        local bi = math.floor(math.min(10000, math.sqrt(bg * bg + bh * bh) + 0.5))
        local bf = resolver.max_wep_speed(ent) or 250
        if math.abs(bi) > 180 then
            resolver.data[ent]["slowwalk_limit"] = true
            resolver.data[ent]["condition_active"] = true
            resolver.data[ent]["condition_yaw_store"] = resolver.info[ent].fake_limit
            resolver.info[ent].fake_limit = bk
        elseif resolver.data[ent]["condition_active"] then
            resolver.info[ent].fake_limit = resolver.data[ent]["condition_yaw_store"]
            resolver.data[ent]["slowwalk_limit"] = false
            resolver.data[ent]["condition_active"] = false
        end
    end
end
cache.crouched_ticks = {}
cache.storedTick = 0
function resolver.fakeduck_detection(ent)
    local bl = entity.get_prop(ent, "m_flDuckAmount")
    local bm = entity.get_prop(ent, "m_flDuckSpeed")
    local bn = entity.get_prop(ent, "m_fFlags")
    local bo = refs.resolver_fakeduck_ticks:GetValue()
    if cache.crouched_ticks[ent] == nil then
        cache.crouched_ticks[ent] = 0
    end
    if bm ~= nil and bl ~= nil then
        if bm == 8 and bl <= 0.9 and bl > 0.01 and cmath.toBits(bn)[1] == 1 then
            if cache.storedTick ~= globals.tickcount() then
                cache.crouched_ticks[ent] = cache.crouched_ticks[ent] + 1
                cache.storedTick = globals.tickcount()
            end
            if cache.crouched_ticks[ent] >= bo and resolver.data[ent].props["is_fakeduck"] ~= true then
                resolver.data[ent].props["m_flLowerBodyFakeDuck"] = resolver.data[ent].props["m_flLowerBodyDelta"]
                resolver.data[ent].props["is_fakeduck"] = true
                mariolua.log('[Resolver] Player: "' .. entity.get_player_name(ent) .. '"' .. " is fake ducking")
            end
        elseif resolver.data[ent].props["is_fakeduck"] then
            resolver.data[ent].props["is_fakeduck"] = false
            cache.crouched_ticks[ent] = 0
            mariolua.log('[Resolver] Player: "' .. entity.get_player_name(ent) .. '"' .. " stopped fake ducking")
        end
    end
end
function resolver.ResolverToggle()
    if not refs.resolver_toggle:IsKeyToggle() and refs.resolver_toggle:IsKeyDown() then
        ui.set(cache.ref.rage.resolver_ref, false)
        resolver.force_off = true
        resolver.OffResolverAll()
    elseif refs.resolver_toggle:IsKeyReleased() and refs.resolver_toggle:IsKeyDown() then
        ui.set(cache.ref.rage.resolver_ref, true)
        resolver.force_off = false
    end
end
function resolver.onshot_shutdown()
    if not refs.resolver_modules:IsEnabled("On Shot") then
        return
    end
    local F = entity.get_players(true)
    if not F then
        return
    end
    for y = 1, #F do
        local ent = F[y]
        m.SetForceBodyYawCheckbox(ent, false)
        m.SetPlayerBodyYaw(ent, 0)
    end
end
function resolver.get_side(ent)
    local bp = Vector3(client.eye_position())
    local bq = 1
    local br = false
    local bs = bp:angle_to(vector.eye_position(ent))[2]
    local bt, bu = 0, 0
    local bv, bw = 0, 0
    for y = -90, 90, 30 do
        if y == 0 then
            return
        end
        local bx, by, bz = vector.angle_vector(0, bs + y, 0)
        bx, by, bz = vector.multiplyvalues(bx, by, bz, 60)
        local bA, bB = renderer.world_to_screen(bp.x, bp.y, bp.z)
        local bC, bD = renderer.world_to_screen(bx, by, bz)
        renderer.line(bA, bB, bC, bD, 15, 150, 90, 170)
        local bE = client.trace_line(ent, bp.x, bp.y, bp.z, bp.x + bx, bp.y + by, bp.z + bz)
        if y > 0 then
            bt = bt + bE
            bv = bv + 1
        else
            bu = bu + bE
            bw = bw + 1
        end
    end
    local bF, bG = bt / bv, bu / bw
    if bF < bG then
        return 1
    end
    return 2
end
cache.resolver = {dmg_pos = {}, get_dmg_pos = {}}
function resolver.render_best_damage(ent)
    if
        not refs.resolver_modules:IsEnabled("Damage") or not mariolua.debug.draw["dmg_trace"] or
            cache.resolver.dmg_pos == nil
     then
        return
    end
    resolver.get_side(ent)
    cache.resolver.dmg_pos["l"] = cache.resolver.dmg_pos["l"] or {0, 0, 0}
    cache.resolver.dmg_pos["r"] = cache.resolver.dmg_pos["r"] or {0, 0, 0}
    cache.resolver.dmg_pos["dmg_l"] = cache.resolver.dmg_pos["dmg_l"] or 0
    cache.resolver.dmg_pos["dmg_r"] = cache.resolver.dmg_pos["dmg_r"] or 0
    local bH, bI, bJ = cache.resolver.dmg_pos["l"][1], cache.resolver.dmg_pos["l"][2], cache.resolver.dmg_pos["l"][3]
    local bK, bL, bM = cache.resolver.dmg_pos["r"][1], cache.resolver.dmg_pos["r"][2], cache.resolver.dmg_pos["r"][3]
    bH = bH or 0
    bI = bI or 0
    bJ = bJ or 0
    bK = bK or 0
    bL = bL or 0
    bM = bM or 0
    local bN, bO, bP = {}, {}, {}
    bN[0], bO[0], bP[0] = entity.hitbox_position(ent, 0)
    bN[0], bO[0], bP[0] =
        centity.ExtrapolatePosition(bN[0], bO[0], bP[0], refs.resolver_predict_local_ticks:GetValue(), ent)
    bN[1], bO[1], bP[1] = bN[0] + 40, bO[0], bP[0]
    bN[2], bO[2], bP[2] = bN[0], bO[0] + 40, bP[0]
    bN[3], bO[3], bP[3] = bN[0] - 40, bO[0], bP[0]
    bN[4], bO[4], bP[4] = bN[0], bO[0] - 40, bP[0]
    bN[5], bO[5], bP[5] = bN[0], bO[0], bP[0] + 40
    bN[6], bO[6], bP[6] = bN[0], bO[0], bP[0] - 40
    for bQ = 0, 6 do
        bN[bQ] = bN[bQ] or 0
        bO[bQ] = bO[bQ] or 0
        bP[bQ] = bP[bQ] or 0
        local bA, bB = renderer.world_to_screen(bN[bQ], bO[bQ], bP[bQ])
        local bR, bS = renderer.world_to_screen(bH, bI, bJ)
        local bT, bU = renderer.world_to_screen(bK, bL, bM)
        bT = bT or 0
        bR = bR or 0
        bU = bU or 0
        bS = bS or 0
        renderer.line(bA, bB, bR, bS, 220, 15, 90, 150)
        renderer.line(bA, bB, bT, bU, 180, 150, 90, 150)
        renderer.text(bT, bU, 255, 100, 0, 255, "c", 0, "dmg r: " .. cache.resolver.dmg_pos["dmg_r"])
        renderer.text(bR, bS, 255, 100, 0, 255, "c", 0, "dmg l: " .. cache.resolver.dmg_pos["dmg_l"])
    end
end
cache.resolver = {dmg_pos2 = {}, get_dmg_pos = {}}
function resolver.render_best_damage2(ent)
    if
        not refs.resolver_modules:IsEnabled("Damage2") or not mariolua.debug.draw["dmg_trace"] or
            cache.resolver.dmg_pos2 == nil
     then
        return
    end
    resolver.get_side(ent)
    cache.resolver.dmg_pos2["l"] = cache.resolver.dmg_pos2["l"] or {0, 0, 0}
    cache.resolver.dmg_pos2["r"] = cache.resolver.dmg_pos2["r"] or {0, 0, 0}
    cache.resolver.dmg_pos2["dmg_l"] = cache.resolver.dmg_pos2["dmg_l"] or 0
    cache.resolver.dmg_pos2["dmg_r"] = cache.resolver.dmg_pos2["dmg_r"] or 0
    local bH, bI, bJ = cache.resolver.dmg_pos2["l"][1], cache.resolver.dmg_pos2["l"][2], cache.resolver.dmg_pos2["l"][3]
    local bK, bL, bM = cache.resolver.dmg_pos2["r"][1], cache.resolver.dmg_pos2["r"][2], cache.resolver.dmg_pos2["r"][3]
    bH = bH or 0
    bI = bI or 0
    bJ = bJ or 0
    bK = bK or 0
    bL = bL or 0
    bM = bM or 0
    if refs.resolver_predict_local_ticks == nil then
        return
    end
    local bN, bO, bP = {}, {}, {}
    bN[0], bO[0], bP[0] = entity.hitbox_position(ent, 0)
    bN[0], bO[0], bP[0] =
        centity.ExtrapolatePosition(bN[0], bO[0], bP[0], refs.resolver_predict_local_ticks:GetValue(), ent)
    bN[1], bO[1], bP[1] = bN[0] + 40, bO[0], bP[0]
    bN[2], bO[2], bP[2] = bN[0], bO[0] + 40, bP[0]
    bN[3], bO[3], bP[3] = bN[0] - 40, bO[0], bP[0]
    bN[4], bO[4], bP[4] = bN[0], bO[0] - 40, bP[0]
    bN[5], bO[5], bP[5] = bN[0], bO[0], bP[0] + 40
    bN[6], bO[6], bP[6] = bN[0], bO[0], bP[0] - 40
    for bQ = 0, 6 do
        bN[bQ] = bN[bQ] or 0
        bO[bQ] = bO[bQ] or 0
        bP[bQ] = bP[bQ] or 0
        local bA, bB = renderer.world_to_screen(bN[bQ], bO[bQ], bP[bQ])
        local bR, bS = renderer.world_to_screen(bH, bI, bJ)
        local bT, bU = renderer.world_to_screen(bK, bL, bM)
        bT = bT or 0
        bR = bR or 0
        bU = bU or 0
        bS = bS or 0
        renderer.line(bA, bB, bR, bS, 220, 15, 90, 150)
        renderer.line(bA, bB, bT, bU, 180, 150, 90, 150)
        renderer.text(bT, bU + 15, 255, 100, 0, 255, "c", 0, "dmg2 r: " .. cache.resolver.dmg_pos2["dmg_r"])
        renderer.text(bR, bS + 15, 255, 100, 0, 255, "c", 0, "dmg2 l: " .. cache.resolver.dmg_pos2["dmg_l"])
    end
end
cache.resolver = {frac_pos = {}, get_frac_pos = {}}
function resolver.render_best_fraction(ent)
    if
        not refs.resolver_modules:IsEnabled("Fraction") or not mariolua.debug.draw["dmg_trace"] or
            cache.resolver.frac_pos == nil
     then
        return
    end
    resolver.get_side(ent)
    cache.resolver.frac_pos["l"] = cache.resolver.frac_pos["l"] or {0, 0, 0}
    cache.resolver.frac_pos["r"] = cache.resolver.frac_pos["r"] or {0, 0, 0}
    cache.resolver.frac_pos["frac_l"] = cache.resolver.frac_pos["frac_l"] or 0
    cache.resolver.frac_pos["frac_r"] = cache.resolver.frac_pos["frac_r"] or 0
    local bH, bI, bJ = cache.resolver.frac_pos["l"][1], cache.resolver.frac_pos["l"][2], cache.resolver.frac_pos["l"][3]
    local bK, bL, bM = cache.resolver.frac_pos["r"][1], cache.resolver.frac_pos["r"][2], cache.resolver.frac_pos["r"][3]
    bH = bH or 0
    bI = bI or 0
    bJ = bJ or 0
    bK = bK or 0
    bL = bL or 0
    bM = bM or 0
    local bN, bO, bP = {}, {}, {}
    bN[0], bO[0], bP[0] = entity.hitbox_position(ent, 0)
    bN[1], bO[1], bP[1] = bN[0] + 40, bO[0], bP[0]
    bN[2], bO[2], bP[2] = bN[0], bO[0] + 40, bP[0]
    bN[3], bO[3], bP[3] = bN[0] - 40, bO[0], bP[0]
    bN[4], bO[4], bP[4] = bN[0], bO[0] - 40, bP[0]
    bN[5], bO[5], bP[5] = bN[0], bO[0], bP[0] + 40
    bN[6], bO[6], bP[6] = bN[0], bO[0], bP[0] - 40
    for bQ = 0, 6 do
        bN[bQ] = bN[bQ] or 0
        bO[bQ] = bO[bQ] or 0
        bP[bQ] = bP[bQ] or 0
        local bA, bB = renderer.world_to_screen(bN[bQ], bO[bQ], bP[bQ])
        local bR, bS = renderer.world_to_screen(bH, bI, bJ)
        local bT, bU = renderer.world_to_screen(bK, bL, bM)
        bT = bT or 0
        bR = bR or 0
        bU = bU or 0
        bS = bS or 0
        renderer.line(bA, bB, bR, bS, 90, 15, 150, 150)
        renderer.line(bA, bB, bT, bU, 15, 90, 90, 150)
        renderer.text(bT, bU + 30, 255, 100, 0, 255, "c", 0, "frac r: " .. cache.resolver.frac_pos["frac_r"])
        renderer.text(bR, bS + 30, 255, 100, 0, 255, "c", 0, "frac l: " .. cache.resolver.frac_pos["frac_l"])
    end
end
cache.resolver = {frac_pos2 = {}, get_frac_pos = {}}
function resolver.render_best_fraction2(ent)
    if
        not refs.resolver_modules:IsEnabled("Fraction2") or not mariolua.debug.draw["dmg_trace"] or
            cache.resolver.frac_pos2 == nil
     then
        return
    end
    resolver.get_side(ent)
    cache.resolver.frac_pos2["l"] = cache.resolver.frac_pos2["l"] or {0, 0, 0}
    cache.resolver.frac_pos2["r"] = cache.resolver.frac_pos2["r"] or {0, 0, 0}
    cache.resolver.frac_pos2["frac_l"] = cache.resolver.frac_pos2["frac_l"] or 0
    cache.resolver.frac_pos2["frac_r"] = cache.resolver.frac_pos2["frac_r"] or 0
    local bH, bI, bJ =
        cache.resolver.frac_pos2["l"][1],
        cache.resolver.frac_pos2["l"][2],
        cache.resolver.frac_pos2["l"][3]
    local bK, bL, bM =
        cache.resolver.frac_pos2["r"][1],
        cache.resolver.frac_pos2["r"][2],
        cache.resolver.frac_pos2["r"][3]
    bH = bH or 0
    bI = bI or 0
    bJ = bJ or 0
    bK = bK or 0
    bL = bL or 0
    bM = bM or 0
    local bN, bO, bP = {}, {}, {}
    bN[0], bO[0], bP[0] = entity.hitbox_position(ent, 0)
    bN[1], bO[1], bP[1] = bN[0] + 40, bO[0], bP[0]
    bN[2], bO[2], bP[2] = bN[0], bO[0] + 40, bP[0]
    bN[3], bO[3], bP[3] = bN[0] - 40, bO[0], bP[0]
    bN[4], bO[4], bP[4] = bN[0], bO[0] - 40, bP[0]
    bN[5], bO[5], bP[5] = bN[0], bO[0], bP[0] + 40
    bN[6], bO[6], bP[6] = bN[0], bO[0], bP[0] - 40
    for bQ = 0, 6 do
        bN[bQ] = bN[bQ] or 0
        bO[bQ] = bO[bQ] or 0
        bP[bQ] = bP[bQ] or 0
        local bA, bB = renderer.world_to_screen(bN[bQ], bO[bQ], bP[bQ])
        local bR, bS = renderer.world_to_screen(bH, bI, bJ)
        local bT, bU = renderer.world_to_screen(bK, bL, bM)
        bT = bT or 0
        bR = bR or 0
        bU = bU or 0
        bS = bS or 0
        renderer.line(bA, bB, bR, bS, 90, 15, 150, 150)
        renderer.line(bA, bB, bT, bU, 15, 90, 90, 150)
        renderer.text(bT, bU + 45, 255, 100, 0, 255, "c", 0, "frac2 r: " .. cache.resolver.frac_pos2["frac_r"])
        renderer.text(bR, bS + 45, 255, 100, 0, 255, "c", 0, "frac2 l: " .. cache.resolver.frac_pos2["frac_l"])
    end
end
cache.stored_bullet_trace = {}
cache.stored_line_trace = {}
function resolver.render_test()
    if cache.stored_bullet_trace == nil then
        return
    end
    for y, a7 in pairs(cache.stored_bullet_trace) do
        if a7 == nil then
            return
        end
        local bV = a7[1]
        local bW = a7[2]
        local bX, bY = renderer.world_to_screen(bV.x, bV.y, bV.z)
        local bZ, b_ = renderer.world_to_screen(bW.x, bW.y, bW.z)
        if not bX then
            return
        end
        renderer.rectangle(bZ, b_, 15, 15, 255, 255, 255, 250)
        renderer.line(bX, bY, bZ, b_, 255, 0, 0, 250)
    end
end
function resolver.render_test2(ent)
    if cache.stored_line_trace[ent] == nil then
        return
    end
    for y = 1, #cache.stored_line_trace[ent] do
        if cache.stored_line_trace[ent][y] == nil then
            return
        end
        local bV = cache.stored_line_trace[ent][y][1]
        local bW = cache.stored_line_trace[ent][y][2]
        local bX, bY = renderer.world_to_screen(bV.x, bV.y, bV.z)
        local bZ, b_ = renderer.world_to_screen(bW.x, bW.y, bW.z)
        if not (bZ or bZ) then
            return
        end
        local w, c0, s, aM = unpack(cache.stored_line_trace[ent][0][y])
        local a5 = {"right", "left", "right 2", "left 2", "mid"}
        renderer.rectangle(bZ - 5, b_ - 5, 10, 10, 255, 255, 255, 100)
        renderer.line(bX, bY, bZ, b_, w, c0, s, aM)
    end
end
function resolver.debug_drawings()
    if not mariolua.debug.draw["dmg_trace"] then
        return
    end
    local F = entity.get_players(true)
    for y = 1, #F do
        local ent = F[y]
        if not entity.is_alive(ent) then
            return
        end
        resolver.render_best_damage(ent)
        resolver.render_best_damage2(ent)
        resolver.render_best_fraction(ent)
        resolver.render_test2(ent)
    end
end
function resolver.set_position_body_yaw(ent, D)
    local c1 = centity.get_side(ent)
    local b9, ba = "right", "left"
    if c1 == "BACKWARDS" then
        b9, ba = "left", "right"
    elseif c1 == "BACKWARDS_LEFT" then
        b9, ba = "left", "right"
    elseif c1 == "BACKWARDS_RIGHT" then
        b9, ba = "right", "left"
    elseif c1 == "LEFT" or "FORWARD_LEFT" then
        b9, ba = "right", "left"
    elseif c1 == "RIGHT" or "FORWARD_RIGHT" then
        b9, ba = "left", "right"
    else
        b9, ba = "right", "left"
    end
    if D < 0 then
        resolver.info[ent]["modules"]["position"]["side"] = ba
    elseif D > 0 then
        resolver.info[ent]["modules"]["position"]["side"] = b9
    else
        resolver.info[ent]["modules"]["position"]["side"] = resolver.info[ent]["modules"]["position"]["side"] or ""
    end
end
function resolver.get_weakest_hitbox(ent)
    local c2, c3, c4 = client.eye_position()
    local v, as, c5 = entity.get_origin(p)
    local c6 = 0
    local c7 = nil
    for y = 0, 4 do
        local c8, c9, ca = entity.hitbox_position(ent, y)
        local bE, cb = client.trace_line(ent, v, as, c4, c8, c9, ca)
        if bE < 1 then
            local cc, cd = client.trace_bullet(p, v, as, c4, c8, c9, ca)
            if cc ~= nil then
                ent_exists = true
            end
            if c6 < cd then
                c6 = cd
                c7 = y
            end
        end
    end
    return c7
end
cache.stored_weakest_hitbox = {}
function resolver.logic_position(ent)
    if not refs.resolver_modules:IsEnabled("Position") then
        return
    end
    cache.stored_weakest_hitbox[ent] = cache.stored_weakest_hitbox[ent] or 3
    cache.stored_weakest_hitbox[ent] = resolver.get_weakest_hitbox(ent) or cache.stored_weakest_hitbox[ent]
    local c7 = Vector3(entity.hitbox_position(ent, cache.stored_weakest_hitbox[ent]))
    if not refs.resolver_predict_ticks then
        return
    end
    local ce = refs.resolver_predict_ticks:GetValue() or 1
    local cf = Vector3(func.ExtrapolatePosition(c7.x, c7.y, c7.z, ce, ent))
    if centity.is_peeking_other(ent, ce, p) and ce >= 1 then
        c7 = cf
    end
    local cg = Vector3(c7.x, c7.y, c7.z)
    local ch = Vector3(entity.get_origin(p))
    local ci = ch.z + 36.7
    local cj = cangle.calculate_angle(ch.x, ch.y, cg.x, cg.y)
    local ck = Vector3(vector.angle_vector(0, cj - 90))
    local cl = cg.x + ck.x * 55
    local cm = cg.y + ck.y * 55
    local co = Vector3(vector.angle_vector(0, cj - 90))
    local cp = cg.x + co.x * 25
    local cq = cg.y + co.y * 25
    local cs = Vector3(vector.angle_vector(0, cj + 90))
    local ct = cg.x + cs.x * 55
    local cu = cg.y + cs.y * 55
    local cw = Vector3(vector.angle_vector(0, cj + 90))
    local cx = cg.x + cw.x * 25
    local cy = cg.y + cw.y * 25
    local cB = resolver.info[ent].fake_limit
    main_lean = main_lean or 0
    local cC = {
        [1] = client.trace_line(p, ch.x, ch.y, ci, cl, cm, cg.z),
        [2] = client.trace_line(p, ch.x, ch.y, ci, ct, cu, cg.z),
        [3] = client.trace_line(p, ch.x, ch.y, ci, cp, cq, cg.z),
        [4] = client.trace_line(p, ch.x, ch.y, ci, cx, cy, cg.z),
        [5] = client.trace_line(p, ch.x, ch.y, ci, cg.z, cg.y, cg.z)
    }
    cache.stored_line_trace[ent] = cache.stored_line_trace[ent] or {}
    cache.stored_line_trace[ent][1] = {Vector3(ch.x, ch.y, ci), Vector3(cl, cm, cg.z)}
    cache.stored_line_trace[ent][2] = {Vector3(ch.x, ch.y, ci), Vector3(ct, cu, cg.z)}
    cache.stored_line_trace[ent][3] = {Vector3(ch.x, ch.y, ci), Vector3(cp, cq, cg.z)}
    cache.stored_line_trace[ent][4] = {Vector3(ch.x, ch.y, ci), Vector3(cx, cy, cg.z)}
    cache.stored_line_trace[ent][5] = {Vector3(ch.x, ch.y, ci), Vector3(cg.x, cg.y, cg.z)}
    cache.stored_line_trace[ent][0] = {
        [2] = {0, 255, 0, 255},
        [4] = {0, 200, 0, 255},
        [1] = {255, 0, 0, 255},
        [3] = {200, 0, 0, 255},
        [5] = {255, 255, 255, 255}
    }
    local cD = {0, 0, 255, 255}
    if cC[1] > cC[2] and cC[5] < 0.98 then
        cache.stored_line_trace[ent][0][4] = cD
        cache.stored_line_trace[ent][0][2] = cD
        main_lean = -cB
        ref_yaw_val = main_lean
        resolver.data[ent]["modules"]["position"]["angle"] = main_lean
        resolver.set_position_body_yaw(ent, main_lean)
        return
    end
    if cC[1] < cC[2] and cC[5] < 0.98 then
        cache.stored_line_trace[ent][0][3] = cD
        cache.stored_line_trace[ent][0][1] = cD
        main_lean = cB
        ref_yaw_val = main_lean
        resolver.data[ent]["modules"]["position"]["angle"] = main_lean
        resolver.set_position_body_yaw(ent, main_lean)
        return
    end
    if cC[4] > cC[3] and cC[5] < 0.98 then
        cache.stored_line_trace[ent][0][3] = cD
        cache.stored_line_trace[ent][0][1] = cD
        main_lean = cB
        ref_yaw_val = main_lean
        resolver.data[ent]["modules"]["position"]["angle"] = main_lean
        resolver.set_position_body_yaw(ent, main_lean)
        return
    end
    if cC[1] > 0.98 and cC[5] < 0.98 then
        cA = -cB / 2
        main_lean = -cB / 2
        ref_yaw_val = main_lean
        resolver.data[ent]["modules"]["position"]["angle"] = main_lean
        return
    end
    if cC[4] > 0.98 and cC[5] < 0.98 then
        cA = cB / 2
        main_lean = cB / 2
        ref_yaw_val = main_lean
        resolver.data[ent]["modules"]["position"]["angle"] = main_lean
        resolver.set_position_body_yaw(ent, main_lean)
        return
    end
    if cC[5] > 0.98 then
        main_lean = 0
        ref_yaw_val = main_lean
        resolver.data[ent]["modules"]["position"]["angle"] = main_lean
        resolver.set_position_body_yaw(ent, main_lean)
        return
    end
end
function resolver.logic_simple(ent)
    if not refs.resolver_modules:IsEnabled("Simple") then
        resolver.info[ent]["modules"]["simple"]["side"] = ""
        resolver.info[ent]["modules"]["simple"]["angle"] = 0
        return
    end
    local b9, ba = "left", "right"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "right", "left"
    end
    local bp = Vector3(entity.get_origin(p))
    local cE = vector.eye_position(ent)
    local cF = cE:angle_to(bp)[2]
    local cG = {left = 0, right = 0}
    for y = cF - 90, cF + 90, 25 do
        if y ~= cF then
            local cH = math.rad(y)
            local cI = Vector3(cE.x + 50 * math.cos(cH), cE.y + 50 * math.sin(cH), cE.z)
            local cJ = Vector3(cE.x + 256 * math.cos(cH), cE.y + 256 * math.sin(cH), cE.z)
            local bE, cK, cL = cI:trace_line_impact(cJ, p)
            local a5 = y < cF and "left" or "right"
            cG[a5] = cG[a5] + bE
        end
    end
    if cG.left == cG.right then
        resolver.info[ent]["modules"]["simple"]["side"] = resolver.info[ent]["modules"]["simple"]["side"] or ""
    else
        resolver.info[ent]["modules"]["simple"]["side"] = cG.left < cG.right and ba or b9
    end
end
function resolver.logic_damage(ent)
    if not refs.resolver_modules:IsEnabled("Damage") then
        resolver.info[ent]["modules"]["damage"]["side"] = ""
        resolver.info[ent]["modules"]["damage"]["angle"] = 0
        return
    end
    local b9, ba = "right", "left"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "left", "right"
    end
    local cM = resolver.get_weakest_hitbox(ent)
    local cN = centity.get_origin_extrapolated(p, refs.resolver_predict_local_ticks:GetValue())
    local cO = Vector3(entity.hitbox_position(ent, cM or 0))
    local c2, c3, c4 = client.eye_position()
    local ce = refs.resolver_predict_enemy_ticks:GetValue()
    if centity.is_peeking_other(ent, ce, p) and ce >= 1 then
        cO = Vector3(centity.ExtrapolatePosition(cO.x, cO.y, cO.z, ce, ent))
    end
    local cP = cangle.calculate_angle(cN.x, cN.y, cO.x, cO.y)
    local ck = Vector3(vector.angle_vector(0, cP + 90))
    local cl = cN.x + ck.x * 15
    local cm = cN.y + ck.y * 15
    local cn = cN.z + 30
    local cs = Vector3(vector.angle_vector(0, cP - 90))
    local ct = cN.x + cs.x * 15
    local cu = cN.y + cs.y * 15
    local cv = cN.z + 30
    local cQ = Vector3(vector.angle_vector(0, cP + 90))
    local cR = cN.x + cQ.x * 100
    local cS = cN.y + cQ.y * 100
    local cT = cN.z + 60
    local cU = Vector3(vector.angle_vector(0, cP - 90))
    local cV = cN.x + cU.x * 100
    local cW = cN.y + cU.y * 100
    local cX = cN.z + 60
    local cY, cZ = client.trace_line(ent, cN.x, cN.y, c4, cR, cS, cT)
    local c_, cZ = client.trace_line(ent, cN.x, cN.y, c4, cV, cW, cX)
    if cY < 0.9 or c_ < 0.9 then
        resolver.info[ent]["modules"]["damage"]["damage_left"] = centity.best_damage(ent, cR, cS, cT)
        resolver.info[ent]["modules"]["damage"]["damage_right"] = centity.best_damage(ent, cV, cW, cX)
        cache.resolver.dmg_pos = {
            ["l"] = {ct, cu, cv},
            ["r"] = {cl, cm, cn},
            ["l2"] = {cV, cW, cX},
            ["r2"] = {cR, cS, cT},
            ["dmg_l"] = resolver.info[ent]["modules"]["damage"]["damage_left"],
            ["dmg_r"] = resolver.info[ent]["modules"]["damage"]["damage_right"]
        }
        if
            resolver.info[ent]["modules"]["damage"]["damage_left"] ==
                resolver.info[ent]["modules"]["damage"]["damage_right"]
         then
            resolver.info[ent]["modules"]["damage"]["side"] = resolver.info[ent]["modules"]["damage"]["side"] or ""
        else
            resolver.info[ent]["modules"]["damage"]["side"] =
                resolver.info[ent]["modules"]["damage"]["damage_right"] >
                resolver.info[ent]["modules"]["damage"]["damage_left"] and
                b9 or
                ba
        end
    end
end
function resolver.logic_damage2(ent)
    if not refs.resolver_modules:IsEnabled("Damage2") then
        resolver.info[ent]["modules"]["damage2"]["side"] = ""
        resolver.info[ent]["modules"]["damage2"]["angle"] = 0
        return
    end
    local b9, ba = "left", "right"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "right", "left"
    end
    local cM = resolver.get_weakest_hitbox(ent)
    local d0 = Vector3(entity.hitbox_position(ent, cM or 0))
    local ce = refs.resolver_predict_enemy_ticks:GetValue()
    if centity.is_peeking_other(ent, ce, p) and ce >= 1 then
        d0 = Vector3(centity.ExtrapolatePosition(d0.x, d0.y, d0.z, ce, ent))
    end
    local d1 = centity.get_origin_extrapolated(p, refs.resolver_predict_local_ticks:GetValue())
    local c2, c3, c4 = client.eye_position()
    local cP = cangle.calculate_angle(d1.x, d1.y, d0.x, d0.y)
    local cs = Vector3(vector.angle_vector(0, cP - 90, 0))
    local ck = Vector3(vector.angle_vector(0, cP + 90, 0))
    local d2 = d0.x + cs.x * 90
    local d3 = d0.y + cs.y * 90
    local d4 = d0.x + ck.x * 90
    local d5 = d0.y + ck.y * 90
    local cY, d6 = client.trace_line(ent, d1.x, d1.y, c4, d4, d5, d0.z)
    local c_, d6 = client.trace_line(ent, d1.x, d1.y, c4, d2, d3, d0.z)
    if cY < 1 or c_ < 1 then
        if d0.y ~= nil then
            leftent, resolver.info[ent]["modules"]["damage2"]["damage_left"] =
                client.trace_bullet(p, d1.x, d1.y, c4, d2, d3, d0.z)
            rightent, resolver.info[ent]["modules"]["damage2"]["damage_right"] =
                client.trace_bullet(p, d1.x, d1.y, c4, d4, d5, d0.z)
            cache.resolver.dmg_pos2 = {
                ["l"] = {d2, d3, d0.z},
                ["r"] = {d4, d5, d0.z},
                ["dmg_l"] = resolver.info[ent]["modules"]["damage2"]["damage_left"],
                ["dmg_r"] = resolver.info[ent]["modules"]["damage2"]["damage_right"]
            }
            if
                resolver.info[ent]["modules"]["damage2"]["damage_left"] ==
                    resolver.info[ent]["modules"]["damage2"]["damage_right"]
             then
                resolver.info[ent]["modules"]["damage2"]["side"] =
                    resolver.info[ent]["modules"]["damage2"]["side"] or ""
            else
                resolver.info[ent]["modules"]["damage2"]["side"] =
                    resolver.info[ent]["modules"]["damage2"]["damage_right"] >
                    resolver.info[ent]["modules"]["damage2"]["damage_left"] and
                    b9 or
                    ba
            end
        end
    end
end
function resolver.logic_fraction(ent)
    if not refs.resolver_modules:IsEnabled("Fraction") then
        resolver.info[ent]["modules"]["fraction"]["side"] = ""
        resolver.info[ent]["modules"]["fraction"]["angle"] = 0
        return
    end
    local b9, ba = "left", "right"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "right", "left"
    end
    local cN = centity.get_origin_extrapolated(p, refs.resolver_predict_local_ticks:GetValue())
    local cO = vector.eye_position(ent)
    local ce = refs.resolver_predict_enemy_ticks:GetValue()
    if centity.is_peeking_other(ent, ce, p) and ce >= 1 then
        cO = Vector3(centity.ExtrapolatePosition(cO.x, cO.y, cO.z, ce, ent))
    end
    local cP = cangle.calculate_angle(cN.x, cN.y, cO.x, cO.y)
    local ck = Vector3(vector.angle_vector(0, cP + 90))
    local cl = cN.x + ck.x * 15
    local cm = cN.y + ck.y * 15
    local cn = cN.z + 40
    local cs = Vector3(vector.angle_vector(0, cP - 90))
    local ct = cN.x + cs.x * 15
    local cu = cN.y + cs.y * 15
    local cv = cN.z + 40
    local cQ = Vector3(vector.angle_vector(0, cP + 90))
    local cR = cN.x + cQ.x * 100
    local cS = cN.y + cQ.y * 100
    local cT = cN.z + 60
    local cU = Vector3(vector.angle_vector(0, cP - 90))
    local cV = cN.x + cU.x * 100
    local cW = cN.y + cU.y * 100
    local cX = cN.z + 60
    resolver.info[ent]["modules"]["fraction"]["fraction_left"] =
        centity.best_fraction(ent, cl, cm, cn, refs.resolver_predict_enemy_ticks:GetValue())
    resolver.info[ent]["modules"]["fraction"]["fraction_right"] =
        centity.best_fraction(ent, ct, cu, cv, refs.resolver_predict_enemy_ticks:GetValue())
    resolver.info[ent]["modules"]["fraction"]["fraction_left2"] =
        centity.best_fraction(ent, cR, cS, cT, refs.resolver_predict_enemy_ticks:GetValue())
    resolver.info[ent]["modules"]["fraction"]["fraction_right2"] =
        centity.best_fraction(ent, cV, cW, cX, refs.resolver_predict_enemy_ticks:GetValue())
    cache.resolver.frac_pos = {
        ["l"] = {ct, cu, cv},
        ["r"] = {cl, cm, cn},
        ["l2"] = {cV, cW, cX},
        ["r2"] = {cR, cS, cT},
        ["frac_l"] = resolver.info[ent]["modules"]["fraction"]["fraction_left"],
        ["frac_r"] = resolver.info[ent]["modules"]["fraction"]["fraction_right"]
    }
    if
        resolver.info[ent]["modules"]["fraction"]["fraction_left"] >= 0.98 and
            resolver.info[ent]["modules"]["fraction"]["fraction_right"] >= 0.98 and
            (resolver.info[ent]["modules"]["fraction"]["fraction_left2"] >= 0.98 and
                resolver.info[ent]["modules"]["fraction"]["fraction_right2"] >= 0.98)
     then
        resolver.info[ent]["modules"]["fraction"]["side"] = resolver.info[ent]["modules"]["fraction"]["side"] or ""
    elseif
        resolver.info[ent]["modules"]["fraction"]["fraction_left"] ==
            resolver.info[ent]["modules"]["fraction"]["fraction_right"] or
            resolver.info[ent]["modules"]["fraction"]["fraction_left"] < 0.49 and
                resolver.info[ent]["modules"]["fraction"]["fraction_right"] < 0.49
     then
        resolver.info[ent]["modules"]["fraction"]["side"] = resolver.info[ent]["modules"]["fraction"]["side"] or ""
    else
        resolver.info[ent]["modules"]["fraction"]["side"] =
            (resolver.info[ent]["modules"]["fraction"]["fraction_left"] >
            resolver.info[ent]["modules"]["fraction"]["fraction_right"] or
            resolver.info[ent]["modules"]["fraction"]["fraction_left2"] >
                resolver.info[ent]["modules"]["fraction"]["fraction_right2"]) and
            b9 or
            ba
    end
end
function resolver.logic_fraction2(ent)
    if not refs.resolver_modules:IsEnabled("Fraction2") then
        resolver.info[ent]["modules"]["fraction2"]["side"] = ""
        resolver.info[ent]["modules"]["fraction2"]["angle"] = 0
        return
    end
    local b9, ba = "left", "right"
    local d7 = Vector3(entity.hitbox_position(ent, 0))
    local d8 = Vector3(client.eye_position()).z
    local d9 = vector.eye_position(ent)
    d9.z = d7.z
    local d6, da = entity.get_prop(ent, "m_angEyeAngles")
    local db = cangle.normalize_angle(da - 60)
    d9.x = d9.x + math.cos(math.rad(db)) * 20
    d9.y = d9.y + math.sin(math.rad(db)) * 12
    local dc = Vector3(entity.get_origin(p))
    local dd = Vector3(entity.get_prop(p, "m_vecViewOffset"))
    dc = dc:__add(dd)
    local de = cangle.normalize_angle(db - 60)
    local df = cangle.normalize_angle(db - -60)
    local d2 = d9.x + math.cos(math.rad(de)) * 20
    local d3 = d9.y + math.sin(math.rad(de)) * 15
    local d4 = d9.x + math.cos(math.rad(df)) * 20
    local d5 = d9.y + math.sin(math.rad(df)) * 15
    local cY, d6 = client.trace_line(ent, dc.x, dc.y, d8, d4, d5, d9.z)
    local c_, d6 = client.trace_line(ent, dc.x, dc.y, d8, d2, d3, d9.z)
    if cY < 1 or c_ < 1 then
        local dg, dh = client.trace_bullet(p, dc.x, dc.y, dc.z, d2, d3, d9.z, true)
        local di, damage2 = client.trace_bullet(p, dc.x, dc.y, dc.z, d4, d5, d9.z, true)
        if dh > damage2 then
            resolver.info[ent]["modules"]["fraction2"]["side"] = ba
        elseif damage2 > dh then
            resolver.info[ent]["modules"]["fraction2"]["side"] = b9
        else
            resolver.info[ent]["modules"]["fraction2"]["side"] =
                resolver.info[ent]["modules"]["fraction2"]["side"] or ""
        end
    end
end
function resolver.logic_predict(ent)
    if not refs.resolver_modules:IsEnabled("Predict") then
        resolver.info[ent]["modules"]["predict"]["side"] = ""
        resolver.info[ent]["modules"]["predict"]["angle"] = 0
        return
    end
    local b9, ba = "left", "right"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "right", "left"
    end
    local dj = centity.get_origin_extrapolated(p, refs.resolver_predict_local_ticks:GetValue())
    local dk = nil
    local dl = math.huge
    local dm = Vector3(client.eye_position())
    local lp_stored_eyepos = lp_stored_eyepos or {}
    local dn = 0
    local cN = centity.get_origin_extrapolated(p, refs.resolver_predict_local_ticks:GetValue())
    local dp = Vector3(client.camera_angles())
    local dq = vector.eye_position(ent)
    local ce = refs.resolver_predict_enemy_ticks:GetValue()
    if centity.is_peeking_other(ent, ce, p) and ce >= 1 then
        dq = Vector3(centity.ExtrapolatePosition(dq.x, dq.y, dq.z, ce, ent))
    end
    local stored_eyepos = stored_eyepos or {}
    local dr = 0
    local cj = cangle.calculate_angle(cN.x, cN.y, dq.x, dq.y)
    local cG = {left = 0, right = 0}
    for y = cj - 90, cj + 90, 20 do
        local br = Vector3(cangle.Angle_Vector(0, cj + y))
        local ds = Vector3(dj.x + br.x * 55, dj.y + br.y * 55, dj.z + y)
        local bE, cZ = client.trace_line(ent, dj.x, dj.y, dm.z, ds.x, ds.y, ds.z)
        if bE < 1 then
            local d6, damage = client.trace_bullet(p, dj.x, dj.y, dm.z, ds.x, ds.y, ds.z + y, true)
            local index2, damage2 = client.trace_bullet(p, dq.x, dq.y, dq.z + 70, ds.x + 12, ds.y, ds.z, true)
            local index3, damage3 =
                client.trace_bullet(
                p,
                stored_eyepos.x or dq.x,
                stored_eyepos.y or dq.y,
                stored_eyepos.z or dq.z + 70,
                lp_stored_eyepos.x or ds.x - 12,
                lp_stored_eyepos.y or ds.y,
                lp_stored_eyepos.z or ds.z,
                true
            )
            if damage < dl then
                dl = damage
                if damage2 > damage then
                    dl = damage2
                end
                if damage3 > damage then
                    dl = damage3
                end
                if dm.x - dq.x > 0 then
                    dk = y
                else
                    dk = y * -1
                end
            elseif damage == dl then
                if dk < 0 then
                    resolver.info[ent]["modules"]["predict"]["side"] = ba
                elseif dk > 0 then
                    resolver.info[ent]["modules"]["predict"]["side"] = b9
                else
                    resolver.info[ent]["modules"]["predict"]["side"] =
                        resolver.info[ent]["modules"]["predict"]["side"] or ""
                end
                resolver.info[ent]["modules"]["predict"]["angle"] = dk
            end
        end
    end
end
function resolver.logic_trace(ent)
    if not refs.resolver_modules:IsEnabled("Trace") then
        resolver.info[ent]["modules"]["trace"]["side"] = ""
        resolver.info[ent]["modules"]["trace"]["angle"] = 0
        return
    end
    local b9, ba = "right", "left"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "left", "right"
    end
    local dk = nil
    local dl = math.huge
    local dm = centity.get_origin_extrapolated(p, refs.resolver_predict_local_ticks:GetValue())
    local dp = Vector3(client.camera_angles())
    local dq = vector.eye_position(ent)
    local ce = refs.resolver_predict_enemy_ticks:GetValue()
    if centity.is_peeking_other(ent, ce, p) and ce >= 1 then
        dq = Vector3(centity.ExtrapolatePosition(dq.x, dq.y, dq.z, ce, ent))
    end
    local stored_eyepos = stored_eyepos or {}
    local lp_stored_eyepos = lp_stored_eyepos or {}
    local cj = cangle.calculate_angle(dm.x, dm.y, dq.x, dq.y)
    local cG = {left = 0, right = 0}
    for y = cj - 90, cj + 90, 20 do
        local dir_x, dir_y, dt = cangle.Angle_Vector(0, cj + y)
        local ds = Vector3(dm.x + dir_x * 55, dm.y + dir_y * 55, dm.z + y)
        local index, damage = client.trace_bullet(p, dq.x, dq.y, dq.z + y, ds.x, ds.x, ds.z, true)
        local index2, damage2 = client.trace_bullet(p, dq.x, dq.y, dq.z + 70, ds.x + 12, ds.x, ds.z, true)
        local index3, damage3 =
            client.trace_bullet(
            p,
            stored_eyepos.x or dq.x,
            stored_eyepos.y or dq.y,
            stored_eyepos.z or dq.z + 70,
            lp_stored_eyepos.x or ds.x - 12,
            lp_stored_eyepos.y or ds.x,
            lp_stored_eyepos.z or ds.z,
            true
        )
        if damage < dl then
            dl = damage
            if damage2 > damage then
                dl = damage2
            end
            if damage3 > damage then
                dl = damage3
            end
            if dm.x - dq.x > 0 then
                dk = y
            else
                dk = y * -1
            end
        elseif damage == dl then
            if dk < 0 then
                resolver.info[ent]["modules"]["trace"]["side"] = b9
            elseif dk > 0 then
                resolver.info[ent]["modules"]["trace"]["side"] = ba
            else
                resolver.info[ent]["modules"]["trace"]["side"] = resolver.info[ent]["modules"]["trace"]["side"] or ""
            end
            resolver.info[ent]["modules"]["trace"]["angle"] = dk
        end
    end
end
resolver.dmg3_lowest = {}
function resolver.logic_trace2(ent)
    if not refs.resolver_modules:IsEnabled("Trace2") then
        resolver.info[ent]["modules"]["trace2"]["side"] = ""
        resolver.info[ent]["modules"]["trace2"]["angle"] = 0
        return
    end
    local b9, ba = "left", "right"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "right", "left"
    end
    resolver.dmg3_lowest[ent] = resolver.dmg3_lowest[ent] or 2
    local dj = centity.get_origin_extrapolated(p, refs.resolver_predict_local_ticks:GetValue())
    local du = Vector3(entity.get_prop(ent, "m_vecOrigin"))
    local ce = refs.resolver_predict_enemy_ticks:GetValue()
    if centity.is_peeking_other(ent, ce, p) and ce >= 1 then
        du = Vector3(centity.ExtrapolatePosition(du.x, du.y, du.z, ce, ent))
    end
    local cj = cangle.CalcAngle(dj.x, dj.y, du.x, du.y)
    for y, a7 in pairs({-60, 60}) do
        dir_x, dir_y = math.cos(math.rad(cj + a7)), math.sin(math.rad(cj + a7)), 0
        local ds = Vector3(dj.x + dir_x * 55, dj.y + dir_y * 55, dj.z + 80)
        index, damage = client.trace_bullet(p, dj.x, dj.y, dj.z + 70, ds.x, ds.y, ds.z)
        index2, damage2 = client.trace_bullet(p, dj.x, dj.y, dj.z + 70, ds.x + 12, ds.y, ds.z)
        index3, damage3 = client.trace_bullet(p, dj.x, dj.y, dj.z + 70, ds.x - 12, ds.y, ds.z)
        if damage < resolver.dmg3_lowest[ent] then
            resolver.dmg3_lowest[ent] = resolver.dmg3_lowest[ent]
            if damage2 > damage then
                resolver.dmg3_lowest[ent] = damage2
            end
            if damage3 > damage then
                resolver.dmg3_lowest[ent] = damage3
            end
            if dj.x - du.x < 0 then
                resolver.info[ent]["modules"]["trace2"]["angle"] = a7
            else
                resolver.info[ent]["modules"]["trace2"]["angle"] = a7 * -1
            end
            resolver.info[ent]["modules"]["trace2"]["side"] =
                resolver.info[ent]["modules"]["trace2"]["angle"] < 0 and ba or b9
        elseif damage == resolver.dmg3_lowest[ent] then
            if resolver.info[ent]["modules"]["trace2"]["angle"] < 0 then
                resolver.info[ent]["modules"]["trace2"]["side"] = ba
            elseif resolver.info[ent]["modules"]["trace2"]["angle"] > 0 then
                resolver.info[ent]["modules"]["trace2"]["side"] = b9
            else
                resolver.info[ent]["modules"]["trace2"]["side"] = resolver.info[ent]["modules"]["trace2"]["side"] or ""
            end
        end
    end
end
function resolver.logic_edge(ent)
    if not refs.resolver_modules:IsEnabled("Edge") then
        return
    end
    local b9, ba = "left", "right"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "right", "left"
    end
    local v, as, c5 = entity.hitbox_position(ent, 1)
    local cj = entity.get_prop(ent, "m_angEyeAngles[1]")
    local cG = {left = 0, right = 0}
    for y = cj - 90, cj + 90, 30 do
        local cH = math.rad(y)
        local G, H, I = v + 256 * math.cos(cH), as + 256 * math.sin(cH), c5
        local bE = client.trace_line(ent, v, as, c5, G, H, I)
        resolver.data[ent].logic.fraction = bE
        resolver.data[ent].lby_side = y < cj and b9 or ba
        cG[resolver.data[ent].lby_side] = cG[resolver.data[ent].lby_side] + bE
    end
    resolver.data[ent].logic.edge_trace_right = cG.right
    resolver.data[ent].logic.edge_trace_left = cG.left
    if cG.right == cG.left then
        resolver.info[ent]["modules"]["edge"]["side"] = resolver.info[ent]["modules"]["edge"]["side"] or ""
    elseif cG.left < cG.right then
        resolver.info[ent]["modules"]["edge"]["side"] = b9
    elseif cG.left > cG.right then
        resolver.info[ent]["modules"]["edge"]["side"] = ba
    end
end
function resolver.logic_edge2(ent)
    if not refs.resolver_modules:IsEnabled("Edge2") then
        resolver.info[ent]["modules"]["edge2"]["side"] = ""
        resolver.info[ent]["modules"]["edge2"]["angle"] = 0
        return
    end
    local dv = 90
    local b9, ba = "left", "right"
    if resolver.data[ent].antiaim["is_backwards"] then
        b9, ba = "right", "left"
    end
    local dw = {}
    local dx = 0
    local dy = 0
    local dz = {}
    local dA = 1
    for y = 0, 2, 1 do
        if dz[y] == nil then
            dz[y] = 0
        end
        if y == 1 then
            dw[y] = dv
        else
            dw[y] = -dv
        end
        if invert == 0 then
            if y == 1 then
                dw[y] = -dv
            else
                dw[y] = dv
            end
        end
        local dB = Vector3(entity.hitbox_position(ent, 0))
        local dj = centity.get_origin_extrapolated(p, refs.resolver_predict_local_ticks:GetValue())
        local dC = Vector3(dj.x, dj.y, dj.z)
        local dD = dB:__sub(dC):normalized()
        local dE = Vector3(0, 0, 1)
        local dF = vector.angle_forward(dD)
        local dG = vector.cross_product(dF, dE)
        local dH = Vector3(-dG.x, -dG.y, dG.z)
        local v = 2
        if y == 0 then
            local dI = dG:__mul(v * 2)
            dB = dB:__add(dI)
        elseif y == 1 then
            local dJ = dH:__mul(v * 2)
            dB = dB:__add(dJ)
        end
        if dB.x then
            return
        end
        local bE = client.trace_line(ent, dB.x, dB.y, dB.z, dC.x, dC.y, dC.z)
        if bE >= 0.90 then
            return
        end
        if bE < 0.90 and bE < dA then
            dA = bE
            dz[y] = dz[y] + 1
        end
        local cb, damage = client.trace_bullet(p, dC.x, dC.y, dC.z, dB.x, dB.y, dB.z)
        if damage <= dx then
            dx = damage
            dz[y] = dz[y] + 1
        end
    end
    if dz[0] > dz[1] then
        resolver.info[ent]["modules"]["edge2"]["angle"] = dw[0]
    elseif dz[0] < dz[1] then
        resolver.info[ent]["modules"]["edge2"]["angle"] = dw[1]
    end
    if resolver.info[ent]["modules"]["edge2"]["angle"] > 0 then
        resolver.info[ent]["modules"]["edge2"]["side"] = ba
    elseif resolver.info[ent]["modules"]["edge2"]["angle"] < 0 then
        resolver.info[ent]["modules"]["edge2"]["side"] = b9
    else
        resolver.info[ent]["modules"]["edge2"]["side"] = resolver.info[ent]["modules"]["edge2"]["side"] or ""
    end
end
function resolver.handle_module_stats(ent)
    if not refs.resolver_modules:IsEnabled("Module Accuracy") then
        return
    end
    for a6, a7 in pairs(resolver.info[ent]["modules"]) do
        local dK = a7["failed"] + a7["success"]
        a7["accuracy"] = cmath.percentof(a7["success"], dK)
    end
end
function resolver.choose_side(ent)
    local dL = {["left"] = 0, ["right"] = 0, ["mid"] = 0, [""] = 0}
    if
        refs.resolver_prefer_animlayer:GetValue() and resolver.data[ent].props["m_flVelocity2D"] <= 1.01 and
            (resolver.info[ent]["modules"]["animlayer"]["side"] == "left" or
                resolver.info[ent]["modules"]["animlayer"]["side"] == "right")
     then
        return resolver.info[ent]["modules"]["animlayer"]["side"]
    elseif refs.resolver_prefer_lby:GetValue() and resolver.data[ent].props["m_flVelocity2D"] <= 1.01 then
        return resolver.info[ent]["modules"]["lby"]["side"]
    end
    for a6, a7 in pairs(resolver.info[ent]["modules"]) do
        dL[a7["side"] or ""] = dL[a7["side"] or ""] + a7["accuracy"]
    end
    if dL["left"] ~= dL["right"] then
        if dL["mid"] > dL["left"] and dL["mid"] > dL["right"] then
            if refs.resolver_middle:GetValue() then
                return "mid"
            else
                return dL["left"] > dL["right"] and "left" or "right"
            end
        else
            return dL["left"] > dL["right"] and "left" or "right"
        end
    end
    return ""
end
function resolver.run_modules(ent)
    resolver.condition(ent)
    resolver.logic_edge(ent)
    resolver.logic_edge2(ent)
    resolver.logic_predict(ent)
    resolver.logic_trace(ent)
    resolver.logic_trace2(ent)
    resolver.logic_damage(ent)
    resolver.logic_damage2(ent)
    resolver.logic_fraction(ent)
    resolver.logic_fraction2(ent)
    resolver.logic_position(ent)
    resolver.logic_simple(ent)
    resolver.lowerbody(ent)
    resolver.animlayer_get_side(ent)
    resolver.get_delta_size(ent)
end
function resolver.set_body_yaw(ent)
    if resolver.data[ent].cur_hit_pattern > 0 then
        return
    end
    if refs.resolver_force_lowdelta:GetValue() or resolver.info[ent]["low_delta"]["active"] then
        resolver.info[ent].fake_limit =
            cmath.clamp(math.abs(resolver.info[ent].hit_yaw), 0, resolver.info[ent]["low_delta"]["yaw"])
    else
        resolver.info[ent].fake_limit = cmath.clamp(math.abs(resolver.info[ent].hit_yaw), 0, 60)
    end
    if resolver.data[ent].logic.side == "mid" then
        cur_lby = 0
    else
        cur_lby =
            resolver.data[ent].logic.side == "left" and resolver.info[ent].fake_limit or -resolver.info[ent].fake_limit
    end
    local a0 = refs.resolver_yawlimit:GetValue()
    local dM = a0
    if resolver.data[ent].props["highdelta"] and not resolver.data[ent].props["lowdelta"] then
        dM = cmath.clamp(60, -a0, a0)
    elseif resolver.data[ent].props["lowdelta"] and not resolver.data[ent].props["highdelta"] then
        dM = cmath.clamp(resolver.data[ent]["animlayer"]["fake_yaw"], -a0, a0)
    else
        dM = cmath.clamp(45, -a0, a0)
    end
    cur_lby = resolver.info[ent].real == "Show" and cur_lby * -1 or cur_lby
    cur_lby = cmath.clamp(cur_lby, -dM, dM)
    if resolver.data[ent].resolved_yaw == cur_lby then
        return
    end
    resolver.data[ent].resolved_yaw = cur_lby
    m.SetForceBodyYawCheckbox(ent, true)
    m.SetPlayerBodyYaw(ent, cur_lby)
    if resolver.data[ent].prev_goalfeet ~= resolver.data[ent].props["m_flGoalFeetDelta"] then
        resolver.data[ent].prev_goalfeet = resolver.data[ent].props["m_flGoalFeetDelta"]
    end
    mariolua.log("[Resolver] Set " .. entity.get_player_name(ent) .. "'s body yaw to: " .. cur_lby)
end
function resolver.count_packets(ent)
    if not refs.resolver_modules:IsEnabled("Desync Detection") then
        return
    end
    local dN = resolver.data[ent].props["m_flChokedPackets"] or 0
    if resolver.data[ent].misses >= 1 then
        resolver.data[ent].props["clean_packets"] = 0
        resolver.data[ent].props["bad_packets"] = 0
        resolver.data[ent].props["pref_packet"] = 0
        resolver.data[ent].props["should_resolve"] = true
        return
    end
    if resolver.data[ent].props["m_flVelocity2D"] > 0 and resolver.data[ent].props["should_resolve"] then
        return
    end
    if dN == resolver.data[ent].props["pref_packet"] then
        resolver.data[ent].props["clean_packets"] = resolver.data[ent].props["clean_packets"] + 1
    else
        resolver.data[ent].props["bad_packets"] = resolver.data[ent].props["bad_packets"] + 1
    end
    resolver.data[ent].props["pref_packet"] = dN
    if
        resolver.data[ent].props["clean_packets"] > 3 and not resolver.data[ent]["animlayer"]["is_desync"] or
            resolver.data[ent].props["clean_packets"] > 4
     then
        resolver.data[ent].props["clean_packets"] = 0
        resolver.data[ent].props["bad_packets"] = 0
        resolver.data[ent].props["should_resolve"] = false
    elseif
        resolver.data[ent].props["bad_packets"] > 10 and resolver.data[ent]["animlayer"]["is_desync"] or
            (resolver.data[ent].antiaim["mode"] == "Rage" or resolver.data[ent].props["is_slowwalking"] or
                resolver.balance_adjust(ent)) and
                resolver.data[ent].props["bad_packets"] > 10 or
            resolver.data[ent].props["bad_packets"] > 17
     then
        resolver.data[ent].props["bad_packets"] = 0
        resolver.data[ent].props["clean_packets"] = 0
        resolver.data[ent].props["should_resolve"] = true
    end
end
function resolver.logic(ent)
    if resolver.info[ent].force_off then
        resolver.OffResolver(ent)
        return
    end
    resolver.run_modules(ent)
    resolver.handle_module_stats(ent)
    resolver.data[ent].logic.side = resolver.choose_side(ent)
    resolver.info[ent].logic.side = resolver.data[ent].logic.side
    resolver.set_body_yaw(ent)
end
function resolver.get_props(ent)
    local aA = {entity.get_prop(ent, "m_vecVelocity")}
    local dO = centity.animstate(ent)
    local d1 = Vector3(entity.get_origin(p))
    local Y = Vector3(entity.get_origin(ent))
    local B = vector.get_FOV(Vector3(0, entity.get_prop(ent, "m_angEyeAngles[1]"), 0), Y, d1)
    resolver.data[ent].props["m_flVelocity2D"] = math.abs(math.sqrt(aA[1] ^ 2 + aA[2] ^ 2))
    resolver.data[ent].props["m_flChokedPackets"] = centity.GetChokedPackets(ent)
    resolver.data[ent].props["m_angEyeAngles"] =
        Angle(entity.get_prop(ent, "m_angEyeAngles[0]"), entity.get_prop(ent, "m_angEyeAngles[1]"), 0)
    resolver.data[ent].props["m_flLowerBodyYawTarget"] = entity.get_prop(ent, "m_flLowerBodyYawTarget")
    resolver.data[ent].props["m_flLowerBodyDelta"] =
        cangle.normalize_yaw(
        resolver.data[ent].props["m_flLowerBodyYawTarget"] - resolver.data[ent].props["m_angEyeAngles"].yaw
    )
    resolver.data[ent].props["m_flLowerBodyYawMoving"] =
        resolver.data[ent].props["m_flVelocity2D"] > 0 and resolver.data[ent].props["m_flLowerBodyYawTarget"] or
        resolver.data[ent].props["m_flLowerBodyYawMoving"]
    resolver.data[ent].props["m_flLowerBodyYawStanding"] =
        resolver.data[ent].props["m_flVelocity2D"] == 0 and resolver.data[ent].props["m_flLowerBodyYawTarget"] or
        resolver.data[ent].props["m_flLowerBodyYawStanding"]
    resolver.data[ent].props["m_flEyeYaw"] = cangle.normalize_as_yaw(dO.m_flEyeYaw)
    resolver.data[ent].props["m_flGoalFeetYaw"] = cangle.normalize_as_yaw(dO.m_flGoalFeetYaw)
    resolver.data[ent].props["m_flGoalFeetDelta"] =
        cangle.normalize_as_yaw(resolver.data[ent].props["m_flGoalFeetYaw"] - resolver.data[ent].props["m_flEyeYaw"])
    resolver.data[ent].props["m_flCurrentFeetYaw"] = cangle.normalize_as_yaw(dO.m_flCurrentFeetYaw)
    resolver.data[ent].antiaim["yaw_base"] = centity.get_side(ent)
    resolver.data[ent].antiaim["mode"] =
        math.abs(resolver.data[ent].props["m_angEyeAngles"].pitch) >= 85 and "Rage" or "Legit"
    resolver.data[ent].antiaim["is_backwards"] = math.abs(B) >= 0 and math.abs(B) <= 90 and true or false
    resolver.info[ent]["average_hit_yaw"] = cmath.average(resolver.info[ent]["hit_yaws_t"])
    resolver.info[ent]["average_missed_yaw"] = cmath.average(resolver.info[ent]["missed_yaws_t"])
end
function resolver.get_entity_data(ent)
    resolver.data[ent] = resolver.data[ent] or resolver.set_round_data()
    resolver.info[ent] = resolver.info[ent] or resolver.set_match_data()
    resolver.set_current_resolver(ent)
    resolver.animlayer_pref(ent, 3)
    resolver.animlayer_pref(ent, 6)
    resolver.animlayer_pref(ent, 12)
    resolver.anim_layer_control(ent, 3)
    resolver.anim_layer_control(ent, 6)
    resolver.get_props(ent)
    resolver.is_slow_walking(ent)
    resolver.fakeduck_detection(ent)
    resolver.match_info_pattern(ent)
    resolver.handle_record(ent)
end
function resolver.fakeduck_resolver(ent)
    if not refs.resolver_modules:IsEnabled("Fakeduck") then
        return
    end
    local bo = refs.resolver_fakeduck_ticks:GetValue()
    local dP = refs.resolver_fakeduck_lby:GetValue()
    if
        math.abs(resolver.data[ent].props["m_flLowerBodyFakeDuck"]) <= dP and resolver.data[ent].props["is_fakeduck"] and
            cache.crouched_ticks[ent] >= bo
     then
        resolver.data[ent].props["no_fake"] = true
        m.SetForceBodyYawCheckbox(ent, true)
        m.SetPlayerBodyYaw(ent, resolver.data[ent].props["m_flLowerBodyFakeDuck"] * -1)
        mariolua.log(
            "[Resolver] Fakeduck set " ..
                entity.get_player_name(ent) ..
                    "'s body yaw to: " .. resolver.data[ent].props["m_flLowerBodyFakeDuck"] * -1 .. " (Fakeducking)"
        )
    elseif resolver.data[ent].props["no_fake"] then
        mariolua.log(
            "[Resolver] Fakeduck restored " ..
                entity.get_player_name(ent) ..
                    "'s body yaw back to: " .. resolver.data[ent].resolved_yaw .. " (Fakeducking)"
        )
        m.SetPlayerBodyYaw(ent, resolver.data[ent].resolved_yaw)
        resolver.data[ent].props["no_fake"] = false
    end
end
local function dQ(dR, dS)
    local dR = dR ~= nil and dR or false
    local dS = dS ~= nil and dS or true
    local dT = {}
    local dU = entity.get_all("CCSPlayerResource")[1]
    for dV = 1, globals.maxplayers() do
        if entity.get_prop(dU, "m_bConnected", dV) == 1 then
            local dW
            if dR then
                dW = entity.get_prop(entity.get_local_player(), "m_iTeamNum")
            end
            local dX = true
            if dR and entity.get_prop(dV, "m_iTeamNum") == dW then
                dX = false
            end
            if dX then
                local dY = true
                if dS and entity.get_prop(dU, "m_bAlive", dV) ~= 1 then
                    dY = false
                end
                if dY then
                    table.insert(dT, dV)
                end
            end
        end
    end
    return dT
end
function resolver.handle()
    p = entity.get_local_player()
    if resolver.IsActive() then
        if not ui.get(cache.ref.rage.resolver_ref) then
            ui.set(cache.ref.rage.resolver_ref, true)
        end
        local dZ = dQ(true, true)
        for y = 1, #dZ do
            local d_ = dZ[y]
            resolver.reset_bruteforce_on_dormancy(d_)
        end
        local F = entity.get_players(true)
        for y = 1, #F do
            local ent = F[y]
            if not entity.is_alive(ent) then
                return
            end
            resolver.handle_resolver_indicator(ent)
            resolver.get_entity_data(ent)
            resolver.count_packets(ent)
            if
                refs.resolver_modules:IsEnabled("Desync Detection") and
                    (resolver.data[ent].props["should_resolve"] and not resolver.data[ent].props["correction"])
             then
                resolver.data[ent].props["correction"] = true
                m.SetForceBodyYawCheckbox(ent, true)
                m.SetCorrectionActive(ent, true)
            end
            if m.IsCorrectionActive(ent) then
                if resolver.data[ent]["bruteforce"]["active"] then
                    return
                end
                if
                    refs.resolver_modules:IsEnabled("Desync Detection") and
                        not resolver.data[ent].props["should_resolve"]
                 then
                    resolver.data[ent].props["correction"] = false
                    m.SetForceBodyYawCheckbox(ent, false)
                    m.SetCorrectionActive(ent, false)
                    return
                end
                resolver.fakeduck_resolver(ent)
                if resolver.data[ent].props["no_fake"] then
                    return
                end
                client.update_player_list()
                resolver.logic(ent)
            end
        end
    elseif not resolver.data_off then
        resolver.data_off = true
        resolver.OffResolverAll()
    end
end
function resolver.get_info_for_player(ent, v, as)
    crender.draw_text(
        v,
        as,
        cache.color[1],
        cache.color[2],
        cache.color[3],
        cache.color[4],
        cache.debug_font,
        entity.get_player_name(ent)
    )
    crender.draw_line(v + 50, as + 35, v + 200, as + 15, cache.color[1], cache.color[2], cache.color[3], cache.color[4])
    resolver.data[ent] = resolver.data[ent] or resolver.set_round_data()
    resolver.info[ent] = resolver.info[ent] or resolver.set_match_data()
    local ah, at = centity.get_animlayer(ent, 3)
    local ai = centity.get_animlayer(ent, 6)
    local e0 = centity.get_animlayer(ent, 8)
    local aj = centity.get_animlayer(ent, 12)
    local e1 = {}
    e1[1] =
        ent == p and {""} or
        {
            "[ Resolver Modules ]",
            string.format(" Edge: %s", resolver.info[ent]["modules"]["edge"]["side"]),
            string.format(
                " Edge2: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["edge2"]["side"],
                resolver.info[ent]["modules"]["edge2"]["success"],
                resolver.info[ent]["modules"]["edge2"]["accuracy"]
            ),
            string.format(
                " Trace: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["trace"]["side"],
                resolver.info[ent]["modules"]["trace"]["success"],
                resolver.info[ent]["modules"]["trace"]["accuracy"]
            ),
            string.format(
                " Trace2: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["trace2"]["side"],
                resolver.info[ent]["modules"]["trace2"]["success"],
                resolver.info[ent]["modules"]["trace2"]["accuracy"]
            ),
            string.format(
                " Fraction: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["fraction"]["side"],
                resolver.info[ent]["modules"]["fraction"]["success"],
                resolver.info[ent]["modules"]["fraction"]["accuracy"]
            ),
            string.format(
                " Fraction2: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["fraction2"]["side"],
                resolver.info[ent]["modules"]["fraction2"]["success"],
                resolver.info[ent]["modules"]["fraction2"]["accuracy"]
            ),
            string.format(
                " Predict: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["predict"]["side"],
                resolver.info[ent]["modules"]["predict"]["success"],
                resolver.info[ent]["modules"]["predict"]["accuracy"]
            ),
            string.format(
                " Damage: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["damage"]["side"],
                resolver.info[ent]["modules"]["damage"]["success"],
                resolver.info[ent]["modules"]["damage"]["accuracy"]
            ),
            string.format(
                " Damage2: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["damage2"]["side"],
                resolver.info[ent]["modules"]["damage2"]["success"],
                resolver.info[ent]["modules"]["damage2"]["accuracy"]
            ),
            string.format(
                " Position: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["position"]["side"],
                resolver.info[ent]["modules"]["position"]["success"],
                resolver.info[ent]["modules"]["position"]["accuracy"]
            ),
            string.format(
                " Simple: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["simple"]["side"],
                resolver.info[ent]["modules"]["simple"]["success"],
                resolver.info[ent]["modules"]["simple"]["accuracy"]
            ),
            string.format(
                " Lby: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["lby"]["side"],
                resolver.info[ent]["modules"]["lby"]["success"],
                resolver.info[ent]["modules"]["lby"]["accuracy"]
            ),
            string.format(
                " Animlayer: %s | Success: %i | Accuracy: %i",
                resolver.info[ent]["modules"]["animlayer"]["side"],
                resolver.info[ent]["modules"]["animlayer"]["success"],
                resolver.info[ent]["modules"]["animlayer"]["accuracy"]
            )
        }
    e1[2] = {
        "[ Player Details ]",
        string.format(
            " Eye Angles: %i | is fakeducking: %s | is slowwalking: %s | yaw_base: %s | lby_break: %s | Choked Packets: %i",
            resolver.data[ent].props["m_angEyeAngles"].yaw or 0,
            tostring(resolver.data[ent].props["is_fakeduck"] or false),
            tostring(resolver.data[ent].props["is_slowwalking"] or false),
            resolver.data[ent].antiaim["yaw_base"],
            tostring(resolver.data[ent].lby_can_update or false),
            resolver.data[ent].props["m_flChokedPackets"] or 0
        ),
        string.format(
            " Misses: %i | Hits: %i | Missed Body Yaw: %i | Hit Body Yaw: %i | Average Missed Yaw: %i | Average Hit Body Yaw: %i",
            resolver.info[ent].misses or 0,
            resolver.info[ent].hits or 0,
            resolver.info[ent].missed_body_yaw or 0,
            resolver.info[ent].hit_yaw or 0,
            resolver.info[ent]["average_missed_yaw"] or 0,
            resolver.info[ent]["average_hit_yaw"] or 0
        ),
        string.format(
            "m_flEyeYaw: %s | m_flGoalFeetYaw: %s | m_flGoalFeetDelta: %s | m_flCurrentFeetYaw: %s | lowerbody delta: %s",
            resolver.data[ent].props["m_flEyeYaw"],
            resolver.data[ent].props["m_flGoalFeetYaw"],
            resolver.data[ent].props["m_flGoalFeetDelta"],
            resolver.data[ent].props["m_flCurrentFeetYaw"],
            resolver.data[ent].props["m_flLowerBodyDelta"]
        )
    }
    cache.draw_ply_info(e1, v, as)
end
function resolver.on_round_start()
    if resolver.IsActive() then
        for y = 1, #resolver.data do
            resolver.data[y] = resolver.set_round_data()
            resolver.set_current_resolver(y)
            mariolua.log("[Resolver] Set '" .. entity.get_player_name(y) .. "'s data table.")
            m.SetForceBodyYawCheckbox(y, false)
        end
        resolver.data = {}
    end
    cache.stored_bullet_trace = {}
    cache.stored_line_trace = {}
    cache.stored_weakest_hitbox = {}
end
function resolver.on_round_end()
    if resolver.IsActive() then
        for y = 1, #resolver.data do
            resolver.data[y] = resolver.set_round_data()
            resolver.set_current_resolver(y)
            mariolua.log("[Resolver] Set '" .. entity.get_player_name(y) .. "'s data table.")
            m.SetForceBodyYawCheckbox(y, false)
        end
        resolver.data = {}
    end
    cache.stored_weakest_hitbox = {}
end
function resolver.on_player_death(ent)
    if resolver.IsActive() then
        if ent == p then
            resolver.data = {}
        elseif resolver.data ~= nil and entity.is_enemy(ent) then
            resolver.data[ent] = resolver.set_round_data()
            resolver.set_current_resolver(ent)
            mariolua.log("[Resolver] Set '" .. entity.get_player_name(ent) .. "'s data table.")
            m.SetForceBodyYawCheckbox(ent, false)
        end
    end
end
function resolver.on_cs_game_disconnected()
    ui.set(cache.ref.plist_resetall_ref, true)
    resolver.full_reset()
    cache.stored_weakest_hitbox = {}
end
function resolver.on_game_start()
    ui.set(cache.ref.plist_resetall_ref, true)
    resolver.full_reset()
    cache.stored_weakest_hitbox = {}
end
function resolver.on_game_end()
    ui.set(cache.ref.plist_resetall_ref, true)
    resolver.full_reset()
    cache.stored_weakest_hitbox = {}
end
function resolver.on_weapon_fire(ent)
    if cache.layer_control[ent] ~= nil then
        if cache.layer_control[ent].is_active == "" or "onshot" then
            cache.layer_control[ent]["onshot"].shot_fired = true
            cache.layer_control[ent].override = true
            cache.layer_control[ent].is_active = "onshot"
            mariolua.log("[Resolver] Layer control for " .. entity.get_player_name(ent) .. " 'on shot' is active ")
        end
    end
end
client.set_event_callback(
    "aim_fire",
    function(a8)
        resolver.aim_fire(a8)
    end
)
client.set_event_callback(
    "aim_hit",
    function(a8)
        resolver.aim_hit(a8)
    end
)
client.set_event_callback(
    "aim_miss",
    function(a8)
        resolver.aim_miss(a8)
    end
)
return {
    resolver = resolver,
    cache = cache,
    bindings = bindings,
    refs = refs,
    cmath = cmath,
    centity = centity,
    crender = crender,
    cangle = cangle,
    Angle = Angle,
    Vector3 = Vector3,
    vector = vector,
    mariolua = mariolua
}
