-- Body aim if Lethal Module
---@diagnostic disable: lowercase-global

local vector = require("vector")
local module = {enabled = false}
local r = {3, 4, 5, 6}

function extrapolate_position(s, t, u, v, w)
    local x, y, z = entity.get_prop(w, "m_vecVelocity")

    for _ = 0, v do
        s = s + x * globals.tickinterval()
        t = t + y * globals.tickinterval()
        u = u + z * globals.tickinterval()
    end

    return s, t, u
end

local function B(C, D)
    local E = 0
    local F, G, H = client.eye_position()
    local I, J, K
    F, G, H = extrapolate_position(F, G, H, 20, D)
    I, J, K = F, G, H

    for _, M in pairs(r) do
        local N = vector(entity.hitbox_position(C, M))
        local _, P = client.trace_bullet(D, I, J, K, N.x, N.y, N.z, true)

        if P > E then
            E = P
        end
    end

    return E
end

function module.on_run_command()
    if not module.enabled then
        return
    end

    local local_player = entity.get_local_player()
    local local_weapon = entity.get_player_weapon(local_player)
    local players = entity.get_players()
    if local_weapon == nil then
        return
    end

    for A = 1, #players do
        local ent = players[A]
        local ent_health = ent.get_prop(ent, "m_iHealth")
        local U = B(ent, local_player) >= ent_health

        if ent_health <= 0 then
            return
        end

        if U then
            plist.set(ent, "Override prefer body aim", "Force")
        else
            plist.set(ent, "Override prefer body aim", "-")
        end
    end
end

return module
