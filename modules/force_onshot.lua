-- Force onshot Module
local module = {
    enabled = false,
    ref_targethitbox = ui.reference("RAGE", "Aimbot", "Target hitbox"),
    ref_multipoint = ui.reference("RAGE", "Aimbot", "Multi-Point"),
    cached_targethitbox = {},
    cached_target_hitbox = {},
    set_whitelist = {}
}
function module.on_enable()
    if module.enabled then
        return
    end
    module.cached_multipoint = ui.get(module.ref_multipoint)
    module.cached_target_hitbox = ui.get(module.ref_targethitbox)
    ui.set(module.ref_multipoint, {"head"})
    ui.set(module.ref_targethitbox, {"head"})
    module.enabled = true
end
function module.on_disable()
    if not module.enabled then
        return
    end
    ui.set(module.ref_multipoint, module.cached_multipoint)
    ui.set(module.ref_targethitbox, module.cached_target_hitbox)
    local l = entity.get_players(true)
    if module.set_whitelist[ent] then
        plist.set(ent, "add to whitelist", false)
    end
    module.set_whitelist = {}
    module.enabled = false
end
function module.handle()
    local m = math.floor(0.2 / globals.tickinterval())
    local l = entity.get_players(true)
    for _, ent in pairs(l) do
        local o = entity.get_player_weapon(ent)
        local p = entity.get_prop(o, "m_fLastShotTime")
        if p then
            local q = globals.curtime() - p
            local r = q / globals.tickinterval()
            plist.set(ent, "add to whitelist", r > m and module.enabled)
            module.set_whitelist[ent] = r > m and module.enabled
        end
    end
end
return module
