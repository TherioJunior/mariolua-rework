---@diagnostic disable: undefined-global, need-check-nil, lowercase-global, ambiguity-1

local mariolua = {}

mariolua.userdata = {
    name = "TherioJunior",
    permissions = "Developer"
}

local ffi = require("ffi")
local bit = require("bit")
local debug = false

mariolua = mariolua or {}
mariolua.name = "MarioLua"
mariolua.version = "v13"
mariolua.debug = {
    enable = false,
    print = {},
    draw = {}
}
mariolua.console = nil
mariolua.module = {}
mariolua.lib = {}
mariolua.cmd = {}
mariolua.userdata = mariolua.userdata or {}
mariolua.userdata.permissions = mariolua.userdata.permissions or "Developer"
mariolua.userdata.uuid = mariolua.userdata.uuid or "Dev"
mariolua.userdata.name = mariolua.userdata.name or "Unknown"
mariolua.userdata.token = database.read("mariolua_db_token") or ""
mariolua.userdata.backup = database.read("mariolua_db_backup") or false

local local_player = entity.get_local_player()
local js = panorama.open()
local LobbyAPI = js.LobbyAPI
local GameStateAPI = js.GameStateAPI
local PartyListAPI = js.PartyListAPI
local MyPersonaAPI = js.MyPersonaAPI

local globalvars = {
    windows = {},
    reference = {},
    cobject = {},
    active_tabs = {},
    mouse1_key_state = false,
    config = {
        can_save = true,
        can_load = true
    },
    clipboard = "",
    moving_gui_object = false,
    used_Object = "",
    lock_input = {
        ["attack"] = false,
        ["move"] = false
    },
    block_moving_indicator = false,
    gs_menu_w = 0,
    gs_menu_h = 0,
    gs_menu_x = 0,
    gs_menu_y = 0,
    config_string = ui.new_string("mariolua_cfg"),
    config_names_string = ui.new_string("mariolua_namespam_names"),
    config_bg_string = ui.new_string("mariolua_background"),
    gui_style = "Gamesense",
    print_to_chat = {},
    indicator = {
        real_yaw = 0,
        fake_yaw = 0,
        old_size = 0,
        old_font = 0,
        container = nil
    },
    antiaim = {
        real = "",
        state = ""
    },
    report = {
        next_report_time = 0,
        should_report = false,
        idx = 1,
        total_reports = 0
    },
    closest_enemy = nil,
    closest_distance = 999999999,
    dynamicfov_new_fov = 1,
    legitautowall = {
        enabled_hitboxes = {}
    },
    getFrametimes = {},
    fps_prev = 0,
    icon_alpha = 150,
    thirdperson = {
        dist = 0
    },
    wep_tab = 1,
    loaded_string = "",
    clantag_off = nil,
    antiresolver = {
        updates = 0,
        targeted = 0
    },
    hitscan2 = {
        {
            0,
            1
        },
        {
            3,
            4,
            5,
            6
        },
        {
            2
        },
        {
            15,
            16,
            17,
            18
        },
        {
            11,
            12,
            13,
            14
        },
        {
            7,
            8
        },
        {
            9,
            10
        }
    },
    hitgroup_names2 = {
        "Head",
        "Chest",
        "Stomach",
        "Arms",
        "Hands",
        "Legs",
        "Feet"
    },
    hitgroups = {
        ["head"] = 0,
        ["chest"] = 1,
        ["stomach"] = 2,
        ["arms"] = 3,
        ["hands"] = 4,
        ["legs"] = 5,
        ["feet"] = 6
    },
    hitboxes2 = {
        ["Head"] = 0,
        ["Neck"] = 1,
        ["Pelvis"] = 2,
        ["Stomach"] = 3,
        ["Chest"] = 4,
        ["Upper chest"] = 5,
        ["Shoulders"] = 6,
        ["Right thigh"] = 7,
        ["Left thigh"] = 8,
        ["Right leg"] = 9,
        ["Left leg"] = 10,
        ["Right foot"] = 11,
        ["Left foot"] = 12,
        ["Right hand"] = 13,
        ["Left hand"] = 14,
        ["Right arm"] = 15,
        ["Right forearm"] = 16,
        ["Left arm"] = 17,
        ["Left forearm"] = 18
    },
    hitboxes = {
        [0] = "Head",
        [1] = "Neck",
        [2] = "Pelvis",
        [3] = "Spine 4",
        [4] = "Spine 3",
        [5] = "Spine 2",
        [6] = "Spine 1",
        [7] = "Leg Upper L",
        [8] = "Leg Upper R",
        [9] = "Leg Lower L",
        [10] = "Leg Lower R",
        [11] = "Foot L",
        [12] = "Foot R",
        [13] = "Hand L",
        [14] = "Hand R",
        [15] = "Arm Upper L",
        [17] = "Arm Upper R",
        [16] = "Arm Lower L",
        [18] = "Arm Lower R"
    },
    hitgroups2 = {
        [0] = "Body",
        [1] = "Head",
        [2] = "Chest",
        [3] = "Stomach",
        [4] = "Left Arm",
        [5] = "Right Arm",
        [6] = "Left Leg",
        [7] = "Right Leg",
        [8] = "Neck",
        [9] = "?",
        [10] = "Gear"
    },
    weapon_tabs = {
        [1] = "Pistols",
        [2] = "H. Pistols",
        [3] = "SMGs",
        [4] = "Rifles",
        [5] = "Scout",
        [6] = "AWP",
        [7] = "A. Sniper",
        [8] = "LMGs"
    },
    weapons = {
        [1] = "Desert Eagle",
        [2] = "Dual Berettas",
        [3] = "Five-SeveN",
        [4] = "Glock-18",
        [7] = "AK-47",
        [8] = "AUG",
        [9] = "AWP",
        [10] = "FAMAS",
        [11] = "G3SG1",
        [13] = "Galil AR",
        [14] = "M249",
        [16] = "M4A4",
        [17] = "MAC-10",
        [19] = "P90",
        [23] = "MP5-SD",
        [24] = "UMP-45",
        [25] = "XM1014",
        [26] = "PP-Bizon",
        [27] = "MAG-7",
        [28] = "Negev",
        [29] = "Sawed-Off",
        [30] = "Tec-9",
        [31] = "Zeus x23",
        [32] = "P2000",
        [33] = "MP7",
        [34] = "MP9",
        [35] = "Nova",
        [36] = "P250",
        [38] = "SCAR-20",
        [39] = "SG 553",
        [40] = "SSG 08",
        [41] = "Knife",
        [42] = "Knife",
        [43] = "Flashbang",
        [44] = "Grenade",
        [45] = "Smoke",
        [46] = "Molotov",
        [47] = "Decoy",
        [48] = "Incendiary",
        [59] = "Knife",
        [60] = "M4A1-S",
        [61] = "USP-S",
        [63] = "CZ75-Auto",
        [64] = "R8 Revolver",
        [500] = "Bayonet",
        [505] = "Flip Knife",
        [506] = "Gut Knife",
        [507] = "Karambit",
        [508] = "M9 Bayonet",
        [509] = "Huntsman Knife",
        [512] = "Falchion Knife",
        [514] = "Bowie Knife",
        [515] = "Butterfly Knife",
        [516] = "Shadow Daggers",
        [519] = "Ursus Knife",
        [520] = "Navaja Knife",
        [522] = "Siletto Knife",
        [523] = "Talon Knife"
    },
    wepcfg_wepgroups = {
        [1] = {
            "Dual Berettas",
            "Five-SeveN",
            "Glock-18",
            "Tec-9",
            "P2000",
            "P250",
            "USP-S",
            "CZ75-Auto"
        },
        [2] = {
            "Desert Eagle",
            "R8 Revolver"
        },
        [3] = {
            "MAC-10",
            "P90",
            "MP5-SD",
            "UMP-45",
            "PP-Bizon",
            "MP7",
            "MP9"
        },
        [4] = {
            "AK-47",
            "AUG",
            "FAMAS",
            "Galil AR",
            "M4A4",
            "SG 553",
            "M4A1-S"
        },
        [5] = {
            "SSG 08"
        },
        [6] = {
            "AWP"
        },
        [7] = {
            "G3SG1",
            "SCAR-20"
        },
        [8] = {
            "M249",
            "XM1014",
            "MAG-7",
            "Negev",
            "Sawed-Off",
            "Nova"
        },
        [9] = {
            "Zeus x23",
            "Knife",
            "Bayonet",
            "Flip Knife",
            "Gut Knife",
            "Karambit",
            "M9 Bayonet",
            "Huntsman Knife",
            "Falchion Knife",
            "Bowie Knife",
            "Butterfly Knife",
            "Shadow Daggers",
            "Ursus Knife",
            "Navaja Knife",
            "Siletto Knife",
            "Talon Knife"
        }
    },
    vote_reveal = {
        team_name = "",
        vote_option = {},
        vote_type = "",
        vote_issue = ""
    }
}

local ffi_utils = {}
local cfg = {}
local util_functions = {}
local resolver = {}
local extended_math = {}
local base64 = {}
local extended_table = {}
local external_modules = {}
local extended_renderer = {}
local functions = {}
local y = {}

ffi.cdef [[
    /* Console is visible */
    typedef bool(__thiscall* console_is_visible)(void*);

    /* cvar */
	struct CUtlVector2 {
		void* vtable;
		void* m_Memory[2];
		int m_Size;
	};
	typedef void*(__thiscall* find_cvar_t)(void*, const char* name);

	/* Clipboard shit */
	typedef void(__thiscall* asp_t)(void*, const char*, const char*, int);
	typedef bool(__thiscall* rsp_t)(void*, const char*, const char*);
	typedef int(__thiscall* gcpbs_t)(void*);
	typedef int(__thiscall* gcpbt_t)(void*, int,  char*, int);
	typedef char(__thiscall* gcd_t)(void*, char*, int);
	typedef bool(__thiscall* mv_t)(void*, const char*, const char*, const char*);
	typedef int(__thiscall* get_clipboard_text_count)(void*);
	typedef void(__thiscall* set_clipboard_text)(void*, const char*, int);
	typedef void(__thiscall* get_clipboard_text)(void*, int, const char*, int);

	/* netchannel info */
    typedef void*(__thiscall* get_net_channel_info_t)(void*);
    typedef const char*(__thiscall* get_name_t)(void*);
    typedef const char*(__thiscall* get_address_t)(void*);
    typedef float(__thiscall* get_local_time_t)(void*);
    typedef float(__thiscall* get_time_connected_t)(void*);
    typedef float(__thiscall* get_avg_latency_t)(void*, int);
    typedef float(__thiscall* get_avg_loss_t)(void*, int);
    typedef float(__thiscall* get_avg_choke_t)(void*, int);

    /* input system */
	typedef unsigned char wchar_t;
	typedef bool (__thiscall *IsButtonDown_t)(void*, int);
	typedef void (__thiscall *EnableInput)(void*, bool);

	/* ingame chat hooking */
	typedef void***(__thiscall* FindHudElement_t)(void*, const char*);
	typedef void(__cdecl* ChatPrintf_t)(void*, int, int, const char*, ...);
	typedef int(__thiscall* ConvertAnsiToUnicode_t)(void*, const char*, wchar_t*, int);
	typedef int(__thiscall* ConvertUnicodeToAnsi_t)(void*, const wchar_t*, char*, int);
	typedef wchar_t*(__thiscall* FindSafe_t)(void*, const char*);

	typedef struct {
		char pad[0x58];
		bool isChatOpen;
	} CCSGO_HudChat;

	/* idk what this is */
	typedef int(__cdecl* plat_messagebox)(const char*, const char*);

	/* animation state resolver stuff */
	typedef float  float_t;

    typedef void*(__thiscall* get_client_entity_t)(void*, int);
    typedef int(__thiscall* get_highest_entity_by_index_t)(void*);

    typedef float(__thiscall* get_spread_t)(void*);
    typedef float(__thiscall* get_innaccuracy_t)(void*);
    typedef bool(__thiscall* is_weapon_t)(void*);

    struct c_animstate_freeze_aa_mariolua {
        char pad[ 3 ];
        char m_bForceWeaponUpdate; //0x4
        char pad1[ 91 ];
        void* m_pBaseEntity; //0x60
        void* m_pActiveWeapon; //0x64
        void* m_pLastActiveWeapon; //0x68
        float m_flLastClientSideAnimationUpdateTime; //0x6C
        int m_iLastClientSideAnimationUpdateFramecount; //0x70
        float m_flAnimUpdateDelta; //0x74
        float m_flEyeYaw; //0x78
        float m_flPitch; //0x7C
        float m_flGoalFeetYaw; //0x80
        float m_flCurrentFeetYaw; //0x84
        float m_flCurrentTorsoYaw; //0x88
        float m_flUnknownVelocityLean; //0x8C
        float m_flLeanAmount; //0x90
        char pad2[ 4 ];
        float m_flFeetCycle; //0x98
        float m_flFeetYawRate; //0x9C
        char pad3[ 4 ];
        float m_fDuckAmount; //0xA4
        float m_fLandingDuckAdditiveSomething; //0xA8
        char pad4[ 4 ];
        float m_vOriginX; //0xB0
        float m_vOriginY; //0xB4
        float m_vOriginZ; //0xB8
        float m_vLastOriginX; //0xBC
        float m_vLastOriginY; //0xC0
        float m_vLastOriginZ; //0xC4
        float m_vVelocityX; //0xC8
        float m_vVelocityY; //0xCC
        char pad5[ 4 ];
        float m_flUnknownFloat1; //0xD4
        char pad6[ 8 ];
        float m_flUnknownFloat2; //0xE0
        float m_flUnknownFloat3; //0xE4
        float m_flUnknown; //0xE8
        float m_flSpeed2D; //0xEC
        float m_flUpVelocity; //0xF0
        float m_flSpeedNormalized; //0xF4
        float m_flFeetSpeedForwardsOrSideWays; //0xF8
        float m_flFeetSpeedUnknownForwardOrSideways; //0xFC
        float m_flTimeSinceStartedMoving; //0x100
        float m_flTimeSinceStoppedMoving; //0x104
        bool m_bOnGround; //0x108
        bool m_bInHitGroundAnimation; //0x109
        float m_flTimeSinceInAir; //0x10A
        float m_flLastOriginZ; //0x10E
        float m_flHeadHeightOrOffsetFromHittingGroundAnimation; //0x112
        float m_flStopToFullRunningFraction; //0x116
        char pad7[ 4 ]; //0x11A
        float m_flMagicFraction; //0x11E
        char pad8[ 60 ]; //0x122
        float m_flWorldForce; //0x15E
        char pad9[ 462 ]; //0x162
        float m_flMaxYaw; //0x334
    };

	struct c_weapon_info_t_mariolua_da {
		char pad_0000[4]; //0x0000
		char* ConsoleName; //0x0004
		char pad_0008[12]; //0x0008
		int iMaxClip1; //0x0014
		char pad_0018[12]; //0x0018
		int iMaxClip2; //0x0024
		char pad_0028[4]; //0x0028
		char* szWorldModel; //0x002C
		char* szViewModel; //0x0030
		char* szDropedModel; //0x0034
		char pad_0038[4]; //0x0038
		char* N00000984; //0x003C
		char pad_0040[56]; //0x0040
		char* szEmptySound; //0x0078
		char pad_007C[4]; //0x007C
		char* szBulletType; //0x0080
		char pad_0084[4]; //0x0084
		char* szHudName; //0x0088
		char* szWeaponName; //0x008C
		char pad_0090[60]; //0x0090
		int WeaponType; //0x00CC
		int iWeaponPrice; //0x00D0
		int iKillAward; //0x00D4
		char* szAnimationPrefex; //0x00D8
		float flCycleTime; //0x00DC
		float flCycleTimeAlt; //0x00E0
		float flTimeToIdle; //0x00E4
		float flIdleInterval; //0x00E8
		bool bFullAuto; //0x00EC
		char pad_00ED[3]; //0x00ED
		int iDamage; //0x00F0
		float flArmorRatio; //0x00F4
		int iBullets; //0x00F8
		float flPenetration; //0x00FC
		float flFlinchVelocityModifierLarge; //0x0100
		float flFlinchVelocityModifierSmall; //0x0104
		float flRange; //0x0108
		float flRangeModifier; //0x010C
		char pad_0110[28]; //0x0110
		int iCrosshairMinDistance; //0x012C
		float flMaxPlayerSpeed; //0x0130
		float flMaxPlayerSpeedAlt; //0x0134
		char pad_0138[4]; //0x0138
		float flSpread; //0x013C
		float flSpreadAlt; //0x0140
		float flInaccuracyCrouch; //0x0144
		float flInaccuracyCrouchAlt; //0x0148
		float flInaccuracyStand; //0x014C
		float flInaccuracyStandAlt; //0x0150
		float flInaccuracyJumpIntial; //0x0154
		float flInaccaurcyJumpApex;
		float flInaccuracyJump; //0x0158
		float flInaccuracyJumpAlt; //0x015C
		float flInaccuracyLand; //0x0160
		float flInaccuracyLandAlt; //0x0164
		float flInaccuracyLadder; //0x0168
		float flInaccuracyLadderAlt; //0x016C
		float flInaccuracyFire; //0x0170
		float flInaccuracyFireAlt; //0x0174
		float flInaccuracyMove; //0x0178
		float flInaccuracyMoveAlt; //0x017C
		float flInaccuracyReload; //0x0180
		int iRecoilSeed; //0x0184
		float flRecoilAngle; //0x0188
		float flRecoilAngleAlt; //0x018C
		float flRecoilVariance; //0x0190
		float flRecoilAngleVarianceAlt; //0x0194
		float flRecoilMagnitude; //0x0198
		float flRecoilMagnitudeAlt; //0x019C
		float flRecoilMagnatiudeVeriance; //0x01A0
		float flRecoilMagnatiudeVerianceAlt; //0x01A4
		float flRecoveryTimeCrouch; //0x01A8
		float flRecoveryTimeStand; //0x01AC
		float flRecoveryTimeCrouchFinal; //0x01B0
		float flRecoveryTimeStandFinal; //0x01B4
		int iRecoveryTransititionStartBullet; //0x01B8
		int iRecoveryTransititionEndBullet; //0x01BC
		bool bUnzoomAfterShot; //0x01C0
		char pad_01C1[31]; //0x01C1
		char* szWeaponClass; //0x01E0
		char pad_01E4[56]; //0x01E4
		float flInaccuracyPitchShift; //0x021C
		float flInaccuracySoundThreshold; //0x0220
		float flBotAudibleRange; //0x0224
		char pad_0228[12]; //0x0228
		bool bHasBurstMode; //0x0234
	};

	struct t_bone_matrix_mariolua {
		char pad3[12];
		float x;
		char pad1[12];
		float y;
		char pad2[12];
		float z;
	};

	struct c_weapon_info_t {
		char pad_0000[4]; //0x0000
		char* ConsoleName; //0x0004
		char pad_0008[12]; //0x0008
		int iMaxClip1; //0x0014
		char pad_0018[12]; //0x0018
		int iMaxClip2; //0x0024
		char pad_0028[4]; //0x0028
		char* szWorldModel; //0x002C
		char* szViewModel; //0x0030
		char* szDropedModel; //0x0034
		char pad_0038[4]; //0x0038
		char* N00000984; //0x003C
		char pad_0040[56]; //0x0040
		char* szEmptySound; //0x0078
		char pad_007C[4]; //0x007C
		char* szBulletType; //0x0080
		char pad_0084[4]; //0x0084
		char* szHudName; //0x0088
		char* szWeaponName; //0x008C
		char pad_0090[60]; //0x0090
		int WeaponType; //0x00CC
		int iWeaponPrice; //0x00D0
		int iKillAward; //0x00D4
		char* szAnimationPrefex; //0x00D8
		float flCycleTime; //0x00DC
		float flCycleTimeAlt; //0x00E0
		float flTimeToIdle; //0x00E4
		float flIdleInterval; //0x00E8
		bool bFullAuto; //0x00EC
		char pad_00ED[3]; //0x00ED
		int iDamage; //0x00F0
		float flArmorRatio; //0x00F4
		int iBullets; //0x00F8
		float flPenetration; //0x00FC
		float flFlinchVelocityModifierLarge; //0x0100
		float flFlinchVelocityModifierSmall; //0x0104
		float flRange; //0x0108
		float flRangeModifier; //0x010C
		char pad_0110[28]; //0x0110
		int iCrosshairMinDistance; //0x012C
		float flMaxPlayerSpeed; //0x0130
		float flMaxPlayerSpeedAlt; //0x0134
		char pad_0138[4]; //0x0138
		float flSpread; //0x013C
		float flSpreadAlt; //0x0140
		float flInaccuracyCrouch; //0x0144
		float flInaccuracyCrouchAlt; //0x0148
		float flInaccuracyStand; //0x014C
		float flInaccuracyStandAlt; //0x0150
		float flInaccuracyJumpIntial; //0x0154
		float flInaccaurcyJumpApex;
		float flInaccuracyJump; //0x0158
		float flInaccuracyJumpAlt; //0x015C
		float flInaccuracyLand; //0x0160
		float flInaccuracyLandAlt; //0x0164
		float flInaccuracyLadder; //0x0168
		float flInaccuracyLadderAlt; //0x016C
		float flInaccuracyFire; //0x0170
		float flInaccuracyFireAlt; //0x0174
		float flInaccuracyMove; //0x0178
		float flInaccuracyMoveAlt; //0x017C
		float flInaccuracyReload; //0x0180
		int iRecoilSeed; //0x0184
		float flRecoilAngle; //0x0188
		float flRecoilAngleAlt; //0x018C
		float flRecoilVariance; //0x0190
		float flRecoilAngleVarianceAlt; //0x0194
		float flRecoilMagnitude; //0x0198
		float flRecoilMagnitudeAlt; //0x019C
		float flRecoilMagnatiudeVeriance; //0x01A0
		float flRecoilMagnatiudeVerianceAlt; //0x01A4
		float flRecoveryTimeCrouch; //0x01A8
		float flRecoveryTimeStand; //0x01AC
		float flRecoveryTimeCrouchFinal; //0x01B0
		float flRecoveryTimeStandFinal; //0x01B4
		int iRecoveryTransititionStartBullet; //0x01B8
		int iRecoveryTransititionEndBullet; //0x01BC
		bool bUnzoomAfterShot; //0x01C0
		char pad_01C1[31]; //0x01C1
		char* szWeaponClass; //0x01E0
		char pad_01E4[56]; //0x01E4
		float flInaccuracyPitchShift; //0x021C
		float flInaccuracySoundThreshold; //0x0220
		float flBotAudibleRange; //0x0224
		char pad_0228[12]; //0x0228
		bool bHasBurstMode; //0x0234
	};

    struct animation_layer_t_mariolua {
		bool m_bClientBlend;		 //0x0000
		float m_flBlendIn;			 //0x0004
		void* m_pStudioHdr;			 //0x0008
		int m_nDispatchSequence;     //0x000C
		int m_nDispatchSequence_2;   //0x0010
		uint32_t m_nOrder;           //0x0014
		uint32_t m_nSequence;        //0x0018
		float_t m_flPrevCycle;       //0x001C
		float_t m_flWeight;          //0x0020
		float_t m_flWeightDeltaRate; //0x0024
		float_t m_flPlaybackRate;    //0x0028
		float_t m_flCycle;           //0x002C
		void* m_pOwner;              //0x0030
		char pad_0038[4];            //0x0034
    };

    struct c_animstate_mariolua {
        char pad[ 3 ];
        char m_bForceWeaponUpdate; //0x4
        char pad1[ 91 ];
        void* m_pBaseEntity; //0x60
        void* m_pActiveWeapon; //0x64
        void* m_pLastActiveWeapon; //0x68
        float m_flLastClientSideAnimationUpdateTime; //0x6C
        int m_iLastClientSideAnimationUpdateFramecount; //0x70
        float m_flAnimUpdateDelta; //0x74
        float m_flEyeYaw; //0x78
        float m_flPitch; //0x7C
        float m_flGoalFeetYaw; //0x80
        float m_flCurrentFeetYaw; //0x84
        float m_flCurrentTorsoYaw; //0x88
        float m_flUnknownVelocityLean; //0x8C
        float m_flLeanAmount; //0x90
        char pad2[ 4 ];
        float m_flFeetCycle; //0x98
        float m_flFeetYawRate; //0x9C
        char pad3[ 4 ];
        float m_fDuckAmount; //0xA4
        float m_fLandingDuckAdditiveSomething; //0xA8
        char pad4[ 4 ];
        float m_vOriginX; //0xB0
        float m_vOriginY; //0xB4
        float m_vOriginZ; //0xB8
        float m_vLastOriginX; //0xBC
        float m_vLastOriginY; //0xC0
        float m_vLastOriginZ; //0xC4
        float m_vVelocityX; //0xC8
        float m_vVelocityY; //0xCC
        char pad5[ 4 ];
        float m_flUnknownFloat1; //0xD4
        char pad6[ 8 ];
        float m_flUnknownFloat2; //0xE0
        float m_flUnknownFloat3; //0xE4
        float m_flUnknown; //0xE8
        float m_flSpeed2D; //0xEC
        float m_flUpVelocity; //0xF0
        float m_flSpeedNormalized; //0xF4
        float m_flFeetSpeedForwardsOrSideWays; //0xF8
        float m_flFeetSpeedUnknownForwardOrSideways; //0xFC
        float m_flTimeSinceStartedMoving; //0x100
        float m_flTimeSinceStoppedMoving; //0x104
        bool m_bOnGround; //0x108
        bool m_bInHitGroundAnimation; //0x109
        float m_flTimeSinceInAir; //0x10A
        float m_flLastOriginZ; //0x10E
        float m_flHeadHeightOrOffsetFromHittingGroundAnimation; //0x112
        float m_flStopToFullRunningFraction; //0x116
        char pad7[ 4 ]; //0x11A
        float m_flMagicFraction; //0x11E
        char pad8[ 60 ]; //0x122
        float m_flWorldForce; //0x15E
        char pad9[ 462 ]; //0x162
        float m_flMaxYaw; //0x334
    };

    /* get friendlist shit */
    typedef void*(__stdcall* GetFriendsListApi_t)();
    typedef void(__thiscall* InviteXuid_t)(void* this, unsigned long long xuid);
]]

-- Console is visible
ffi_utils.engine_client = ffi.cast(ffi.typeof("void***"), client.create_interface("engine.dll", "VEngineClient014"))
ffi_utils.console_is_visible = ffi.cast("console_is_visible", ffi_utils.engine_client[0][11])
-- Console is visible

-- cvar stuff
ffi_utils.cvar_raw = client.create_interface("vstdlib.dll", "VEngineCvar007") or error("Interface not found")
ffi_utils.cvar_real = ffi.cast(ffi.typeof("void***"), ffi_utils.cvar_raw) or error("can't cast")
ffi_utils.find_cvar = ffi.cast("find_cvar_t", ffi_utils.cvar_real[0][15]) or error("could not find function")
ffi_utils.name_cvar = ffi_utils.find_cvar(ffi_utils.cvar_raw, "name")
ffi_utils.cvar_raw = ffi.cast("char*", ffi_utils.name_cvar)
ffi_utils.fnChangeCallback = ffi.cast("struct CUtlVector2*", ffi_utils.cvar_raw + 0x44)
ffi_utils.fnChangeCallback.m_Size = 0
-- cvar stuff

-- Clipboard
ffi_utils.VGUI_System010 =
    client.create_interface("vgui2.dll", "VGUI_System010") or print("Error finding VGUI_System010")
ffi_utils.VGUI_System = ffi.cast(ffi.typeof("void***"), ffi_utils.VGUI_System010)
ffi_utils.get_clipboard_text_count =
    ffi.cast("get_clipboard_text_count", ffi_utils.VGUI_System[0][7]) or print("get_clipboard_text_count Invalid")
ffi_utils.set_clipboard_text =
    ffi.cast("set_clipboard_text", ffi_utils.VGUI_System[0][9]) or print("set_clipboard_text Invalid")
ffi_utils.get_clipboard_text =
    ffi.cast("get_clipboard_text", ffi_utils.VGUI_System[0][11]) or print("get_clipboard_text Invalid")
ffi_utils.fs = ffi.cast(ffi.typeof("void***"), client.create_interface("filesystem_stdio.dll", "VFileSystem017"))
ffi_utils.rsp =
    ffi.cast(
    "rsp_t",
    client.find_signature(
        "filesystem_stdio.dll",
        string.char(85, 139, 236, 129, 236, 204, 204, 204, 204, 139, 85, 8, 83, 139)
    )
)
ffi_utils.gcd =
    ffi.cast(
    "gcd_t",
    client.find_signature("filesystem_stdio.dll", string.char(85, 139, 236, 86, 139, 117, 8, 86, 255))
)
ffi_utils.isystem = ffi.cast(ffi.typeof("void***"), client.create_interface("vgui2.dll", "VGUI_System010"))
ffi_utils.gcpbs = ffi.cast("gcpbs_t", ffi_utils.isystem[0][7])
ffi_utils.gcpbt = ffi.cast("gcpbt_t", ffi_utils.isystem[0][11])
-- Clipboard

-- net_channel_info
ffi_utils.interface_ptr = ffi.typeof("void***")
ffi_utils.rawivengineclient =
    client.create_interface("engine.dll", "VEngineClient014") or error("VEngineClient014 wasnt found", 2)
ffi_utils.ivengineclient =
    ffi.cast(ffi_utils.interface_ptr, ffi_utils.rawivengineclient) or error("rawivengineclient is nil", 2)
ffi_utils.get_net_channel_info =
    ffi.cast("get_net_channel_info_t", ffi_utils.ivengineclient[0][78]) or error("ivengineclient is nil")
-- net_channel_info

-- input system
ffi_utils.raw_inputsystem = client.create_interface("inputsystem.dll", "InputSystemVersion001")
ffi_utils.inputsystem = ffi.cast(ffi_utils.interface_ptr, ffi_utils.raw_inputsystem)
ffi_utils.inputsystem_vtbl = ffi_utils.inputsystem[0]
ffi_utils.raw_EnableInput = ffi_utils.inputsystem_vtbl[11]
ffi_utils.raw_IsButtonDown = ffi_utils.inputsystem_vtbl[15]
ffi_utils.is_button_pressed = ffi.cast("IsButtonDown_t", ffi_utils.raw_IsButtonDown)
ffi_utils.enable_input = ffi.cast("EnableInput", ffi_utils.raw_EnableInput)
-- input system

-- ingame chat hooking
ffi_utils.signature_gHud = string.char(185, 204, 204, 204, 204, 136, 70, 9)
ffi_utils.signature_FindElement = string.char(85, 139, 236, 83, 139, 93, 8, 86, 87, 139, 249, 51, 246, 57, 119, 40)
ffi_utils.chat_match_temp = client.find_signature("client.dll", ffi_utils.signature_gHud) or error("sig1 not found")
ffi_utils.hud = ffi.cast("void**", ffi.cast("char*", ffi_utils.chat_match_temp) + 1)[0] or error("hud is nil")
ffi_utils.chat_match =
    client.find_signature("client.dll", ffi_utils.signature_FindElement) or error("FindHudElement not found")
ffi_utils.find_hud_element = ffi.cast("FindHudElement_t", ffi_utils.chat_match)
ffi_utils.hudchat = ffi_utils.find_hud_element(ffi_utils.hud, "CHudChat") or error("CHudChat not found")
ffi_utils.chudchat_vtbl = ffi_utils.hudchat[0] or error("CHudChat instance vtable is nil")
ffi_utils.print_to_chat = ffi.cast("ChatPrintf_t", ffi_utils.chudchat_vtbl[27])
ffi_utils.hudElement2 = ffi_utils.find_hud_element(ffi_utils.hud, "CCSGO_HudChat") or error("CCSGO_HudChat not found")
ffi_utils.hudchat2 = nil

if (ffi_utils.hudElement2 ~= nil) then
    ffi_utils.hudchat2 = ffi.cast("CCSGO_HudChat*", ffi_utils.hudElement2)
end

function ffi_utils.initHudChat()
    ffi_utils.hudElement2 =
        ffi_utils.find_hud_element(ffi_utils.hud, "CCSGO_HudChat") or error("CCSGO_HudChat not found")

    if (ffi_utils.hudElement2 ~= nil) then
        ffi_utils.hudchat2 = ffi.cast("CCSGO_HudChat*", ffi_utils.hudElement2)
    end
end
-- ingame chat hooking

-- idk what this is
ffi_utils.crr_t = ffi.typeof("void*(__thiscall*)(void*)")
ffi_utils.cr_t = ffi.typeof("void*(__thiscall*)(void*)")
ffi_utils.gm_t = ffi.typeof("const void*(__thiscall*)(void*)")
ffi_utils.gsa_t = ffi.typeof("int(__fastcall*)(void*, void*, int)")
-- idk what this is

-- Animation state resolver stuff
functions.rawientitylist =
    client.create_interface("client_panorama.dll", "VClientEntityList003") or
    error("VClientEntityList003 wasnt found", 2)
functions.ientitylist = ffi.cast(ffi_utils.interface_ptr, functions.rawientitylist) or error("rawientitylist is nil", 2)
functions.get_client_networkable =
    ffi.cast("void*(__thiscall*)(void*, int)", functions.ientitylist[0][0]) or
    error("get_client_networkable_t is nil", 2)
functions.get_client_entity =
    ffi.cast("void*(__thiscall*)(void*, int)", functions.ientitylist[0][3]) or error("get_client_entity is nil", 2)
functions.get_highest_entity_by_index = ffi.cast("get_highest_entity_by_index_t", functions.ientitylist[0][6])
functions.rawivmodelinfo = client.create_interface("engine.dll", "VModelInfoClient004")
functions.ivmodelinfo = ffi.cast(ffi_utils.interface_ptr, functions.rawivmodelinfo) or error("rawivmodelinfo is nil", 2)
functions.get_studio_model = ffi.cast("void*(__thiscall*)(void*, const void*)", functions.ivmodelinfo[0][32])
functions.seq_activity_sig =
    client.find_signature("client.dll", string.char(85, 139, 236, 83, 139, 93, 8, 86, 139, 241, 131)) or
    error("error getting seq_activity")

function functions.get_model(a5)
    if (a5) then
        a5 = ffi.cast(ffi_utils.interface_ptr, a5)
        local ar = ffi.cast(ffi_utils.crr_t, a5[0][0])
        local at = ar(a5) or error("error getting client unknown", 2)

        if (at) then
            at = ffi.cast(ffi_utils.interface_ptr, at)
            local au = ffi.cast(ffi_utils.cr_t, at[0][5])(at) or error("error getting client renderable", 2)

            if (au) then
                au = ffi.cast(ffi_utils.interface_ptr, au)

                return ffi.cast(ffi_utils.gm_t, au[0][8])(au) or error("error getting model_t", 2)
            end
        end
    end
end

function functions.get_sequence_activity(br, bs, bt)
    br = ffi.cast(ffi_utils.interface_ptr, br)
    local bu = functions.get_studio_model(functions.ivmodelinfo, functions.get_model(bs))

    if (bu == nil) then
        return -1
    end

    local aq = ffi.cast(ffi_utils.gsa_t, functions.seq_activity_sig)

    return aq(br, bu, bt)
end

function functions.get_anim_layer(br, bv)
    bv = bv or 1
    br = ffi.cast(ffi_utils.interface_ptr, br)

    return ffi.cast("struct animation_layer_t_mariolua**", ffi.cast("char*", br) + 0x2980)[0][bv]
end

function util_functions.GetAvgLatency()
    local bw = ffi.cast("void***", ffi_utils.get_net_channel_info(ffi_utils.ivengineclient))

    if (bw) then
        local bx = ffi.cast("get_avg_latency_t", bw[0][10])

        if (bx == nil) then
            return 0
        else
            return bx(bw, 1)
        end
    end

    return 0.0
end

function functions.animstate(ent)
    local br = functions.get_client_entity(functions.ientitylist, ent)
    local by = ffi.cast(ffi_utils.interface_ptr, br)
    local bz = ffi.cast("char*", by) + 0x3914
    local bA = ffi.cast("struct c_animstate_mariolua**", bz)[0]

    if (ent ~= nil) then
        return bA
    end
end

functions.teams = {[0] = "None", [1] = "Spec", [2] = "T", [3] = "CT"}
globalvars.enemies_t = {}
globalvars.enemie_names_t = {}
globalvars.enemie_index_t = {}
globalvars.enemie_count = 0

function functions.get_all_enemies2()
    local bB = entity.get_all("CCSPlayer")

    for I = 1, #bB do
        local ent = bB[I]

        if
            not extended_table.Contains(globalvars.enemies_t, ent) and entity.is_enemy(ent) and ent ~= nil and
                entity.get_player_name(ent) ~= "unknown" and
                (entity.get_steam64(ent) ~= 0 and entity.get_steam64(ent) ~= nil or functions.is_bot(ent))
         then
            table.insert(globalvars.enemies_t, ent)
            table.insert(globalvars.enemie_names_t, entity.get_player_name(ent))
            table.insert(globalvars.enemie_index_t, I)
            globalvars.enemie_count = globalvars.enemie_count + 1
            mariolua.log(
                "Playerlist: Added idx: " ..
                    I .. " | player: " .. entity.get_player_name(ent) .. " | enemy count: " .. globalvars.enemie_count
            )
        end
    end

    local bC = globals.maxplayers()

    for I = 1, bC do
        local ent = globalvars.enemie_index_t[I]

        if entity.is_enemy(ent) or not globalvars.enemies_t[ent] == ent then
            return
        end

        if entity.get_player_name(ent) == "unknown" or globalvars.enemie_names_t[ent] == "unknown" then
            mariolua.log(
                "Playerlist: Removed idx: " ..
                    tostring(ent) ..
                        " | player: " ..
                            tostring(globalvars.enemie_names_t[ent]) ..
                                " | enemy count: " .. tostring(globalvars.enemie_count)
            )
            table.remove(globalvars.enemies_t, ent)
            table.remove(globalvars.enemie_names_t, ent)
            globalvars.enemie_index_t[I] = nil
            table.remove(globalvars.enemie_index_t, I)
            globalvars.enemie_count = globalvars.enemie_count - 1
        end
    end

    return globalvars.enemies_t
end

function functions.get_all_enemies()
    local enemies = {}
    local enemy_entity = entity.get_all("CCSPlayer")

    if #enemy_entity == 0 then
        return
    end

    for I = 1, #enemy_entity do
        local ent = enemy_entity[I]

        if
            entity.is_enemy(ent) and entity.get_player_name(ent) ~= "unknown" and
                (entity.get_steam64(ent) ~= 0 and entity.get_steam64(ent) ~= nil or functions.is_bot(I))
         then
            enemies[I - 1] = ent
        end
    end

    return enemies
end

function functions.GetChokedPackets(ent)
    local bE = entity.get_prop(ent, "m_flSimulationTime")
    local bF = entity.get_prop(entity.get_player_resource(), "m_iPing", ent) or 0
    local bG = extended_math.TimeToTicks(globals.curtime() - bE - bF)

    return bG
end

function functions.get_all_player_positions(bH)
    local bD = {}
    local bI = {}
    local bJ = entity.get_players(bH)

    if #bJ == 0 then
        return
    end

    for I = 1, #bJ do
        local bK = bJ[I]
        local bL, bM, bN = entity.get_prop(bK, "m_vecOrigin")
        local bO = entity.get_prop(bK, "m_vecViewOffset[2]")

        if bN ~= nil and bO ~= nil then
            bN = bN + bO * 0.5
            local bP, bQ = client.world_to_screen(ctx, bL, bM, bN)

            if bP ~= nil and bQ ~= nil then
                if bP >= 0 and bP <= globalvars.screen_size.w and bQ >= 0 and bQ <= globalvars.screen_size.h then
                    bD[#bD + 1] = bK
                    bI[#bI + 1] = {bP, bQ}
                end
            end
        end
    end

    return bD, bI
end

function functions.is_peeking_other(ent, bR, bS)
    bR = bR or 1
    local bT, bU, bO = entity.get_prop(ent, "m_vecVelocity")
    local bV = math.sqrt(bT * bT + bU * bU + bO * bO)

    if bV < 5 then
        return false
    end

    local bW = A(entity.get_origin(bS))
    local bX = A(entity.get_origin(ent))
    local bY = math.abs(B.distance_3d(bW, bX))
    local bZ = 999999

    for b_ = 1, bR do
        local c0 = A(functions.ExtrapolatePosition(bX.x, bX.y, bX.z, bR, ent))
        local c1 = B.distance_3d(bW, c0)

        if c1 < bZ then
            bZ = math.abs(c1)
        end

        if bZ < bY then
            return true
        end
    end

    return bZ < bY
end

function functions.is_local_peeking_enemy(ent, bR)
    bR = bR or 1
    local bT, bU, bO = entity.get_prop(local_player, "m_vecVelocity")
    local bV = math.sqrt(bT * bT + bU * bU + bO * bO)

    if bV < 5 then
        return false
    end

    local bX = A(entity.get_origin(ent))
    local c2 = A(entity.get_origin(local_player))
    local bY = math.abs(B.distance_3d(bX, c2))
    local bZ = 999999

    for b_ = 1, bR do
        local c0 = A(functions.ExtrapolatePosition(c2.x, c2.y, c2.z, bR, ent))
        local c1 = B.distance_3d(bX, c0)

        if c1 < bZ then
            bZ = math.abs(c1)
        end

        if bZ < bY then
            return true
        end
    end

    return bZ < bY
end

-- Animation layer resolver stuff
local c3 = {}
local c4 = {}

c3.__index = c3
c3.native_GetNetChannelInfo = vtable_bind("engine.dll", "VEngineClient014", 78, "void*(__thiscall*)(void*)")
c3.native_GetName = vtable_thunk(0, "const char*(__thiscall*)(void*)")
c3.native_GetAddress = vtable_thunk(1, "const char*(__thiscall*)(void*)")
c3.native_IsLoopback = vtable_thunk(6, "bool(__thiscall*)(void*)")
c3.native_IsTimingOut = vtable_thunk(7, "bool(__thiscall*)(void*)")
c3.native_GetAvgLoss = vtable_thunk(11, "float(__thiscall*)(void*, int)")
c3.native_GetAvgChoke = vtable_thunk(12, "float(__thiscall*)(void*, int)")
c3.native_GetTimeSinceLastReceived = vtable_thunk(22, "float(__thiscall*)(void*)")
c3.native_GetRemoteFramerate = vtable_thunk(25, "void(__thiscall*)(void*, float*, float*, float*)")
c3.native_GetTimeoutSeconds = vtable_thunk(26, "float(__thiscall*)(void*)")

function c4.GetAddress(c5)
    local c6 = c3.native_GetAddress(c5)

    if c6 ~= nil then
        return ffi.string(c6)
    end
end

function c4.GetName(c5)
    local aU = c3.native_GetName(c5)

    if aU ~= nil then
        return ffi.string(aU)
    end
end

functions.GetWeapon = function(c7)
    local c8 = entity.get_prop(c7, "m_hActiveWeapon")

    if c8 == nil then
        return
    end

    local c9 = entity.get_prop(c8, "m_iItemDefinitionIndex")

    if c9 == nil then
        return
    end

    local ca = bit.band(c9, 0xFFFF)

    return globalvars.weapons[ca]
end

functions.GetWeaponGroup = function(ent)
    local c8 = entity.get_prop(ent, "m_hActiveWeapon")

    if c8 == nil then
        return
    end

    local c9 = entity.get_prop(c8, "m_iItemDefinitionIndex")

    if c9 == nil then
        return
    end

    local ca = bit.band(c9, 0xFFFF)

    for I = 1, #globalvars.wepcfg_wepgroups do
        if extended_table.Contains(globalvars.wepcfg_wepgroups[I], globalvars.weapons[ca]) then
            return I
        end
    end
end

local function z()
end
local function Vector3()
end
local B = {}
local C = {}

function mariolua.prefix()
    return {
        client.color_log(3, 161, 252, "[\0"),
        client.color_log(3, 161, 252, "Mario\0"),
        client.color_log(247, 247, 247, "Lua\0"),
        client.color_log(3, 161, 252, "]\0")
    }
end

function extended_table.exists(D, E)
    local F

    for _, F in pairs(D) do
        if F == E then
            return true
        elseif type(F) == "table" then
            return extended_table.exists(F, E)
        end
    end

    return false
end

function extended_table.Contains(G, H)
    if G == nil or H == nil then
        return false
    end

    for I = 1, #G do
        if G[I] == H then
            return true
        end
    end

    return false
end

cache_log = {}
cache_log.index = 0
cache_log.last_log_time = 0
cache_log.last_con_log = {}
cache_log.last_log_print = ""

function mariolua.log(J)
    if not mariolua.debug.print then
        return
    end

    if globals.realtime() >= cache_log.last_log_time + 5 and #cache_log.last_con_log >= 2 then
        cache_log.last_log_time = globals.realtime()
    end

    if not extended_table.Contains(cache_log.last_con_log, J) then
        local K, L, M = client.system_time()
        client.color_log(255, 50, 1870, "[" .. K .. ":" .. L .. ":" .. M .. "] [" .. mariolua.name .. " Debug]\0")
        client.color_log(195, 195, 195, " ", J, "\0")
        client.color_log(126, 215, 135, " ")
        table.insert(cache_log.last_con_log, J)

        if mariolua.console ~= nil then
            mariolua.console.add_log(J)
        end

        local N = #cache_log.last_con_log
        client.delay_call(
            5,
            function()
                cache_log.last_con_log[N] = nil
            end
        )
    end
end

function mariolua.print(J)
    while not (J == cache_log.last_log_print) do
        mariolua.prefix()
        client.color_log(195, 195, 195, " ", J, "\0")
        cache_log.last_log_print = J
        client.color_log(126, 215, 135, " ")

        if mariolua.console ~= nil then
            mariolua.console.add_log(J)
        end
    end
end

function extended_table.merge(O, P)
    for Q, F in pairs(P) do
        if type(F) == "table" then
            if type(O[Q] or false) == "table" then
                extended_table.merge(O[Q] or {}, P[Q] or {})
            else
                O[Q] = F
            end
        else
            O[Q] = F
        end
    end

    return O
end

function string:split(R, S, T)
    assert(R ~= "")
    assert(S == nil or S >= 1)
    local U = {}

    if self:len() > 0 then
        local V = not T
        S = S or -1
        local W, X = 1, 1
        local Y, Z = self:find(R, X, V)

        while Y and S ~= 0 do
            U[W] = self:sub(X, Y - 1)
            W = W + 1
            X = Z + 1
            Y, Z = self:find(R, X, V)
            S = S - 1
        end

        U[W] = self:sub(X)
    end

    return U
end

function string:starts_with(J)
    return self:sub(1, J:len()) == J
end

function base64.stripchars(J, a0)
    local a1 = J:gsub("[" .. a0:gsub("%W", "%%%1") .. "]", "")

    return a1
end

function extended_table.spairs(a2, a3)
    local a4 = {}

    for Q in pairs(a2) do
        a4[#a4 + 1] = Q
    end

    if a3 then
        table.sort(
            a4,
            function(G, a5)
                return a3(a2, G, a5)
            end
        )
    else
        table.sort(a4)
    end

    local I = 0

    return function()
        I = I + 1
        if a4[I] then
            return a4[I], a2[a4[I]]
        end
    end
end

function extended_table.table_to_string3(a6, a7)
    local a8 = "{"

    if type(a6) == "string" then
        return a6
    end

    for Q, F in extended_table.spairs(a6) do
        if type(Q) == "string" and tonumber(Q) ~= nil then
            if math.abs(tonumber(Q)) >= 0 then
                a8 = a8 .. "[" .. Q .. "]" .. "="
            end
        elseif type(Q) == "string" then
            a8 = a8 .. "['" .. Q .. "']" .. "="
        end

        if type(F) == "table" then
            a8 = a8 .. extended_table.table_to_string3(F)
        elseif type(F) == "boolean" then
            a8 = a8 .. tostring(F)
        elseif type(F) == "string" then
            a8 = a8 .. "'" .. F .. "'"
        elseif type(F) ~= "function" then
            a8 = a8 .. F
        end

        a8 = a8 .. ","
    end

    if a8 ~= "" then
        a8 = a8:sub(1, a8:len() - 1)
    end

    if a8:len() <= 1 then
        a8 = "{"
    end

    return a8 .. "}"
end

globalvars.intersect_t = {}

function extended_math.inBounds(a9, aa, ab, ac, G)
    G = G or 50
    globalvars.intersect_t = globalvars.intersect_t or {}
    globalvars.intersect_t[a9 .. aa .. ab - a9 .. ac - aa] = {a9, aa, ab - a9, ac - aa, G}

    return (globalvars.mouse_x or 0) >= a9 and (globalvars.mouse_x or 0) <= ab and (globalvars.mouse_y or 0) >= aa and
        (globalvars.mouse_y or 0) <= ac
end

function extended_math.intersect(ad, ae, af, ag, ah, ai, G)
    G = G or 50
    globalvars.intersect_t = globalvars.intersect_t or {}
    globalvars.intersect_t[af .. ag .. ah .. ai] = {af, ag, ah, ai, G}

    return ad >= af and ad <= af + ah and ae >= ag and ae <= ag + ai
end

function extended_math.clamp(a9, aj, ak)
    a9 = a9 or 0
    ak = ak or 0
    aj = aj or 0

    return math.min(math.max(aj, a9), ak)
end

function extended_math.round(al, am)
    al = al or 0
    local an = 10 ^ (am or 0)

    return math.floor(al * an + 0.5) / an
end

base64.charlist = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
function base64.decode(ao)
    local ao = string.gsub(ao, "[^" .. base64.charlist .. "=]", "")

    return ao:gsub(
        ".",
        function(a9)
            if a9 == "=" then
                return ""
            end

            local ap, aq = "", base64.charlist:find(a9) - 1

            for I = 6, 1, -1 do
                ap = ap .. (aq % 2 ^ I - aq % 2 ^ (I - 1) > 0 and "1" or "0")
            end

            return ap
        end
    ):gsub(
        "%d%d%d?%d?%d?%d?%d?%d?",
        function(a9)
            if #a9 ~= 8 then
                return ""
            end

            local ar = 0

            for I = 1, 8 do
                ar = ar + (a9:sub(I, I) == "1" and 2 ^ (8 - I) or 0)
            end

            return string.char(ar)
        end
    )
end

mariolua.logo =
    "iVBORw0KGgoAAAANSUhEUgAAALQAAACyCAMAAADcShUdAAAA+VBMVEUAAAAAAQMAAQIAAAAAAgUACxQAEh8APGUAaK4AToMANlsAaK4AGisAZKYAFycASXkAJT4AER0AZ64ALk0AKEIAMlQASXkAZ6wAZ6wAbbcAQ3EAYqIAOV8AT4QAGisAMlIAZ60AQGoAWJMAXp0AVpEAaKwAY6wAZawAZK0AS30AQ3AAX6AAVo8AaKwAZqwASXkALUsAZqsAYaPj4+MAWZEBX5jS1NXKy8vb3Nzp6egAU4gAa7NunblMi66Fq8AUaJa1x9FZkrGXtcWlvswndKDJ0tY4faTD0Naeucd4pLxjl7QXb6AFZ56/y9Egb5uPsMMufKpEhKetws3DYSeQAAAAMXRSTlMAKR0QFDxUzen2uNZst2PggUlFppQ9tGj29JaUjH95UsVw5tO/ei8iEOvb9PSmWMlhAvE5dAAAEGBJREFUeNrU1tFqgzAYBeC/rReBkJtAZ0ootdvUQFF2BK+ym+127/8+k8LYVpiNMybxewA9hOMx5JupKynb/KptpaxqQ+kylSy14Iwp/KQYLxqdy5oSY6pcFwyjFBdlOsmrvOEKbhjXbfTgRmquMA0TZUXxDInxL6qIlLsuOWZQojUUmGwU5mI65HGbnMMPISkMU3L4U7S0PFMy+FUsftq/i7GKksgCy2hqWkjdYDGspEXkDDeSr3YtsDBVruyYcVVU5FHWIAh1JG/2HIFYkZEfZ4tgLN+TD9oipMPJQ52FRVh2drG3HMFZPfMTZIjAit2czAdEMG9ETgdEYp+2q8t8Tb2qbnylzlaXeSBosg3DRPFTZxzxaZqmsEjAMeX7xp9eEr3XjVJ79+FIJTPgPHxZ/OH49kBuRDoHPTiTi2NSmXHZrOFPeOvZZaGRmuPayoHB5d7ubVIrh0tBBBLUjS/IyeKeru86f3H63uV5j9uRzDt+/y0fr+9vvZ/YXf9Jm5n1Og0DUVhik1jE8gAPgIQEPCKBPONhHLK2SdMWusD//zHMjA1hKTcOiAO3V7pN4y/Hx+PxveirfeVxFvvpP/Qczn0sD8y8a/DfsZ0wf9xCgPUZZ9y2Yr18/05zWbVM9J6g/Ywz2DnJ+NQNwABA5ce52z1ZWu6mUUqm9yaGL/0c9SzzuQ1A75nFhENVXH07d/MPzNfmjK7ImPWVYTyi+yfofgxB7gQgd2SqZoL9ZLHRaZgVkDKD+gOwt2H+HroEAI1aUGpaoXMu1+r8jtT5bYI2canD/AP0iciCBoHfvx9W3l15O/dkudE2zJpjnqNKjMMsRE+Xu091jDOo1UQCPVHnF5C7s6MZNEPSrpBxENF79C6/zKmcQTMACbQmm1igr6Z2T5dvhgYd0xEgaDy2KCqKarvucrGdx74+nfVy92kPrFab0cAn72aoX95Y3HXYQmQCFleYQxBoIS6wBCaue0SXg/xpPxLR7ohid8UAMR/EQLV3SRkdSH575/yGAwDpWlfoSpCrNdgcUwa2Q1+1oPYCVYi4EV6ZOZ00COMG3Qy1e714GZpRHQksEYnTMm7RrCCkSAJ1iHPMJTMRmDqhLgGs5DPAF91hZ6kfL1uGJieYpwA6CLSbArG2qQ1xZ6fNzJ6GFQQ2RqYAQoldC0GzMVYocpPml2L+rzpQArEfGMLQYSH/vlhYAKJdJxn2SugTWB5sXYRaF4TfEwDXTRGhJ+UtxWcZ0B98Idq0Q30sbJg1W8ABBAJgq8NeBb0mChousLVbmI67VYcm72ap3YuF6bBhRQXWJzSXEL/w+2lbV+irqH0zCrTGmDTZtd5Lp6uPd3W/aCYfuelwqKBYDkNZFDZeO0FThBa7roBmUGj5iEEb7GY1rjZYILoc6gfXl57BnRbX8xZCgNXGsA3aqBkU+tJO7EwGfWAGhVaxOY2llelOzcjx2t1c+Jsw69pLFma1q2wEWhsokz5IKBU6jT6tg7RvO4uHIrJ9xpzerOLjQlthltfuYXYnPR0PV5FZNUjNqjhRs/5UtxcsjlXVC+R3m/2nc1V5fQvXzKCyuamaJtZpAhHvFHqe+klu3zFB75jYmDlR7xnYKkgIYp2WlG6kw3qvkHFu0FfbcdhuUI0dgMlyDWOHWFlUEvShQ5dB/fLaokhbP03fS4XAlrqOtjbhAG1XiPq0TZ+OWg40FefTOIwEvPqoV39hMFll3kVmDhrrw8q7DGr3aIJ+nQX9aUvf0qBml5qG4vMBgmZSmXWXNwXaNVp6tf1ksir+5ahLt2p1jirrZ2tgmyObOhLoDGr3MD/SJncegdO6+34IwAK7WpD1AUoFMMn3tVLXwT7BQakbq5hVXdo0OF/bpGnroiET6BzqO9+hb37Igu4oQlOYoBW7MAg8EpAoQjN3kpYhHgAhxK5Qr1IZH3apvhMl6BzqBwv/LuT2pFNNxhAitFfLEkQ/clpYBAItO1DT2tTb9elMGZOeoO1Ub+JBoHOo33w/dD3Ngz4frAkFJpVBGLWhiNPD1A8Rg0FHIvVeq5r7ESwuRAKIma7R5VBPK/F1HjSWxMxpTyPaJOf0v1enB3Pajqv2TNjb5Ef7DfpH4Wdmqx6gao/ocqjdvcR8/VUmNHZjADPSCitGWpde/BdjpjgTvNPCHISLlJvTGpikVZ4pmk21Ls4s6uf5xcOEWDR7ZWLrgeMwE7ZP514pBxqjXYHVIUAq7cRs2/wkj7hpAUiLeI+FvJlFfWfhn4acNpObtSCve6sZk9MqrIEStDJWBfbxBGnOA21+g8amHAFoLzdzoizqt/mbuOmDUWO117pcfPMmvSrEyRZdYDW3TBs3sD2Cov0cAG/t1cdaO9MCBSuP+kE6vbwT6DypO0Wvo3gs0gpMr7bPfG4hFrlhj6ZKqVnj1F/qiBDx3PkCRT7T69vXUpleAu1L4q200wr9s9POGmQW6rE8YtNZk9TXpLXh86V1ZhvkqFlDfTuPGlOhfp4JrVN6Xgeh4n2Mx+R0wtAWajj1EtYWoO66zx6r0+HUXGDSi/va4lQ2iC5DsVBn7y0m7z+WFMgS0Op5/BenfXRPCKSdS/vml0p/4C/a3NQ6LXpVu/FZ2Ap9ayF0rAbqjbzsEI31J6djUPsRDBqIAtT+Io4wrw05nYTyvJ6gn+RBW1/GRMyx+H7GH0h/ikptLFbulOcijramJruSaY8ui9rdXwbdD8zvo4jV6sSYuKeobIEZEhIP6+YitN8yMCTRsP1P0OcBbPuQLxCVf3BaoSkebphE7UVo5+Oplq09p2GFLk/3lkF/Whs0w3s26MtOGw6LKM3JH6BxB6roAB1O/wn6Y5saPOtP/+w0noAjr6XkMrTHNYg4GsDjyf8f6LMGw8TA/GenbTsnO7CAaIV/cFrf1RWdzoj/y2kmYmuKBOkKp2sAuyLWj90foDuIiSZKh4A83V9Ypys7ZyRzvvJyrrtNA0EUbq4oCAEVCAHiF/CHiwTezhjLceI0dpM4N0jf/2HYPbuu1bqb3bAiU3MTqvPp6Hg8MzuwK61Ko07SBWoc70oLtGrGklrpYv+/oGmxQs+qUtmuTC1KK55KzmVUoFa2QFM5Q2rE+5VTT2genlh7UCqx8T5YYpZnyx4SKJeVk4zZhG0FHBPtpzeQucqJt8Iv6PLEKg913DqRVRxLZLIqjTr1MMPLmdkKzYSWolgQcTQeezFH3wen1dOmAl5kG0yPtmxTGr8SL3I9lbFAsy7BJznKafZCFtGoc1rnwlCQJlkJ5q2wK62xyUVCRIesRBPgWzC96KIXvxi8828BaFHIxDFRPmE6qrQ7KE3zaSzNlnszC/GjfwHqjnc3zpvKzPFyNLY2pX2Z1UgiUed7XtCRup5q5ovup7En9GSGtjWRYlf5kezhE0ylKmFxuzhjYj+lX5tZTe+zJ/Qh1ieBqCmKPEhpvIJwxo5isGJyAkPpl5rZe5Yn5pIZyqAZuLYo7Rk8T+pZ5q8Yd/NxBw3rsZjn1HR7Kz+mmUCu+FGl/aHVdDqJ9ZApXjV/0fq50VmMBob54tIbWn0OfIi1kiClKVPAd0VjxV5Kf0XG800fqPKwdKS7P2s97RlMFU4V68J7TUd5a6Wf9mvo/uexX7uFj9D+aHUuJwZJeyhf/DLUjnraMMvnsFenj/d+0Dt5e0CjomwrfSo0xsCgxuKVW+eIhhIXzJ7Vh9hW8d1hpfwxCVS6Qt+rpVaDYIfOuL50erU9fE29neo2CtXpnIKUFmo5NpG+APMyY3LojOtJH9Cg7vqZmlmeFGrqZEppkNKCKTdtWYKjUbYnoKixdK9RuvfBzx+UlnNk6tmBHtYep0MTXauOC2fO97+f2zobS4PZYHv9y9Sx/L40ncx2sjsiSkOVRoMzXc50eR65/IwsDUvXOa/zycseIqKU8jJXynDj6X8Pov2eEJHDz7hewdIm+v33flsq6lOq9YbS5ukL4SYqs2t0vhy5dRb0HJauTd336V6Y5NdElsDLNcHQgUoz0Vp2tjfZliQ3O3UWLzr31sWkP8Y+0H/mpufHGT7j69+ZywJ3kz37461L9EDp130DbUzd98gfKuPpAk+dyuVpoKdJ3S1JcHZebFrQjcpN7tCWbqgHb92exnp9XSzMchIhSnO9WQMRCqaWlR+CP+lq6MbU7veL2NzGiRmbyiuZkggITLGNAGp0OqEHQrcuelm7o8kfP8fuJZW6AMaeZsFh6e62hkYbkJHdzfrPXwYQuokeUrULWs8V67p9xWFKF0ntNXWsVPFRmZGkW1vfeBQd0PEdMx5FFkGhoWOzqVKwg3l02YKWpu58dG7WmO0NjHCTFQVCX5lRN6hXLqWfdrv33YFKzyW1WNyoBGXMEV+FPojK02bl5QrQtlyHKx223dG76HcH3xxSVxiuQGn5m0MQNM4LEBr6mmzPIK620KDudbsOqYn0UEz35BlxGPSmADNyB851DbOv0MbVg4+u8XRZ4IAVu+qB0IJ4cou2Xq0tELEV2QgtoduhpHZvfWdLtcP4G9VpYLBaGAJynhIB2XaNhvpt2HY1EogLO91PUbYLDobGZk2m7pY6hOZXFqHh6mfulY/WADwomPQNrVnDvAwfF9rk6jdOamFBDg67O+ilEdri6stvkRMbcS5klHcdm9BwNZ7F81K7mUdDI7TV1Z0f56V2M9NrMB+lHn5zUp+TGebAy9BOrQwinNTnY3aYw0DDIOelBp6NGpnD9T/RSerBx7NSA86mN/9t1m5WIwWiMAxzuimLKsQf7LWL2SkDQ6TceP8XNvGo8zl2UqfkgLHagMuHly+akHhhHJx6bl2MV6o/Yql7Ex8HXuaYtU6tN7tMGgfei+SvUsP3lXpqH4RxiOo/F6k/Itf8hKbPcUAdf1g/THmBGjz+vJkbwvtbOjzr7NcFamzjq87+jPm5qH9r1PrOozWEQaeqC4Va0xlmDDptILM6P6nWd4Z6M6ef5+elUCs6w4zOpwYyL8SlqvWd96XDYgY5Vf2c1VmXqNZ33qmDF8zx3wioLtPU+s6gh4YEs7AQMnaQ2frOu9LTK2KW2aymJiSo9Z03vGtls9yaqoSHiL7zco19tpojavmna6JHUcoT0T6f8S0odZZbs5qMDwq11Bl6x3PGO0XVmqhyYuxzZuTFv6/Z7GhWDru2YuyTZqiRmc2HOWseIlR1UuxT5sNdsAW9zVk/bKrFZUtimA/Nx27JjMeGnr3Fzq2KfZCi9NTU9D5n/bCX2FSVcXZ6adxNthAya2ObVzmKbLkzrmBz2jJHzJrYzI7XTlv0VhlkAayMTaaKbluIjHmMzhcbWcisiM1sPrl346BTh7LJiFLWrN/Ixs5efRiGqHqE+Jg5ONsaOr0MPdsUTfmNO1aaxa85MsjJaj2b3f00DjH14Zl8EF9BxrbhzipfTnPxiJq/GNy0Ne3JF4hRG7kZ3jaW5W/q9SYE1/tXPieG+Hxlfe01N+R51Xhbdi6EMGx/251c11vftEVtCOdyMdhw7+2mzoo8b+eT50VWm0ULMM9CS9b3fpB8AJ7FOrCezcFl+YPB6yp+/DyXpaxy4IFdvVz4DuL/4Ez/pwd28d4KfKAzH4fr3qtwTM6feyz4Fucvi0/xEgmUKwkAAAAASUVORK5CYII="
mariolua.logo = base64.decode(mariolua.logo)
mariolua.notify =
    (function(as, J)
    local G = {callback_registered = false, maximum_count = 7, data = {}}

    function G:register_callback()
        if self.callback_registered then
            return
        end

        client.set_event_callback(
            "paint_ui",
            function()
                local a5 = {client.screen_size()}
                local ar = {0, 132, 235}
                local at = 5
                local au = self.data

                for aq = #au, 1, -1 do
                    self.data[aq].time = self.data[aq].time - globals.frametime()
                    local av, aw = 180, 0
                    local I = au[aq]

                    if I.time < 0 then
                        table.remove(self.data, aq)
                    else
                        local ax = I.def_time - I.time
                        local ax = ax > 1 and 1 or ax

                        if I.time < 0.5 or ax < 0.5 then
                            aw = (ax < 1 and ax or I.time) / 0.5
                            av = aw * 255
                            if aw < 0.2 then
                                at = at + 15 * (1.0 - aw / 0.2)
                            end
                        end

                        local Q = {renderer.measure_text("c", I.draw)}
                        local ay = {a5[1] / 2 - Q[1] / 2 + 3, a5[2] - a5[2] / 100 * 17.4 + at}
                        renderer.circle(ay[1] - 30, ay[2], ar[1], ar[2], ar[3], av, 20, 180, 0.5)
                        renderer.circle(ay[1] + Q[1], ay[2], ar[1], ar[2], ar[3], av, 20, 0, 0.5)
                        renderer.rectangle(ay[1] - 30, ay[2] - 20, Q[1] + 30, 40, ar[1], ar[2], ar[3], av)
                        local az = renderer.load_png(mariolua.logo, 180, 180)
                        renderer.texture(az, ay[1] - 50, ay[2] - 20, 40, 40, 255, 255, 255, av, "f")
                        renderer.text(ay[1] + Q[1] / 2, ay[2], 255, 255, 255, av, "c", nil, I.draw)
                        at = at - 50
                    end
                end

                self.callback_registered = true
            end
        )
    end

    function G:paint(aA, aB)
        local aC = tonumber(aA) + 1

        for aq = self.maximum_count, 2, -1 do
            self.data[aq] = self.data[aq - 1]
        end

        self.data[1] = {time = aC, def_time = aC, draw = aB}
        self:register_callback()
    end

    local aD = function(as, J)
        return G:paint(as, J)
    end

    return aD
end)()

mariolua.notify(5, "Welcome " .. mariolua.userdata.name or "Unknown" .. "!")
globalvars.active_events = {}
globalvars.event_callback = {}
globalvars.ui_callback = {}
globalvars.old_client_set_event_callback = client.set_event_callback
globalvars.old_client_unset_event_callback = client.unset_event_callback
globalvars.old_ui_set_callback = ui.set_callback
function util_functions.IsEventActive(aE)
    return extended_table.Contains(globalvars.active_events, aE)
end

function ui.set_callback(aF, callback, aG)
    if mariolua.is_library then
        globalvars.old_ui_set_callback(
            aF,
            function()
                if globalvars.ui_callback[aF] == nil then
                    aG = aG or ""
                    globalvars.ui_callback[aF] = globalvars.ui_callback[aF] or {}
                    globalvars.ui_callback[aF]["last_update"] =
                        globalvars.ui_callback[aF]["last_update"] or globals.realtime()
                    globalvars.ui_callback[aF]["module"] = aG
                    globalvars.ui_callback[aF]["callback"] = callback
                else
                    globalvars.ui_callback[aF]["last_update"] = globals.realtime()
                end
            end
        )

        globalvars.old_ui_set_callback(aF, callback)
    else
        client.delay_call(
            .5,
            function()
                ui.set_callback(aF, callback)
            end
        )
    end
end

function client.set_event_callback(aE, callback, aG)
    if mariolua.is_library then
        globalvars.old_client_set_event_callback(
            aE,
            function()
                if globalvars.event_callback[aE] == nil then
                    aG = aG or ""
                    globalvars.event_callback[aE] = globalvars.event_callback[aE] or {}
                    globalvars.event_callback[aE]["last_update"] =
                        globalvars.event_callback[aE]["last_update"] or globals.realtime()
                    globalvars.event_callback[aE]["module"] = aG
                    globalvars.event_callback[aE]["callback"] = callback

                    if not extended_table.Contains(globalvars.active_events, aE) then
                        table.insert(globalvars.active_events, aE)
                    end
                else
                    globalvars.event_callback[aE]["last_update"] = globals.realtime()
                end
            end
        )

        globalvars.old_client_set_event_callback(aE, callback)
    else
        client.delay_call(
            .5,
            function()
                client.set_event_callback(aE, callback)
            end
        )
    end
end

function client.unset_event_callback(aE, callback, aG)
    aG = aG or ""
    globalvars.old_client_unset_event_callback(
        aE,
        function()
            globalvars.event_callback[aE]["active"] = false
            globalvars.event_callback[aE]["module"] = aG

            if extended_table.Contains(globalvars.active_events, aE) then
                globalvars.active_events[aE] = nil
            end
        end
    )

    globalvars.old_client_unset_event_callback(aE, callback)
end

mariolua.env = getfenv(1)

function mariolua.findloader(aU)
    local aV
    local aW

    for _, a1 in ipairs(package.searchers) do
        aV, aW = a1(aU)
        if type(aV) == "function" then
            return aV, aW
        end
    end

    return package.loaders[aU]
end

function mariolua.add_module(aU)
    mariolua.module[aU] = {["loaded"] = false, ["data"] = nil, ["loadstring"] = nil}

    if aU == "library" then
        mariolua.notify(5, "Loading library. Please wait...")
    end

    mariolua.log("Adding module '" .. aU .. "'...")

    local debug = true
    local code = debug and readfile("modules/" .. aU .. ".lua") or _G["mario"]["getModule"](aU)

    if (code ~= "") then
        mariolua.module[aU].data = code
        mariolua.module[aU].added = true
        mariolua.log("Module '" .. aU .. "' pre-loaded.")
    end
end

function mariolua.load_module(aU)
    if mariolua.module[aU] == nil then
        return mariolua.print("Could not run module '" .. aU .. "'")
    end

    if mariolua.module[aU].data then
        if not mariolua.module[aU].loaded then
            if not package.preload[aU] then
                mariolua.module[aU].loadstring = loadstring(mariolua.module[aU].data)()
                package.preload[aU] = not package.preload[aU] and mariolua.module[aU].loadstring or package.preload[aU]
            end

            if not package.loaded[aU] then
                local aV = mariolua.findloader(aU)

                if aV == nil then
                    mariolua.log("Unable to load module " .. aU)
                end

                package.loaded[aU] = true
                mariolua.module[aU].loaded = true
                local aX = aV

                if aX ~= nil then
                    package.loaded[aU] = aX
                end
            end

            mariolua.log("Running module '" .. aU .. "'.")
        end

        package.preload[aU].client = package.preload[aU].client or {}
        package.preload[aU].client.set_event_callback = function(aE, callback, aU)
            return client.set_event_callback(aE, callback, aU)
        end

        package.preload[aU].client.unset_event_callback = function(aE, callback, aU)
            return client.unset_event_callback(aE, callback, aU)
        end

        local aY = mariolua.module[aU].loadstring

        if aY == nil then
            return mariolua.log("Error while running module '" .. aU .. "'")
        end

        package.loaded[aU] = package.preload[aU]

        return package.loaded[aU]
    elseif mariolua.module[aU].loadstring then
        package.loaded[aU] = true
        mariolua.module[aU].loaded = true
        package.preload[aU] = not package.preload[aU] and mariolua.module[aU].loadstring or package.preload[aU]
        package.loaded[aU] = package.preload[aU]

        return package.loaded[aU]
    else
        client.delay_call(
            1,
            function()
                mariolua.load_module(aU)
            end
        )
    end
end

function mariolua.unload_module(aU)
    if (not mariolua.module[aU]) then
        return
    end

    if not mariolua.module[aU].loaded then
        return
    end

    mariolua.module[aU].loaded = false
    mariolua.module[aU].data = nil

    for Q, F in pairs(globalvars.event_callback) do
        if F["module"] == aU then
            client.unset_event_callback(Q, F["callback"], F["module"])
            mariolua.log('Module "' .. aU .. '" unset callback "' .. Q .. '".')
        end
    end

    package.loaded[aU] = nil
    setfenv(1, mariolua.env)
    mariolua.log('Module "' .. aU .. '" unloaded.')
end

mariolua.add_module("library" .. (debug and "_dev" or ""))
mariolua.add_module("svg")

function util_functions.load_library()
    local aZ = mariolua.load_module("library" .. (debug and "_dev" or ""))
    if aZ then
        extended_math = extended_table.merge(extended_math, aZ.cmath)
        Vector2 = aZ.Vector2
        Vector3 = aZ.Vector3
        B = extended_table.merge(B, aZ.vector)
        extended_renderer = extended_table.merge(extended_renderer, aZ.crender)
        z = aZ.Angle
        external_modules = extended_table.merge(external_modules, aZ.cangle)
        functions = extended_table.merge(functions, aZ.centity)
        base64 = extended_table.merge(base64, aZ.cstring)
        extended_table = extended_table.merge(extended_table, aZ.ctable)
        y = aZ.cweapon
        mariolua.is_library = true
        mariolua.notify(5, "Done!")
        return
    else
        client.delay_call(0.7, util_functions.load_library)
    end
end
util_functions.load_library()
function util_functions.load_svg()
    local a_ = mariolua.load_module("svg")
    if a_ then
        extended_renderer.gpx = extended_renderer.gpx or {}
        extended_renderer.gpx.svg = mariolua.load_module("svg")
    else
        client.delay_call(1, util_functions.load_svg)
    end
end
util_functions.load_svg()
mariolua.add_module("dormant_aim" .. (debug and "_dev" or ""))
mariolua.add_module("resolver" .. (debug and "_dev" or ""))
mariolua.add_module("angle_indicator")
mariolua.add_module("force_onshot")
mariolua.add_module("sv_pure_fix")
mariolua.add_module("freezetime_antiaim" .. (debug and "_dev" or ""))
mariolua.add_module("e_peek" .. (debug and "_dev" or ""))
mariolua.add_module("antibruteforce")
mariolua.add_module("bodyaim_if_lethal")
mariolua.add_module("penetration_damage")
mariolua.add_module("antiaim" .. (debug and "_dev" or ""))
local b0 = {}
b0.__index = b0
b0.new_charbuffer = ffi.typeof("char[?]")
b0.new_intptr = ffi.typeof("int[1]")
b0.new_widebuffer = ffi.typeof("wchar_t[?]")
b0.native_Surface_CreateFont =
    vtable_bind("vguimatsurface.dll", "VGUI_Surface031", 71, "unsigned int(__thiscall*)(void*)")
b0.native_Surface_SetFontGlyph =
    vtable_bind(
    "vguimatsurface.dll",
    "VGUI_Surface031",
    72,
    "void(__thiscall*)(void*, unsigned long, const char*, int, int, int, int, unsigned long, int, int)"
)
b0.play_sound = vtable_bind("vguimatsurface.dll", "VGUI_Surface031", 82, "void(__thiscall*)(void*, const char*)")
local b1 = ui.reference("PLAYERS", "Players", "Player list")
function util_functions.steam_64(b2)
    if not b2 then
        return
    end
    local aa = 0
    local b3 = 0
    if b2 % 2 == 0 then
        aa = 0
        b3 = b2 / 2
    else
        aa = 1
        b3 = (b2 - 1) / 2
    end
    return "7656119" .. b3 * 2 + 7960265728 + aa
end
globalvars.ref = {
    removerecoil = ui.reference("RAGE", "Other", "Remove recoil"),
    targethitbox = ui.reference("RAGE", "Aimbot", "Target hitbox"),
    multipoint = ui.reference("RAGE", "Aimbot", "Multi-Point"),
    multipoint_scale = ui.reference("RAGE", "Aimbot", "Multi-Point scale"),
    prefer_safepoint = ui.reference("RAGE", "Aimbot", "Prefer safe point"),
    avoid_hitbox = ui.reference("RAGE", "Aimbot", "Avoid unsafe hitboxes"),
    automatic_fire = ui.reference("RAGE", "Other", "Automatic fire"),
    silentaim = ui.reference("RAGE", "Other", "Silent aim"),
    hitchance = ui.reference("RAGE", "Aimbot", "Minimum hit chance"),
    min_dmg = ui.reference("RAGE", "Aimbot", "Minimum damage"),
    automatic_scope = ui.reference("RAGE", "Aimbot", "Automatic scope"),
    quickstop = ui.reference("RAGE", "Aimbot", "Quick stop"),
    quickstop_options = ui.reference("RAGE", "Aimbot", "Quick stop"),
    prefer_bodyaim = ui.reference("RAGE", "Aimbot", "Prefer body aim"),
    prefer_bodyaim_disablers = ui.reference("RAGE", "Aimbot", "Prefer body aim disablers"),
    double_tap = {ui.reference("RAGE", "Aimbot", "Double tap")},
    double_tap_mode = ui.reference("RAGE", "Aimbot", "Double tap"),
    double_tap_hitchance = ui.reference("RAGE", "Aimbot", "Double tap hit chance"),
    double_tap_fakelaglimit = ui.reference("RAGE", "Aimbot", "Double tap fake lag limit"),
    double_tap_quick_stop = ui.reference("RAGE", "Aimbot", "Double tap quick stop"),
    double_tap_hit_chance = ui.reference("RAGE", "Aimbot", "Double tap hit chance"),
    double_tap_fake_lag_limit = ui.reference("RAGE", "Aimbot", "Double tap fake lag limit"),
    correction_active_ref = ui.reference("PLAYERS", "Adjustments", "Correction active"),
    plist_resetall_ref = ui.reference("PLAYERS", "Players", "Reset all"),
    fakelag_limit = ui.reference("AA", "Fake lag", "Limit"),
    thirdperson_death_ref = ui.reference("Visuals", "Effects", "Force third person (dead)"),
    slidewalk = ui.reference("AA", "Other", "leg movement")
}
globalvars.ref.misc = {
    infinite_duck = ui.reference("MISC", "Movement", "Infinite duck"),
    namesteal = ui.reference("MISC", "Miscellaneous", "Steal player name"),
    sv_maxusrcmdprocessticks_holdaim = ui.reference("MISC", "Settings", "sv_maxusrcmdprocessticks_holdaim"),
    sv_maxusrcmdprocessticks = ui.reference("MISC", "Settings", "sv_maxusrcmdprocessticks2")
}
globalvars.ref.rage = {
    fakeduck = ui.reference("RAGE", "Other", "Duck peek assist"),
    autowall = ui.reference("RAGE", "Other", "Automatic Penetration"),
    force_baim_key = ui.reference("RAGE", "Aimbot", "Force body aim"),
    aimbot_enabled = {ui.reference("RAGE", "Aimbot", "Enabled")},
    rage_fov = ui.reference("RAGE", "Other", "Maximum FOV"),
    accuracyboost = ui.reference("RAGE", "Other", "Accuracy boost"),
    delay_shot = ui.reference("RAGE", "Other", "Delay shot"),
    forcesafepoint = ui.reference("RAGE", "Aimbot", "Force safe point"),
    resolver_ref = ui.reference("RAGE", "Other", "Anti-aim correction")
}
globalvars.ref.aa = {
    enabled = ui.reference("aa", "anti-aimbot angles", "enabled"),
    pitch = ui.reference("aa", "anti-aimbot angles", "pitch"),
    yaw_base = ui.reference("aa", "anti-aimbot angles", "yaw base"),
    yaw = {ui.reference("aa", "anti-aimbot angles", "yaw")},
    jitter = {ui.reference("aa", "anti-aimbot angles", "yaw jitter")},
    body_yaw = {ui.reference("aa", "anti-aimbot angles", "body yaw")},
    freestanding_body_yaw = ui.reference("aa", "anti-aimbot angles", "freestanding body yaw"),
    freestanding = {ui.reference("aa", "anti-aimbot angles", "freestanding")},
    edge = ui.reference("aa", "anti-aimbot angles", "edge yaw"),
    onshot_aa = {ui.reference("AA", "Other", "On shot anti-aim")},
    slow_motion = {ui.reference("AA", "other", "slow motion")},
    fakelag = {
        amount = ui.reference("AA", "fake lag", "amount"),
        limit = ui.reference("AA", "fake lag", "limit"),
        variance = ui.reference("AA", "fake lag", "variance")
    }
}

globalvars.TRUE = {
    ["1"] = true,
    ["t"] = true,
    ["T"] = true,
    ["true"] = true,
    ["TRUE"] = true,
    ["True"] = true
}
globalvars.FALSE = {
    ["0"] = false,
    ["f"] = false,
    ["F"] = false,
    ["false"] = false,
    ["FALSE"] = false,
    ["False"] = false
}

local function b4(J)
    if type(J) == "boolean" then
        return J
    end

    assert(type(J) == "string", "str must be string")

    if globalvars.TRUE[J] == true then
        return true
    elseif globalvars.FALSE[J] == false then
        return false
    else
        return false, string.format("cannot convert %q to boolean", J)
    end
end

globalvars.old_key_state = client.key_state
globalvars.block_key_state = {}

function client.key_state(H, b5)
    b5 = b5 or false
    globalvars.block_key_state[H] = globalvars.block_key_state[H] or false

    if b5 then
        return globalvars.old_key_state(H)
    else
        return globalvars.block_key_state[H] and "false" or globalvars.old_key_state(H)
    end
end

globalvars.event_list_x, globalvars.event_list_y, globalvars.event_list_w = 1200, 15, 100

function util_functions.draw_event_list()
    local aw = 24 + #globalvars.active_events * 10
    extended_renderer.watermark_container(
        globalvars.event_list_x,
        globalvars.event_list_y,
        globalvars.event_list_w,
        aw + 5,
        230,
        true,
        "Active Events"
    )

    for I = 1, #globalvars.active_events do
        local ap, av, a5 = 255, 255, 255
        local aE = globalvars.active_events[I]
        renderer.text(globalvars.event_list_x + 5, globalvars.event_list_y + 13 + I * 10, ap, av, a5, 250, nil, 0, aE)
    end

    if ui.is_menu_open() then
        if globalvars.mouse1_key_state then
            local b6, b7 = ui.mouse_position()
            if
                b6 > globalvars.event_list_x and b6 < globalvars.event_list_x + globalvars.event_list_w and
                    (b7 > globalvars.event_list_y and b7 < globalvars.event_list_y + aw)
             then
                globalvars.event_list_x = b6 - globalvars.event_list_w / 2
                globalvars.event_list_y = b7 - aw / 2
                database.write("mariolua.debug.event_list", {globalvars.event_list_x, globalvars.event_list_y})
            end
        end
    end
end

function util_functions.debug_event_infos()
    for I = 1, #globalvars.active_events do
        local aE = globalvars.active_events[I]

        if globalvars.event_callback[aE] == nil then
            return
        end

        globalvars.event_callback[aE] = globalvars.event_callback[aE] or {}

        if globalvars.event_callback[aE]["last_update"] == globals.realtime() - 1 then
            globalvars.event_callback[aE]["last_update"] = globals.realtime()
            globalvars.event_callback[aE]["active"] = true

            if not extended_table.Contains(globalvars.active_events, aE) then
                table.insert(globalvars.active_events, I)
                mariolua.log('Event "' .. aE .. '" added!')
            end
        elseif globalvars.event_callback[aE]["last_update"] < globals.realtime() - 1 then
            globalvars.event_callback[aE]["active"] = false

            if extended_table.Contains(globalvars.active_events, aE) then
                table.remove(globalvars.active_events, I)
                globalvars.event_callback[aE] = nil
                mariolua.log('Event "' .. aE .. '" removed!')
            end
        end
    end

    globalvars.event_list_x, globalvars.event_list_y = unpack(database.read("mariolua.debug.event_list") or {1200, 15})

    if mariolua.debug.draw["event_infos"] then
        util_functions.draw_event_list()
    end
end

globalvars.color = {}
globalvars.color[1], globalvars.color[2], globalvars.color[3], globalvars.color[4] =
    ui.get(ui.reference("MISC", "Settings", "Menu color"))

function draw_tracers()
    if not tracer_enable:GetValue() then
        return
    end

    for I, F in ipairs(tracers) do
        if globals.CurTime() < F.time + tracer_duration:GetValue() then
            local a9, aa = client.WorldToScreen(F.hit_pos)
            local b8, b9 = client.WorldToScreen(F.lp_pos)

            if a9 and b8 and aa and b9 then
                draw.Color(unpack(F.color))
                draw.Line(a9, aa, b8, b9)
            end
        else
            table.remove(tracers, I)
        end
    end
end

local ba = {}

function extended_renderer.create_font(bb, bc, bd, be)
    local bf = 0
    local a2 = type(be)

    if a2 == "number" then
        bf = be
    elseif a2 == "table" then
        for I = 1, #be do
            bf = bf + be[I]
        end
    else
        error("invalid flags type, has to be number or table", 2)
    end

    if bc == nil then
        bc = 0
        bd = 0
        bf = 0
    end

    local bg = string.format("%s\0%d\0%d\0%d", bb, bc, bd, bf)

    if ba[bg] == nil then
        ba[bg] = b0.native_Surface_CreateFont()
        b0.native_Surface_SetFontGlyph(ba[bg], bb, bc, bd, 0, 0, bit.bor(bf), 0, 0)
    end

    return ba[bg]
end

globalvars.get_report_req = {}
globalvars.report_delay = false
globalvars.retry_count = 0

function util_functions.do_request()
    if globalvars.get_report_req.finished then
        local bh = json.parse(globalvars.get_report_req.code).success and "Success" or "Failed"
        local bi = json.parse(globalvars.get_report_req.code).message or ""
        local bj = "[Reportbot] " .. bh .. "! " .. bi
        mariolua.print(bj)
        mariolua.PrintInChat(bj)
    else
        globalvars.retry_count = globalvars.retry_count + 1
        client.delay_call(globalvars.retry_count == 1 and 0.5 or 3, util_functions.do_request)

        if globalvars.retry_count == 1 then
        elseif globalvars.retry_count <= 10 then
        else
            return
        end
    end
end

function util_functions.community_report_all_enemies(ent)
    if globalvars.report_delay then
        return
    end

    globalvars.report_delay = true
    mariolua.userdata.token = database.read("mariolua_db_token")

    if mariolua.userdata.token == "" then
        mariolua.print("[Reportbot] API Token not set!")
        mariolua.PrintInChat("[Reportbot] API Token not set!")
        return
    end

    if ent then
        if functions.is_bot(ent) then
            return
        end

        local bk = util_functions.steam_64(entity.get_steam64(ent))
        local bl = "[Reportbot] Reporting Player: '" .. entity.get_player_name(ent) .. "' | Steam64: '" .. bk .. "'"
        mariolua.print(bl)
        mariolua.PrintInChat(bl)
        local bm =
            base64.decode("aHR0cHM6Ly9hcGkubWFyaW9sdWEuY2YvcmVwb3J0P3N0ZWFtX2lkPQ==") ..
            bk .. "&token=" .. mariolua.userdata.token
        globalvars.get_report_req =
            panorama.loadstring(
            [[
			let Status = {
				finished: false
			};
			$.AsyncWebRequest("]] ..
                bm ..
                    [[", {
					type:"GET",
					complete:function(e){
						Status.finished = true;
						Status.code = e.responseText;
					},
					timeout: 6000
				}
			);
			return Status;
		]]
        )()
        util_functions.do_request()
    end

    client.delay_call(
        1,
        function()
            globalvars.report_delay = false
        end
    )
end

function util_functions.apply_community_reportbot()
    if globalvars.report_delay then
        return
    end

    globalvars.report_delay = true
    mariolua.userdata.token = database.read("mariolua_db_token")

    if mariolua.userdata.token == "" then
        mariolua.print("[Reportbot] API Token not set!")
        mariolua.PrintInChat("[Reportbot] API Token not set!")
        return
    end

    local plist = ui.get(b1)

    if plist then
        if functions.is_bot(plist) then
            return
        end

        local bk = util_functions.steam_64(entity.get_steam64(plist))
        local bl = "[Reportbot] Reporting Player: '" .. entity.get_player_name(plist) .. "' | Steam64: '" .. bk .. "'"
        mariolua.print(bl)
        mariolua.PrintInChat(bl)
        local bm =
            base64.decode("aHR0cHM6Ly9hcGkubWFyaW9sdWEuY2YvcmVwb3J0P3N0ZWFtX2lkPQ==") ..
            bk .. "&token=" .. mariolua.userdata.token
        globalvars.get_report_req =
            panorama.loadstring(
            [[
			let Status = {
				finished: false
			};
			$.AsyncWebRequest("]] ..
                bm ..
                    [[", {
					type:"GET",
					complete:function(e){
						Status.finished = true;
						Status.code = e.responseText;
					},
					timeout: 6000
				}
			);
			return Status;
		]]
        )()
        util_functions.do_request()
    end

    client.delay_call(
        1,
        function()
            globalvars.report_delay = false
        end
    )
end

ui.new_button(
    "Players",
    "Adjustments",
    "Apply Community Reportbot",
    function()
        util_functions.apply_community_reportbot()
    end
)

local chathud_colours = {
    white = "\x01",
    red = "\x02",
    purple = "\x03",
    green = "\x04",
    light_green = "\x05",
    turquoise = "\x06",
    light_red = "\x07",
    gray = "\x08",
    yellow = "\x09",
    light_steel_blue = "\x0A",
    light_blue = "\x0B",
    blue = "\x0C",
    dark_purple = "\x0D",
    pink = "\x0E",
    dark_orange = "\x0F",
    orange = "\x10"
}
mariolua.chathud_prefix = {
    [1] = string.format(
        "%s[Mario%sLua%s] %s",
        chathud_colours.light_blue,
        chathud_colours.white,
        chathud_colours.light_blue,
        chathud_colours.gray
    ),
    [2] = string.format(
        "%s[Mario%sLua%s] %s",
        chathud_colours.blue,
        chathud_colours.white,
        chathud_colours.blue,
        chathud_colours.gray
    ),
    [3] = string.format(
        "%s[Debug Mario%sLua%s] %s",
        chathud_colours.light_blue,
        chathud_colours.white,
        chathud_colours.light_blue,
        chathud_colours.gray
    )
}

function mariolua.PrintInChat(bo)
    if bo == nil then
        return
    end

    return ffi_utils.print_to_chat(ffi_utils.hudchat, 0, 0, mariolua.chathud_prefix[1] .. bo)
end

function mariolua.PrintInLobbyChat(bp)
    return PartyListAPI.SessionCommand(
        "Game::Chat",
        string.format("run all xuid %s chat %s", MyPersonaAPI.GetXuid(), bp:gsub(" ", " "))
    )
end

local mariolua_menu = {
    Reference = {},
    Groupbox = {},
    Window = {},
    Tab = {},
    Checkbox = {},
    Slider = {},
    Editbox = {},
    Combobox = {},
    Multibox = {},
    Text = {},
    Keybox = {},
    Button = {},
    Indicator = {},
    CObject = {},
    ColorPicker = {},
    MultiboxItem = {},
    ComboboxItem = {},
    IndicatorItem = {},
    CObjectItem = {},
    SaveVar = {},
    GetValue = {},
    SetValue = {},
    LoadConfig = LoadConfig or {},
    SaveConfig = SaveConfig or {}
}

globalvars.AccumulateFps = function()
    local cb = globals.absoluteframetime()

    if cb > 0 then
        table.insert(globalvars.getFrametimes, 1, cb)
    end

    local cc = #globalvars.getFrametimes

    if cc == 0 then
        return 0
    end

    local I, cd = 0, 0

    while cd < 0.5 do
        I = I + 1
        cd = cd + globalvars.getFrametimes[I]

        if I >= cc then
            break
        end
    end

    cd = cd / I

    while I < cc do
        I = I + 1
        table.remove(globalvars.getFrametimes)
    end

    local ce = 1 / cd
    local cf = globals.realtime()

    if math.abs(ce - globalvars.fps_prev) > 4 or cf - last_update_time > 2 then
        globalvars.fps_prev = ce
        last_update_time = cf
    else
        ce = globalvars.fps_prev
    end

    return math.floor(ce + 0.5)
end

local cg = {
    ["arrow_1_l"] = "4peE",
    ["arrow_1_r"] = "4pa6",
    ["arrow_2_l"] = "4q+H",
    ["arrow_2_r"] = "4q+I",
    ["arrow_3_l"] = "4q6Y",
    ["arrow_3_r"] = "4q6a",
    ["arrow_4"] = "4oCS"
}
local ch = {
    ["left_1"] = base64.decode(cg["arrow_1_l"]),
    ["right_1"] = base64.decode(cg["arrow_1_r"]),
    ["left_2"] = base64.decode(cg["arrow_2_l"]),
    ["right_2"] = base64.decode(cg["arrow_2_r"]),
    ["left_3"] = base64.decode(cg["arrow_3_l"]),
    ["right_3"] = base64.decode(cg["arrow_3_r"]),
    ["4"] = base64.decode(cg["arrow_4"])
}

mariolua_menu.__index = mariolua_menu
globalvars.buttons = {
    KEY_0 = 1,
    KEY_1 = 2,
    KEY_2 = 3,
    KEY_3 = 4,
    KEY_4 = 5,
    KEY_5 = 6,
    KEY_6 = 7,
    KEY_7 = 8,
    KEY_8 = 9,
    KEY_9 = 10,
    KEY_A = 11,
    KEY_B = 12,
    KEY_C = 13,
    KEY_D = 14,
    KEY_E = 15,
    KEY_F = 16,
    KEY_G = 17,
    KEY_H = 18,
    KEY_I = 19,
    KEY_J = 20,
    KEY_K = 21,
    KEY_L = 22,
    KEY_M = 23,
    KEY_N = 24,
    KEY_O = 25,
    KEY_P = 26,
    KEY_Q = 27,
    KEY_R = 28,
    KEY_S = 29,
    KEY_T = 30,
    KEY_U = 31,
    KEY_V = 32,
    KEY_W = 33,
    KEY_X = 34,
    KEY_Y = 35,
    KEY_Z = 36,
    KEY_PAD_0 = 37,
    KEY_PAD_1 = 38,
    KEY_PAD_2 = 39,
    KEY_PAD_3 = 40,
    KEY_PAD_4 = 41,
    KEY_PAD_5 = 42,
    KEY_PAD_6 = 43,
    KEY_PAD_7 = 44,
    KEY_PAD_8 = 45,
    KEY_PAD_9 = 46,
    KEY_PAD_DIVIDE = 47,
    KEY_PAD_MULTIPLY = 48,
    KEY_PAD_MINUS = 49,
    KEY_PAD_PLUS = 50,
    KEY_PAD_ENTER = 51,
    KEY_PAD_DECIMAL = 52,
    KEY_LBRACKET = 53,
    KEY_RBRACKET = 54,
    KEY_SEMICOLON = 55,
    KEY_APOSTROPHE = 56,
    KEY_BACKQUOTE = 57,
    KEY_COMMA = 58,
    KEY_PERIOD = 59,
    KEY_SLASH = 60,
    KEY_BACKSLASH = 61,
    KEY_MINUS = 62,
    KEY_EQUAL = 63,
    KEY_ENTER = 64,
    KEY_SPACE = 65,
    KEY_BACKSPACE = 66,
    KEY_TAB = 67,
    KEY_CAPSLOCK = 68,
    KEY_NUMLOCK = 69,
    KEY_ESCAPE = 70,
    KEY_SCROLLLOCK = 71,
    KEY_INSERT = 72,
    KEY_DELETE = 73,
    KEY_HOME = 74,
    KEY_END = 75,
    KEY_PAGEUP = 76,
    KEY_PAGEDOWN = 77,
    KEY_BREAK = 78,
    KEY_LSHIFT = 79,
    KEY_RSHIFT = 80,
    KEY_LALT = 81,
    KEY_RALT = 82,
    KEY_LCONTROL = 83,
    KEY_RCONTROL = 84,
    KEY_LWIN = 85,
    KEY_RWIN = 86,
    KEY_APP = 87,
    KEY_UP = 88,
    KEY_LEFT = 89,
    KEY_DOWN = 90,
    KEY_RIGHT = 91,
    KEY_F1 = 92,
    KEY_F2 = 93,
    KEY_F3 = 94,
    KEY_F4 = 95,
    KEY_F5 = 96,
    KEY_F6 = 97,
    KEY_F7 = 98,
    KEY_F8 = 99,
    KEY_F9 = 100,
    KEY_F10 = 101,
    KEY_F11 = 102,
    KEY_F12 = 103,
    KEY_CAPSLOCKTOGGLE = 104,
    KEY_NUMLOCKTOGGLE = 105,
    KEY_SCROLLLOCKTOGGLE = 106,
    MOUSE_LEFT = 107,
    MOUSE_RIGHT = 108,
    MOUSE_MIDDLE = 109,
    MOUSE_4 = 110,
    MOUSE_5 = 111,
    MOUSE_WHEEL_UP = 112,
    MOUSE_WHEEL_DOWN = 113
}

globalvars.vk = {
    MOUSE_LEFT = 0x01,
    MOUSE_RIGHT = 0x02,
    KEY_CANCEL = 0x03,
    MOUSE_MIDDLE = 0x04,
    MOUSE_4 = 0x05,
    MOUSE_5 = 0x06,
    KEY_BACK = 0x08,
    KEY_TAB = 0x09,
    KEY_CLEAR = 0x0C,
    KEY_RETURN = 0x0D,
    KEY_SHIFT = 0x10,
    KEY_CONTROL = 0x11,
    KEY_MENU = 0x12,
    KEY_PAUSE = 0x13,
    KEY_CAPITAL = 0x14,
    KEY_KANA = 0x15,
    KEY_HANGUEL = 0x15,
    KEY_HANGUL = 0x15,
    KEY_JUNJA = 0x17,
    KEY_FINAL = 0x18,
    KEY_HANJA = 0x19,
    KEY_KANJI = 0x19,
    KEY_ESCAPE = 0x1B,
    KEY_CONVERT = 0x1C,
    KEY_NONCONVERT = 0x1D,
    KEY_ACCEPT = 0x1E,
    KEY_MODECHANGE = 0x1F,
    KEY_SPACE = 0x20,
    KEY_PRIOR = 0x21,
    KEY_NEXT = 0x22,
    KEY_END = 0x23,
    KEY_HOME = 0x24,
    KEY_LEFT = 0x25,
    KEY_UP = 0x26,
    KEY_RIGHT = 0x27,
    KEY_DOWN = 0x28,
    KEY_SELECT = 0x29,
    KEY_PRINT = 0x2A,
    KEY_EXECUTE = 0x2B,
    KEY_SNAPSHOT = 0x2C,
    KEY_INSERT = 0x2D,
    KEY_DELETE = 0x2E,
    KEY_HELP = 0x2F,
    KEY_0 = 0x30,
    KEY_1 = 0x31,
    KEY_2 = 0x32,
    KEY_3 = 0x33,
    KEY_4 = 0x34,
    KEY_5 = 0x35,
    KEY_6 = 0x36,
    KEY_7 = 0x37,
    KEY_8 = 0x38,
    KEY_9 = 0x39,
    KEY_A = 0x41,
    KEY_B = 0x42,
    KEY_C = 0x43,
    KEY_D = 0x44,
    KEY_E = 0x45,
    KEY_F = 0x46,
    KEY_G = 0x47,
    KEY_H = 0x48,
    KEY_I = 0x49,
    KEY_J = 0x4A,
    KEY_K = 0x4B,
    KEY_L = 0x4C,
    KEY_M = 0x4D,
    KEY_N = 0x4E,
    KEY_O = 0x4F,
    KEY_P = 0x50,
    KEY_Q = 0x51,
    KEY_R = 0x52,
    KEY_S = 0x53,
    KEY_T = 0x54,
    KEY_U = 0x55,
    KEY_V = 0x56,
    KEY_W = 0x57,
    KEY_X = 0x58,
    KEY_Y = 0x59,
    KEY_Z = 0x5A,
    KEY_LWIN = 0x5B,
    KEY_RWIN = 0x5C,
    KEY_APPS = 0x5D,
    KEY_SLEEP = 0x5F,
    KEY_PAD_0 = 0x60,
    KEY_PAD_1 = 0x61,
    KEY_PAD_2 = 0x62,
    KEY_PAD_3 = 0x63,
    KEY_PAD_4 = 0x64,
    KEY_PAD_5 = 0x65,
    KEY_PAD_6 = 0x66,
    KEY_PAD_7 = 0x67,
    KEY_PAD_8 = 0x68,
    KEY_PAD_9 = 0x69,
    KEY_PAD_MULTIPLY = 0x6A,
    KEY_PAD_PLUS = 0x6B,
    KEY_SEPARATOR = 0x6C,
    KEY_PAD_MINUS = 0x6D,
    KEY_PAD_DECIMAL = 0x6E,
    KEY_PAD_DIVIDE = 0x6F,
    KEY_F1 = 0x70,
    KEY_F2 = 0x71,
    KEY_F3 = 0x72,
    KEY_F4 = 0x73,
    KEY_F5 = 0x74,
    KEY_F6 = 0x75,
    KEY_F7 = 0x76,
    KEY_F8 = 0x77,
    KEY_F9 = 0x78,
    KEY_F10 = 0x79,
    KEY_F11 = 0x7A,
    KEY_F12 = 0x7B,
    KEY_F13 = 0x7C,
    KEY_F14 = 0x7D,
    KEY_F15 = 0x7E,
    KEY_F16 = 0x7F,
    KEY_F17 = 0x80,
    KEY_F18 = 0x81,
    KEY_F19 = 0x82,
    KEY_F20 = 0x83,
    KEY_F21 = 0x84,
    KEY_F22 = 0x85,
    KEY_F23 = 0x86,
    KEY_F24 = 0x87,
    KEY_NUMLOCK = 0x90,
    KEY_SCROLL = 0x91,
    KEY_LSHIFT = 0xA0,
    KEY_RSHIFT = 0xA1,
    KEY_LCONTROL = 0xA2,
    KEY_RCONTROL = 0xA3,
    KEY_LMENU = 0xA4,
    KEY_RMENU = 0xA5,
    KEY_BROWSER_BACK = 0xA6,
    KEY_BROWSER_FORWARD = 0xA7,
    KEY_BROWSER_REFRESH = 0xA8,
    KEY_BROWSER_STOP = 0xA9,
    KEY_BROWSER_SEARCH = 0xAA,
    KEY_BROWSER_FAVORITES = 0xAB,
    KEY_BROWSER_HOME = 0xAC,
    KEY_VOLUME_MUTE = 0xAD,
    KEY_VOLUME_DOWN = 0xAE,
    KEY_VOLUME_UP = 0xAF,
    KEY_MEDIA_NEXT_TRACK = 0xB0,
    KEY_MEDIA_PREV_TRACK = 0xB1,
    KEY_MEDIA_STOP = 0xB2,
    KEY_MEDIA_PLAY_PAUSE = 0xB3,
    KEY_LAUNCH_MAIL = 0xB4,
    KEY_LAUNCH_MEDIA_SELECT = 0xB5,
    KEY_LAUNCH_APP1 = 0xB6,
    KEY_LAUNCH_APP2 = 0xB7,
    KEY_OEM_1 = 0xBA,
    KEY_OEM_PLUS = 0xBB,
    KEY_OEM_COMMA = 0xBC,
    KEY_OEM_MINUS = 0xBD,
    KEY_OEM_PERIOD = 0xBE,
    KEY_OEM_2 = 0xBF,
    KEY_OEM_3 = 0xC0,
    KEY_OEM_4 = 0xDB,
    KEY_OEM_5 = 0xDC,
    KEY_OEM_6 = 0xDD,
    KEY_OEM_7 = 0xDE,
    KEY_OEM_8 = 0xDF,
    KEY_OEM_102 = 0xE2,
    KEY_PROCESSKEY = 0xE5,
    KEY_PACKET = 0xE7,
    KEY_ATTN = 0xF6,
    KEY_CRSEL = 0xF7,
    KEY_EXSEL = 0xF8,
    KEY_EREOF = 0xF9,
    KEY_PLAY = 0xFA,
    KEY_ZOOM = 0xFB,
    KEY_NONAME = 0xFC,
    KEY_PA1 = 0xFD,
    KEY_OEM_CLEAR = 0xFE
}

globalvars.vk_name = {
    "MOUSE LEFT",
    "MOUSE RIGHT",
    "CANCEL",
    "MOUSE MIDDLE",
    "MOUSE 4",
    "MOUSE 5",
    "BACK",
    "TAB",
    "CLEAR",
    "RETURN",
    "SHIFT",
    "CONTROL",
    "MENU",
    "PAUSE",
    "CAPITAL",
    "KANA",
    "HANGUEL",
    "HANGUL",
    "JUNJA",
    "FINAL",
    "HANJA",
    "KANJI",
    "ESCAPE",
    "CONVERT",
    "NONCONVERT",
    "ACCEPT",
    "MODECHANGE",
    "SPACE",
    "PRIOR",
    "NEXT",
    "END",
    "HOME",
    "LEFT",
    "UP",
    "RIGHT",
    "DOWN",
    "SELECT",
    "PRINT",
    "EXECUTE",
    "SNAPSHOT",
    "INSERT",
    "DELETE",
    "HELP",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "LWIN",
    "RWIN",
    "APPS",
    "SLEEP",
    "PAD 0",
    "PAD 1",
    "PAD 2",
    "PAD 3",
    "PAD 4",
    "PAD 5",
    "PAD 6",
    "PAD 7",
    "PAD 8",
    "PAD 9",
    "PAD MULTIPLY",
    "PAD PLUS",
    "PAD SEPARATOR",
    "PAD MINUS",
    "PAD DECIMAL",
    "PAD DIVIDE",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12",
    "F13",
    "F14",
    "F15",
    "F16",
    "F17",
    "F18",
    "F19",
    "F20",
    "F21",
    "F22",
    "F23",
    "F24",
    "NUMLOCK",
    "SCROLL",
    "LSHIFT",
    "RSHIFT",
    "LCONTROL",
    "RCONTROL",
    "LMENU",
    "RMENU",
    "BROWSER_BACK",
    "BROWSER_FORWARD",
    "BROWSER_REFRESH",
    "BROWSER_STOP",
    "BROWSER_SEARCH",
    "BROWSER_FAVORITES",
    "BROWSER_HOME",
    "VOLUME_MUTE",
    "VOLUME_DOWN",
    "VOLUME_UP",
    "MEDIA_NEXT_TRACK",
    "MEDIA_PREV_TRACK",
    "MEDIA_STOP",
    "MEDIA_PLAY_PAUSE",
    "LAUNCH_MAIL",
    "LAUNCH_MEDIA_SELECT",
    "LAUNCH_APP1",
    "LAUNCH_APP2",
    "OEM_1",
    "OEM_PLUS",
    "OEM_COMMA",
    "OEM_MINUS",
    "OEM_PERIOD",
    "OEM_2",
    "OEM_3",
    "OEM_4",
    "OEM_5",
    "OEM_6",
    "OEM_7",
    "OEM_8",
    "OEM_102",
    "PROCESSKEY",
    "PACKET",
    "ATTN",
    "CRSEL",
    "EXSEL",
    "EREOF",
    "PLAY",
    "ZOOM",
    "NONAME",
    "PA1",
    "OEM_CLEAR"
}

globalvars.button_name = {
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "PAD 0",
    "PAD 1",
    "PAD 2",
    "PAD 3",
    "PAD 4",
    "PAD 5",
    "PAD 6",
    "PAD 7",
    "PAD 8",
    "PAD 9",
    "PAD DIVIDE",
    "PAD MULTIPLY",
    "PAD MINUS",
    "PAD PLUS",
    "PAD ENTER",
    "PAD DECIMAL",
    "LBRACKET",
    "RBRACKET",
    "SEMICOLON",
    "APOSTROPHE",
    "BACKQUOTE",
    "COMMA",
    "PERIOD",
    "SLASH",
    "BACKSLASH",
    "MINUS",
    "EQUAL",
    "ENTER",
    "SPACE",
    "BACKSPACE",
    "TAB",
    "CAPSLOCK",
    "NUMLOCK",
    "ESCAPE",
    "SCROLLLOCK",
    "INSERT",
    "DELETE",
    "HOME",
    "END",
    "PAGEUP",
    "PAGEDOWN",
    "BREAK",
    "LSHIFT",
    "RSHIFT",
    "LALT",
    "RALT",
    "LCONTROL",
    "RCONTROL",
    "LWIN",
    "RWIN",
    "APP",
    "UP",
    "LEFT",
    "DOWN",
    "RIGHT",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12",
    "CAPSLOCKTOGGLE",
    "NUMLOCKTOGGLE",
    "SCROLLLOCKTOGGLE",
    "MOUSE LEFT",
    "MOUSE RIGHT",
    "MOUSE MIDDLE",
    "MOUSE 4",
    "MOUSE 5",
    "MOUSE WHEEL UP",
    "MOUSE WHEEL DOWN"
}

globalvars.key_text = {
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "/",
    "*",
    "-",
    "+",
    "",
    ",",
    "{",
    "}",
    "'",
    "�",
    "`",
    ",",
    ".",
    "/",
    "\\",
    "-",
    "=",
    "",
    " ",
    ""
}

local ci = {
    is_pressed = ffi_utils.is_button_pressed,
    key_downtime = {}
}

function ci.get_pressed_key()
    if globalvars.is_input_blocked then
        for Q, F in pairs(globalvars.vk) do
            if client.key_state(F) then
                local cj = globalvars.buttons[Q] or -1

                return cj
            end
        end
    else
        for I = 1, 113 do
            if I ~= 104 and I ~= 105 and I ~= 106 then
                if ci.is_pressed(ffi_utils.inputsystem, I) then
                    return I
                end
            end
        end
    end
end

globalvars.wep_type = {
    [1] = "pistol",
    [2] = "hpistol",
    [3] = "smg",
    [4] = "rifle",
    [5] = "scout",
    [6] = "awp",
    [7] = "asniper",
    [8] = "lmg",
    [9] = "other"
}

cfg.adaptive_wep_cfg = {}

function util_functions.temp_wepcfg()
    local temp_wepcfg = temp_wepcfg or {}

    for I = 1, 9 do
        temp_wepcfg[I] = {
            mariolua_menu.Groupbox:new("weapon." .. globalvars.wep_type[I], "Aimbot", 0, 0, 210, 530),
            mariolua_menu.Multibox:new(
                "weapon." .. globalvars.wep_type[I] .. ".targethitbox",
                "Target Hitbox",
                0,
                0,
                190,
                22,
                {
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".targethitbox", "Head"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".targethitbox", "Chest"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".targethitbox", "Stomach"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".targethitbox", "Arms"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".targethitbox", "Legs"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".targethitbox", "Feet")
                },
                nil
            ),
            mariolua_menu.Multibox:new(
                "weapon." .. globalvars.wep_type[I] .. ".multipoint",
                "Multi-Point",
                0,
                0,
                190,
                22,
                {
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".multipoint", "Head"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".multipoint", "Chest"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".multipoint", "Stomach"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".multipoint", "Arms"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".multipoint", "Legs"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".multipoint", "Feet")
                },
                nil
            ),
            mariolua_menu.Slider:new(
                "weapon." .. globalvars.wep_type[I] .. ".multipoint.scale",
                "Multi-Point Scale",
                0,
                0,
                190,
                12,
                70,
                24,
                100
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".safepoint.prefer",
                "Prefer Safe Point",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Multibox:new(
                "weapon." .. globalvars.wep_type[I] .. ".avoid",
                "Avoid Unsafe Hitboxes",
                0,
                0,
                190,
                22,
                {
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".avoid", "Head"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".avoid", "Chest"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".avoid", "Stomach"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".avoid", "Arms"),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".avoid", "Legs")
                },
                nil
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".autofire",
                "Automatic Fire",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".silentaim",
                "Silent Aim",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Slider:new(
                "weapon." .. globalvars.wep_type[I] .. ".hitchance",
                "Minimum Hit Chance",
                0,
                0,
                190,
                12,
                55,
                0,
                100
            ),
            mariolua_menu.Slider:new(
                "weapon." .. globalvars.wep_type[I] .. ".damage",
                "Minimum Damage",
                0,
                0,
                190,
                12,
                30,
                0,
                126
            ),
            mariolua_menu.Slider:new(
                "weapon." .. globalvars.wep_type[I] .. ".damage.override",
                "Minimum Damage Override",
                0,
                0,
                190,
                12,
                30,
                0,
                126
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".autoscope",
                "Automatic Scope",
                0,
                0,
                18,
                18,
                nil
            )
        }

        local ck = temp_wepcfg[I]

        for cl = 1, #ck do
            if cfg.adaptive_wep_cfg[I] == nil then
                cfg.adaptive_wep_cfg[I] = {}
            end

            cfg.adaptive_wep_cfg[I][ck[cl].label:lower()] = mariolua_menu.Reference(ck[cl].reference)
        end
    end

    return temp_wepcfg
end

function util_functions.temp_wepcfg2()
    local temp_wepcfg2 = temp_wepcfg2 or {}

    for I = 1, 9 do
        temp_wepcfg2[I] = {
            mariolua_menu.Groupbox:new("weapon." .. globalvars.wep_type[I], "Other", 0, 0, 210, 580),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".removerecoil",
                "Remove Recoil",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".delayshot",
                "Delay Shot",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".quickstop",
                "Quick Stop",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Multibox:new(
                "weapon." .. globalvars.wep_type[I] .. ".quickstop.options",
                "Quick Stop Options",
                0,
                0,
                190,
                22,
                {
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".quickstop.options", "Early"),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".quickstop.options",
                        "Slow Motion"
                    ),
                    mariolua_menu.MultiboxItem:new("weapon." .. globalvars.wep_type[I] .. ".quickstop.options", "Duck"),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".quickstop.options",
                        "Fake Duck"
                    ),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".quickstop.options",
                        "Move between Shots"
                    ),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".quickstop.options",
                        "Ignore molotov"
                    )
                },
                nil
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".bodyaim.lethal",
                "Body Aim If Lethal",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".bodyaim.prefer",
                "Prefer Body Aim",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Multibox:new(
                "weapon." .. globalvars.wep_type[I] .. ".bodyaim.disablers",
                "Prefer Body Aim Disablers",
                0,
                0,
                190,
                22,
                {
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".bodyaim.disablers",
                        "Low Inaccuracy"
                    ),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".bodyaim.disablers",
                        "Target Shot Fired"
                    ),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".bodyaim.disablers",
                        "Target Resolved"
                    ),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".bodyaim.disablers",
                        "Safe Point Headshot"
                    ),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".bodyaim.disablers",
                        "Low Damage"
                    )
                },
                nil
            ),
            mariolua_menu.Checkbox:new(
                "weapon." .. globalvars.wep_type[I] .. ".doubletap",
                "Double Tap",
                0,
                0,
                18,
                18,
                nil
            ),
            mariolua_menu.Combobox:new(
                "weapon." .. globalvars.wep_type[I] .. ".doubletap.mode",
                "Double Tap Mode",
                0,
                0,
                190,
                22,
                {"Offensive", "Defensive"},
                nil
            ),
            mariolua_menu.Slider:new(
                "weapon." .. globalvars.wep_type[I] .. ".doubletap.hitchance",
                "Double Tap Hit Chance",
                0,
                0,
                190,
                12,
                5,
                0,
                100
            ),
            mariolua_menu.Slider:new(
                "weapon." .. globalvars.wep_type[I] .. ".doubletap.fakelaglimit",
                "Double Tap Fake Lag Limit",
                0,
                0,
                190,
                12,
                1,
                1,
                10
            ),
            mariolua_menu.Multibox:new(
                "weapon." .. globalvars.wep_type[I] .. ".doubletap.quickstop",
                "Double Tap Quick Stop",
                0,
                0,
                190,
                22,
                {
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".doubletap.quickstop",
                        "Slow Motion"
                    ),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".doubletap.quickstop",
                        "Duck"
                    ),
                    mariolua_menu.MultiboxItem:new(
                        "weapon." .. globalvars.wep_type[I] .. ".doubletap.quickstop",
                        "Move Between Shots"
                    )
                },
                nil
            )
        }

        local ck = temp_wepcfg2[I]

        for cl = 1, #ck do
            if cfg.adaptive_wep_cfg[I] == nil then
                cfg.adaptive_wep_cfg[I] = {}
            end

            cfg.adaptive_wep_cfg[I][ck[cl].label:lower()] = mariolua_menu.Reference(ck[cl].reference)
        end
    end

    return temp_wepcfg2
end

function globalvars.get_aa_settings()
    local cm = {}
    local aa_modes = {"standing", "moving", "slowmotion"}

    for I = 1, #aa_modes do
        cm[aa_modes[I]] = {
            enable = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".enable"):GetValue(),
            state = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".state"):GetValue(),
            fake = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".fake"):GetValue(),
            fake_jitter = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".fake.jitter"):GetValue(),
            fake_jitter_enable = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".fake.jitter.enable"):GetValue(),
            fake_jitter_speed = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".fake.jitter.speed"):GetValue(),
            body = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".body"):GetValue(),
            body_mode = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".body.mode"):GetValue(),
            lbyextras_mode = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".lbyextras"):GetValue(),
            freestanding = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".mode"):GetValue(),
            lby_mode = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".lby"):GetValue(),
            enable_anti_bruteforce = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.enable"):GetValue(

            ),
            anti_bruteforce_timeout = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.timeout"):GetValue(

            ),
            default_yaw_offset = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.default"):GetValue(

            ),
            max_yaw_delta = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.max"):GetValue(),
            yaw_step = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.step"):GetValue(),
            default_body_yaw_offset = mariolua_menu.Reference(
                "antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.default"
            ):GetValue(),
            max_body_yaw_delta = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.max"):GetValue(

            ),
            body_yaw_step = mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.step"):GetValue(

            )
        }
    end

    return cm
end

function globalvars.set_aa_settings(a6)
    local aa_modes = {"standing", "moving", "slowmotion"}

    for I = 1, #aa_modes do
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".enable"):SetValue(a6[aa_modes[I]].enable)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".state"):SetValue(a6[aa_modes[I]].state)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".fake"):SetValue(a6[aa_modes[I]].fake)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".fake.jitter"):SetValue(a6[aa_modes[I]].fake_jitter)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".fake.jitter.enable"):SetValue(
            a6[aa_modes[I]].fake_jitter_enable
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".fake.jitter.speed"):SetValue(
            a6[aa_modes[I]].fake_jitter_speed
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".body"):SetValue(a6[aa_modes[I]].body)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".body.mode"):SetValue(a6[aa_modes[I]].body_mode)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".lbyextras"):SetValue(a6[aa_modes[I]].lbyextras_mode)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".mode"):SetValue(a6[aa_modes[I]].freestanding)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".lby"):SetValue(a6[aa_modes[I]].lby_mode)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".slowwalk.enabled"):SetValue(a6[aa_modes[I]].slowwalk)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".slowwalk.speed"):SetValue(a6[aa_modes[I]].slowwalk_speed)
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.enable"):SetValue(
            a6[aa_modes[I]].enable_anti_bruteforce
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.timeout"):SetValue(
            a6[aa_modes[I]].anti_bruteforce_timeout
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.default"):SetValue(
            a6[aa_modes[I]].default_yaw_offset
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.max"):SetValue(
            a6[aa_modes[I]].max_yaw_delta
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.step"):SetValue(
            a6[aa_modes[I]].yaw_step
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.default"):SetValue(
            a6[aa_modes[I]].default_body_yaw_offset
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.max"):SetValue(
            a6[aa_modes[I]].max_body_yaw_delta
        )
        mariolua_menu.Reference("antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.step"):SetValue(
            a6[aa_modes[I]].body_yaw_step
        )
    end
end

function util_functions.generate_aa_tab()
    local co = {}
    local aa_modes = {"standing", "moving", "slowmotion"}

    for I = 1, #aa_modes do
        co[aa_modes[I]] = {
            [1] = {
                mariolua_menu.Groupbox:new("antiaim." .. aa_modes[I] .. ".gb", "Anti-Aim", 0, 0, 220, 520),
                mariolua_menu.Checkbox:new("antiaim." .. aa_modes[I] .. ".enable", "Enable", 0, 0, 18, 18, nil),
                mariolua_menu.Combobox:new(
                    "antiaim." .. aa_modes[I] .. ".state",
                    "Anti-Aim Mode",
                    0,
                    0,
                    200,
                    22,
                    {"Legit", "Rage"},
                    nil
                ),
                mariolua_menu.Combobox:new(
                    "antiaim." .. aa_modes[I] .. ".lby",
                    "LBY Mode",
                    0,
                    0,
                    200,
                    22,
                    {"Local View", "Opposite", "Match Real", "Sway"},
                    nil
                ),
                mariolua_menu.Combobox:new(
                    "antiaim." .. aa_modes[I] .. ".mode",
                    "Legit Anti-Aim Mode",
                    0,
                    0,
                    200,
                    22,
                    {"MarioLua Freestanding", "Skeet Freestanding", "Manual"},
                    nil
                ),
                mariolua_menu.Combobox:new(
                    "antiaim." .. aa_modes[I] .. ".lbyextras",
                    "Lowerbody Extras",
                    0,
                    0,
                    200,
                    22,
                    {"Off", "Fake Twist", "Fake Jitter", "Fake Max", "Cradle", "Shake"},
                    nil
                ),
                mariolua_menu.Combobox:new(
                    "antiaim." .. aa_modes[I] .. ".body.mode",
                    "Body Yaw Mode",
                    0,
                    0,
                    200,
                    22,
                    {"Off", "Static", "Opposite", "Jitter"},
                    2
                ),
                mariolua_menu.Checkbox:new(
                    "antiaim." .. aa_modes[I] .. ".fake.jitter.enable",
                    "Enable Fake Jitter",
                    0,
                    0,
                    18,
                    18,
                    nil
                ),
                mariolua_menu.Slider:new("antiaim." .. aa_modes[I] .. ".body", "Body Yaw", 0, 0, 190, 12, 60, 0, 180),
                mariolua_menu.Slider:new("antiaim." .. aa_modes[I] .. ".fake", "Fake Yaw", 0, 0, 190, 12, 60, 0, 60),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".fake.jitter",
                    "Fake Yaw Jitter",
                    0,
                    0,
                    190,
                    12,
                    60,
                    0,
                    60
                ),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".fake.jitter.speed",
                    "Fake Jitter Speed",
                    0,
                    0,
                    190,
                    12,
                    30,
                    0,
                    1000
                )
            },
            [2] = {
                mariolua_menu.Groupbox:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce",
                    "Anti Bruteforce",
                    0,
                    0,
                    230,
                    380
                ),
                mariolua_menu.Checkbox:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce.enable",
                    "Enable",
                    0,
                    0,
                    18,
                    18,
                    nil
                ),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce.timeout",
                    "Timeout",
                    0,
                    0,
                    200,
                    12,
                    10,
                    0,
                    30
                ),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.default",
                    "Default Yaw Offset",
                    0,
                    0,
                    200,
                    12,
                    0,
                    -180,
                    180
                ),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.max",
                    "Max Yaw Delta",
                    0,
                    0,
                    200,
                    12,
                    0,
                    0,
                    180
                ),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce.yaw.step",
                    "Yaw Step",
                    0,
                    0,
                    200,
                    12,
                    5,
                    1,
                    10
                ),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.default",
                    "Default body Yaw Offset",
                    0,
                    0,
                    200,
                    12,
                    0,
                    -180,
                    180
                ),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.max",
                    "Max Body Yaw Delta",
                    0,
                    0,
                    200,
                    12,
                    100,
                    0,
                    180
                ),
                mariolua_menu.Slider:new(
                    "antiaim." .. aa_modes[I] .. ".antibruteforce.bodyyaw.step",
                    "Body Yaw Step	",
                    0,
                    0,
                    200,
                    12,
                    5,
                    1,
                    10
                )
            }
        }
    end

    return co
end

globalvars.screen_size = {client.screen_size()}
globalvars.screen_size.w = globalvars.screen_size[1]
globalvars.screen_size.h = globalvars.screen_size[2]

function util_functions.draw_Skeleton(ent, cp, cq)
    cq = cq or false
    cp = cp or 0

    if ent == local_player and not functions.is_thirdperson() then
        return
    end

    if #functions.get_hitbox_pos(ent) == 0 then
        return
    end

    local cr, cs, ct

    for I = 0, 18 do
        hbox_t = functions.get_hitbox_pos(ent)[I]

        if cp > 0 then
            cr, cs, ct = functions.ExtrapolatePosition(hbox_t[1], hbox_t[2], hbox_t[3], cp, ent)
        else
            cr, cs, ct = hbox_t[1], hbox_t[2], hbox_t[3]
        end

        if cq then
            extended_renderer.drawCubeAtPoint(3, {cr, cs, ct}, 170, 100, 220, 250, 20)
        end

        if cr == nil then
            return
        end

        local cu, cv = renderer.world_to_screen(cr, cs, ct)
        globalvars.sk_table[I] = {cu, cv}
    end

    for I = 1, #globalvars.sk_table do
        local F = globalvars.sk_table[I]

        if F[1] and F[2] and F[3] then
            globalvars.sk_table[I] = {renderer.world_to_screen(F[1], F[2], F[3])}
        end
    end

    if not cq then
        cr, cs, ct =
            functions.ExtrapolatePosition(
            functions.get_hitbox_pos(ent)[0][1],
            functions.get_hitbox_pos(ent)[0][2],
            functions.get_hitbox_pos(ent)[0][3],
            cp,
            ent
        )
        extended_renderer.drawCubeAtPoint(3, {cr, cs, ct}, 255, 255, 255, 255, 100)
    end

    extended_renderer.draw_line(
        globalvars.sk_table[0][1],
        globalvars.sk_table[0][2],
        globalvars.sk_table[1][1],
        globalvars.sk_table[1][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[1][1],
        globalvars.sk_table[1][2],
        globalvars.sk_table[6][1],
        globalvars.sk_table[6][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[6][1],
        globalvars.sk_table[6][2],
        globalvars.sk_table[17][1],
        globalvars.sk_table[17][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[6][1],
        globalvars.sk_table[6][2],
        globalvars.sk_table[15][1],
        globalvars.sk_table[15][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[6][1],
        globalvars.sk_table[6][2],
        globalvars.sk_table[4][1],
        globalvars.sk_table[4][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[4][1],
        globalvars.sk_table[4][2],
        globalvars.sk_table[2][1],
        globalvars.sk_table[2][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[2][1],
        globalvars.sk_table[2][2],
        globalvars.sk_table[7][1],
        globalvars.sk_table[7][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[2][1],
        globalvars.sk_table[2][2],
        globalvars.sk_table[8][1],
        globalvars.sk_table[8][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[7][1],
        globalvars.sk_table[7][2],
        globalvars.sk_table[9][1],
        globalvars.sk_table[9][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[9][1],
        globalvars.sk_table[9][2],
        globalvars.sk_table[11][1],
        globalvars.sk_table[11][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[8][1],
        globalvars.sk_table[8][2],
        globalvars.sk_table[10][1],
        globalvars.sk_table[10][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[10][1],
        globalvars.sk_table[10][2],
        globalvars.sk_table[12][1],
        globalvars.sk_table[12][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[17][1],
        globalvars.sk_table[17][2],
        globalvars.sk_table[18][1],
        globalvars.sk_table[18][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[18][1],
        globalvars.sk_table[18][2],
        globalvars.sk_table[14][1],
        globalvars.sk_table[14][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[15][1],
        globalvars.sk_table[15][2],
        globalvars.sk_table[16][1],
        globalvars.sk_table[16][2],
        255,
        255,
        255,
        255
    )
    extended_renderer.draw_line(
        globalvars.sk_table[16][1],
        globalvars.sk_table[16][2],
        globalvars.sk_table[13][1],
        globalvars.sk_table[13][2],
        255,
        255,
        255,
        255
    )
end

globalvars.old_ui_set = ui.set
globalvars.ui_set = {}

function ui.set(cw, ...)
    if globalvars.ui_set[cw] ~= ... then
        if type(...) == "table" then
            local cx = extended_table.table_to_string3(...)

            if globalvars.ui_set[cw] ~= cx then
                globalvars.ui_set[cw] = cx
                globalvars.old_ui_set(cw, unpack(...))
            end
        else
            globalvars.ui_set[cw] = ...
            globalvars.old_ui_set(cw, ...)
        end
    end
end

function mariolua_menu.SaveConfig()
    local a8 = "{\n"
    ui.set(globalvars.config_string, "")

    for I = 1, #globalvars.reference do
        local cy = globalvars.reference[globalvars.reference[I]]

        if
            cy:GetValue() ~= nil and (cy.type ~= "text" or cy.type == "text" and cy.should_save) and
                (cy.type ~= "tab" or cy.type == "tab" and cy.should_save) and
                cy.type ~= "button" and
                cy.type ~= "groupbox" and
                cy.type ~= "editbox" and
                cy.type ~= "multibox_item"
         then
            if type(cy:GetValue()) == "table" or cy.type == "save_var" or cy.type == "multibox" then
                a8 =
                    a8 ..
                    "['" ..
                        globalvars.reference[I] ..
                            "']" .. " = " .. extended_table.table_to_string3(cy:GetValue()) .. ",\n"
            elseif type(cy:GetValue()) == "string" then
                a8 = a8 .. "['" .. globalvars.reference[I] .. "']" .. " = " .. cy:GetValue() .. ",\n"
            elseif type(cy:GetValue()) == "boolean" or "number" then
                a8 = a8 .. "['" .. globalvars.reference[I] .. "']" .. " = " .. tostring(cy:GetValue()) .. ",\n"
            end
        end
    end

    ui.set(globalvars.config_string, a8 .. "}")
end

function mariolua_menu.LoadConfig(cz)
    if
        ui.get(globalvars.config_string) == nil or ui.get(globalvars.config_string) == "" or
            ui.get(globalvars.config_string):len() < 3
     then
        return
    end

    local cA = ui.get(globalvars.config_string) or ""

    if cA:match("antiaim.exception,") then
        ffi_utils.set_clipboard_text(ffi_utils.VGUI_System, cA, cA:len())
        mariolua.notify(10, "[Info] Your confing got exported to your clipboard.")
        mariolua.notify(20, "[Error] Your confing is corrupted! Trying to repair config...")

        if cz then
            cz = cz:gsub("antiaim.exception,", "false,")
        else
            cA = cA:gsub("antiaim.exception,", "false,")
        end

        mariolua.notify(30, "[Info] Config repair attempt done. Loading now config...")
    end

    local cB

    if cz ~= nil and cz ~= "" and type(cz) == "string" then
        local cC = cz
        cC = cC
        cB = loadstring("return " .. cC)()
    else
        cA = cA:gsub("antiaim.exception,", "false,")
        cB = loadstring("return " .. cA)()
    end

    for I = 1, #globalvars.reference do
        local cy = cB[globalvars.reference[I]]
        local cD = globalvars.reference[I]

        if cB[cD] == nil then
            cB[cD] = cD
        elseif globalvars.reference[cD].type == "keybox" then
            globalvars.reference[cD].key = tonumber(cy)
            if cy == 0 then
                globalvars.reference[cD].key_name = "None"
            else
                globalvars.reference[cD].key_name = globalvars.button_name[cy]
            end
        elseif globalvars.reference[cD].type == "checkbox" then
            if cy == nil or cy == "nil" then
                globalvars.reference[cD].isEnabled = false
            else
                globalvars.reference[cD].isEnabled = cy
            end
        elseif globalvars.reference[cD].type == "save_var" then
            if cy ~= nil and cy ~= "nil" then
                globalvars.reference[cD]:SetValue(cy)
            end
        elseif globalvars.reference[cD].type == "combobox" then
            if type(cy) ~= "boolean" then
                globalvars.reference[cD].enabledItem = cy
            else
                globalvars.reference[cD].enabledItem = 1
            end
        elseif globalvars.reference[cD].type == "combobox_item" then
            globalvars.reference[cD].isEnabled = cy
        elseif globalvars.reference[cD].type == "multibox" then
            globalvars.reference[cD]:SetValue(cy)
        elseif globalvars.reference[cD].type == "slider" then
            if cy == nil or cy == "nil" then
                globalvars.reference[cD]:SetValue(globalvars.reference[cD].def_val)
            else
                globalvars.reference[cD]:SetValue(tonumber(cy))
            end
        elseif globalvars.reference[cD].type == "indicator_item" then
            if type(cy) == "table" then
                if cy[1] ~= nil and cy[2] ~= nil then
                    globalvars.reference[cD]:SetValue(cy[1], cy[2])
                end
            end
        elseif globalvars.reference[cD].type == "cobject_item" then
            if type(cy) == "table" then
                if cy[1] ~= nil and cy[2] ~= nil then
                    globalvars.reference[cD]:SetValue(cy[1], cy[2])
                end
            end
        end
    end

    mariolua.log("Config loaded!")
end

function mariolua_menu.SetValue(cw, H)
    for I = 1, #globalvars.reference do
        local cy = globalvars.reference[globalvars.reference[I]]

        if cy:GetValue() ~= nil and globalvars.reference[I] == cw then
            cy:SetValue(H)
        end
    end
end

function mariolua_menu.GetValue(cw)
    for I = 1, #globalvars.reference do
        local cy = globalvars.reference[globalvars.reference[I]]

        if cy:GetValue() ~= nil and globalvars.reference[I] == cw then
            cy:GetValue()
        end
    end
end

local cE = false

function mariolua_menu.Reference(cw)
    if cw == nil then
        return false
    end

    if not cE then
        mariolua_menu.SaveConfig()
        cE = true
    end

    for I = 1, #globalvars.reference do
        local cy = globalvars.reference[globalvars.reference[I]]

        if (cy:GetValue() ~= nil or cy.type ~= nil) and globalvars.reference[I] == cw then
            return cy
        end
    end
end

function mariolua_menu.Editbox:new(cF, cG, cH, cI, cJ, aw, callback)
    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "editbox",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        reference = cF,
        label = cG,
        isHovered = false,
        isClicked = false,
        isWriting = false,
        last_key = 0,
        released_key = 0,
        input = "",
        visible = true,
        force_invisible = false,
        func = callback
    }
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.Editbox:SetActive(cL)
    self.visible = cL
end

function mariolua_menu.Editbox:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end

function mariolua_menu.Editbox:IsActive()
    return self.visible
end

function mariolua_menu.Editbox:GetPosition()
    return self.pos
end

function mariolua_menu.Editbox:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end

function mariolua_menu.Editbox:SetFunction(cN)
    self.func = cN
end

function mariolua_menu.Editbox:Update()
    if self.isWriting then
        local pressed_key = ci.get_pressed_key()
        pressed_key = pressed_key or 0
        ci.key_downtime[pressed_key] = ci.key_downtime[pressed_key] or 0

        if ci.is_pressed(ffi_utils.inputsystem, pressed_key) then
            self.released_key = 0
            ci.key_downtime[pressed_key] = ci.key_downtime[pressed_key] + 1
        else
            self.released_key = ci.key_downtime[pressed_key] > 0 and pressed_key or self.released_key
            self.last_key = 0
            ci.key_downtime = {}
        end

        if pressed_key ~= nil then
            if
                not (pressed_key == globalvars.buttons.KEY_ESCAPE or pressed_key == globalvars.buttons.CAPSLOCKTOGGLE or
                    pressed_key == globalvars.buttons.NUMLOCKTOGGLE) and
                    self.visible and
                    pressed_key <= #globalvars.key_text
             then
                self.last_key = self.last_key == self.released_key and 0 or self.last_key

                if self.last_key ~= pressed_key then
                    if pressed_key == globalvars.buttons.KEY_BACKSPACE then
                        self.input = self.input:sub(1, #self.input - 1)
                    else
                        self.input = self.input .. globalvars.key_text[pressed_key]:lower()
                    end
                end

                self.last_key = pressed_key
            else
                self.isClicked = false
                self.isWriting = false
                globalvars.lock_input["move"] = false
                self.input = self.input:len() > 0 and self.input or ""
                mariolua.log("editbox abort")
            end
        end
    end

    if self.visible then
        if ci.is_pressed(ffi_utils.inputsystem, globalvars.buttons.KEY_ESCAPE) and self.isWriting then
            self.isClicked = false
            self.isWriting = false
            globalvars.lock_input["move"] = false
            pressed_key = nil
            self.input = ""
            mariolua.log("editbox abort")
        end

        if
            extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height) or
                self.isWriting
         then
            self.isHovered = true

            if globalvars.mouse1_key_state and not self.isClicked then
                self.isClicked = true
                self.isWriting = true
                self.input = self.input:len() > 0 and self.input or ""
                globalvars.lock_input["move"] = true
            end
        else
            self.isWriting = globalvars.mouse1_key_state and self.isWriting and false or self.isWriting
            self.isClicked = globalvars.mouse1_key_state and self.isWriting and false or self.isClicked
            self.isHovered = false
        end
    else
        if self.isWriting then
            self.isWriting = false
            self.isClicked = false
            globalvars.lock_input["move"] = false
            self.input = self.input:len() > 0 and self.input or ""
            mariolua.log("editbox abort")
        end

        self.isHovered = false
    end
end

function mariolua_menu.Editbox:GetValue()
    return self.input
end

function mariolua_menu.Editbox:SetValue(cO)
    if cO == nil or type(cO) ~= "string" and type(cO) ~= "number" and type(cO) ~= "boolean" then
        self.input = ""
        mariolua.log(self.label .. ', editbox input is nil. Set input to "" ')
    else
        mariolua.log(self.label .. ", set to " .. cO)
        self.input = tostring(cO)
    end
end

function mariolua_menu.Editbox:Render()
    if self.visible then
        self.alpha = extended_math.clamp(self.alpha, 0, 255)

        if cfg.settings_guistyle:GetItemName() == "UwU" then
            local cP =
                self.isWriting and {255, 80, 255} or
                (self.isClicked and {200, 60, 120} or (self.isHovered and {235, 80, 235} or {220, 80, 140}))
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["keybox"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                cP[1],
                cP[2],
                cP[3],
                self.alpha
            )
            local cQ, cR = renderer.measure_text("c", self.label)
            local cS = self.isWriting and {250, 250, 250} or {240, 240, 240}
            renderer.text(self.pos.x + cQ / 2, self.pos.y - cR, 240, 240, 240, self.alpha - 15, "c", 0, self.label)
            renderer.text(
                self.pos.x + self.width / 2,
                self.pos.y + self.height / 2,
                cS[1],
                cS[2],
                cS[3],
                self.alpha - 15,
                "c",
                0,
                self.input .. (self.isWriting and "_" or "")
            )
        elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
            extended_renderer.rectangle_outline(self.pos.x, self.pos.y, self.width, self.height, 12, 12, 12, self.alpha)
            extended_renderer.rectangle_outline(
                self.pos.x + 1,
                self.pos.y + 1,
                self.width - 2,
                self.height - 2,
                50,
                50,
                50,
                self.alpha
            )

            if self.isHovered then
                local cP =
                    self.isClicked and (self.isWriting and {{32, 32, 32}, {40, 40, 40}} or {{22, 22, 22}, {30, 30, 30}}) or
                    (self.isWriting and {{50, 50, 50}, {40, 40, 40}} or {{40, 40, 40}, {30, 30, 30}})
                renderer.gradient(
                    self.pos.x + 2,
                    self.pos.y + 2,
                    self.width - 4,
                    self.height - 4,
                    cP[1][1],
                    cP[1][2],
                    cP[1][3],
                    self.alpha - 35,
                    cP[2][1],
                    cP[2][2],
                    cP[2][3],
                    self.alpha - 35,
                    true
                )
            else
                local cP = self.isWriting and {35, 35, 35} or {25, 25, 25}
                renderer.gradient(
                    self.pos.x + 2,
                    self.pos.y + 2,
                    self.width - 4,
                    self.height - 4,
                    cP[1],
                    cP[2],
                    cP[3],
                    self.alpha - 35,
                    cP[1] - 5,
                    cP[2] - 5,
                    cP[3] - 5,
                    self.alpha - 75,
                    true
                )
            end

            local cQ, cR = renderer.measure_text("c", self.label)
            renderer.text(self.pos.x + cQ / 2, self.pos.y - cR, 240, 240, 240, self.alpha - 15, "c", 0, self.label)
            local cP =
                self.isWriting and {globalvars.color[1], globalvars.color[2], globalvars.color[3]} or {240, 240, 240}
            renderer.text(
                self.pos.x + self.width / 2,
                self.pos.y + self.height / 2,
                cP[1],
                cP[2],
                cP[3],
                self.alpha - 15,
                "c",
                0,
                self.input .. (self.isWriting and "_" or "")
            )
        else
            local cP =
                self.isWriting and {255, 255, 255} or
                (self.isClicked and {200, 200, 200} or (self.isHovered and {235, 235, 235} or {220, 220, 220}))
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["keybox"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                cP[1],
                cP[2],
                cP[3],
                self.alpha
            )
            local cQ, cR = renderer.measure_text("c", self.label)
            local cS = self.isWriting and {250, 250, 250} or {240, 240, 240}
            renderer.text(self.pos.x + cQ / 2, self.pos.y - cR, 240, 240, 240, self.alpha - 15, "c", 0, self.label)
            renderer.text(
                self.pos.x + self.width / 2,
                self.pos.y + self.height / 2,
                cS[1],
                cS[2],
                cS[3],
                self.alpha - 15,
                "c",
                0,
                self.input .. (self.isWriting and "_" or "")
            )
        end
    end
end

function mariolua_menu.SaveVar:new(cF, cT)
    if type(cF) ~= "string" then
        mariolua.log(cT .. ": " .. tostring(cF) .. " is not a valid reference!")

        return
    end

    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {type = "save_var", reference = cF, var = cT}
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.SaveVar:GetValue()
    return self.var
end

function mariolua_menu.SaveVar:SetValue(cU)
    self.var = cU
    mariolua_menu.SaveConfig()
end

function mariolua_menu.CObjectItem:new(cV, cH, cI, cJ, aw, cU)
    table.insert(globalvars.reference, "cobject_item_" .. cV:gsub(" ", ""))
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "cobject_item",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        x_dif = 0,
        y_dif = 0,
        reference = "cobject_item_" .. cV:gsub(" ", ""),
        name = cV,
        isHovered = false,
        isClicked = false,
        visible = true,
        force_invisible = false,
        is_moving = false,
        check_ref = false,
        value = cU or nil
    }
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.CObjectItem:GetValue()
    local cW = "{" .. self.pos.x .. "," .. self.pos.y .. "}"

    return cW
end

function mariolua_menu.CObjectItem:SetValue(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end

function mariolua_menu.CObjectItem:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end

function mariolua_menu.CObjectItem:Update()
    if globalvars.block_moving_indicator or not self.visible then
        return
    end

    self.pos.x, self.pos.y =
        extended_math.clamp(self.pos.x, 5, globalvars.screen_size.w - 5),
        extended_math.clamp(self.pos.y, 5, globalvars.screen_size.h - 5)
    self.x_dif = not self.is_moving and self.pos.x - globalvars.mouse_x or self.x_dif
    self.y_dif = not self.is_moving and self.pos.y - globalvars.mouse_y or self.y_dif
    if
        (extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height) or
            self.is_moving) and
            (globalvars.used_Object == "" or globalvars.used_Object == self.reference)
     then
        if globalvars.mouse1_key_state then
            globalvars.used_Object = self.reference
            self.isClicked = true
            self.is_moving = true
            self:SetPosition(globalvars.mouse_x + self.x_dif, globalvars.mouse_y + self.y_dif)
        elseif not globalvars.mouse1_key_state and self.isClicked ~= false then
            globalvars.used_Object = ""
            self.isClicked = false
            self.is_moving = false
            mariolua_menu.SaveConfig()
        end
    end
end

function mariolua_menu.CObjectItem:Render()
    return
end

function mariolua_menu.CObject:new(cF, cX)
    table.insert(globalvars.cobject, cF)
    local cK = globalvars.cobject[#globalvars.cobject]
    globalvars.cobject[cK] = {
        type = "cobject",
        items = cX or {},
        reference = "cobject_" .. cF
    }
    self.__index = self

    return setmetatable(globalvars.cobject[cK], self)
end

function mariolua_menu.CObject:GetValue()
    return items
end

function mariolua_menu.CObject:AddItem(bo, a9, aa, cJ, aw, cY, q)
    table.insert(self.items, mariolua_menu.CObjectItem:new(bo, a9, aa, cJ, aw, cY))
end

function mariolua_menu.IndicatorItem:new(cZ, cH, cI, cJ, aw, cN)
    table.insert(globalvars.reference, "indicator_item_" .. cZ:gsub(" ", ""))
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "indicator_item",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        x_dif = 0,
        y_dif = 0,
        reference = "indicator_item_" .. cZ:gsub(" ", ""),
        text = cZ,
        isHovered = false,
        isClicked = false,
        visible = true,
        force_invisible = false,
        is_moving = false,
        check_ref = false,
        func = cN or 0
    }
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.IndicatorItem:GetValue()
    local cW = "{ " .. self.pos.x .. ", " .. self.pos.y .. " }"

    return cW
end

function mariolua_menu.IndicatorItem:SetValue(cH, cI)
    cH = extended_math.clamp(cH, 5, globalvars.screen_size.w - 5)
    cI = extended_math.clamp(cI, 5, globalvars.screen_size.h - 5)
    self.pos.x = cH
    self.pos.y = cI
end

function mariolua_menu.IndicatorItem:SetPosition(cH, cI)
    cH = extended_math.clamp(cH, 5, globalvars.screen_size.w - 5)
    cI = extended_math.clamp(cI, 5, globalvars.screen_size.h - 5)
    self.pos.x = cH
    self.pos.y = cI
end

function mariolua_menu.IndicatorItem:Update()
    if globalvars.block_moving_indicator or not self.visible then
        return
    end

    self.pos.x, self.pos.y =
        extended_math.clamp(self.pos.x, 5, globalvars.screen_size.w - 5),
        extended_math.clamp(self.pos.y, 5, globalvars.screen_size.h - 5)
    self.x_dif = not self.is_moving and self.pos.x - globalvars.mouse_x or self.x_dif
    self.y_dif = not self.is_moving and self.pos.y - globalvars.mouse_y or self.y_dif
    if
        (extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height) or
            self.is_moving) and
            (globalvars.used_Object == "" or globalvars.used_Object == self.reference)
     then
        if globalvars.mouse1_key_state then
            globalvars.used_Object = self.reference
            self.isClicked = true
            self.is_moving = true
            self:SetPosition(globalvars.mouse_x + self.x_dif, globalvars.mouse_y + self.y_dif)
        elseif not globalvars.mouse1_key_state and self.isClicked ~= false then
            globalvars.used_Object = ""
            self.isClicked = false
            self.is_moving = false
            mariolua_menu.SaveConfig()
        end
    end
end

function mariolua_menu.IndicatorItem:Render()
    return
end

function mariolua_menu.Indicator:new(cF, cX)
    table.insert(globalvars.indicator, cF)
    local cK = globalvars.indicator[#globalvars.indicator]
    globalvars.indicator[cK] = {
        type = "indicator",
        items = cX or {},
        reference = "indicator_" .. cF
    }
    self.__index = self

    return setmetatable(globalvars.indicator[cK], self)
end

function mariolua_menu.Indicator:GetValue()
    return self.items
end

function mariolua_menu.Indicator:AddItem(bo, a9, aa, cJ, aw, cY, q)
    a9 = extended_math.clamp(a9, 0, globalvars.screen_size.w - cJ / 4)
    aa = extended_math.clamp(aa, 0, globalvars.screen_size.h - aw / 4)
    table.insert(self.items, mariolua_menu.IndicatorItem:new(bo, a9, aa, cJ, aw, cY))
end

function mariolua_menu.Text:new(cF, cG, cH, cI, cJ, aw, c_, d0, callback)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")

        return
    end

    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "text",
        pos = {x = cH or 0, y = cI or 0},
        flag = c_ or "c",
        width = cJ or 0,
        height = aw or 0,
        alpha = 255,
        reference = cF,
        label = cG,
        should_save = d0 or false,
        isHovered = false,
        isClicked = false,
        visible = true,
        force_invisible = false,
        func = callback
    }
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.Text:SetActive(cL)
    self.visible = cL
end

function mariolua_menu.Text:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end

function mariolua_menu.Text:IsActive()
    return self.visible
end

function mariolua_menu.Text:GetPosition()
    return self.pos
end

function mariolua_menu.Text:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end

function mariolua_menu.Text:SetFunction(cN)
    self.func = cN
end

function mariolua_menu.Text:Update()
    self.visible = self.alpha == 0 and false or self.visible
end

function mariolua_menu.Text:Render()
    if not self.visible then
        return
    end

    local cQ, cR = renderer.measure_text(self.flag, self.label)

    if self.width == 0 then
        self.width = cQ
    end

    if self.height == 0 then
        self.height = cR
    end

    renderer.text(
        self.pos.x,
        self.pos.y,
        240,
        240,
        240,
        extended_math.clamp(self.alpha - 15, 0, 255),
        self.flag,
        0,
        self.label
    )
end

function mariolua_menu.Text:GetValue()
    return base64.stripchars("'" .. self.label .. "'", "][")
end

function mariolua_menu.Text:SetValue(cU)
    self.label = cU
end

function mariolua_menu.Groupbox:new(cF, cG, cH, cI, cJ, aw)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")

        return
    end

    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "groupbox",
        pos = {x = cH or 0, y = cI or 0},
        pos_x = cH or 0,
        pos_y = cI or 0,
        width = cJ or 0,
        height = aw or 0,
        def_width = cJ or 0,
        def_height = aw or 0,
        alpha = 255,
        reference = cF,
        label = cG,
        isHovered = false,
        isClicked = false,
        visible = true,
        force_invisible = false
    }
    self.__index = self
    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.Groupbox:SetActive(cL)
    self.visible = cL
end

function mariolua_menu.Groupbox:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end

function mariolua_menu.Groupbox:IsActive()
    return self.visible
end

function mariolua_menu.Groupbox:GetPosition()
    return self.pos
end

function mariolua_menu.Groupbox:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end

globalvars.menu = {
    x = 0,
    y = 0,
    w = 0,
    h = 0
}

function mariolua_menu.Groupbox:Update()
    if not self.visible then
        return
    end

    local _, d2, _, _, _, _ =
        extended_math.rect_getSegmentIntersectionIndices(
        globalvars.menu.x,
        globalvars.menu.y,
        globalvars.menu.w,
        globalvars.menu.h,
        self.pos_x + self.pos.x,
        self.pos_y + self.pos.y,
        self.pos_x + self.pos.x + self.width,
        self.pos_y + self.pos.y + self.height,
        0,
        globalvars.screen_size.h
    )
    d2 = d2 or 0

    if d2 < 0.98 then
        self.height = extended_math.clamp(self.height - (d2 * 100 - 100) * -1, 0, self.def_height)
    elseif d2 > 1.08 then
        self.height = extended_math.clamp(self.height + d2 * 100 - 100, 0, self.def_height)
    end
end

function mariolua_menu.Groupbox:Render()
    if not self.visible then
        return
    end

    local d7 = self.pos_x + self.pos.x
    local d8 = self.pos_y + self.pos.y
    local d9 = self.width
    local da = self.height
    local db = self.alpha
    local dc, _ = renderer.measure_text("b", self.label)
    renderer.rectangle(d7 + 2, d8 + 2, d9 - 4, da - 4, 40, 40, 40, extended_math.clamp(self.alpha - 205, 0, 255))
    renderer.line(d7, d8, d7 + 12, d8, 0, 0, 0, db)
    renderer.line(d7 + 14 + dc + 2, d8, d7 + d9, d8, 0, 0, 0, db)
    renderer.line(d7 + d9, d8, d7 + d9, d8 + da, 0, 0, 0, db)
    renderer.line(d7 + d9, d8 + da, d7, d8 + da, 0, 0, 0, db)
    renderer.line(d7, d8 + da, d7, d8, 0, 0, 0, db)
    renderer.line(d7 + 1, d8 + 1, d7 + 12, d8 + 1, 48, 48, 48, db)
    renderer.line(d7 + 14 + dc + 2, d8 + 1, d7 + d9 - 1, d8 + 1, 48, 48, 48, db)
    renderer.line(d7 + d9 - 1, d8 + 1, d7 + d9 - 1, d8 + da - 1, 48, 48, 48, db)
    renderer.line(d7 + d9 - 1, d8 + da - 1, d7 + 1, d8 + da - 1, 48, 48, 48, db)
    renderer.line(d7 + 1, d8 + da - 1, d7 + 1, d8 + 1, 48, 48, 48, db)
    renderer.text(d7 + 14, d8 - 6, 203, 203, 203, db, "b", 0, self.label)
end

function mariolua_menu.Groupbox:GetValue()
    return base64.stripchars("'" .. self.label .. "'", "][")
end

function mariolua_menu.Groupbox:SetValue(cU)
    self.label = cU
end

function mariolua_menu.Slider:new(cF, cG, cH, cI, cJ, aw, de, df, dg)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")

        return
    end

    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "slider",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 100,
        height = aw or 24,
        alpha = 255,
        min_val = df,
        max_val = dg,
        def_val = de,
        relative_pos = 0,
        circle_pos = cJ * de / dg or 0,
        x_var = x_var,
        reference = cF,
        label = cG,
        onRefreshRender = false,
        onRefreshUpdate = false,
        isHovered = false,
        isClicked = false,
        holding_slider = false,
        released_slider = false,
        visible = true,
        force_invisible = false
    }
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.Slider:SetActive(cL)
    self.visible = cL
end

function mariolua_menu.Slider:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end

function mariolua_menu.Slider:IsActive()
    return self.visible
end

function mariolua_menu.Slider:GetPosition()
    return self.pos
end

function mariolua_menu.Slider:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end

function mariolua_menu.Slider:SetFunction(cN)
    self.func = cN
end

function mariolua_menu.Slider:Update()
    self.visible = self.alpha == 0 and false or self.visible

    if not self.visible then
        return
    end

    local _, cR = renderer.measure_text("c", self.label)
    local dh = extended_math.IsNumberNegative(self.min_val)
    local di = dh and self.width / 2 or self.width
    local dj = self.pos.y + cR + 6
    local dk = dh and self.width / 2 + self.pos.x + 6 or self.pos.x + 6
    local dl = {dk - 2, dj + self.height, dk + di + 2, dj + self.height * 2}
    dl[1] = dh and dl[1] - di or dl[1]

    if
        (extended_math.inBounds(dl[1], dl[2], dl[3], dl[4]) or self.isClicked) and
            (globalvars.used_Object == "" or globalvars.used_Object == self.reference)
     then
        self.isHovered = true

        if globalvars.mouse1_key_state then
            globalvars.used_Object = self.reference
            self.isClicked = true
        else
            globalvars.used_Object = ""
            self.isClicked = false
        end

        if globalvars.mouse1_key_state and self.holding_slider ~= true then
            self.holding_slider = true
        elseif not globalvars.mouse1_key_state and self.holding_slider ~= false then
            self.holding_slider = false
            self.released_slider = true
            if self.released_slider then
                mariolua_menu.SaveConfig()
                self.released_slider = false
            end
        end
    else
        if self.isClicked and not globalvars.mouse1_key_state then
            globalvars.used_Object = ""
            self.isClicked = false
        end

        self.isHovered = false
    end

    if self.x_var == nil and self.def_val == nil then
        self.x_var = 0
    elseif self.def_val ~= nil and self.x_var == nil then
        self.x_var = self.def_val
    end

    if self.isClicked then
        self.relative_pos = extended_math.clamp(globalvars.mouse_x - dk, self.min_val, di)
        local dm = self.relative_pos / di * self.max_val
        self.x_var = dh and extended_math.clamp(dm, self.min_val, self.max_val) or dm
        self.circle_pos = di * self.x_var / self.max_val
    else
        self.circle_pos = di * self.x_var / self.max_val
    end
end

function mariolua_menu.Slider:Render()
    if self.visible then
        local cQ, cR = renderer.measure_text("c", self.label)
        local dh = extended_math.IsNumberNegative(self.min_val)
        local di = dh and self.width / 2 or self.width
        local dj = self.pos.y + cR + 6
        local dk = dh and self.width / 2 + self.pos.x + 6 or self.pos.x + 6
        local dn = {dk, dj + 2, self.circle_pos, self.height - 4}
        local dp = {dk + self.circle_pos, dj + 2, di - self.circle_pos, self.height - 4}
        local dq = {dk, dj + self.height / 2}
        local dr = {dk + di, dj + self.height / 2}
        dq[1] = dh and dq[1] - di or dq[1]
        dn[1] = dh and dn[1] - di or dn[1]
        dn[3] = dh and dn[3] + di or dn[3]

        if cfg.settings_guistyle:GetItemName() == "UwU" then
            renderer.circle_outline(dq[1], dq[2], 150, 100, 200, self.alpha, self.height / 2 - 2, 0, 1, self.height / 2)
            renderer.circle_outline(dr[1], dr[2], 130, 100, 150, self.alpha, self.height / 2 - 2, 0, 1, self.height / 2)
            renderer.rectangle(dn[1], dn[2], dn[3], dn[4], 150, 100, 200, self.alpha)
            renderer.rectangle(dp[1], dp[2], dp[3], dp[4], 130, 100, 150, self.alpha)
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["slider_circle"],
                dk + self.circle_pos - 6,
                dj,
                self.height,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
            dk = dh and dk - di or dk
            renderer.text(
                dk + cQ / 2,
                dj - cR,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
            local ds, dt = renderer.measure_text("b", extended_math.round(self.x_var))

            renderer.text(
                dk + self.circle_pos - ds / 2,
                dj + self.height,
                220,
                220,
                220,
                self.alpha,
                "b",
                0,
                extended_math.round(self.x_var, 0)
            )
        else
            renderer.circle_outline(dr[1], dr[2], 53, 53, 53, self.alpha, self.height / 2 - 2, 0, 1, self.height / 2)
            renderer.circle_outline(dq[1], dq[2], 151, 151, 151, self.alpha, self.height / 2 - 2, 0, 1, self.height / 2)
            renderer.rectangle(dn[1], dn[2], dn[3], dn[4], 151, 151, 151, self.alpha)
            renderer.rectangle(dp[1], dp[2], dp[3], dp[4], 53, 53, 53, self.alpha)
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["slider_circle"],
                dk + self.circle_pos - 6,
                dj,
                self.height,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
            dk = dh and dk - di or dk
            renderer.text(
                dk + cQ / 2,
                dj - cR,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
            local ds, dt = renderer.measure_text("b", extended_math.round(self.x_var))

            renderer.text(
                dp[1] - ds / 2,
                dj + self.height,
                220,
                220,
                220,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "b",
                0,
                extended_math.round(self.x_var, 0)
            )
        end
    end
end

function mariolua_menu.Slider:GetValue()
    return extended_math.clamp(extended_math.round(self.x_var), self.min_val, self.max_val)
end

function mariolua_menu.Slider:SetValue(du)
    if du == nil then
        mariolua.log("Trying to set slider value to a nil!")

        return
    end

    self.x_var = extended_math.clamp(du, self.min_val, self.max_val)
end

function mariolua_menu.MultiboxItem:new(cw, cZ)
    table.insert(globalvars.reference, "multibox_item_" .. cw .. "_" .. cZ:gsub(" ", ""))
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "multibox_item",
        text = cZ,
        isEnabled = false,
        isClicked = false,
        reference = "multibox_item_" .. cw .. "_" .. cZ:gsub(" ", "")
    }
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.MultiboxItem:GetValue()
    return self.isEnabled
end

function mariolua_menu.MultiboxItem:SetValue(as)
    self.isEnabled = as
    self.onRefresh = true
end

function mariolua_menu.Multibox:new(cF, cG, cH, cI, cJ, aw, cX, dv)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")

        return
    end

    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "multibox",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        old_w = cJ or 0,
        old_h = aw or 0,
        alpha = 255,
        reference = cF,
        label = cG,
        isHovered = false,
        isClicked = false,
        isOpen = false,
        maxSize = 0,
        visible = true,
        force_invisible = false,
        items = cX or {},
        enabled_items = enabled_items or {},
        enabledItemsText = "",
        one_enabled = dv or false,
        func = callback
    }
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end

function mariolua_menu.Multibox:SetActive(cL)
    self.visible = cL
end

function mariolua_menu.Multibox:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end

function mariolua_menu.Multibox:IsActive()
    return self.visible
end

function mariolua_menu.Multibox:GetPosition()
    return self.pos
end

function mariolua_menu.Multibox:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end

function mariolua_menu.Multibox:SetFunction(cN)
    self.func = cN
end

function mariolua_menu.Multibox:GetValue()
    return self.enabled_items
end

function mariolua_menu.Multibox:IsEnabled(dw)
    for I = 1, #self.items do
        if self.items[I].text == dw and self.items[I].isEnabled then
            return true
        end
    end
end

function mariolua_menu.Multibox:GetByIndex(dx)
    local dy = self.items[dx].isEnabled

    return dy
end

function mariolua_menu.Multibox:GetByName(aU)
    for I = 1, #self.items do
        if self.items[I].text == aU then
            return self.items[I]
        end
    end
end

function mariolua_menu.Multibox:SetValue(H)
    H = H or {}
    local enabled_items = {}

    for I = 1, #self.items do
        local dz = self.items[I]

        if extended_table.exists(H, dz.text) then
            dz.isEnabled = true
            table.insert(enabled_items, dz.text)
        else
            dz.isEnabled = false
        end
    end

    mariolua_menu.SaveConfig()
    self.enabled_items = enabled_items

    if extended_table.table_to_text then
        self.enabledItemsText = extended_table.table_to_text(self.enabled_items, self.width - 15)
    else
        self.enabledItemsText = ""
    end
end

function mariolua_menu.Multibox:SetItems(items)
    self.items = items
end

function mariolua_menu.Multibox:AddItem(cw, aU)
    table.insert(self.items, mariolua_menu.MultiboxItem:new(cw, aU))
end

function mariolua_menu.Multibox:Update()
    self.visible = self.alpha == 0 and false or self.visible

    if self.visible or self.onRefresh and globalvars.used_Object == "" then
        if extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height) then
            self.isHovered = true

            if self.maxSize == nil then
                self.maxSize = 0
            end

            if
                globalvars.mouse1_key_state and
                    (globalvars.used_Object == "" or globalvars.used_Object == self.reference) or
                    globalvars.mouse1_key_state and self.isOpen
             then
                self.isClicked = true
                globalvars.used_Object = self.reference
            elseif self.isClicked then
                globalvars.used_Object = ""
                self.isClicked = false

                if self.isOpen == false then
                    self.isOpen = true
                    self.maxSize = self.height * (#self.items + 1)
                elseif self.isOpen then
                    self.isOpen = false
                    self.maxSize = 0
                    mariolua_menu.SaveConfig()
                end
            end
        elseif
            not extended_math.inBounds(
                self.pos.x,
                self.pos.y + self.height,
                self.pos.x + self.width,
                self.pos.y + self.height * (#self.items + 1)
            ) and
                globalvars.mouse1_key_state and
                self.isOpen
         then
            self.isOpen = false
            self.maxSize = 0
            self.isHovered = false
            mariolua_menu.SaveConfig()
        elseif
            extended_math.inBounds(
                self.pos.x,
                self.pos.y + self.height,
                self.pos.x + self.width,
                self.pos.y + self.height * (#self.items + 1)
            ) and
                self.isOpen or
                self.onRefresh and globalvars.used_Object == ""
         then
            for I = 1, #self.items do
                local dz = self.items[I]

                if dz.onRefresh and globalvars.used_Object == "" then
                    if dz.isEnabled then
                        self.enabledItemsText = extended_table.table_to_text(self.enabled_items, self.width - 15)
                        dz.onRefresh = false
                    end

                    if I >= #self.items then
                        dz.onRefresh = false
                    end
                end

                if
                    extended_math.inBounds(
                        self.pos.x,
                        self.pos.y + I * self.height,
                        self.pos.x + self.width,
                        self.pos.y + I * self.height + self.height
                    ) and
                        (globalvars.used_Object == "" or globalvars.used_Object == dz.reference)
                 then
                    if globalvars.mouse1_key_state then
                        globalvars.used_Object = dz.reference
                        dz.isClicked = true

                        if mariolua_menu.SaveConfig() ~= nil then
                            mariolua_menu.SaveConfig()
                        end
                    elseif dz.isClicked then
                        globalvars.used_Object = ""
                        dz.isClicked = false

                        if dz.isEnabled ~= true then
                            dz.isEnabled = true
                            self.enabledItemsText = extended_table.table_to_text(self.enabled_items, self.width - 15)
                        elseif dz.isEnabled ~= false then
                            dz.isEnabled = false
                            self.enabledItemsText = extended_table.table_to_text(self.enabled_items, self.width - 15)
                        end

                        if mariolua_menu.SaveConfig() ~= nil then
                            mariolua_menu.SaveConfig()
                        end
                    end
                end
            end
        else
            self.isHovered = false
        end

        if self.maxSize == nil then
            self.maxSize = 0
        end
    else
        self.isOpen = false
        self.maxSize = 0
        self.isClicked = false
        self.isHovered = false
    end

    for I = 1, #self.items do
        local dz = self.items[I]

        if dz.isEnabled and not extended_table.Contains(self.enabled_items, dz.text) then
            table.insert(self.enabled_items, dz.text)
            self.enabledItemsText = extended_table.table_to_text(self.enabled_items, self.width - 15)
        elseif not dz.isEnabled and extended_table.Contains(self.enabled_items, dz.text) then
            for cl = 1, #self.enabled_items do
                if self.enabled_items[cl] == dz.text then
                    table.remove(self.enabled_items, cl)
                    self.enabledItemsText = extended_table.table_to_text(self.enabled_items, self.width - 15)
                end
            end
        end

        if dz.onRefresh then
            self.onRefresh = true
        else
            self.onRefresh = false
        end
    end
end

function mariolua_menu.Multibox:Render()
    if self.visible then
        if self.isOpen then
            if cfg.settings_guistyle:GetItemName() == "Aimware" or cfg.settings_guistyle:GetItemName() == "Fuhrer" then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["combobox_open"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            elseif cfg.settings_guistyle:GetItemName() == "UwU" then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["combobox_open"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    150,
                    100,
                    180,
                    self.alpha
                )
            elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
                extended_renderer.rectangle_outline(
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    12,
                    12,
                    12,
                    self.alpha
                )
                renderer.gradient(
                    self.pos.x + 1,
                    self.pos.y + 1,
                    self.width - 2,
                    self.height - 2,
                    35,
                    35,
                    35,
                    self.alpha,
                    30,
                    30,
                    30,
                    self.alpha,
                    true
                )
            end

            for I = 1, #self.items do
                local dz = self.items[I]

                if cfg.settings_guistyle:GetItemName() == "Aimware" then
                    if
                        extended_math.inBounds(
                            self.pos.x,
                            self.pos.y + I * self.height,
                            self.pos.x + self.width,
                            self.pos.y + I * self.height + self.height
                        )
                     then
                        if I == #self.items then
                            extended_renderer.draw_svg(
                                extended_renderer.gpx.svg["combobox_itembox_mouse_over"],
                                self.pos.x,
                                self.pos.y + I * self.height,
                                self.width,
                                self.height,
                                255,
                                255,
                                255,
                                self.alpha
                            )
                        else
                            extended_renderer.draw_svg(
                                extended_renderer.gpx.svg["combobox_itembox_between_mouse_over"],
                                self.pos.x,
                                self.pos.y + I * self.height,
                                self.width,
                                self.height,
                                255,
                                255,
                                255,
                                self.alpha
                            )
                        end
                    else
                        if dz.isEnabled then
                            if I == #self.items then
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_mouse_over"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    255,
                                    255,
                                    self.alpha
                                )
                            else
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_between_mouse_over"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    255,
                                    255,
                                    self.alpha
                                )
                            end
                        else
                            if I == #self.items then
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_between"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    255,
                                    255,
                                    self.alpha
                                )
                            else
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    255,
                                    255,
                                    self.alpha
                                )
                            end
                        end
                    end

                    local cQ, _ = renderer.measure_text("c", dz.text)
                    renderer.text(
                        self.pos.x + cQ / 2 + 5,
                        self.pos.y + I * self.height + self.height / 2,
                        240,
                        240,
                        240,
                        extended_math.clamp(self.alpha - 15, 0, 255),
                        "c",
                        0,
                        dz.text
                    )
                elseif cfg.settings_guistyle:GetItemName() == "UwU" then
                    if
                        extended_math.inBounds(
                            self.pos.x,
                            self.pos.y + I * self.height,
                            self.pos.x + self.width,
                            self.pos.y + I * self.height + self.height
                        )
                     then
                        if I == #self.items then
                            extended_renderer.draw_svg(
                                extended_renderer.gpx.svg["combobox_itembox_mouse_over"],
                                self.pos.x,
                                self.pos.y + I * self.height,
                                self.width,
                                self.height,
                                255,
                                60,
                                180,
                                self.alpha
                            )
                        else
                            extended_renderer.draw_svg(
                                extended_renderer.gpx.svg["combobox_itembox_between_mouse_over"],
                                self.pos.x,
                                self.pos.y + I * self.height,
                                self.width,
                                self.height,
                                255,
                                60,
                                180,
                                self.alpha
                            )
                        end
                    else
                        if dz.isEnabled then
                            if I == #self.items then
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_mouse_over"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    60,
                                    180,
                                    self.alpha
                                )
                            else
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_between_mouse_over"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    60,
                                    180,
                                    self.alpha
                                )
                            end
                        else
                            if I == #self.items then
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_between"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    60,
                                    180,
                                    self.alpha
                                )
                            else
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    60,
                                    180,
                                    self.alpha
                                )
                            end
                        end
                    end

                    local cQ, _ = renderer.measure_text("c", dz.text)
                    renderer.text(
                        self.pos.x + cQ / 2 + 5,
                        self.pos.y + I * self.height + self.height / 2,
                        240,
                        240,
                        240,
                        extended_math.clamp(self.alpha - 15, 0, 255),
                        "c",
                        0,
                        dz.text
                    )
                elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
                    if
                        extended_math.inBounds(
                            self.pos.x,
                            self.pos.y + I * self.height,
                            self.pos.x + self.width,
                            self.pos.y + I * self.height + self.height
                        )
                     then
                        local dA, dB, dC, dD = 25, 25, 25, self.alpha

                        if I == #self.items then
                            renderer.gradient(
                                self.pos.x + 1,
                                self.pos.y + I * self.height + 1,
                                self.width - 2,
                                self.height,
                                dA,
                                dB,
                                dC,
                                dD,
                                dA - 5,
                                dB - 5,
                                dC - 5,
                                dD,
                                true
                            )
                        else
                            renderer.gradient(
                                self.pos.x,
                                self.pos.y + I * self.height + 2,
                                self.width,
                                self.height,
                                dA,
                                dB,
                                dC,
                                dD,
                                dA - 5,
                                dB - 5,
                                dC - 5,
                                dD,
                                true
                            )
                        end
                    else
                        local dA, dB, dC, dD = 35, 35, 35, self.alpha

                        if I == #self.items then
                            renderer.gradient(
                                self.pos.x + 1,
                                self.pos.y + I * self.height + 1,
                                self.width - 2,
                                self.height,
                                dA,
                                dB,
                                dC,
                                dD,
                                dA - 5,
                                dB - 5,
                                dC - 5,
                                dD,
                                true
                            )
                        else
                            renderer.gradient(
                                self.pos.x,
                                self.pos.y + I * self.height + 2,
                                self.width,
                                self.height,
                                dA,
                                dB,
                                dC,
                                dD,
                                dA - 5,
                                dB - 5,
                                dC - 5,
                                dD,
                                true
                            )
                        end
                    end

                    extended_renderer.rectangle_outline(
                        self.pos.x,
                        self.pos.y + self.height + 2,
                        self.width,
                        #self.items * self.height,
                        12,
                        12,
                        12,
                        self.alpha
                    )
                    local cQ, _ = renderer.measure_text("c", dz.text)

                    if dz.isEnabled then
                        renderer.text(
                            self.pos.x + cQ / 2 + 5,
                            self.pos.y + I * self.height + self.height / 2,
                            globalvars.color[1],
                            globalvars.color[2],
                            globalvars.color[3],
                            self.alpha,
                            "c",
                            0,
                            dz.text
                        )
                    else
                        renderer.text(
                            self.pos.x + cQ / 2 + 5,
                            self.pos.y + I * self.height + self.height / 2,
                            240,
                            240,
                            240,
                            extended_math.clamp(self.alpha - 15, 0, 255),
                            "c",
                            0,
                            dz.text
                        )
                    end
                end
            end
        end

        if cfg.settings_guistyle:GetItemName() == "Aimware" or cfg.settings_guistyle:GetItemName() == "Fuhrer" then
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["combobox_closed"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
        elseif cfg.settings_guistyle:GetItemName() == "UwU" then
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["combobox_closed"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                150,
                100,
                180,
                self.alpha
            )
        elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
            local ap, av, a5, G

            if self.isHovered then
                ap, av, a5, G = 45, 45, 45, self.alpha
            else
                ap, av, a5, G = 35, 35, 35, self.alpha
            end

            extended_renderer.rectangle_outline(self.pos.x, self.pos.y, self.width, self.height, 12, 12, 12, self.alpha)
            renderer.gradient(
                self.pos.x + 1,
                self.pos.y + 1,
                self.width - 2,
                self.height - 2,
                ap,
                av,
                a5,
                G,
                ap - 5,
                av - 5,
                a5 - 5,
                G,
                true
            )

            if not self.isOpen then
                renderer.rectangle(self.pos.x + self.width - 14, self.pos.y + 11, 5, 1, 181, 181, 181, self.alpha)
                renderer.rectangle(self.pos.x + self.width - 13, self.pos.y + 12, 3, 1, 181, 181, 181, self.alpha)
                renderer.rectangle(self.pos.x + self.width - 12, self.pos.y + 13, 1, 1, 181, 181, 181, self.alpha)
            end
        end

        if cfg.settings_guistyle:GetItemName() == "Gamesense" then
            if self.isOpen then
                renderer.rectangle(self.pos.x + self.width - 12, self.pos.y + 11, 1, 1, 181, 181, 181, self.alpha)
                renderer.rectangle(self.pos.x + self.width - 13, self.pos.y + 12, 3, 1, 181, 181, 181, self.alpha)
                renderer.rectangle(self.pos.x + self.width - 14, self.pos.y + 13, 5, 1, 181, 181, 181, self.alpha)
            end
        elseif cfg.settings_guistyle:GetItemName() == "Aimware" or cfg.settings_guistyle:GetItemName() == "Fuhrer" then
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["combobox_arrow"],
                self.pos.x + self.width - (12 + 24 - self.height) - 5,
                self.pos.y,
                12 + 24 - self.height,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
        end

        local dE, dF, dG, dH

        if cfg.settings_guistyle:GetItemName() == "Aimware" or cfg.settings_guistyle:GetItemName() == "Fuhrer" then
            dE, dF, dG, dH = 30, 30, 30, extended_math.clamp(self.alpha - 15, 0, 255)
        elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
            dE, dF, dG, dH = 180, 180, 180, extended_math.clamp(self.alpha - 15, 0, 255)
        else
            dE, dF, dG, dH = 200, 150, 180, extended_math.clamp(self.alpha - 15, 0, 255)
        end

        -- Fix added by TherioJunior to avoid having selected items inside a multiselect go out of bounds of the rendered box
        local torender = ""

        if (self.enabledItemsText:len() > 28) then
            torender = self.enabledItemsText:sub(1, 28) .. "..."
        else
            torender = self.enabledItemsText
        end

        local dI, _ = renderer.measure_text("c", torender)
        local cQ, cR = renderer.measure_text("c", self.label)

        renderer.text(self.pos.x + dI / 2 + 5, self.pos.y + self.height / 2, dE, dF, dG, dH, "c", 0, torender)
        renderer.text(
            self.pos.x + cQ / 2,
            self.pos.y - cR,
            240,
            240,
            240,
            extended_math.clamp(self.alpha - 15, 0, 255),
            "c",
            0,
            self.label
        )
    end
end

function mariolua_menu.ComboboxItem:new(cw, cZ)
    if cZ == nil then
        return
    end

    local cK = cw .. ".combobox." .. cZ:gsub(" ", "_"):lower()
    globalvars.reference[cK] = {
        type = "combobox_item",
        text = cZ,
        isEnabled = false,
        isClicked = false,
        reference = cw .. ".item." .. cZ:gsub(" ", "_")
    }
    self.__index = self

    return setmetatable(globalvars.reference[cK], self)
end
function mariolua_menu.Combobox:new(cF, cG, cH, cI, cJ, aw, cX, dK)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")
        return
    end
    cX = cX or {}
    local dL = {}
    for I = 1, #cX do
        cX[I] = mariolua_menu.ComboboxItem:new(cF, cX[I])
        dL[I] = cX[I]
    end
    dK = type(dK) == "number" and dK or 1
    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "combobox",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        alpha = 255,
        reference = cF,
        label = cG,
        isHovered = false,
        isClicked = false,
        isOpen = false,
        maxSize = 0,
        visible = true,
        force_invisible = false,
        items = cX or {},
        item_list = dL or {},
        enabledItem = dK or 1
    }
    self.__index = self
    return setmetatable(globalvars.reference[cK], self)
end
function mariolua_menu.Combobox:Update()
    self.visible = self.alpha == 0 and false or self.visible
    if self.enabledItem == nil then
        self.enabledItem = 1
    else
        if type(self.enabledItem) == "boolean" or type(self.enabledItem) == "string" then
            self.enabledItem = 1
        end
        if tonumber(self.enabledItem) <= 0 then
            self.enabledItem = 1
        end
    end
    self.visible = self.alpha == 0 and false or self.visible
    if self.visible then
        if
            extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height) and
                (globalvars.used_Object == "" or globalvars.used_Object == self.reference)
         then
            self.isHovered = true
            if self.maxSize == nil then
                self.maxSize = 0
            end
            if globalvars.mouse1_key_state then
                globalvars.used_Object = self.reference
                self.isClicked = true
            elseif self.isClicked then
                globalvars.used_Object = ""
                self.isClicked = false
                if self.isOpen == false then
                    self.isOpen = true
                    self.maxSize = self.height * (#self.items + 1)
                elseif self.isOpen then
                    self.isOpen = false
                    self.maxSize = 0
                end
            end
        else
            if
                not extended_math.inBounds(
                    self.pos.x,
                    self.pos.y + self.height,
                    self.pos.x + self.width,
                    self.pos.y + self.maxSize
                ) and
                    globalvars.mouse1_key_state and
                    self.isOpen
             then
                self.isOpen = false
                self.maxSize = 0
            end
            self.isHovered = false
        end
        if self.maxSize == nil then
            self.maxSize = 0
        end
        for I = 1, #self.items do
            local dz = self.items[I]
            if
                self.isOpen and
                    extended_math.inBounds(
                        self.pos.x,
                        self.pos.y + I * self.height,
                        self.pos.x + self.width,
                        self.pos.y + I * self.height + self.height
                    )
             then
                if globalvars.mouse1_key_state then
                    dz.isClicked = true
                elseif dz.isClicked then
                    dz.isClicked = false
                    if dz.isEnabled == false then
                        if self.enabledItem ~= I and self.enabledItem ~= 0 then
                            if self.items[self.enabledItem] ~= nil then
                                self.items[self.enabledItem].isEnabled = false
                            end
                            dz.isEnabled = true
                            self.enabledItem = I
                        end
                        if self.enabledItem <= 0 then
                            self.items[1].isEnabled = true
                            self.enabledItem = 1
                        end
                        mariolua_menu.SaveConfig()
                    elseif dz.isEnabled then
                        if self.enabledItem == I then
                            dz.isEnabled = false
                            self.enabledItem = 1
                            mariolua_menu.SaveConfig()
                        end
                    end
                end
            end
            if dz.isEnabled == false and self.enabledItem == I then
                dz.isEnabled = true
                mariolua_menu.SaveConfig()
            elseif dz.isEnabled == true and self.enabledItem ~= I then
                dz.isEnabled = false
                mariolua_menu.SaveConfig()
            end
        end
    else
        self.isOpen = false
        self.maxSize = 0
        self.isClicked = false
        self.isHovered = false
    end
end
function mariolua_menu.Combobox:Render()
    if self.visible then
        if self.isOpen then
            if cfg.settings_guistyle:GetItemName() == "Aimware" or cfg.settings_guistyle:GetItemName() == "Fuhrer" then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["combobox_open"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            elseif cfg.settings_guistyle:GetItemName() == "UwU" then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["combobox_open"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    150,
                    100,
                    180,
                    self.alpha
                )
            elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
                extended_renderer.rectangle_outline(
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    12,
                    12,
                    12,
                    self.alpha
                )
                renderer.gradient(
                    self.pos.x + 1,
                    self.pos.y + 1,
                    self.width - 2,
                    self.height - 2,
                    35,
                    35,
                    35,
                    self.alpha,
                    30,
                    30,
                    30,
                    self.alpha,
                    true
                )
            end
            for I = 1, #self.items do
                local dz = self.items[I]
                if
                    extended_math.inBounds(
                        self.pos.x,
                        self.pos.y + I * self.height,
                        self.pos.x + self.width,
                        self.pos.y + I * self.height + self.height
                    )
                 then
                    if
                        cfg.settings_guistyle:GetItemName() == "Aimware" or
                            cfg.settings_guistyle:GetItemName() == "Fuhrer"
                     then
                        if I == #self.items then
                            extended_renderer.draw_svg(
                                extended_renderer.gpx.svg["combobox_itembox_mouse_over"],
                                self.pos.x,
                                self.pos.y + I * self.height,
                                self.width,
                                self.height,
                                255,
                                255,
                                255,
                                self.alpha
                            )
                        else
                            extended_renderer.draw_svg(
                                extended_renderer.gpx.svg["combobox_itembox_between_mouse_over"],
                                self.pos.x,
                                self.pos.y + I * self.height,
                                self.width,
                                self.height,
                                255,
                                255,
                                255,
                                self.alpha
                            )
                        end
                    elseif cfg.settings_guistyle:GetItemName() == "UwU" then
                        if I == #self.items then
                            extended_renderer.draw_svg(
                                extended_renderer.gpx.svg["combobox_itembox_mouse_over"],
                                self.pos.x,
                                self.pos.y + I * self.height,
                                self.width,
                                self.height,
                                255,
                                60,
                                180,
                                self.alpha
                            )
                        else
                            extended_renderer.draw_svg(
                                extended_renderer.gpx.svg["combobox_itembox_between_mouse_over"],
                                self.pos.x,
                                self.pos.y + I * self.height,
                                self.width,
                                self.height,
                                255,
                                60,
                                180,
                                self.alpha
                            )
                        end
                    elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
                        local dA, dB, dC, dD = 25, 25, 25, self.alpha
                        if I == #self.items then
                            renderer.gradient(
                                self.pos.x + 1,
                                self.pos.y + I * self.height + 1,
                                self.width - 2,
                                self.height,
                                dA,
                                dB,
                                dC,
                                dD,
                                dA - 5,
                                dB - 5,
                                dC - 5,
                                dD,
                                true
                            )
                        else
                            renderer.gradient(
                                self.pos.x,
                                self.pos.y + I * self.height + 2,
                                self.width,
                                self.height,
                                dA,
                                dB,
                                dC,
                                dD,
                                dA - 5,
                                dB - 5,
                                dC - 5,
                                dD,
                                true
                            )
                        end
                    end
                else
                    if
                        cfg.settings_guistyle:GetItemName() == "Aimware" or
                            cfg.settings_guistyle:GetItemName() == "Fuhrer"
                     then
                        if dz.isEnabled then
                            if I == #self.items then
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_mouse_over"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    255,
                                    255,
                                    self.alpha
                                )
                            else
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_between_mouse_over"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    255,
                                    255,
                                    self.alpha
                                )
                            end
                        else
                            if I == #self.items then
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_between"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    255,
                                    255,
                                    self.alpha
                                )
                            else
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    255,
                                    255,
                                    self.alpha
                                )
                            end
                        end
                    elseif cfg.settings_guistyle:GetItemName() == "UwU" then
                        if dz.isEnabled then
                            if I == #self.items then
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_mouse_over"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    60,
                                    180,
                                    self.alpha
                                )
                            else
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_between_mouse_over"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    60,
                                    180,
                                    self.alpha
                                )
                            end
                        else
                            if I == #self.items then
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox_between"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    60,
                                    180,
                                    self.alpha
                                )
                            else
                                extended_renderer.draw_svg(
                                    extended_renderer.gpx.svg["combobox_itembox"],
                                    self.pos.x,
                                    self.pos.y + I * self.height,
                                    self.width,
                                    self.height,
                                    255,
                                    60,
                                    180,
                                    self.alpha
                                )
                            end
                        end
                    elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
                        renderer.gradient(
                            self.pos.x,
                            self.pos.y + I * self.height + 2,
                            self.width,
                            self.height,
                            35,
                            35,
                            35,
                            self.alpha,
                            30,
                            30,
                            30,
                            self.alpha,
                            true
                        )
                        extended_renderer.rectangle_outline(
                            self.pos.x,
                            self.pos.y + self.height + 2,
                            self.width,
                            #self.items * self.height,
                            12,
                            12,
                            12,
                            self.alpha
                        )
                        renderer.rectangle(
                            self.pos.x + self.width - 12,
                            self.pos.y + 11,
                            1,
                            1,
                            181,
                            181,
                            181,
                            self.alpha
                        )
                        renderer.rectangle(
                            self.pos.x + self.width - 13,
                            self.pos.y + 12,
                            3,
                            1,
                            181,
                            181,
                            181,
                            self.alpha
                        )
                        renderer.rectangle(
                            self.pos.x + self.width - 14,
                            self.pos.y + 13,
                            5,
                            1,
                            181,
                            181,
                            181,
                            self.alpha
                        )
                    end
                end
                local cQ, cR = renderer.measure_text("c", dz.text)
                if dz.isEnabled and cfg.settings_guistyle:GetItemName() == "Gamesense" then
                    renderer.text(
                        self.pos.x + cQ / 2 + 5,
                        self.pos.y + I * self.height + self.height / 2,
                        globalvars.color[1],
                        globalvars.color[1],
                        globalvars.color[1],
                        self.alpha,
                        "c",
                        0,
                        dz.text
                    )
                else
                    renderer.text(
                        self.pos.x + cQ / 2 + 5,
                        self.pos.y + I * self.height + self.height / 2,
                        240,
                        240,
                        240,
                        extended_math.clamp(self.alpha - 15, 0, 255),
                        "c",
                        0,
                        dz.text
                    )
                end
                if
                    self.enabledItem == 0 or self.enabledItem == tostring(0) or
                        self.enabledItem == I and not self.items[I].isEnabled
                 then
                    if I > 0 then
                        self.enabledItem = I
                        self.items[I].isEnabled = true
                        if mariolua_menu.SaveConfig() ~= nil then
                            mariolua_menu.SaveConfig()
                        end
                    end
                end
            end
        else
            if cfg.settings_guistyle:GetItemName() == "Aimware" or cfg.settings_guistyle:GetItemName() == "Fuhrer" then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["combobox_closed"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            elseif cfg.settings_guistyle:GetItemName() == "UwU" then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["combobox_closed"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    150,
                    100,
                    180,
                    self.alpha
                )
            elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
                local ap, av, a5, G
                if self.isHovered or self.isOpen then
                    ap, av, a5, G = 45, 45, 45, self.alpha
                else
                    ap, av, a5, G = 35, 35, 35, self.alpha
                end
                extended_renderer.rectangle_outline(
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    12,
                    12,
                    12,
                    self.alpha
                )
                renderer.gradient(
                    self.pos.x + 1,
                    self.pos.y + 1,
                    self.width - 2,
                    self.height - 2,
                    ap,
                    av,
                    a5,
                    G,
                    ap - 5,
                    av - 5,
                    a5 - 5,
                    G,
                    true
                )
                renderer.rectangle(self.pos.x + self.width - 14, self.pos.y + 11, 5, 1, 181, 181, 181, self.alpha)
                renderer.rectangle(self.pos.x + self.width - 13, self.pos.y + 12, 3, 1, 181, 181, 181, self.alpha)
                renderer.rectangle(self.pos.x + self.width - 12, self.pos.y + 13, 1, 1, 181, 181, 181, self.alpha)
            end
        end
        local dM
        if self.enabledItem == 0 or self.items[self.enabledItem] == nil then
            dM = ""
        else
            dM = self.items[self.enabledItem].text
        end
        if self.enabledItem == 0 or tonumber(self.enabledItem) == 0 then
            self.enabledItem = 1
            self.items[1].isEnabled = true
            mariolua_menu.SaveConfig()
        end
        self.enabledItem = extended_math.clamp(self.enabledItem, 1, 99)
        if self.items[self.enabledItem] ~= nil then
            self.items[self.enabledItem].isEnabled = true
        end
        if cfg.settings_guistyle:GetItemName() == "Aimware" or cfg.settings_guistyle:GetItemName() == "Fuhrer" then
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["combobox_arrow"],
                self.pos.x + self.width - (12 + 24 - self.height) - 5,
                self.pos.y,
                12 + 24 - self.height,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
        elseif cfg.settings_guistyle:GetItemName() == "UwU" then
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["combobox_arrow"],
                self.pos.x + self.width - (12 + 24 - self.height) - 5,
                self.pos.y,
                12 + 24 - self.height,
                self.height,
                255,
                60,
                180,
                self.alpha
            )
        end
        local dI, dJ = renderer.measure_text("c", dM)
        if cfg.settings_guistyle:GetItemName() == "Aimware" or cfg.settings_guistyle:GetItemName() == "Fuhrer" then
            renderer.text(
                self.pos.x + dI / 2 + 5,
                self.pos.y + self.height / 2,
                30,
                30,
                30,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                dM
            )
        elseif cfg.settings_guistyle:GetItemName() == "UwU" then
            renderer.text(
                self.pos.x + dI / 2 + 5,
                self.pos.y + self.height / 2,
                200,
                150,
                180,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                dM
            )
        elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
            renderer.text(
                self.pos.x + dI / 2 + 5,
                self.pos.y + self.height / 2,
                180,
                180,
                180,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                dM
            )
        end
        local cQ, cR = renderer.measure_text("c", self.label)
        renderer.text(
            self.pos.x + cQ / 2,
            self.pos.y - cR,
            240,
            240,
            240,
            extended_math.clamp(self.alpha - 15, 0, 255),
            "c",
            0,
            self.label
        )
    end
end
function mariolua_menu.Combobox:SetActive(cL)
    self.visible = cL
end
function mariolua_menu.Combobox:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end
function mariolua_menu.Combobox:IsActive()
    return self.visible
end
function mariolua_menu.Combobox:GetPosition()
    return self.pos
end
function mariolua_menu.Combobox:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end
function mariolua_menu.Combobox:SetFunction(cN)
    self.func = cN
end
function mariolua_menu.Combobox:SetItems(items)
    if items == nil then
        return
    end
    for I = 1, #items do
        if items[I] == nil then
            return
        end
        table.insert(self.items, mariolua_menu.ComboboxItem:new(self.reference, items[I]))
    end
    self.item_list = items
end
function mariolua_menu.Combobox:AddItem(aU)
    if aU == nil then
        return
    end
    table.insert(self.items, mariolua_menu.ComboboxItem:new(self.reference, aU))
    table.insert(self.item_list, aU)
end
function mariolua_menu.Combobox:GetValue()
    return self.enabledItem
end
function mariolua_menu.Combobox:GetItemName()
    if self.items[self.enabledItem] ~= nil then
        return self.items[self.enabledItem].text
    else
        return false
    end
end
function mariolua_menu.Combobox:SetValue(dw, H)
    H = H or true
    if type(dw) == "string" then
        for I = 1, #self.items do
            local dN = self.items[I].text
            if dN == dw then
                self.items[I].isEnabled = H
                self.enabledItem = H and I or self.enabledItem
                return
            end
        end
    else
        self.enabledItem = H and dw or self.enabledItem
        self.items[dw].isEnabled = H
    end
end
function mariolua_menu.Combobox:GetItems()
    return self.item_list
end
function mariolua_menu.Checkbox:new(cF, cG, cH, cI, cJ, aw, dO, callback, dP, dQ)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")
        return
    end
    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "checkbox",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        alpha = 255,
        reference = cF,
        label = cG,
        isHovered = false,
        isClicked = false,
        isEnabled = dO or false,
        visible = true,
        force_invisible = false,
        func = callback or nil,
        on_enable_func = dP or nil,
        on_disable_func = dQ or nil
    }
    self.__index = self
    return setmetatable(globalvars.reference[cK], self)
end
function mariolua_menu.Checkbox:SetActive(cL)
    self.visible = cL
end
function mariolua_menu.Checkbox:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end
function mariolua_menu.Checkbox:IsActive()
    return self.visible
end
function mariolua_menu.Checkbox:GetPosition()
    return self.pos
end
function mariolua_menu.Checkbox:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end
function mariolua_menu.Checkbox:SetFunction(cN)
    self.func = cN
end
function mariolua_menu.Checkbox:Update()
    self.visible = self.alpha == 0 and false or self.visible
    if self.visible then
        self.isEnabled = self.isEnabled or false
        if self.isEnabled and self.func then
            self.func()
        end
        if
            extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height) and
                (globalvars.used_Object == "" or globalvars.used_Object == self.reference)
         then
            self.isHovered = true
            if globalvars.mouse1_key_state then
                globalvars.used_Object = self.reference
                self.isClicked = true
            elseif self.isClicked then
                globalvars.used_Object = ""
                self.isClicked = false
                if not self.isEnabled then
                    self.isEnabled = true
                    if self.on_enable_func then
                        self.on_enable_func()
                    end
                elseif self.isEnabled then
                    self.isEnabled = false
                    if self.on_enable_func then
                        self.on_disable_func()
                    end
                end
                mariolua_menu.SaveConfig()
            end
        else
            self.isHovered = false
        end
    end
end
function mariolua_menu.Checkbox:Render()
    if self.visible then
        if cfg.settings_guistyle:GetItemName() == "Aimware" then
            if self.isHovered then
                if self.isEnabled then
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["checkbox_checked"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        self.alpha
                    )
                    renderer.rectangle(
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        extended_math.clamp(self.alpha - 205, 0, 255)
                    )
                else
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["checkbox_unchecked_mouse_over"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        self.alpha
                    )
                end
            elseif self.isEnabled then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["checkbox_checked"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["checkbox_unchecked"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            end
            local cQ, cR = renderer.measure_text("c", self.label)
            renderer.text(
                self.pos.x + self.width + cQ / 2 + 7,
                self.pos.y + self.height / 2,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
        elseif cfg.settings_guistyle:GetItemName() == "UwU" then
            if self.isHovered then
                if self.isEnabled then
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["checkbox_checked"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        60,
                        180,
                        self.alpha
                    )
                    renderer.rectangle(
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        extended_math.clamp(self.alpha - 205, 0, 255)
                    )
                else
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["checkbox_unchecked_mouse_over"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        60,
                        180,
                        self.alpha
                    )
                end
            elseif self.isEnabled then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["checkbox_checked"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    60,
                    180,
                    self.alpha
                )
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["checkbox_unchecked"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    60,
                    180,
                    self.alpha
                )
            end
            local cQ, cR = renderer.measure_text("c", self.label)
            renderer.text(
                self.pos.x + self.width + cQ / 2 + 7,
                self.pos.y + self.height / 2,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
        elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
            extended_renderer.rectangle_outline(self.pos.x + 8, self.pos.y + 8, 10, 10, 12, 12, 12, self.alpha)
            if self.isEnabled then
                renderer.rectangle(
                    self.pos.x + 9,
                    self.pos.y + 9,
                    8,
                    8,
                    globalvars.color[1],
                    globalvars.color[2],
                    globalvars.color[3],
                    self.alpha
                )
            else
                renderer.rectangle(self.pos.x + 9, self.pos.y + 9, 8, 8, 35, 35, 35, self.alpha)
            end
            local cQ, cR = renderer.measure_text("c", self.label)
            renderer.text(
                self.pos.x + self.width + cQ / 2 + 7,
                self.pos.y + 12,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
        else
            if self.isHovered then
                if self.isEnabled then
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["checkbox_checked"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        self.alpha
                    )
                    renderer.rectangle(
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        extended_math.clamp(self.alpha - 205, 0, 255)
                    )
                else
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["checkbox_unchecked_mouse_over"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        self.alpha
                    )
                end
            elseif self.isEnabled then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["checkbox_checked"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["checkbox_unchecked"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            end
            local cQ, cR = renderer.measure_text("c", self.label)
            renderer.text(
                self.pos.x + self.width + cQ / 2 + 7,
                self.pos.y + self.height / 2,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
        end
    end
end
function mariolua_menu.Checkbox:GetValue()
    return self.isEnabled
end
function mariolua_menu.Checkbox:SetValue(dR)
    if dR == nil or type(dR) ~= "boolean" then
        if type(dR) == "string" then
            if dR == "true" then
                self.isEnabled = true
            else
                self.isEnabled = false
            end
        else
            mariolua.log("Can set " .. self.label .. " " .. self.type .. " argument is a nil")
            self.isEnabled = false
        end
    else
        self.isEnabled = dR
    end
end
function mariolua_menu.Keybox:new(cF, cG, cH, cI, cJ, aw, callback)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")
        return
    end
    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "keybox",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        old_w = cJ or 0,
        old_h = aw or 0,
        alpha = 255,
        reference = cF,
        label = cG,
        bSet = false,
        hold_count = 0,
        isHovered = false,
        isClicked = false,
        isBinding = false,
        isKeyDown = false,
        isKeyReleased = false,
        isKeyToggle = false,
        key_down_time = 0,
        key = 0,
        key_name = "None",
        visible = true,
        force_invisible = false,
        func = callback
    }
    self.__index = self
    return setmetatable(globalvars.reference[cK], self)
end
function mariolua_menu.Keybox:SetActive(cL)
    self.visible = cL
end
function mariolua_menu.Keybox:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end
function mariolua_menu.Keybox:IsActive()
    return self.visible
end
function mariolua_menu.Keybox:GetPosition()
    return self.pos
end
function mariolua_menu.Keybox:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end
function mariolua_menu.Keybox:SetFunction(cN)
    self.func = cN
end
function mariolua_menu.Keybox:Update()
    self.visible = self.alpha == 0 and false or self.visible
    if self.isBinding then
        local pressed_key = ci.get_pressed_key()
        if pressed_key ~= nil then
            if pressed_key ~= globalvars.buttons.KEY_ESCAPE and self.visible then
                self.key = pressed_key
                self.key_name = base64.FirstToUpper(globalvars.button_name[pressed_key])
                self.isClicked = false
                self.isBinding = false
                mariolua.log("key bound to " .. pressed_key)
                pressed_key = nil
                mariolua_menu.SaveConfig()
            else
                pressed_key = nil
                self.isClicked = false
                self.isBinding = false
                globalvars.lock_input["move"] = false
                self.key = 0
                self.key_name = "None"
                mariolua.log("keybind abort")
                mariolua_menu.SaveConfig()
            end
        end
    end
    if self.visible then
        if ci.is_pressed(ffi_utils.inputsystem, globalvars.buttons.KEY_ESCAPE) and self.isBinding then
            self.isClicked = false
            self.isBinding = false
            globalvars.lock_input["move"] = false
            pressed_key = nil
            self.key = 0
            self.key_name = "None"
            mariolua_menu.SaveConfig()
        end
        if
            extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height) and
                (globalvars.used_Object == "" or globalvars.used_Object == self.reference) or
                self.isBinding
         then
            self.isHovered = true
            if globalvars.mouse1_key_state and self.isClicked ~= true then
                globalvars.used_Object = self.reference
                self.isClicked = true
                globalvars.lock_input["move"] = true
                self.key = 0
                self.key_name = ""
                self.isBinding = true
            elseif self.isClicked then
                globalvars.used_Object = ""
            end
        else
            self.isHovered = false
        end
    else
        if self.isBinding then
            pressed_key = nil
            self.isBinding = false
            self.isClicked = false
            globalvars.lock_input["move"] = false
            self.key = 0
            self.key_name = "None"
            mariolua_menu.SaveConfig()
        end
        self.isHovered = false
    end
    if self.key ~= 0 and self.key ~= nil and ffi_utils.hudchat2 and not ffi_utils.hudchat2.isChatOpen then
        self.isKeyDown = ci.is_pressed(ffi_utils.inputsystem, self.key)
        self.isKeyReleased = not self.isKeyDown and self.key_down_time ~= 0 and true or false
        self.key_down_time = self.isKeyDown and self.key_down_time + 1 or 0
        if self.isKeyDown and self.key_down_time == 1 then
            self.isKeyToggle = not self.isKeyToggle
        end
    elseif not self.key then
        pressed_key = 0
        self.isBinding = false
        self.isClicked = false
        globalvars.lock_input["move"] = false
        self.key = 0
        self.key_name = "None"
    end
end
function mariolua_menu.Keybox:GetValue()
    return self.key
end
function mariolua_menu.Keybox:GetKeyName()
    return self.key_name
end
function mariolua_menu.Keybox:SetValue(du)
    if du == nil then
        pressed_key = nil
        self.isBinding = false
        self.isClicked = false
        self.key = 0
        self.key_name = "None"
    elseif type(du) == "string" or type(du) == "number" then
        du = tonumber(du)
        if du <= 0 or du > 113 then
            self.key = 0
            self.key_name = "None"
        elseif du <= 113 then
            self.key = du
            self.key_name = base64.FirstToUpper(globalvars.button_name[du])
        end
        mariolua.log(self.label .. " key set to " .. base64.FirstToUpper(globalvars.button_name[du]) .. " " .. du)
    end
end
function mariolua_menu.Keybox:IsKeyReleased()
    return self.isKeyReleased
end
function mariolua_menu.Keybox:IsKeyDown()
    return self.isKeyDown
end
function mariolua_menu.Keybox:IsKeyToggle()
    return self.isKeyToggle
end
function mariolua_menu.Keybox:Render()
    if self.visible then
        if cfg.settings_guistyle:GetItemName() == "Aimware" then
            if self.isHovered then
                if self.isClicked then
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["keybox"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        self.alpha
                    )
                else
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["keybox"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        255,
                        255,
                        self.alpha
                    )
                end
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["keybox"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            end
            local cQ, cR = renderer.measure_text("c", self.label)
            renderer.text(
                self.pos.x + cQ / 2,
                self.pos.y - cR,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
            renderer.text(
                self.pos.x + self.width / 2,
                self.pos.y + self.height / 2,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.key_name
            )
        elseif cfg.settings_guistyle:GetItemName() == "UwU" then
            if self.isHovered then
                if self.isClicked then
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["keybox"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        60,
                        180,
                        self.alpha
                    )
                else
                    extended_renderer.draw_svg(
                        extended_renderer.gpx.svg["keybox"],
                        self.pos.x,
                        self.pos.y,
                        self.width,
                        self.height,
                        255,
                        60,
                        180,
                        self.alpha
                    )
                end
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["keybox"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    60,
                    180,
                    self.alpha
                )
            end
            local cQ, cR = renderer.measure_text("c", self.label)
            renderer.text(
                self.pos.x + cQ / 2,
                self.pos.y - cR,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
            renderer.text(
                self.pos.x + self.width / 2,
                self.pos.y + self.height / 2,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.key_name
            )
        elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
            extended_renderer.rectangle_outline(self.pos.x, self.pos.y, self.width, self.height, 12, 12, 12, self.alpha)
            extended_renderer.rectangle_outline(
                self.pos.x + 1,
                self.pos.y + 1,
                self.width - 2,
                self.height - 2,
                50,
                50,
                50,
                self.alpha
            )
            if self.isHovered then
                if self.isClicked then
                    renderer.gradient(
                        self.pos.x + 2,
                        self.pos.y + 2,
                        self.width - 4,
                        self.height - 4,
                        22,
                        22,
                        22,
                        extended_math.clamp(self.alpha - 35, 0, 255),
                        30,
                        30,
                        30,
                        extended_math.clamp(self.alpha - 35, 0, 255),
                        true
                    )
                else
                    renderer.gradient(
                        self.pos.x + 2,
                        self.pos.y + 2,
                        self.width - 4,
                        self.height - 4,
                        40,
                        40,
                        40,
                        extended_math.clamp(self.alpha - 35, 0, 255),
                        30,
                        30,
                        30,
                        extended_math.clamp(self.alpha - 35, 0, 255),
                        true
                    )
                end
            else
                renderer.gradient(
                    self.pos.x + 2,
                    self.pos.y + 2,
                    self.width - 4,
                    self.height - 4,
                    25,
                    25,
                    25,
                    extended_math.clamp(self.alpha - 35, 0, 255),
                    20,
                    20,
                    20,
                    extended_math.clamp(self.alpha - 75, 0, 255),
                    true
                )
            end
            local cQ, cR = renderer.measure_text("c", self.label)
            renderer.text(
                self.pos.x + cQ / 2,
                self.pos.y - cR,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.label
            )
            renderer.text(
                self.pos.x + self.width / 2,
                self.pos.y + self.height / 2,
                240,
                240,
                240,
                extended_math.clamp(self.alpha - 15, 0, 255),
                "c",
                0,
                self.key_name
            )
        end
    end
end
function mariolua_menu.Tab:new(cF, cG, cH, cI, cJ, aw, dS, callback, dT, dU, dV)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")
        return
    end
    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "tab",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        alpha = 255,
        reference = cF,
        ref_group = dS,
        label = cG,
        isHovered = false,
        isClicked = false,
        visible = true,
        force_invisible = false,
        is_curtab = false,
        icon = dT,
        tabs = dU or {},
        is_main = dV or false,
        exec_func = false,
        func = callback
    }
    self.__index = self
    return setmetatable(globalvars.reference[cK], self)
end
function mariolua_menu.Tab:SetActive(cL)
    self.visible = cL
end
function mariolua_menu.Tab:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end
function mariolua_menu.Tab:IsActive()
    return self.visible
end
function mariolua_menu.Tab:GetPosition()
    return self.pos
end
function mariolua_menu.Tab:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end
function mariolua_menu.Tab:SetCurrentTab()
    globalvars.active_tabs[self.ref_group] = self.label
end
function mariolua_menu.Tab:SetIcon(a_)
    self.icon = a_
end
function mariolua_menu.Tab:SetFunction(cN)
    self.func = cN
end
function mariolua_menu.Tab:Update()
    self.visible = self.alpha == 0 and false or self.visible
    if self.visible and self.isClicked and self.exec_func == false then
        self.exec_func = true
        if self.func ~= nil then
            self.func(self)
        end
    end
    if
        self.visible and
            extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height)
     then
        self.isHovered = true
        globalvars.used_Object =
            globalvars.mouse1_key_state and (globalvars.used_Object == "" or globalvars.used_Object == self.reference) and
            self.reference or
            globalvars.used_Object
        globalvars.current_wep_tab = globalvars.mouse1_key_state and self.label or globalvars.current_wep_tab
        globalvars.active_tabs[self.ref_group] =
            globalvars.mouse1_key_state and self.label or globalvars.active_tabs[self.ref_group]
        self.isClicked = globalvars.mouse1_key_state
        self.exec_func = false
    else
        globalvars.used_Object = globalvars.used_Object == self.reference and "" or globalvars.used_Object
        self.isClicked = false
        self.isHovered = false
        self.exec_func = false
    end
    globalvars.active_tabs[self.ref_group] = globalvars.active_tabs[self.ref_group] or self.label
    self.is_curtab = globalvars.active_tabs[self.ref_group] == self.label
end
function mariolua_menu.Tab:Render()
    if not self.visible then
        globalvars.used_Object = globalvars.used_Object == self.reference and "" or globalvars.used_Object
        self.isClicked = false
        self.isHovered = false
        return
    end
    if cfg.settings_guistyle:GetItemName() == "Aimware" then
        if self.is_curtab then
            globalvars.icon_alpha = extended_math.clamp(self.alpha - 20, 0, 255)
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["button_pressed"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
        elseif self.isHovered then
            if self.isClicked then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_pressed"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_mouse_over"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            end
        else
            globalvars.icon_alpha = extended_math.clamp(self.alpha - 135, 0, 255)
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["button_normal"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
        end
    elseif cfg.settings_guistyle:GetItemName() == "UwU" then
        if self.is_curtab then
            globalvars.icon_alpha = extended_math.clamp(self.alpha - 20, 0, 255)
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["button_pressed"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                255,
                60,
                180,
                self.alpha
            )
        elseif self.isHovered then
            if self.isClicked then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_pressed"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    60,
                    180,
                    self.alpha
                )
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_mouse_over"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    60,
                    180,
                    self.alpha
                )
            end
        else
            globalvars.icon_alpha = extended_math.clamp(self.alpha - 135, 0, 255)
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["button_normal"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                255,
                60,
                180,
                self.alpha
            )
        end
    elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
        extended_renderer.rectangle_outline(self.pos.x, self.pos.y, self.width, self.height, 12, 12, 12, self.alpha)
        extended_renderer.rectangle_outline(
            self.pos.x + 1,
            self.pos.y + 1,
            self.width - 2,
            self.height - 2,
            50,
            50,
            50,
            self.alpha
        )
        if self.is_curtab then
            globalvars.icon_alpha = extended_math.clamp(self.alpha - 20, 0, 255)
            renderer.gradient(
                self.pos.x + 2,
                self.pos.y + 2,
                self.width - 4,
                self.height - 4,
                22,
                22,
                22,
                self.alpha,
                30,
                30,
                30,
                self.alpha,
                true
            )
        elseif self.isHovered then
            if self.isClicked then
                renderer.gradient(
                    self.pos.x + 2,
                    self.pos.y + 2,
                    self.width - 4,
                    self.height - 4,
                    22,
                    22,
                    22,
                    self.alpha,
                    30,
                    30,
                    30,
                    self.alpha,
                    true
                )
            else
                renderer.gradient(
                    self.pos.x + 2,
                    self.pos.y + 2,
                    self.width - 4,
                    self.height - 4,
                    40,
                    40,
                    40,
                    self.alpha,
                    35,
                    35,
                    35,
                    self.alpha,
                    true
                )
            end
        else
            globalvars.icon_alpha = extended_math.clamp(self.alpha - 135, 0, 255)
            renderer.gradient(
                self.pos.x + 2,
                self.pos.y + 2,
                self.width - 4,
                self.height - 4,
                35,
                35,
                35,
                self.alpha,
                30,
                30,
                30,
                self.alpha,
                true
            )
        end
    end
    if self.icon == nil then
        renderer.text(
            self.pos.x + self.width / 2,
            self.pos.y + self.height / 2,
            240,
            240,
            240,
            extended_math.clamp(self.alpha - 15, 0, 255),
            "c",
            0,
            self.label
        )
    else
        extended_renderer.draw_svg(
            extended_renderer.gpx.svg[self.icon],
            self.pos.x + self.width / 100 * 30 / 2,
            self.pos.y + self.height / 100 * 30 / 2,
            self.width - self.width / 100 * 30,
            self.height - self.height / 100 * 30,
            255,
            255,
            255,
            globalvars.icon_alpha
        )
    end
end
function mariolua_menu.Tab:GetValue()
    return false
end
function mariolua_menu.Tab:SetValue(dW)
end
function mariolua_menu.Button:new(cF, cG, cH, cI, cJ, aw, callback)
    if type(cF) ~= "string" then
        mariolua.log(cG .. ": " .. tostring(cF) .. " is not a valid reference!")
        return
    end
    table.insert(globalvars.reference, cF)
    local cK = globalvars.reference[#globalvars.reference]
    globalvars.reference[cK] = {
        type = "button",
        pos = {x = cH or 0, y = cI or 0},
        width = cJ or 0,
        height = aw or 0,
        alpha = 255,
        reference = cF,
        label = cG,
        isHovered = false,
        isClicked = false,
        visible = true,
        force_invisible = false,
        exec_func = false,
        func = callback
    }
    self.__index = self
    return setmetatable(globalvars.reference[cK], self)
end
function mariolua_menu.Button:SetActive(cL)
    self.visible = cL
end
function mariolua_menu.Button:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end
function mariolua_menu.Button:IsActive()
    return self.visible
end
function mariolua_menu.Button:GetPosition()
    return self.pos
end
function mariolua_menu.Button:SetPosition(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end
function mariolua_menu.Button:SetFunction(cN)
    self.func = cN
end
function mariolua_menu.Button:Update()
    self.visible = self.alpha == 0 and false or self.visible
    if self.visible and self.isClicked and self.exec_func == false then
        self.exec_func = true
        if self.func() ~= nil then
            self.func()
        end
    end
    if
        self.visible and
            extended_math.inBounds(self.pos.x, self.pos.y, self.pos.x + self.width, self.pos.y + self.height) and
            (globalvars.used_Object == "" or globalvars.used_Object == self.reference)
     then
        self.isHovered = true
        if globalvars.mouse1_key_state then
            globalvars.used_Object =
                (globalvars.used_Object == "" or globalvars.used_Object == self.reference) and self.reference or
                globalvars.used_Object
            self.isClicked = globalvars.used_Object == self.reference and true or false
        else
            globalvars.used_Object = globalvars.used_Object == self.reference and "" or globalvars.used_Object
            self.isClicked = false
            self.exec_func = false
        end
    else
        globalvars.used_Object = globalvars.used_Object == self.reference and "" or globalvars.used_Object
        self.isClicked = false
        self.exec_func = false
        self.isHovered = false
    end
end
function mariolua_menu.Button:Render()
    if not self.visible then
        return
    end
    if cfg.settings_guistyle:GetItemName() == "Aimware" then
        if self.isHovered then
            if self.isClicked then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_pressed"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_mouse_over"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            end
        else
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["button_normal"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
        end
        renderer.text(
            self.pos.x + self.width / 2,
            self.pos.y + self.height / 2,
            240,
            240,
            240,
            extended_math.clamp(self.alpha - 15, 0, 255),
            "c",
            0,
            self.label
        )
    elseif cfg.settings_guistyle:GetItemName() == "UwU" then
        if self.isHovered then
            if self.isClicked then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_pressed"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    60,
                    180,
                    self.alpha
                )
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_mouse_over"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    60,
                    180,
                    self.alpha
                )
            end
        else
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["button_normal"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                255,
                60,
                180,
                self.alpha
            )
        end
        renderer.text(
            self.pos.x + self.width / 2,
            self.pos.y + self.height / 2,
            240,
            240,
            240,
            extended_math.clamp(self.alpha - 15, 0, 255),
            "c",
            0,
            self.label
        )
    elseif cfg.settings_guistyle:GetItemName() == "Gamesense" then
        extended_renderer.rectangle_outline(self.pos.x, self.pos.y, self.width, self.height, 12, 12, 12, self.alpha)
        extended_renderer.rectangle_outline(
            self.pos.x + 1,
            self.pos.y + 1,
            self.width - 2,
            self.height - 2,
            50,
            50,
            50,
            self.alpha
        )
        if self.isHovered then
            if self.isClicked then
                renderer.gradient(
                    self.pos.x + 2,
                    self.pos.y + 2,
                    self.width - 4,
                    self.height - 4,
                    22,
                    22,
                    22,
                    self.alpha,
                    30,
                    30,
                    30,
                    self.alpha,
                    true
                )
            else
                renderer.gradient(
                    self.pos.x + 2,
                    self.pos.y + 2,
                    self.width - 4,
                    self.height - 4,
                    40,
                    40,
                    40,
                    self.alpha,
                    35,
                    35,
                    35,
                    self.alpha,
                    true
                )
            end
        else
            renderer.gradient(
                self.pos.x + 2,
                self.pos.y + 2,
                self.width - 4,
                self.height - 4,
                35,
                35,
                35,
                self.alpha,
                30,
                30,
                30,
                self.alpha,
                true
            )
        end
        renderer.text(
            self.pos.x + self.width / 2,
            self.pos.y + self.height / 2,
            240,
            240,
            240,
            extended_math.clamp(self.alpha - 15, 0, 255),
            "c",
            0,
            self.label
        )
    else
        if self.isHovered then
            if self.isClicked then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_pressed"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            else
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["button_mouse_over"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    self.alpha
                )
            end
        else
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["button_normal"],
                self.pos.x,
                self.pos.y,
                self.width,
                self.height,
                255,
                255,
                255,
                self.alpha
            )
        end
        renderer.text(
            self.pos.x + self.width / 2,
            self.pos.y + self.height / 2,
            240,
            240,
            240,
            extended_math.clamp(self.alpha - 15, 0, 255),
            "c",
            0,
            self.label
        )
    end
end
function mariolua_menu.Button:GetValue()
    return false
end
function mariolua_menu.Button:SetValue(dW)
end
function mariolua_menu.Window:new(dX, cH, cI, dY, dZ, d_, e0)
    table.insert(globalvars.windows, dX)
    local cK = globalvars.windows[#globalvars.windows]
    globalvars.windows[cK] = {
        varname = dX,
        idx = cK,
        pos = {x = cH or 0, y = cI or 0, drag_x = 0, drag_y = 0, ty = 0, drag = false, resize = false},
        scrll = {y = (cI or 0) - 12, drag = false, fix = 0},
        mouse = {},
        width = dY or 800,
        height = dZ or 400,
        alpha = 255,
        px_up = 0,
        px_down = 0,
        visible = false,
        moveable = true,
        is_moving = false,
        onResize = false,
        can_scroll = false,
        overlap = 0,
        overlap_object = 0,
        overlaped_obj = 0,
        isOverlap = false,
        isOverlap2 = false,
        prev_tab = "",
        cur_tab = "",
        tab = {},
        count = {done = false, keybox = 0, button = 0, checkbox = 0},
        guiObjects = {},
        currentHoveredGUIObject = 1,
        on_render = d_ or nil,
        on_update = e0 or nil
    }
    self.__index = self
    return setmetatable(globalvars.windows[cK], self)
end
function mariolua_menu.Window:SetMoveable(e1)
    self.moveable = e1
end
function mariolua_menu.Window:GetValue()
    return self.varname
end
function mariolua_menu.Window:SetValue()
end
function mariolua_menu.Window:IsMoveable()
    return self.moveable
end
function mariolua_menu.Window:SetActive(cL)
    self.visible = cL
end
function mariolua_menu.Window:SetInvisible(cM)
    self.force_invisible = cM
    self.visible = not cM
end
function mariolua_menu.Window:IsActive()
    return self.visible
end
function mariolua_menu.Window:SetWindowPos(cH, cI)
    self.pos.x = cH
    self.pos.y = cI
end
function mariolua_menu.Window:SetWindowSize(dY, dZ)
    self.width = dY
    self.height = dZ
    if globalvars.mouse1_key_state then
        self.onResize = true
    end
end
function mariolua_menu.Window:GetWindowSize()
    return self.width, self.height
end
function mariolua_menu.Window:GetWindowPos()
    return self.pos
end
function util_functions.get_bounds_alpha(a9, aa, cJ, aw, b8, b9)
    if extended_math.isInsideRect(a9, aa, a9 + cJ, aa + aw, b8, b9) then
        local e2 = extended_math.clamp((aa - b9) * -1, 0, aw)
        local e3 = extended_math.clamp(aa + aw - b9, 0, aw)
        local e4 = extended_math.round(extended_math.percentof(e2, aw))
        local e5 = extended_math.round(extended_math.percentof(e3, aw))
        if e4 < 1 then
            return extended_math.percent(extended_math.percentof(e4, 1), 255)
        elseif e5 < 10 then
            return extended_math.percent(extended_math.percentof(e5, 10), 255)
        else
            return 255
        end
    else
        return 0
    end
end
function util_functions.should_update(dU)
    if dU[3] and dU[1] then
        return dU[1] == dU[2] and dU[3] == dU[4]
    elseif dU[1] then
        return dU[1] == dU[2]
    end
    return false
end
function mariolua_menu.Window:UpdateTab(al, e6, e7, a9, aa, e8, dU)
    dU = dU or {}
    local e9 = util_functions.should_update(dU)
    local ea, eb, ec = 0, 0, 0
    for I = 1, #e6 do
        local ed = e6[I]
        if not self.visible and not ed.type == "keybox" then
            return
        end
        local db = util_functions.get_bounds_alpha(self.pos.x, self.pos.y, self.width, self.height, ed.pos.x, ed.pos.y)
        ed.alpha = db
        if ed.type == "combobox" or ed.type == "multibox" then
            if ed.maxSize > ed.height and self.overlap ~= ed.maxSize then
                self.overlap = ed.pos.y + ed.maxSize
                self.overlap_object = I
            end
        end
        local cQ, cR = renderer.measure_text("c", ed.label)
        if self.overlap_object > 0 and (self.cur_tab == e7 and e9 or self.cur_tab == e7 and not dU[1]) then
            if e6[self.overlap_object] ~= nil then
                if e6[self.overlap_object].maxSize == nil then
                    e6[self.overlap_object].maxSize = 0
                end
                if al == 1 then
                    if
                        I ~= self.overlap_object and ed.pos.y - cR <= self.overlap and
                            ed.pos.y - cR >= self.overlap - e6[self.overlap_object].maxSize
                     then
                        self.overlaped_obj = I
                        ed.visible = false
                        self.isOverlap = true
                    elseif I == self.overlaped_obj and self.isOverlap then
                        self.isOverlap = false
                    end
                else
                    if
                        I ~= self.overlap_object and ed.pos.y - cR <= self.overlap and
                            ed.pos.y - cR >= self.overlap - e6[self.overlap_object].maxSize
                     then
                        self.overlaped_obj = I
                        ed.visible = false
                        self.isOverlap2 = true
                    elseif I == self.overlaped_obj and self.isOverlap2 then
                        self.isOverlap2 = false
                    end
                end
            end
        end
        if not ed.force_invisible then
            if ed.type == "slider" then
                ed:SetPosition(a9 + 5, aa - 15 + ea * 15 + ec + I * e8 - (ea * e8 - 8))
            elseif ed.type == "groupbox" then
                ed:SetPosition(a9 - 5, aa - 15 + ea * 15 + ec + I * e8 - (ea * e8 - 8))
                ea = ea + 1
                eb = eb + ed.pos_x
                ec = ec + ed.pos_y
            else
                ed:SetPosition(a9 + 5, aa + ea * 15 + ec + I * e8 - (ea * e8 - 8))
            end
        end
        ed:Update()
    end
end
globalvars.draging_ui_obj = false
function mariolua_menu.Window:Scroll()
    if self.cur_tab ~= self.prev_tab then
        self.prev_tab = self.cur_tab
        self.mouse = {}
        self.pos.drag_x = 0
        self.pos.drag_y = 0
        self.pos.drag = false
        globalvars.draging_ui_obj = false
        self.pos.drag_dif = 0
    end
    if not self.can_scroll then
        return self.pos.x, self.pos.y
    end
    local ee = self.pos.x
    local ef = self.pos.y + self.pos.drag_dif or self.pos.y
    self.mouse.left, self.mouse.pos = client.key_state(0x01, true), {ui.mouse_position()}
    self.mouse.fixed_pos = self.mouse.fixed_pos or {self.pos.x, self.pos.y}
    if self.pos.drag and not self.mouse.left then
        self.pos.drag = false
        globalvars.draging_ui_obj = false
        globalvars.block_key_state[0x01] = false
        self.mouse.pressed = false
    end
    if self.pos.drag and self.mouse.left then
        ee = self.mouse.pos[1] - self.pos.drag_x
        ef = extended_math.clamp(self.mouse.pos[2] - self.pos.drag_y, self.pos.y, self.pos.y + 2000)
        self.mouse.fixed_pos[1] = self.mouse.pos[1] - self.pos.drag_x
        self.mouse.fixed_pos[2] =
            extended_math.clamp(self.mouse.pos[2] - self.pos.drag_y, self.pos.y, self.pos.y + 2000)
        self.pos.drag_dif = self.mouse.fixed_pos[2] - self.pos.y
    end
    if
        extended_math.intersect(self.mouse.pos[1], self.mouse.pos[2], ee, ef, self.width - 10, self.height - 10) and
            self.mouse.left
     then
        if not self.mouse.pressed then
            self.mouse.press_pos_x = self.mouse.pos[1]
            self.mouse.press_pos_y = self.mouse.pos[2]
            self.mouse.pressed = true
        end
        if not extended_math.between(self.mouse.press_pos_y, self.mouse.pos[2] - 40, self.mouse.pos[2] + 40) then
            self.pos.drag = true
            globalvars.draging_ui_obj = true
            globalvars.block_key_state[0x01] = true
            self.pos.drag_x = self.mouse.pos[1] - ee
            self.pos.drag_y = extended_math.clamp(self.mouse.pos[2] - ef, self.pos.y, self.pos.y + 2000)
        end
    end
    return ee, ef
end
function mariolua_menu.Window:Update()
    self.on_update(self)
end
function mariolua_menu.Window:RenderTab(al, e6, e7, dU)
    dU = dU or {}
    local eg = util_functions.should_update(dU)
    for I = 1, #e6 do
        local ed = e6[I]
        if self.cur_tab == e7 and eg or self.cur_tab == e7 and not dU[1] then
            if al == 1 then
                ed.visible =
                    (not self.isOverlap or not self.visible) and not ed.force_invisible and self.visible or
                    (self.force_invisible and false or ed.visible)
            else
                ed.visible =
                    (not self.isOverlap2 or not self.visible) and not ed.force_invisible and self.visible or
                    (self.force_invisible and false or ed.visible)
            end
            ed:Render()
        elseif ed.visible then
            ed.visible = false
        end
    end
end
globalvars.bg_link = ""
function mariolua_menu.Window:Render()
    if self.visible then
        if cfg.settings_guistyle:GetItemName() == "Gamesense" then
            if self.onResize or cfg.settings_custombackground:GetValue() then
                extended_renderer.gs_container(
                    self.pos.x,
                    self.pos.y - 24,
                    self.width,
                    self.height + 28,
                    255,
                    true,
                    false
                )
            else
                extended_renderer.gs_container(
                    self.pos.x,
                    self.pos.y - 24,
                    self.width,
                    self.height + 28,
                    255,
                    true,
                    true
                )
            end
        elseif cfg.settings_guistyle:GetItemName() == "Fuhrer" then
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["window_top"],
                self.pos.x,
                self.pos.y - 24,
                self.width,
                24,
                50,
                50,
                50,
                255
            )
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["window_bottom"],
                self.pos.x,
                self.pos.y + self.height,
                self.width,
                4,
                50,
                50,
                50,
                255
            )
            renderer.rectangle(self.pos.x, self.pos.y, self.width, self.height, 20, 20, 20, 150)
            if not cfg.settings_custombackground:GetValue() then
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["hitler"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    250,
                    "r"
                )
            end
        elseif cfg.settings_guistyle:GetItemName() == "UwU" then
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["window_top"],
                self.pos.x,
                self.pos.y - 24,
                self.width,
                24,
                80,
                20,
                250,
                255
            )
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["window_bottom"],
                self.pos.x,
                self.pos.y + self.height,
                self.width,
                4,
                80,
                20,
                250,
                255
            )
            if not cfg.settings_custombackground:GetValue() then
                renderer.rectangle(self.pos.x, self.pos.y, self.width, self.height, 20, 20, 20, 150)
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["uwu"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    255,
                    "r"
                )
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["uwu_avatar"],
                    self.pos.x - 200,
                    self.pos.y - 120,
                    300,
                    500,
                    255,
                    255,
                    255,
                    250,
                    "r"
                )
            end
        elseif cfg.settings_guistyle:GetItemName() == "Minimal" then
            extended_renderer.watermark_container(self.pos.x, self.pos.y - 24, self.width, self.height + 28, true)
        else
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["window_top"],
                self.pos.x,
                self.pos.y - 24,
                self.width,
                24,
                255,
                255,
                255,
                255
            )
            extended_renderer.draw_svg(
                extended_renderer.gpx.svg["window_bottom"],
                self.pos.x,
                self.pos.y + self.height,
                self.width,
                4,
                255,
                255,
                255,
                255
            )
            if not cfg.settings_custombackground:GetValue() then
                renderer.rectangle(self.pos.x, self.pos.y, self.width, self.height, 0, 0, 0, 190)
                extended_renderer.draw_svg(
                    extended_renderer.gpx.svg["window_bg"],
                    self.pos.x,
                    self.pos.y,
                    self.width,
                    self.height,
                    255,
                    255,
                    255,
                    230,
                    "r"
                )
            end
        end
        if cfg.settings_custombackground:GetValue() then
            local eh = ui.get(globalvars.config_bg_string)
            local ei = cfg.settings_guistyle:GetItemName()
            local ej = ei == "Gamesense" and self.pos.x + 6 or self.pos.x
            local ek = ei == "Gamesense" and self.pos.y - 14 or self.pos.y
            local el = ei == "Gamesense" and self.width - 12 or self.width
            local em = ei == "Gamesense" and self.height + 12 or self.height
            if
                extended_renderer.gpx.jpg.custom_bg ~= nil and extended_renderer.gpx.jpg.custom_bg ~= "" and
                    globalvars.bg_link == eh
             then
                renderer.rectangle(ej, ek, el, self.height, 20, 20, 20, 150)
                if globalvars.img_type == "jpg" then
                    extended_renderer.draw_jpg(extended_renderer.gpx.jpg.custom_bg, ej, ek, el, em, 255, 255, 255, 255)
                elseif globalvars.img_type == "png" then
                    extended_renderer.draw_png(extended_renderer.gpx.jpg.custom_bg, ej, ek, el, em, 255, 255, 255, 255)
                end
            else
                local en = readfile("mariolua_bg.dat")
                en = en or "return {}"
                local eo = loadstring(en)()
                if eo[eh] then
                    img_file = base64.decode(eo[eh])
                    globalvars.img_type = img_file:match("�PNG") and "png" or "jpg"
                    extended_renderer.gpx.jpg.custom_bg = img_file
                    globalvars.bg_link = eh
                end
            end
        end

        local ep = debug and "Dev" or "" or false
        renderer.text(
            self.pos.x + 25,
            self.pos.y - 17,
            255,
            255,
            255,
            200,
            nil,
            0,
            string.format("%s %s Rework by TherioJunior", mariolua.name, mariolua.version)
        )
    end

    if self.on_render then
        self.on_render(self)
    end
end

function util_functions.on_update_ui(self)
    local ee, ef = self:Scroll()
    ee = self.pos.x
    if self.onResize and globalvars.mouse1_key_state ~= true then
        self.onResize = false
    end
    for I = 1, #self.guiObjects.tab do
        local ed = self.guiObjects.tab[I]
        if I == 1 then
            if globalvars.is_window_hidden and ui.is_menu_open() then
                if globalvars.moving_window then
                    ed.pos.x = globalvars.temp_menu_pos_x
                else
                    ed.pos.x = globalvars.gs_menu_x - 18
                end
                ed.visible = true
            else
                ed.visible = self.visible
            end
            if self.visible then
                ed:SetPosition(self.pos.x - ed.width + 18, self.pos.y - 24 - ed.height + ed.height)
            else
                ed:SetPosition(globalvars.gs_menu_x - ed.width, globalvars.gs_menu_y - ed.height + ed.height)
            end
        else
            ed:SetPosition(self.pos.x - ed.width, self.pos.y - 32 - ed.height + ed.height * I - 1)
        end
        ed:Update()
    end
    for I = 1, #self.guiObjects.aimbot.tabs do
        local ed = self.guiObjects.aimbot.tabs[I]
        ed:SetPosition(self.pos.x - 60 + I * 80, self.pos.y + 20)
        ed:Update()
    end
    for I = 1, #self.guiObjects.antiaim.tabs do
        local ed = self.guiObjects.antiaim.tabs[I]
        ed:SetPosition(self.pos.x - 60 + I * 80, self.pos.y + 20)
        ed:Update()
    end
    for I = 1, #self.guiObjects.antiaim["conditions"] do
        local ed = self.guiObjects.antiaim["conditions"][I]
        ed:SetPosition(self.pos.x - 40 + I * 65, self.pos.y + 80)
        ed:Update()
    end
    for I = 1, #self.guiObjects.weapons do
        local ed = self.guiObjects.weapons[I]
        ed:SetPosition(self.pos.x - 43 + I * 54, self.pos.y + 30)
        ed:Update()
    end
    for I = 1, #self.guiObjects.wepcfg_top do
        local ed = self.guiObjects.wepcfg_top[I]
        ed:SetPosition(self.pos.x - 50 + I * 60, self.pos.y + 5)
        ed:Update()
    end
    self:UpdateTab(
        1,
        self.guiObjects.aimbot.keybinds,
        "Aimbot",
        ee + 20,
        ef + 30,
        45,
        {globalvars.aimbot_tab, "Keybinds"}
    )
    self:UpdateTab(
        2,
        self.guiObjects.aimbot.keybinds2,
        "Aimbot",
        ee + 250,
        ef + 30,
        45,
        {globalvars.aimbot_tab, "Keybinds"}
    )
    self:UpdateTab(1, self.guiObjects.aimbot.other, "Aimbot", ee + 20, ef + 30, 45, {globalvars.aimbot_tab, "Other"})
    self:UpdateTab(2, self.guiObjects.aimbot.other2, "Aimbot", ee + 250, ef + 30, 45, {globalvars.aimbot_tab, "Other"})
    self:UpdateTab(
        1,
        self.guiObjects.antiaim.tab_1["standing"][1],
        "Anti-Aim",
        ee + 20,
        ef + 90,
        45,
        {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Standing"}
    )
    self:UpdateTab(
        2,
        self.guiObjects.antiaim.tab_1["standing"][2],
        "Anti-Aim",
        ee + 250,
        ef + 90,
        45,
        {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Standing"}
    )
    self:UpdateTab(
        1,
        self.guiObjects.antiaim.tab_1["moving"][1],
        "Anti-Aim",
        ee + 20,
        ef + 90,
        45,
        {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Moving"}
    )
    self:UpdateTab(
        2,
        self.guiObjects.antiaim.tab_1["moving"][2],
        "Anti-Aim",
        ee + 250,
        ef + 90,
        45,
        {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Moving"}
    )
    self:UpdateTab(
        1,
        self.guiObjects.antiaim.tab_1["slowmotion"][1],
        "Anti-Aim",
        ee + 20,
        ef + 90,
        45,
        {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Slow Motion"}
    )
    self:UpdateTab(
        2,
        self.guiObjects.antiaim.tab_1["slowmotion"][2],
        "Anti-Aim",
        ee + 250,
        ef + 90,
        45,
        {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Slow Motion"}
    )
    self:UpdateTab(1, self.guiObjects.antiaim.tab_2[1], "Anti-Aim", ee + 20, ef + 30, 45, {globalvars.aa_tab, "AA 2"})
    self:UpdateTab(2, self.guiObjects.antiaim.tab_2[2], "Anti-Aim", ee + 270, ef + 30, 45, {globalvars.aa_tab, "AA 2"})
    self:UpdateTab(1, self.guiObjects.resolver, "Resolver", ee + 20, ef - 30, 45)
    self:UpdateTab(2, self.guiObjects.resolver2, "Resolver", ee + 260, ef - 30, 45)
    self:UpdateTab(1, self.guiObjects.misc, "Misc", ee + 20, ef - 30, 45)
    self:UpdateTab(2, self.guiObjects.misc2, "Misc", ee + 250, ef - 30, 45)
    self:UpdateTab(1, self.guiObjects.indicators, "Indicators", ee + 20, ef - 30, 45)
    self:UpdateTab(2, self.guiObjects.indicators2, "Indicators", ee + 210, ef - 30, 45)
    self:UpdateTab(1, self.guiObjects.visuals, "Visuals", ee + 20, ef - 30, 45)
    self:UpdateTab(1, self.guiObjects.settings, "Settings", ee + 20, ef - 30, 45)
    self:UpdateTab(2, self.guiObjects.settings2, "Settings", ee + 250, ef - 30, 45)
    for I = 1, #self.guiObjects.wepcfg do
        local eq = self.guiObjects.wepcfg[I]
        for cl = 1, #eq do
            local ed = eq[cl]
            if self.visible or ed.type == "keybox" then
                local db =
                    util_functions.get_bounds_alpha(self.pos.x, self.pos.y, self.width, self.height, ed.pos.x, ed.pos.y)
                ed.alpha = db
                if ed ~= nil then
                    if ed.type == "combobox" or ed.type == "multibox" then
                        if ed.maxSize > ed.height and self.overlap ~= ed.maxSize then
                            self.overlap = ed.pos.y + ed.maxSize
                            self.overlap_object = cl
                            mariolua.log("Overlap pos: " .. self.overlap)
                        elseif self.type == "multibox" and self.overlap > 0 and ed.type ~= "combobox" then
                            self.overlap = 0
                            self.overlap_object = 0
                        end
                    end
                    local cQ, cR = renderer.measure_text("c", ed.label)
                    if self.overlap_object > 0 and self.cur_tab == "Weapons" and globalvars.wep_tab == I then
                        if eq[self.overlap_object] then
                            if eq[self.overlap_object].maxSize == nil then
                                eq[self.overlap_object].maxSize = 0
                            end
                            if
                                cl ~= self.overlap_object and ed.pos.y - cR <= self.overlap and
                                    ed.pos.y - cR >= self.overlap - eq[self.overlap_object].maxSize
                             then
                                mariolua.log("Overlaped Object: " .. ed.label)
                                self.overlaped_obj = cl
                                ed.visible = false
                                self.isOverlap = true
                            elseif cl == self.overlaped_obj and self.isOverlap then
                                self.isOverlap = false
                            end
                        end
                    end
                    if not ed.force_invisible then
                        if ed.type == "slider" then
                            ed:SetPosition(ee + 25, ef + cl * 45)
                        elseif ed.type == "groupbox" then
                            ed:SetPosition(ee + 20, ef + 20 + cl * 45)
                        elseif ed.type == "checkbox" then
                            ed:SetPosition(ee + 25, ef + 10 + cl * 45)
                        else
                            ed:SetPosition(ee + 25, ef + 15 + cl * 45)
                        end
                    end
                    ed:Update()
                end
            end
        end
    end
    for I = 1, #self.guiObjects.wepcfg2 do
        local eq = self.guiObjects.wepcfg2[I]
        for cl = 1, #eq do
            local ed = eq[cl]
            if self.visible or ed.type == "keybox" then
                local db =
                    util_functions.get_bounds_alpha(self.pos.x, self.pos.y, self.width, self.height, ed.pos.x, ed.pos.y)
                ed.alpha = db
                if ed ~= nil then
                    if ed.type == "combobox" or ed.type == "multibox" then
                        if ed.maxSize > ed.height and self.overlap ~= ed.maxSize then
                            self.overlap = ed.pos.y + ed.maxSize
                            self.overlap_object = cl
                            mariolua.log("Overlap pos: " .. self.overlap)
                        elseif self.type == "multibox" and self.overlap > 0 and ed.type ~= "combobox" then
                            self.overlap = 0
                            self.overlap_object = 0
                        end
                    end
                    local cQ, cR = renderer.measure_text("c", ed.label)
                    if self.overlap_object > 0 and self.cur_tab == "Weapons" and globalvars.wep_tab == I then
                        if eq[self.overlap_object].maxSize == nil then
                            eq[self.overlap_object].maxSize = 0
                        end
                        if
                            cl ~= self.overlap_object and ed.pos.y - cR <= self.overlap and
                                ed.pos.y - cR >= self.overlap - eq[self.overlap_object].maxSize
                         then
                            mariolua.log("Overlaped Object: " .. ed.label)
                            self.overlaped_obj = cl
                            ed.visible = false
                            self.isOverlap2 = true
                        elseif cl == self.overlaped_obj and self.isOverlap2 then
                            self.isOverlap2 = false
                        end
                    end
                    if not ed.force_invisible then
                        if ed.type == "slider" then
                            ed:SetPosition(ee + 255, ef + cl * 45)
                        elseif ed.type == "groupbox" then
                            ed:SetPosition(ee + 250, ef + 20 + cl * 45)
                        elseif ed.type == "checkbox" then
                            ed:SetPosition(ee + 255, ef + 10 + cl * 45)
                        else
                            ed:SetPosition(ee + 255, ef + 15 + cl * 45)
                        end
                    end
                    ed:Update()
                end
            end
        end
    end
end
function util_functions.on_render_ui(self)
    for I = 1, #self.guiObjects.tab do
        local ed = self.guiObjects.tab[I]
        if self.visible and ed.visible or I == 1 then
            ed:Render()
        end
    end
    if self.visible then
        self:RenderTab(1, self.guiObjects.aimbot.tabs, "Aimbot")
        self:RenderTab(1, self.guiObjects.aimbot.keybinds, "Aimbot", {globalvars.aimbot_tab, "Keybinds"})
        self:RenderTab(2, self.guiObjects.aimbot.keybinds2, "Aimbot", {globalvars.aimbot_tab, "Keybinds"})
        self:RenderTab(1, self.guiObjects.aimbot.other, "Aimbot", {globalvars.aimbot_tab, "Other"})
        self:RenderTab(2, self.guiObjects.aimbot.other2, "Aimbot", {globalvars.aimbot_tab, "Other"})
        self:RenderTab(1, self.guiObjects.resolver, "Resolver")
        self:RenderTab(2, self.guiObjects.resolver2, "Resolver")
        self:RenderTab(1, self.guiObjects.antiaim.tabs, "Anti-Aim")
        self:RenderTab(1, self.guiObjects.antiaim["conditions"], "Anti-Aim", {globalvars.aa_tab, "AA"})
        self:RenderTab(
            1,
            self.guiObjects.antiaim.tab_1["standing"][1],
            "Anti-Aim",
            {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Standing"}
        )
        self:RenderTab(
            2,
            self.guiObjects.antiaim.tab_1["standing"][2],
            "Anti-Aim",
            {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Standing"}
        )
        self:RenderTab(
            1,
            self.guiObjects.antiaim.tab_1["moving"][1],
            "Anti-Aim",
            {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Moving"}
        )
        self:RenderTab(
            2,
            self.guiObjects.antiaim.tab_1["moving"][2],
            "Anti-Aim",
            {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Moving"}
        )
        self:RenderTab(
            1,
            self.guiObjects.antiaim.tab_1["slowmotion"][1],
            "Anti-Aim",
            {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Slow Motion"}
        )
        self:RenderTab(
            2,
            self.guiObjects.antiaim.tab_1["slowmotion"][2],
            "Anti-Aim",
            {globalvars.aa_tab, "AA", globalvars.aa_condition_tab, "Slow Motion"}
        )
        self:RenderTab(1, self.guiObjects.antiaim.tab_2[1], "Anti-Aim", {globalvars.aa_tab, "AA 2"})
        self:RenderTab(2, self.guiObjects.antiaim.tab_2[2], "Anti-Aim", {globalvars.aa_tab, "AA 2"})
        self:RenderTab(1, self.guiObjects.misc, "Misc")
        self:RenderTab(2, self.guiObjects.misc2, "Misc")
        self:RenderTab(1, self.guiObjects.indicators, "Indicators")
        self:RenderTab(2, self.guiObjects.indicators2, "Indicators")
        self:RenderTab(1, self.guiObjects.visuals, "Visuals")
        self:RenderTab(1, self.guiObjects.settings, "Settings")
        self:RenderTab(2, self.guiObjects.settings2, "Settings")
        for I = 1, #self.guiObjects.weapons do
            local ed = self.guiObjects.weapons[I]
            if self.cur_tab == "Weapons" then
                ed.visible = self.visible
                ed:Render()
            elseif ed.visible then
                ed.visible = false
            end
        end
        for I = 1, #self.guiObjects.wepcfg_top do
            local ed = self.guiObjects.wepcfg_top[I]
            if self.cur_tab == "Weapons" then
                ed.visible = self.visible
                ed:Render()
            elseif ed.visible then
                ed.visible = false
            end
        end
        for I = 1, #self.guiObjects.wepcfg do
            local eq = self.guiObjects.wepcfg[I]
            for cl = 1, #eq do
                local ed = eq[cl]
                if self.cur_tab == "Weapons" and globalvars.wep_tab == I then
                    if (self.isOverlap == false or self.visible == false) and ed.force_invisible == false then
                        ed.visible = self.visible
                    elseif self.force_invisible then
                        ed.visible = false
                    end
                    ed:Render()
                elseif ed.visible then
                    ed.visible = false
                end
            end
        end
        for I = 1, #self.guiObjects.wepcfg2 do
            local eq = self.guiObjects.wepcfg2[I]
            for cl = 1, #eq do
                local ed = eq[cl]
                if self.cur_tab == "Weapons" and globalvars.wep_tab == I then
                    if (self.isOverlap2 == false or self.visible == false) and ed.force_invisible == false then
                        ed.visible = self.visible
                    elseif self.force_invisible then
                        ed.visible = false
                    end
                    ed:Render()
                elseif ed.visible then
                    ed.visible = false
                end
            end
        end
    end
end
local er =
    mariolua_menu.Window:new("Main", 100, 200, 510, 585, util_functions.on_render_ui, util_functions.on_update_ui)
er.cur_tab = "Aimbot"
globalvars.aimbot_tab = "Keybinds"
globalvars.aa_tab = "AA"
globalvars.aa_condition_tab = "Standing"
globalvars.current_wep_tab = "Pistols"
globalvars.moving_window = false
globalvars.is_window_hidden = false
function util_functions.show_window()
    globalvars.temp_menu_pos_x = globalvars.menu.x
    globalvars.temp_menu_pos_y = globalvars.menu.y
    globalvars.gs_menu_x, globalvars.gs_menu_y = ui.menu_position()
    if globalvars.menu.x <= globalvars.gs_menu_x - globalvars.temp_w then
        globalvars.is_window_hidden = false
        globalvars.moving_window = false
        client.unset_event_callback("paint_ui", util_functions.show_window)
    else
        er:SetWindowPos(globalvars.menu.x - 10, globalvars.gs_menu_y + 24)
        client.set_event_callback("paint_ui", util_functions.show_window)
    end
end
function util_functions.hide_window()
    globalvars.gs_menu_x, globalvars.gs_menu_y = ui.menu_position()
    if globalvars.menu.x >= globalvars.gs_menu_x then
        globalvars.is_window_hidden = true
        globalvars.moving_window = false
        er:SetActive(false)
        client.unset_event_callback("paint_ui", util_functions.hide_window)
    else
        er:SetWindowPos(globalvars.menu.x + 10, globalvars.gs_menu_y + 24)
        client.set_event_callback("paint_ui", util_functions.hide_window)
    end
end
function util_functions.hide_mariolua_menu()
    if globalvars.moving_window then
        return
    end
    globalvars.gs_menu_x, globalvars.gs_menu_y = ui.menu_position()
    globalvars.temp_w = globalvars.menu.w
    globalvars.temp_h = globalvars.menu.h
    globalvars.moving_window = true
    if globalvars.is_window_hidden then
        er:SetWindowPos(globalvars.gs_menu_x, globalvars.gs_menu_y)
        er:SetActive(true)
        util_functions.show_window()
    else
        util_functions.hide_window()
    end
end

er.guiObjects = {
    tab = {
        mariolua_menu.Tab:new("tab.hide", "-", 0, 0, 18, 18, "hide", util_functions.hide_mariolua_menu),
        mariolua_menu.Tab:new(
            "tab.aimbot",
            "Aimbot",
            0,
            0,
            75,
            32,
            "tab",
            function(self)
                self:SetCurrentTab()
                er.cur_tab = "Aimbot"
                mariolua.log("Aimbot tab pressed")
            end,
            nil,
            {"Keybinds", "Other"},
            true
        ),
        mariolua_menu.Tab:new(
            "tab.weapons",
            "Weapons",
            0,
            0,
            75,
            32,
            "tab",
            function(self)
                self:SetCurrentTab()
                er.cur_tab = "Weapons"
                mariolua.log("Weapons tab pressed")
            end,
            nil,
            {"Pistols", "H. Pistols", "SMGs", "Rifles", "Scout", "AWP", "A. Sniper", "LMGs", "Other"},
            true
        ),
        mariolua_menu.Tab:new(
            "tab.resolver",
            "Resolver",
            0,
            0,
            75,
            32,
            "tab",
            function(self)
                self:SetCurrentTab()
                er.cur_tab = "Resolver"
                mariolua.log("Resolver tab pressed")
            end,
            nil,
            {}
        ),
        mariolua_menu.Tab:new(
            "tab.legitaa",
            "Anti-Aim",
            0,
            0,
            75,
            32,
            "tab",
            function(self)
                self:SetCurrentTab()
                er.cur_tab = "Anti-Aim"
                mariolua.log("AntiAim tab pressed")
            end,
            nil,
            {"AA", "AA 2"},
            true
        ),
        mariolua_menu.Tab:new(
            "tab.misc",
            "Misc",
            0,
            0,
            75,
            32,
            "tab",
            function(self)
                self:SetCurrentTab()
                er.cur_tab = "Misc"
                mariolua.log("Misc tab pressed")
            end,
            nil,
            {}
        ),
        mariolua_menu.Tab:new(
            "tab.indicators",
            "Indicators",
            0,
            0,
            75,
            32,
            "tab",
            function(self)
                self:SetCurrentTab()
                er.cur_tab = "Indicators"
                mariolua.log("Indicators tab pressed")
            end,
            nil,
            {},
            true
        ),
        mariolua_menu.Tab:new(
            "tab.visuals",
            "Visuals",
            0,
            0,
            75,
            32,
            "tab",
            function(self)
                self:SetCurrentTab()
                er.cur_tab = "Visuals"
                mariolua.log("Visuals tab pressed")
            end,
            nil,
            {},
            true
        ),
        mariolua_menu.Tab:new(
            "tab.settings",
            "Settings",
            0,
            0,
            75,
            32,
            "tab",
            function(self)
                self:SetCurrentTab()
                er.cur_tab = "Settings"
                mariolua.log("Settings tab pressed")
            end,
            nil,
            {},
            true
        )
    },
    aimbot = {
        tabs = {
            mariolua_menu.Tab:new(
                "aimbot.keybinds.tab",
                "Keybinds",
                0,
                0,
                80,
                35,
                "aimbot",
                function(self)
                    self:SetCurrentTab()
                    globalvars.aimbot_tab = "Keybinds"
                    mariolua.log("Aimbot Keybinds tab pressed")
                end,
                nil
            ),
            mariolua_menu.Tab:new(
                "aimbot.other.tab",
                "Other",
                0,
                0,
                80,
                35,
                "aimbot",
                function(self)
                    self:SetCurrentTab()
                    globalvars.aimbot_tab = "Other"
                    mariolua.log("Aimbot Other tab pressed")
                end,
                nil
            )
        },
        keybinds = {
            mariolua_menu.Groupbox:new("aimbot.keybinds", "Keybinds", 0, 0, 210, 420),
            mariolua_menu.Checkbox:new("trigger.ragetrigger", "Rage Trigger", 0, 0, 18, 18, nil),
            mariolua_menu.Combobox:new(
                "trigger.ragetrigger.mode",
                "Rage Trigger Mode",
                0,
                0,
                185,
                22,
                {"Toggle", "Hold", "Always"},
                nil
            ),
            mariolua_menu.Keybox:new("trigger.triggerkey", "Rage Trigger Key", 0, 0, 185, 22, nil),
            mariolua_menu.Checkbox:new("trigger.autowall", "Auto Wall", 0, 0, 18, 18, nil),
            mariolua_menu.Combobox:new(
                "trigger.autowall.mode",
                "Auto Wall Mode",
                0,
                0,
                185,
                22,
                {"Toggle", "Hold", "Always"},
                nil
            ),
            mariolua_menu.Keybox:new("trigger.autowallkey", "Auto Wall Key", 0, 0, 185, 22, nil),
            mariolua_menu.Checkbox:new("trigger.forcebaim", "Force Body Aim", 0, 0, 18, 18, nil),
            mariolua_menu.Combobox:new(
                "trigger.forcebaim.mode",
                "Force Body Aim Mode",
                0,
                0,
                185,
                22,
                {"Toggle", "Hold", "Always"},
                nil
            ),
            mariolua_menu.Keybox:new("trigger.forcebaimkey", "Force Body Aim Key", 0, 0, 185, 22, nil)
        },
        keybinds2 = {
            mariolua_menu.Groupbox:new("aimbot.keybinds2", "Keybinds", 0, 0, 210, 420),
            mariolua_menu.Checkbox:new("trigger.damage.override", "Min Damage Override", 0, 0, 18, 18, nil),
            mariolua_menu.Combobox:new(
                "trigger.damage.override.mode",
                "Min Damage Override Mode",
                0,
                0,
                185,
                22,
                {"Toggle", "Hold"},
                nil
            ),
            mariolua_menu.Keybox:new("trigger.damage.override.key", "Min Damage Override Key", 0, 0, 185, 22, nil),
            mariolua_menu.Checkbox:new("aimbot.keybinds.onshot", "Force OnShot", 0, 0, 18, 18, nil),
            mariolua_menu.Combobox:new(
                "aimbot.keybinds.onshot.mode",
                "Force OnShot Mode",
                0,
                0,
                185,
                22,
                {"Toggle", "Hold", "Always"},
                nil
            ),
            mariolua_menu.Keybox:new("aimbot.keybinds.onshot.key", "Force OnShot Key", 0, 0, 185, 22, nil),
            mariolua_menu.Checkbox:new("aimbot.dormant", "Dormant Aimbot", 0, 0, 18, 18, nil),
            mariolua_menu.Combobox:new(
                "aimbot.dormant.mode",
                "Dormant Aimbot Mode",
                0,
                0,
                185,
                22,
                {"Toggle", "Hold", "Always"},
                nil
            ),
            mariolua_menu.Keybox:new("aimbot.dormant.key", "Dormant Aimbot Key", 0, 0, 185, 22, nil)
        },
        other = {
            mariolua_menu.Groupbox:new("aimbot.legitautowall", "Legit Auto Wall", 0, 0, 210, 160),
            mariolua_menu.Checkbox:new("trigger.legitautowall.enabled", "Enable", 0, 0, 18, 18, nil),
            mariolua_menu.Multibox:new(
                "trigger.legitautowall.hitbox",
                "Trace Hitbox",
                0,
                0,
                185,
                22,
                {
                    mariolua_menu.MultiboxItem:new("trigger", "Head"),
                    mariolua_menu.MultiboxItem:new("trigger", "Chest"),
                    mariolua_menu.MultiboxItem:new("trigger", "Stomach"),
                    mariolua_menu.MultiboxItem:new("trigger", "Arms"),
                    mariolua_menu.MultiboxItem:new("trigger", "Hands"),
                    mariolua_menu.MultiboxItem:new("trigger", "Legs"),
                    mariolua_menu.MultiboxItem:new("trigger", "Feet")
                },
                nil
            ),
            mariolua_menu.Slider:new(
                "trigger.legitautowall.extrapolated",
                "Extrapolate Position",
                0,
                0,
                180,
                12,
                0,
                0,
                32
            ),
            mariolua_menu.Text:new("trigger.dynamicfov.space", "", 0, 0, 0, 0, "b", nil),
            mariolua_menu.Groupbox:new("aimbot.dynamicfov", "Dynamic Fov", 0, 0, 210, 250),
            mariolua_menu.Combobox:new(
                "trigger.dynamicfov.mode",
                "Dynamic Fov Mode",
                0,
                0,
                180,
                22,
                {"Off", "Static", "Automatic"},
                nil
            ),
            mariolua_menu.Slider:new("trigger.dynamicfov.min", "Minimum Fov", 0, 0, 180, 12, 1, 1, 180),
            mariolua_menu.Slider:new("trigger.dynamicfov.max", "Maximum Fov", 0, 0, 180, 12, 1, 1, 180),
            mariolua_menu.Slider:new("trigger.dynamicfov.dynamicfactor", "Automatic Factor", 0, 0, 180, 12, 0, 0, 250),
            mariolua_menu.Checkbox:new("trigger.dynamicfov.draw", "Draw Fov", 0, 0, 18, 18, nil)
        },
        other2 = {
            mariolua_menu.Groupbox:new("aimbot.dormant", "Dormant Aimbot", 0, 0, 210, 160),
            mariolua_menu.Multibox:new(
                "aimbot.dormant.extra",
                "Options",
                0,
                0,
                180,
                22,
                {
                    mariolua_menu.MultiboxItem:new("multibox_item", "Force Baim"),
                    mariolua_menu.MultiboxItem:new("multibox_item", "Quick Stop"),
                    mariolua_menu.MultiboxItem:new("multibox_item", "Automatic Scope")
                },
                nil
            ),
            mariolua_menu.Slider:new("aimbot.dormant.hitchance", "Minimum Hit Chance", 0, 0, 180, 12, 1, 1, 100),
            mariolua_menu.Slider:new("aimbot.dormant.mindmg", "Minimum Damage", 0, 0, 180, 12, 1, 1, 100),
            mariolua_menu.Text:new("aimbot.blank.headline", "", 0, 0, 0, 0, "b", nil),
            mariolua_menu.Groupbox:new("aimbot.dynamicfov", "Double Tap", 0, 0, 210, 60),
            mariolua_menu.Combobox:new(
                "aimbot.doubletap.speed",
                "Double Tap Speed",
                0,
                0,
                180,
                22,
                {"Off", "Fast", "+Fast", "Max"},
                nil
            )
        }
    },
    weapons = {
        mariolua_menu.Tab:new(
            "weapons.pistols",
            "Pistols",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 1
                mariolua.log("Pistols tab pressed")
            end,
            "usp_silencer"
        ),
        mariolua_menu.Tab:new(
            "weapons.hpistols",
            "H. Pistols",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 2
                mariolua.log("Heavy Pistols tab pressed")
            end,
            "deagle"
        ),
        mariolua_menu.Tab:new(
            "weapons.smgs",
            "SMGs",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 3
                mariolua.log("SMGs tab pressed")
            end,
            "mp7"
        ),
        mariolua_menu.Tab:new(
            "weapons.rifles",
            "Rifles",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 4
                mariolua.log("Rifles tab pressed")
            end,
            "ak47"
        ),
        mariolua_menu.Tab:new(
            "weapons.scout",
            "Scout",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 5
                mariolua.log("Scout tab pressed")
            end,
            "ssg08"
        ),
        mariolua_menu.Tab:new(
            "weapons.awp",
            "AWP",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 6
                mariolua.log("AWP tab pressed")
            end,
            "awp"
        ),
        mariolua_menu.Tab:new(
            "weapons.auto",
            "A. Sniper",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 7
                mariolua.log("Auto Sniper tab pressed")
            end,
            "scar20"
        ),
        mariolua_menu.Tab:new(
            "weapons.lmg",
            "LMGs",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 8
                mariolua.log("LMGs tab pressed")
            end,
            "negev"
        ),
        mariolua_menu.Tab:new(
            "weapons.other",
            "Other",
            0,
            0,
            54,
            28,
            "weapons",
            function(self)
                self:SetCurrentTab()
                globalvars.wep_tab = 9
                mariolua.log("Others tab pressed")
            end,
            "taser"
        )
    },
    wepcfg_top = {
        mariolua_menu.Checkbox:new("settings.adaptivewep", "Enable Adaptive Weapon Config", 0, 0, 18, 18, nil)
    },
    wepcfg = util_functions.temp_wepcfg(),
    wepcfg2 = util_functions.temp_wepcfg2(),
    resolver = {
        mariolua_menu.Groupbox:new("resolver.gb", "MarioLua Resolver", 0, 0, 220, 480),
        mariolua_menu.Checkbox:new("resolver.enable", "Enable", 0, 0, 18, 18, nil),
        mariolua_menu.Checkbox:new("resolver.lowdelta", "Force Low Delta", 0, 0, 18, 18, nil),
        mariolua_menu.Keybox:new("resolver.toggle", "Resolver On/Off Key", 0, 0, 200, 22, nil),
        mariolua_menu.Combobox:new(
            "resolver.performance",
            "Resolver Performance",
            0,
            0,
            200,
            22,
            {"Low", "Medium", "High", "Very High"},
            nil
        ),
        mariolua_menu.Text:new("resolver.headline4", "[Others]", 0, 0, 0, 0, "b", nil),
        mariolua_menu.Checkbox:new("resolver.indicator", "Draw Side Indicator", 0, 0, 18, 18, nil),
        mariolua_menu.Checkbox:new("resolver.log.miss", "Chat Log Missed Shots", 0, 0, 18, 18, nil),
        mariolua_menu.Checkbox:new("resolver.log.hit", "Chat Log Hit Shots", 0, 0, 18, 18, nil),
        mariolua_menu.Button:new(
            "resolver.reset",
            "Reset MarioLua Resolver",
            0,
            0,
            200,
            28,
            function()
                if globalvars.resolver_t ~= nil then
                    resolver.full_reset()
                end
            end
        ),
        mariolua_menu.Button:new(
            "settings.resolverstats",
            "Print Resolver Stats In Console",
            0,
            0,
            200,
            28,
            function()
                mariolua.PrintInChat(resolver.print_resolver_stats[1])
                mariolua.PrintInChat(resolver.print_resolver_stats[2])
                mariolua.PrintInChat(resolver.print_resolver_stats[3])
                mariolua.PrintInChat(resolver.print_resolver_stats[4])
                mariolua.print(resolver.print_resolver_stats[1])
                mariolua.print(resolver.print_resolver_stats[2])
                mariolua.print(resolver.print_resolver_stats[3])
                mariolua.print(resolver.print_resolver_stats[4])
            end
        )
    },
    resolver2 = {
        mariolua_menu.Groupbox:new("resolver.debug.bg", "MarioLua Resolver DEBUG", 0, 0, 235, 480),
        mariolua_menu.Text:new("resolver.headline3", "[ DEBUG ]", 0, 0, 0, 0, "b", nil),
        mariolua_menu.Multibox:new(
            "resolver.module",
            "Resolver Modules",
            0,
            0,
            200,
            22,
            {
                mariolua_menu.MultiboxItem:new("module", "Edge"),
                mariolua_menu.MultiboxItem:new("module", "Edge2"),
                mariolua_menu.MultiboxItem:new("module", "Trace"),
                mariolua_menu.MultiboxItem:new("module", "Trace2"),
                mariolua_menu.MultiboxItem:new("module", "Fraction"),
                mariolua_menu.MultiboxItem:new("module", "Fraction2"),
                mariolua_menu.MultiboxItem:new("module", "Predict"),
                mariolua_menu.MultiboxItem:new("module", "Damage"),
                mariolua_menu.MultiboxItem:new("module", "Damage2"),
                mariolua_menu.MultiboxItem:new("module", "Simple"),
                mariolua_menu.MultiboxItem:new("module", "Position"),
                mariolua_menu.MultiboxItem:new("module", "Condition"),
                mariolua_menu.MultiboxItem:new("module", "Lby"),
                mariolua_menu.MultiboxItem:new("module", "Animlayer Side"),
                mariolua_menu.MultiboxItem:new("module", "Animlayer Switch"),
                mariolua_menu.MultiboxItem:new("module", "Animlayer Low Delta"),
                mariolua_menu.MultiboxItem:new("module", "Pattern"),
                mariolua_menu.MultiboxItem:new("module", "Module Accuracy"),
                mariolua_menu.MultiboxItem:new("module", "Bruteforce"),
                mariolua_menu.MultiboxItem:new("module", "Low Delta"),
                mariolua_menu.MultiboxItem:new("module", "Fakeduck"),
                mariolua_menu.MultiboxItem:new("module", "Desync Detection")
            },
            nil
        ),
        mariolua_menu.Slider:new(
            "resolver.predict.localticks",
            "Local Position-Prediction Ticks",
            0,
            0,
            200,
            12,
            2,
            0,
            50
        ),
        mariolua_menu.Slider:new(
            "resolver.predict.enemyticks",
            "Enemy Position-Prediction Ticks",
            0,
            0,
            200,
            12,
            2,
            0,
            50
        ),
        mariolua_menu.Slider:new("resolver.fakelimit", "Yaw Limit", 0, 0, 200, 12, 60, 0, 60),
        mariolua_menu.Checkbox:new("resolver.lby.pref", "Prefer Lby While Standing", 0, 0, 18, 18, nil),
        mariolua_menu.Checkbox:new("resolver.animlayer.pref", "Prefer Animlayer Side", 0, 0, 18, 18, true),
        mariolua_menu.Text:new("resolver.fdheadline", "Fake duck detection", 0, 0, 0, 0, "b", nil),
        mariolua_menu.Slider:new("resolver.fakeduck.tick", "Crouched Ticks", 0, 0, 200, 12, 5, 0, 64),
        mariolua_menu.Slider:new("resolver.fakeduck.lby", "Lby lower than", 0, 0, 200, 12, 10, 0, 60)
    },
    antiaim = {
        tabs = {
            mariolua_menu.Tab:new(
                "antiaim.tab",
                "AA",
                0,
                0,
                80,
                35,
                "antiaim",
                function(self)
                    self:SetCurrentTab()
                    globalvars.aa_tab = "AA"
                    mariolua.log("Anti-Aim tab pressed")
                end,
                nil
            ),
            mariolua_menu.Tab:new(
                "antiaim.tab2",
                "AA 2",
                0,
                0,
                80,
                35,
                "antiaim",
                function(self)
                    self:SetCurrentTab()
                    globalvars.aa_tab = "AA 2"
                    mariolua.log("Anti-Aim 2 tab pressed")
                end,
                nil
            )
        },
        ["conditions"] = {
            mariolua_menu.Tab:new(
                "antiaim.tab.standing",
                "Standing",
                0,
                0,
                65,
                30,
                "antiaim.tab",
                function(self)
                    self:SetCurrentTab()
                    globalvars.aa_condition_tab = "Standing"
                    mariolua.log("Anti-Aim Standing tab pressed")
                end,
                nil
            ),
            mariolua_menu.Tab:new(
                "antiaim.tab.moving",
                "Moving",
                0,
                0,
                65,
                30,
                "antiaim.tab",
                function(self)
                    self:SetCurrentTab()
                    globalvars.aa_condition_tab = "Moving"
                    mariolua.log("Anti-Aim Moving tab pressed")
                end,
                nil
            ),
            mariolua_menu.Tab:new(
                "antiaim.tab.slowmotion",
                "Slow Motion",
                0,
                0,
                65,
                30,
                "antiaim.tab",
                function(self)
                    self:SetCurrentTab()
                    globalvars.aa_condition_tab = "Slow Motion"
                    mariolua.log("Anti-Aim Slow Motion tab pressed")
                end,
                nil
            )
        },
        tab_1 = util_functions.generate_aa_tab(),
        tab_2 = {
            [1] = {
                mariolua_menu.Groupbox:new("antiaim.gb.keybinds", "Keybinds", 0, 0, 230, 105),
                mariolua_menu.Keybox:new("antiaim.switchkey", "Legit Anti-Aim Switch Key", 0, 0, 200, 22, nil),
                mariolua_menu.Keybox:new("antiaim.offkey", "Legit Anti-Aim Off Key", 0, 0, 200, 22, nil),
                mariolua_menu.Groupbox:new("antiaim.exception.gb", "Anti-Aim Exceptions", 0, 20, 230, 240),
                mariolua_menu.Checkbox:new("antiaim.exception", "Enable", 0, 0, 18, 18, nil),
                mariolua_menu.Slider:new("antiaim.exception.ping", "Disable On Ping", 0, 0, 200, 12, 100, 0, 1000),
                mariolua_menu.Slider:new("antiaim.exception.fps", "Disable On Fps", 0, 0, 200, 12, 45, 0, 100),
                mariolua_menu.Slider:new("antiaim.exception.loss", "Disable On Loss", 0, 0, 200, 12, 50, 0, 100),
                mariolua_menu.Slider:new("antiaim.exception.choke", "Disable On Choke", 0, 0, 200, 12, 50, 0, 100),
                mariolua_menu.Groupbox:new("antiaim.other", "Other", 0, 20, 210, 60),
                mariolua_menu.Checkbox:new("antiaim.freezetime", "Freeze-time Anti-Aim", 0, 0, 18, 18, nil)
            },
            [2] = {
                mariolua_menu.Groupbox:new("antiaim.epeek", "E-Peek", 0, 0, 210, 200),
                mariolua_menu.Checkbox:new("antiaim.epeek.enabled", "Enable", 0, 0, 18, 18, nil),
                mariolua_menu.Combobox:new(
                    "antiaim.epeek.method",
                    "E-Peek Method",
                    0,
                    0,
                    185,
                    22,
                    {"Default", "Automatic"},
                    nil
                ),
                mariolua_menu.Combobox:new(
                    "antiaim.epeek.mode",
                    "E-Peek Mode",
                    0,
                    0,
                    185,
                    22,
                    {"Toggle", "Hold", "Always"},
                    nil
                ),
                mariolua_menu.Keybox:new("antiaim.epeek.key", "E-Peek Key", 0, 0, 185, 22, nil),
                mariolua_menu.Groupbox:new("antiaim.config.gb", "Anti-Aim Presets", 0, 20, 210, 250),
                mariolua_menu.Combobox:new("antiaim.config.list", "Presets", 0, 0, 185, 22, {}, 1),
                mariolua_menu.Editbox:new("antiaim.config.input", "", 0, 0, 185, 22),
                mariolua_menu.Button:new(
                    "antiaim.config.load",
                    "Load",
                    0,
                    0,
                    185,
                    30,
                    function()
                        globalvars.antiaim.LoadConfig()
                    end
                ),
                mariolua_menu.Button:new(
                    "antiaim.config.save",
                    "Save",
                    0,
                    0,
                    185,
                    30,
                    function()
                        globalvars.antiaim.SaveConfig()
                    end
                ),
                mariolua_menu.Button:new(
                    "antiaim.config.delete",
                    "Delete",
                    0,
                    0,
                    185,
                    30,
                    function()
                        globalvars.antiaim.RemoveConfig()
                    end
                )
            }
        }
    },
    misc = {
        mariolua_menu.Groupbox:new("misc.thirdperson", "Misc", 0, 0, 220, 600),
        mariolua_menu.Keybox:new("misc.thirdpersonkey", "Thirdperson Key", 0, 0, 200, 22, nil),
        mariolua_menu.Slider:new("misc.thirdperson.dist", "Thirdperson Distance", 0, 0, 190, 12, 120, 1, 200),
        mariolua_menu.Checkbox:new("misc.thirdperson.collison", "Disable Thirdperson Cam Collision", 0, 0, 18, 18, nil),
        mariolua_menu.Button:new(
            "misc.reportallenemies",
            "Report All Enemies",
            0,
            0,
            200,
            28,
            function()
                if globalvars.report.should_report ~= true then
                    mariolua.PrintInChat("Reporting all enemies...")
                end
                globalvars.report.should_report = true
            end
        ),
        mariolua_menu.Button:new(
            "misc.reportbotallenemies",
            "Community Report All Enemies",
            0,
            0,
            200,
            28,
            function()
                local cc = 0
                for ent = 1, globals.maxplayers() do
                    if entity.is_enemy(ent) then
                        cc = cc + 2
                        client.delay_call(
                            -1 + cc,
                            function()
                                util_functions.community_report_all_enemies(ent)
                            end
                        )
                    end
                end
            end
        ),
        mariolua_menu.Checkbox:new("misc.reportwrite", "Write Report In Chat", 0, 0, 18, 18, nil),
        mariolua_menu.Multibox:new(
            "misc.features",
            "Features",
            0,
            0,
            200,
            22,
            {
                mariolua_menu.MultiboxItem:new("feature", "Slide Walk"),
                mariolua_menu.MultiboxItem:new("feature", "Disable Engine Sleep"),
                mariolua_menu.MultiboxItem:new("feature", "Auto Hold Bot"),
                mariolua_menu.MultiboxItem:new("feature", "Vote Revealer"),
                mariolua_menu.MultiboxItem:new("feature", "Legit Fake Duck Fix"),
                mariolua_menu.MultiboxItem:new("feature", "Bypass sv_pure Fix")
            },
            nil
        ),
        mariolua_menu.Button:new(
            "misc.stopqueue",
            "Stop Match Search",
            0,
            0,
            200,
            28,
            function()
                return LobbyAPI.StopMatchmaking()
            end
        ),
        mariolua_menu.Checkbox:new("misc.clantag.enabled", "Clantag Changer", 0, 0, 18, 18, nil),
        mariolua_menu.Combobox:new(
            "misc.clantag.tag",
            "Clantag Presets",
            0,
            0,
            200,
            22,
            {"Custom", "MarioLua", "MarioLua 2", "Aimware", "Cooking", "pphud", "Iniuria", "Neverlose"},
            nil
        ),
        mariolua_menu.Editbox:new("misc.clantag.input", "Custom Clantag", 0, 0, 200, 22),
        mariolua_menu.Combobox:new(
            "misc.clantag.anim",
            "Clantag Animation",
            0,
            0,
            200,
            22,
            {"Default", "Static", "Time", "Reverse", "Classic", "Loop"},
            nil
        ),
        mariolua_menu.Slider:new("misc.clantag.speed", "Clantag Speed", 0, 0, 190, 12, 25, 0, 100)
    },
    misc2 = {
        mariolua_menu.Groupbox:new("misc.fakelag", "Fakelag", 0, 0, 220, 230),
        mariolua_menu.Checkbox:new("misc.fakelag.enabled", "Fakelag On-Peek", 0, 0, 18, 18, nil),
        mariolua_menu.Checkbox:new("misc.fakelag.drawprediction", "Draw Prediction", 0, 0, 18, 18, nil),
        mariolua_menu.Slider:new("misc.fakelag.prediction", "Peek Prediction", 0, 0, 190, 12, 7, 5, 15),
        mariolua_menu.Slider:new("misc.fakelag.time", "Lag Time", 0, 0, 190, 12, 75, 0, 300),
        mariolua_menu.Groupbox:new("misc.namespam.gb", "Name Spamer", 0, 45, 220, 260),
        mariolua_menu.Combobox:new("misc.namespam.name", "Spam Name", 0, 0, 200, 22, {}, 1),
        mariolua_menu.Button:new(
            "misc.namespam",
            "Apply Name Spam",
            0,
            0,
            200,
            30,
            function()
                globalvars.should_namespam = true
                client.delay_call(
                    10,
                    function()
                        globalvars.should_namespam = false
                        globalvars.spam_counter = 0
                    end
                )
            end
        ),
        mariolua_menu.Button:new(
            "misc.namespam.addname",
            "Add Name",
            0,
            0,
            200,
            25,
            function()
                globalvars.should_addnamespam = true
                if globalvars.should_addnamespam then
                    util_functions.NameSpamAdd()
                end
            end
        ),
        mariolua_menu.Button:new(
            "misc.namespam.removename",
            "Remove Name",
            0,
            0,
            200,
            25,
            function()
                globalvars.should_removenamespam = true
                if globalvars.should_removenamespam then
                    util_functions.NameSpamRemove()
                end
            end
        ),
        mariolua_menu.Checkbox:new("misc.namespam.restore", "Restore Name", 0, 0, 18, 18, nil)
    },
    indicators = {
        mariolua_menu.Groupbox:new("indicators.gb", "Indicators", 0, 0, 185, 360),
        mariolua_menu.Multibox:new("indicators.itemlist", "Indicators", 0, 0, 170, 22, {}, nil),
        mariolua_menu.Combobox:new(
            "indicators.font",
            "Indicator Font",
            0,
            0,
            170,
            22,
            {
                "Arial",
                "Arial Black",
                "Calibri",
                "Comic Sans MS",
                "Courier",
                "Courier New",
                "Impact",
                "Lucida Console",
                "Malgun Gothic",
                "Microsoft Himalaya",
                "Microsoft Sans Serif",
                "Modern",
                "MS Gothic",
                "Nirmala UI",
                "Segoe Print",
                "Segoe Script",
                "Segoe UI",
                "Segoe UI Black",
                "Segoe UI Symbol",
                "System",
                "Tahoma",
                "Times New Roman",
                "Verdana"
            },
            nil
        ),
        mariolua_menu.Combobox:new(
            "indicators.container.style",
            "Container Style",
            0,
            0,
            170,
            22,
            {"Gamesense", "Aimware", "Shadow"},
            nil
        ),
        mariolua_menu.Combobox:new(
            "indicators.arrow.style",
            "Arrow Indicator Style",
            0,
            0,
            170,
            22,
            {
                ch["left_1"] .. " " .. ch["right_1"],
                ch["left_2"] .. " " .. ch["right_2"],
                ch["left_3"] .. " " .. ch["right_3"],
                ch["4"] .. " " .. ch["4"]
            },
            nil
        ),
        mariolua_menu.Multibox:new(
            "indicators.extras",
            "Indicator Extras",
            0,
            0,
            170,
            22,
            {
                mariolua_menu.MultiboxItem:new("extra", "Draw Shadow"),
                mariolua_menu.MultiboxItem:new("extra", "Draw While Active"),
                mariolua_menu.MultiboxItem:new("extra", "Short Text"),
                mariolua_menu.MultiboxItem:new("extra", "Draw Container"),
                mariolua_menu.MultiboxItem:new("extra", "Rainbow Header"),
                mariolua_menu.MultiboxItem:new("extra", "Antialias"),
                mariolua_menu.MultiboxItem:new("extra", "Hide Skeet Indicators"),
                mariolua_menu.MultiboxItem:new("extra", "Use Skeet Indicators")
            },
            nil
        ),
        mariolua_menu.Button:new(
            "indicators.arrow.center",
            "Center Arrow Indicators",
            0,
            0,
            170,
            28,
            function()
                util_functions.center_arrow_indicator()
            end
        ),
        mariolua_menu.Button:new(
            "indicators.color.button",
            "Apply Color From Clipboard",
            0,
            0,
            170,
            28,
            function()
                if globalvars.clipboard == nil then
                    return mariolua.log("Cant set color, clipboard is nil!")
                end
                if globalvars.clipboard:gsub(globalvars.clipboard:sub(-globalvars.clipboard:len() + 1), "") == "#" then
                    local ap, av, a5 = extended_math.hex2rgb(globalvars.clipboard)
                    mariolua.log("Indicator color set to " .. extended_table.table_to_string3({ap, av, a5, 255}))
                    cfg.indicator_color:SetValue({ap, av, a5, 255})
                else
                    mariolua.log(
                        "Incompartible color code! Only hex colors. " ..
                            globalvars.clipboard:gsub(globalvars.clipboard:sub(-globalvars.clipboard:len() + 1), "")
                    )
                end
            end
        )
    },
    indicators2 = {
        mariolua_menu.Groupbox:new("indicators.others.gb", "Others", 0, 0, 260, 330),
        mariolua_menu.Slider:new("indicators.alpha", "Indicator Alpha", 0, 0, 230, 12, 255, 0, 255),
        mariolua_menu.Slider:new("indicators.size", "Indicator Size", 0, 0, 230, 12, 14, 0, 150),
        mariolua_menu.Slider:new("indicators.arrow.alpha", "Arrow Indicator Alpha", 0, 0, 230, 12, 255, 0, 255),
        mariolua_menu.Slider:new("indicators.arrow.size", "Arrow Indicator Size", 0, 0, 230, 12, 20, 0, 150),
        mariolua_menu.Slider:new("indicators.arrow.spacing", "Arrow Indicator Spacing", 0, 0, 230, 12, 30, 0, 300),
        mariolua_menu.Slider:new("indicators.skeet.pos", "Skeet Indicator Position", 0, 0, 230, 12, 0, 0, 20)
    },
    visuals = {
        mariolua_menu.Groupbox:new("visuals.gb", "Visuals", 0, 0, 210, 440),
        mariolua_menu.Multibox:new(
            "visuals.flags.items",
            "ESP Flags",
            0,
            0,
            180,
            22,
            {
                mariolua_menu.MultiboxItem:new("watermark", "Lethal"),
                mariolua_menu.MultiboxItem:new("watermark", "Resolving"),
                mariolua_menu.MultiboxItem:new("watermark", "Low-Delta"),
                mariolua_menu.MultiboxItem:new("watermark", "Slow-walk")
            },
            nil
        ),
        mariolua_menu.Checkbox:new("visuals.watermark", "Watermark", 0, 0, 18, 18, nil),
        mariolua_menu.Multibox:new(
            "visuals.watermark.items",
            "Watermark Items",
            0,
            0,
            180,
            22,
            {
                mariolua_menu.MultiboxItem:new("watermark", "Username"),
                mariolua_menu.MultiboxItem:new("watermark", "Time"),
                mariolua_menu.MultiboxItem:new("watermark", "FPS"),
                mariolua_menu.MultiboxItem:new("watermark", "Latency"),
                mariolua_menu.MultiboxItem:new("watermark", "KDR"),
                mariolua_menu.MultiboxItem:new("watermark", "Velocity"),
                mariolua_menu.MultiboxItem:new("watermark", "Server tickrate")
            },
            nil
        ),
        mariolua_menu.Combobox:new(
            "visuals.watermark.style",
            "Watermark Style",
            0,
            0,
            180,
            22,
            {"Minimal", "Gamesense", "Aimware"},
            nil
        ),
        mariolua_menu.Multibox:new(
            "visuals.angle",
            "Enemy Angles",
            0,
            0,
            180,
            22,
            {
                mariolua_menu.MultiboxItem:new("angle", "Real"),
                mariolua_menu.MultiboxItem:new("angle", "Fake"),
                mariolua_menu.MultiboxItem:new("angle", "LBY")
            },
            nil
        ),
        mariolua_menu.Checkbox:new("visuals.fakelagchams", "Fakelag Chams", 0, 0, 18, 18, nil),
        mariolua_menu.Checkbox:new("visuals.angle.circle", "Angle Circle", 0, 0, 18, 18, nil),
        mariolua_menu.Checkbox:new("visuals.penetration.damage", "Penetration Damage", 0, 0, 18, 18, nil),
        mariolua_menu.Checkbox:new("visuals.skeleton", "Skeleton", 0, 0, 18, 18, nil)
    },
    settings = {
        mariolua_menu.Groupbox:new("settings.gb", "Settings", 0, 0, 210, 480),
        mariolua_menu.Combobox:new(
            "settings.guistyle",
            "GUI Style",
            0,
            0,
            190,
            22,
            {"Gamesense", "Aimware", "Fuhrer", "UwU", "Minimal"},
            nil
        ),
        mariolua_menu.Checkbox:new("settings.custombackground", "Custom GUI Background", 0, 0, 18, 18, nil),
        mariolua_menu.Button:new(
            "settings.importjpg",
            "Load JPG/PNG From URL",
            0,
            0,
            190,
            28,
            function()
                mariolua.notify(5, "Loading custom background image")
                local ao =
                    [[
                'url': ']] ..
                    globalvars.clipboard ..
                        [[',
                'user': ']] .. mariolua.userdata.name .. [['
            ]]
                local eh = globalvars.clipboard

                aO.post(
                    "https://mariolua.at/hdf/auth/encode_image.php",
                    ao,
                    function(aQ)
                        globalvars.img_type = globalvars.clipboard:match(".jpg") and "jpg" or "png"
                        local es = base64.decode(aQ)
                        local en = readfile("mariolua_bg.dat")
                        en = en or "return {\n}"
                        if en:match(eh) then
                            ui.set(globalvars.config_bg_string, eh)
                            return
                        end
                        en = en:sub(1, #en - 1)
                        writefile("mariolua_bg.dat", en .. '["' .. eh .. '"] = [[' .. aQ .. "]],\n}")
                        ui.set(globalvars.config_bg_string, eh)
                        extended_renderer.gpx.jpg.custom_bg = es
                    end,
                    nil,
                    "custom_background"
                )
            end
        ),
        mariolua_menu.Checkbox:new(
            "settings.backup",
            "Auto Backup Config",
            0,
            0,
            18,
            18,
            mariolua.userdata.backup,
            nil,
            function()
                database.write("mariolua_db_backup", true)
                print("true")
            end,
            function()
                database.write("mariolua_db_backup", false)
                print("false")
            end
        ),
        mariolua_menu.Multibox:new(
            "settings.debugmode",
            "Debug",
            0,
            0,
            190,
            22,
            {
                mariolua_menu.MultiboxItem:new("debug", "Logs"),
                mariolua_menu.MultiboxItem:new("debug", "Show In Bounds"),
                mariolua_menu.MultiboxItem:new("debug", "Show Debug Gui Objects"),
                mariolua_menu.MultiboxItem:new("debug", "Event Infos"),
                mariolua_menu.MultiboxItem:new("debug", "Player Infos"),
                mariolua_menu.MultiboxItem:new("debug", "Side Tracing"),
                mariolua_menu.MultiboxItem:new("debug", "Player Prediction")
            },
            nil
        ),
        mariolua_menu.Button:new(
            "settings.runstring",
            "Run String From Clipboard",
            0,
            0,
            190,
            28,
            function()
                if not globalvars.clipboard then
                    return
                end
                mariolua.notify(5, "Running lua string from clipboard")
                return loadstring(globalvars.clipboard)()
            end
        ),
        mariolua_menu.Checkbox:new("settings.guilock", "Unlock GUI", 0, 0, 18, 18, nil),
        mariolua_menu.Button:new(
            "settings.exportcfg",
            "Export Config To Clipboard",
            0,
            0,
            190,
            28,
            function()
                mariolua_menu.SaveConfig()
                ffi_utils.set_clipboard_text(
                    ffi_utils.VGUI_System,
                    ui.get(globalvars.config_string),
                    ui.get(globalvars.config_string):len()
                )
                mariolua.notify(5, "Exported Config to Clipboard and Console")
            end
        ),
        mariolua_menu.Button:new(
            "settings.importcfg",
            "Import Config From Clipboard",
            0,
            0,
            190,
            28,
            function()
                if not globalvars.clipboard then
                    return
                end
                mariolua_menu.LoadConfig(globalvars.clipboard)
                mariolua.notify(5, "Loaded Config from Clipboard")
                mariolua_menu.SaveConfig()
            end
        ),
        mariolua_menu.Text:new("settings.clipboardtext", "Clipboard: no text", 0, 0, 0, 0, "b", nil)
    },
    settings2 = {
        mariolua_menu.Groupbox:new("settings.gb2", "Community Reportbot", 0, 0, 210, 170),
        mariolua_menu.Button:new(
            "settings.token.set",
            "Set API Token",
            0,
            0,
            190,
            28,
            function()
                if not globalvars.clipboard then
                    return
                end
                database.write("mariolua_db_token", globalvars.clipboard)
                mariolua.userdata.token = globalvars.clipboard
                mariolua.notify(5, "Reportbot token set")
            end
        ),
        mariolua_menu.Button:new(
            "settings.token.remove",
            "Remove API Token",
            0,
            0,
            190,
            28,
            function()
                database.write("mariolua_db_token", "")
                mariolua.userdata.token = ""
                mariolua.notify(5, "Reportbot token removed")
            end
        ),
        mariolua_menu.Button:new(
            "settings.token.print",
            "Print Set API Token",
            0,
            0,
            190,
            28,
            function()
                mariolua.print("Reportbot token: " .. database.read("mariolua_db_token"))
                mariolua.PrintInChat("Reportbot token: " .. database.read("mariolua_db_token"))
                mariolua.notify(5, "Reportbot token: " .. database.read("mariolua_db_token"))
            end
        )
    }
}

cfg.aimbot_enable = mariolua_menu.Reference("trigger.ragetrigger")
cfg.aimbot_mode = mariolua_menu.Reference("trigger.ragetrigger.mode")
cfg.aimbot_key = mariolua_menu.Reference("trigger.triggerkey")
cfg.forcebaim_enable = mariolua_menu.Reference("trigger.forcebaim")
cfg.forcebaim_mode = mariolua_menu.Reference("trigger.forcebaim.mode")
cfg.forcebaim_key = mariolua_menu.Reference("trigger.forcebaimkey")
cfg.autowall_enable = mariolua_menu.Reference("trigger.autowall")
cfg.autowall_mode = mariolua_menu.Reference("trigger.autowall.mode")
cfg.autowall_key = mariolua_menu.Reference("trigger.autowallkey")
cfg.dmg_override_enable = mariolua_menu.Reference("trigger.damage.override")
cfg.dmg_override_mode = mariolua_menu.Reference("trigger.damage.override.mode")
cfg.dmg_override_key = mariolua_menu.Reference("trigger.damage.override.key")
cfg.aimbot_wait_onshot = mariolua_menu.Reference("aimbot.keybinds.onshot")
cfg.aimbot_wait_onshot_mode = mariolua_menu.Reference("aimbot.keybinds.onshot.mode")
cfg.aimbot_wait_onshot_key = mariolua_menu.Reference("aimbot.keybinds.onshot.key")
cfg.dormant_enable = mariolua_menu.Reference("aimbot.dormant")
cfg.dormant_mode = mariolua_menu.Reference("aimbot.dormant.mode")
cfg.dormant_key = mariolua_menu.Reference("aimbot.dormant.key")
cfg.dormant_hitchance = mariolua_menu.Reference("aimbot.dormant.hitchance")
cfg.dormant_mindmg = mariolua_menu.Reference("aimbot.dormant.mindmg")
cfg.dormant_extra = mariolua_menu.Reference("aimbot.dormant.extra")
cfg.dyn_fov_mode = mariolua_menu.Reference("trigger.dynamicfov.mode")
cfg.dyn_fov_factor = mariolua_menu.Reference("trigger.dynamicfov.dynamicfactor")
cfg.dyn_fov_draw = mariolua_menu.Reference("trigger.dynamicfov.draw")
cfg.dyn_fov_min = mariolua_menu.Reference("trigger.dynamicfov.min")
cfg.dyn_fov_max = mariolua_menu.Reference("trigger.dynamicfov.max")
cfg.tr_legitaw_enabled = mariolua_menu.Reference("trigger.legitautowall.enabled")
cfg.tr_legitaw_hitbox = mariolua_menu.Reference("trigger.legitautowall.hitbox")
cfg.tr_legitaw_extrapolated = mariolua_menu.Reference("trigger.legitautowall.extrapolated")
cfg.double_tap_speed = mariolua_menu.Reference("aimbot.doubletap.speed")
cfg.resolver_enable = mariolua_menu.Reference("resolver.enable")
cfg.resolver_experimental = mariolua_menu.Reference("resolver.experimental")
cfg.resolver_force_lowdelta = mariolua_menu.Reference("resolver.lowdelta")
cfg.resolver_toggle = mariolua_menu.Reference("resolver.toggle")
cfg.resolver_performance = mariolua_menu.Reference("resolver.performance")
cfg.resolver_draw_side_indicator = mariolua_menu.Reference("resolver.indicator")
cfg.resolver_log_miss = mariolua_menu.Reference("resolver.log.miss")
cfg.resolver_log_hit = mariolua_menu.Reference("resolver.log.hit")
cfg.resolver_debug_headline = mariolua_menu.Reference("resolver.headline3")
cfg.resolver_fdheadline = mariolua_menu.Reference("resolver.fdheadline")
cfg.resolver_fakeduck_ticks = mariolua_menu.Reference("resolver.fakeduck.tick")
cfg.resolver_fakeduck_lby = mariolua_menu.Reference("resolver.fakeduck.lby")
cfg.resolver_pl_show = mariolua_menu.Reference("resolver.pl.show")
cfg.resolver_modules = mariolua_menu.Reference("resolver.module")
cfg.resolver_predict_local_ticks = mariolua_menu.Reference("resolver.predict.localticks")
cfg.resolver_predict_enemy_ticks = mariolua_menu.Reference("resolver.predict.enemyticks")
cfg.resolver_yawlimit = mariolua_menu.Reference("resolver.fakelimit")
cfg.resolver_prefer_lby = mariolua_menu.Reference("resolver.lby.pref")
cfg.resolver_prefer_animlayer = mariolua_menu.Reference("resolver.animlayer.pref")
cfg.antiaim_enable = mariolua_menu.Reference("antiaim.standing.enable")
cfg.antiaim_state = mariolua_menu.Reference("antiaim.standing.state")
cfg.antiaim_fake = mariolua_menu.Reference("antiaim.standing.fake")
cfg.antiaim_fake_jitter = mariolua_menu.Reference("antiaim.standing.fake.jitter")
cfg.antiaim_fake_jitter_enable = mariolua_menu.Reference("antiaim.standing.fake.jitter.enable")
cfg.antiaim_fake_jitter_speed = mariolua_menu.Reference("antiaim.standing.fake.jitter.speed")
cfg.antiaim_body = mariolua_menu.Reference("antiaim.standing.body")
cfg.antiaim_body_mode = mariolua_menu.Reference("antiaim.standing.body.mode")
cfg.antiaim_lbyextras_mode = mariolua_menu.Reference("antiaim.standing.lbyextras")
cfg.antiaim_freestanding = mariolua_menu.Reference("antiaim.standing.mode")
cfg.antiaim_switch_key = mariolua_menu.Reference("antiaim.switchkey")
cfg.antiaim_off_key = mariolua_menu.Reference("antiaim.offkey")
cfg.antiaim_lby_mode = mariolua_menu.Reference("antiaim.standing.lby")
cfg.antiaim_slowwalk = mariolua_menu.Reference("antiaim.standing.slowwalk.enabled")
cfg.antiaim_slowwalk_speed = mariolua_menu.Reference("antiaim.standing.slowwalk.speed")
cfg.antiaim_enable_anti_bruteforce = mariolua_menu.Reference("antiaim.standing.antibruteforce.enable")
cfg.antiaim_anti_bruteforce_timeout = mariolua_menu.Reference("antiaim.standing.antibruteforce.timeout")
cfg.antiaim_default_yaw_offset = mariolua_menu.Reference("antiaim.standing.antibruteforce.yaw.default")
cfg.antiaim_max_yaw_delta = mariolua_menu.Reference("antiaim.standing.antibruteforce.yaw.max")
cfg.antiaim_yaw_step = mariolua_menu.Reference("antiaim.standing.antibruteforce.yaw.step")
cfg.antiaim_default_body_yaw_offset = mariolua_menu.Reference("antiaim.standing.antibruteforce.bodyyaw.default")
cfg.antiaim_max_body_yaw_delta = mariolua_menu.Reference("antiaim.standing.antibruteforce.bodyyaw.max")
cfg.antiaim_body_yaw_step = mariolua_menu.Reference("antiaim.standing.antibruteforce.bodyyaw.step")
cfg.antiaim_exception_enable = mariolua_menu.Reference("antiaim.exception")
cfg.antiaim_exception_fps = mariolua_menu.Reference("antiaim.exception.fps")
cfg.antiaim_exception_ping = mariolua_menu.Reference("antiaim.exception.ping")
cfg.antiaim_exception_choke = mariolua_menu.Reference("antiaim.exception.choke")
cfg.antiaim_exception_loss = mariolua_menu.Reference("antiaim.exception.loss")
cfg.antiaim_epeek = mariolua_menu.Reference("antiaim.epeek.enabled")
cfg.antiaim_epeek_mode = mariolua_menu.Reference("antiaim.epeek.mode")
cfg.antiaim_epeek_method = mariolua_menu.Reference("antiaim.epeek.method")
cfg.antiaim_epeek_key = mariolua_menu.Reference("antiaim.epeek.key")
cfg.antiaim_freezetime = mariolua_menu.Reference("antiaim.freezetime")
cfg.antiaim_config_list = mariolua_menu.Reference("antiaim.config.list")
cfg.antiaim_config_input = mariolua_menu.Reference("antiaim.config.input")
cfg.antiaim_configs = mariolua_menu.SaveVar:new("antiaim.config.saves", {})
cfg.clantag_enabled = mariolua_menu.Reference("misc.clantag.enabled")
cfg.clantag_tag = mariolua_menu.Reference("misc.clantag.tag")
cfg.clantag_speed = mariolua_menu.Reference("misc.clantag.speed")
cfg.clantag_anim = mariolua_menu.Reference("misc.clantag.anim")
cfg.clantag_custom = mariolua_menu.Reference("misc.clantag.input")
cfg.thirdperson_key = mariolua_menu.Reference("misc.thirdpersonkey")
cfg.thirdperson_dist = mariolua_menu.Reference("misc.thirdperson.dist")
cfg.thirdperson_collision = mariolua_menu.Reference("misc.thirdperson.collison")
cfg.misc_features = mariolua_menu.Reference("misc.features")
cfg.report_write_chat = mariolua_menu.Reference("misc.reportwrite")
cfg.misc_maxusrcmdprocessticks = mariolua_menu.Reference("misc.maxusrcmdprocessticks")
cfg.misc_fakelagonpeek = mariolua_menu.Reference("misc.fakelag.enabled")
cfg.misc_fakelagonpeek_drawprediction = mariolua_menu.Reference("misc.fakelag.drawprediction")
cfg.misc_fakelagonpeek_prediction = mariolua_menu.Reference("misc.fakelag.prediction")
cfg.misc_fakelagonpeek_time = mariolua_menu.Reference("misc.fakelag.time")
cfg.misc_forcehvh = mariolua_menu.Reference("misc.forcehvh")
cfg.misc_aimbot_log = mariolua_menu.Reference("misc.aim.log")
cfg.misc_namespam_restore = mariolua_menu.Reference("misc.namespam.restore")
cfg.misc_namespam_name = mariolua_menu.Reference("misc.namespam.name")
cfg.misc_namespam_setname = mariolua_menu.Reference("misc.namespam.setname")
cfg.misc_namespam_addname = mariolua_menu.Reference("misc.namespam.addname")
cfg.misc_namespam_removename = mariolua_menu.Reference("misc.namespam.removename")
cfg.indicator = mariolua_menu.Indicator:new("main")
cfg.indicator_size = mariolua_menu.Reference("indicators.size")
cfg.indicator_alpha = mariolua_menu.Reference("indicators.alpha")
cfg.indicator_extras = mariolua_menu.Reference("indicators.extras")
cfg.indicator_container = mariolua_menu.Reference("indicators.container.style")
cfg.indicator_arrow_style = mariolua_menu.Reference("indicators.arrow.style")
cfg.indicator_arrow_size = mariolua_menu.Reference("indicators.arrow.size")
cfg.indicator_arrow_alpha = mariolua_menu.Reference("indicators.arrow.alpha")
cfg.indicator_arrow_spacing = mariolua_menu.Reference("indicators.arrow.spacing")
cfg.indicator_itemlist = mariolua_menu.Reference("indicators.itemlist")
cfg.indicator_fontitems = mariolua_menu.Reference("indicators.font")
cfg.indicator_skeet_pos = mariolua_menu.Reference("indicators.skeet.pos")
cfg.indicator_color = mariolua_menu.SaveVar:new("indicator.color", globalvars.color)
cfg.esp_flags = mariolua_menu.Reference("visuals.flags.items")
cfg.visuals_fakelagchams = mariolua_menu.Reference("visuals.fakelagchams")
cfg.visuals_watermark = mariolua_menu.Reference("visuals.watermark")
cfg.visuals_watermark_items = mariolua_menu.Reference("visuals.watermark.items")
cfg.visuals_watermark_style = mariolua_menu.Reference("visuals.watermark.style")
cfg.visuals_angle_circle = mariolua_menu.Reference("visuals.angle.circle")
cfg.visuals_penetration_damage = mariolua_menu.Reference("visuals.penetration.damage")
cfg.visuals_angle = mariolua_menu.Reference("visuals.angle")
cfg.visuals_skeleton = mariolua_menu.Reference("visuals.skeleton")
cfg.settings_guistyle = mariolua_menu.Reference("settings.guistyle")
cfg.settings_custombackground = mariolua_menu.Reference("settings.custombackground")
cfg.settings_clipboardtext = mariolua_menu.Reference("settings.clipboardtext")
cfg.settings_debugmode = mariolua_menu.Reference("settings.debugmode")
cfg.settings_guilock = mariolua_menu.Reference("settings.guilock")
cfg.settings_backup = mariolua_menu.Reference("settings.backup")
cfg.settings_adaptivewep = mariolua_menu.Reference("settings.adaptivewep")
cfg.settings_crack_target = mariolua_menu.Reference("settings.crack.target")
cfg.settings_crack_start = mariolua_menu.Reference("settings.crack.start")
cfg.wep_pistol_targethitbox = mariolua_menu.Reference("weapon.pistol.targethitbox")
cfg.wep_pistol_multipoint = mariolua_menu.Reference("weapon.pistol.multipoint")
cfg.wep_pistol_multipoint_scale = mariolua_menu.Reference("weapon.pistol.multipoint.scale")
cfg.wep_pistol_safepoint_prefer = mariolua_menu.Reference("weapon.pistol.safepoint.prefer")
cfg.wep_pistol_avoid = mariolua_menu.Reference("weapon.pistol.avoid")
cfg.wep_pistol_autofire = mariolua_menu.Reference("weapon.pistol.autofire")
cfg.wep_pistol_silentaim = mariolua_menu.Reference("weapon.pistol.silentaim")
cfg.wep_pistol_hitchance = mariolua_menu.Reference("weapon.pistol.hitchance")
cfg.wep_pistol_damage = mariolua_menu.Reference("weapon.pistol.damage")
cfg.wep_pistol_damage_override = mariolua_menu.Reference("weapon.pistol.damage.override")
cfg.wep_pistol_removerecoil = mariolua_menu.Reference("weapon.pistol.removerecoil")
cfg.wep_pistol_delayshot = mariolua_menu.Reference("weapon.pistol.delayshot")
cfg.wep_pistol_quickstop = mariolua_menu.Reference("weapon.pistol.quickstop")
cfg.wep_pistol_quickstop_options = mariolua_menu.Reference("weapon.pistol.quickstop.options")
cfg.wep_pistol_bodyaim_lethal = mariolua_menu.Reference("weapon.pistol.bodyaim.lethal")
cfg.wep_pistol_bodyaim_prefer = mariolua_menu.Reference("weapon.pistol.bodyaim.prefer")
cfg.wep_pistol_bodyaim_disablers = mariolua_menu.Reference("weapon.pistol.bodyaim.disablers")
cfg.wep_pistol_doubletap = mariolua_menu.Reference("weapon.pistol.doubletap")
cfg.wep_pistol_doubletap_mode = mariolua_menu.Reference("weapon.pistol.doubletap.mode")
cfg.wep_pistol_doubletap_hitchance = mariolua_menu.Reference("weapon.pistol.doubletap.hitchance")
cfg.wep_pistol_doubletap_fakelaglimit = mariolua_menu.Reference("weapon.pistol.doubletap.fakelaglimit")
cfg.wep_pistol_doubletap_quickstop = mariolua_menu.Reference("weapon.pistol.doubletap.quickstop")
cfg.wep_hpistol_targethitbox = mariolua_menu.Reference("weapon.hpistol.targethitbox")
cfg.wep_hpistol_multipoint = mariolua_menu.Reference("weapon.hpistol.multipoint")
cfg.wep_hpistol_multipoint_scale = mariolua_menu.Reference("weapon.hpistol.multipoint.scale")
cfg.wep_hpistol_safepoint_prefer = mariolua_menu.Reference("weapon.hpistol.safepoint.prefer")
cfg.wep_hpistol_avoid = mariolua_menu.Reference("weapon.hpistol.avoid")
cfg.wep_hpistol_autofire = mariolua_menu.Reference("weapon.hpistol.autofire")
cfg.wep_hpistol_silentaim = mariolua_menu.Reference("weapon.hpistol.silentaim")
cfg.wep_hpistol_hitchance = mariolua_menu.Reference("weapon.hpistol.hitchance")
cfg.wep_hpistol_damage = mariolua_menu.Reference("weapon.hpistol.damage")
cfg.wep_hpistol_damage_override = mariolua_menu.Reference("weapon.hpistol.damage.override")
cfg.wep_hpistol_removerecoil = mariolua_menu.Reference("weapon.hpistol.removerecoil")
cfg.wep_hpistol_delayshot = mariolua_menu.Reference("weapon.hpistol.delayshot")
cfg.wep_hpistol_quickstop = mariolua_menu.Reference("weapon.hpistol.quickstop")
cfg.wep_hpistol_quickstop_options = mariolua_menu.Reference("weapon.hpistol.quickstop.options")
cfg.wep_hpistol_bodyaim_lethal = mariolua_menu.Reference("weapon.hpistol.bodyaim.lethal")
cfg.wep_hpistol_bodyaim_prefer = mariolua_menu.Reference("weapon.hpistol.bodyaim.prefer")
cfg.wep_hpistol_bodyaim_disablers = mariolua_menu.Reference("weapon.hpistol.bodyaim.disablers")
cfg.wep_hpistol_doubletap = mariolua_menu.Reference("weapon.hpistol.doubletap")
cfg.wep_hpistol_doubletap_mode = mariolua_menu.Reference("weapon.hpistol.doubletap.mode")
cfg.wep_hpistol_doubletap_hitchance = mariolua_menu.Reference("weapon.hpistol.doubletap.hitchance")
cfg.wep_hpistol_doubletap_fakelaglimit = mariolua_menu.Reference("weapon.hpistol.doubletap.fakelaglimit")
cfg.wep_hpistol_doubletap_quickstop = mariolua_menu.Reference("weapon.hpistol.doubletap.quickstop")
cfg.wep_smg_targethitbox = mariolua_menu.Reference("weapon.smg.targethitbox")
cfg.wep_smg_multipoint = mariolua_menu.Reference("weapon.smg.multipoint")
cfg.wep_smg_multipoint_scale = mariolua_menu.Reference("weapon.smg.multipoint.scale")
cfg.wep_smg_safepoint_prefer = mariolua_menu.Reference("weapon.smg.safepoint.prefer")
cfg.wep_smg_avoid = mariolua_menu.Reference("weapon.smg.avoid")
cfg.wep_smg_autofire = mariolua_menu.Reference("weapon.smg.autofire")
cfg.wep_smg_silentaim = mariolua_menu.Reference("weapon.smg.silentaim")
cfg.wep_smg_hitchance = mariolua_menu.Reference("weapon.smg.hitchance")
cfg.wep_smg_damage = mariolua_menu.Reference("weapon.smg.damage")
cfg.wep_smg_damage_override = mariolua_menu.Reference("weapon.smg.damage.override")
cfg.wep_smg_removerecoil = mariolua_menu.Reference("weapon.smg.removerecoil")
cfg.wep_smg_delayshot = mariolua_menu.Reference("weapon.smg.delayshot")
cfg.wep_smg_quickstop = mariolua_menu.Reference("weapon.smg.quickstop")
cfg.wep_smg_quickstop_options = mariolua_menu.Reference("weapon.smg.quickstop.options")
cfg.wep_smg_bodyaim_lethal = mariolua_menu.Reference("weapon.smg.bodyaim.lethal")
cfg.wep_smg_bodyaim_prefer = mariolua_menu.Reference("weapon.smg.bodyaim.prefer")
cfg.wep_smg_bodyaim_disablers = mariolua_menu.Reference("weapon.smg.bodyaim.disablers")
cfg.wep_smg_doubletap = mariolua_menu.Reference("weapon.smg.doubletap")
cfg.wep_smg_doubletap_mode = mariolua_menu.Reference("weapon.smg.doubletap.mode")
cfg.wep_smg_doubletap_hitchance = mariolua_menu.Reference("weapon.smg.doubletap.hitchance")
cfg.wep_smg_doubletap_fakelaglimit = mariolua_menu.Reference("weapon.smg.doubletap.fakelaglimit")
cfg.wep_smg_doubletap_quickstop = mariolua_menu.Reference("weapon.smg.doubletap.quickstop")
cfg.wep_rifle_targethitbox = mariolua_menu.Reference("weapon.rifle.targethitbox")
cfg.wep_rifle_multipoint = mariolua_menu.Reference("weapon.rifle.multipoint")
cfg.wep_rifle_multipoint_scale = mariolua_menu.Reference("weapon.rifle.multipoint.scale")
cfg.wep_rifle_safepoint_prefer = mariolua_menu.Reference("weapon.rifle.safepoint.prefer")
cfg.wep_rifle_avoid = mariolua_menu.Reference("weapon.rifle.avoid")
cfg.wep_rifle_autofire = mariolua_menu.Reference("weapon.rifle.autofire")
cfg.wep_rifle_silentaim = mariolua_menu.Reference("weapon.rifle.silentaim")
cfg.wep_rifle_hitchance = mariolua_menu.Reference("weapon.rifle.hitchance")
cfg.wep_rifle_damage = mariolua_menu.Reference("weapon.rifle.damage")
cfg.wep_rifle_damage_override = mariolua_menu.Reference("weapon.rifle.damage.override")
cfg.wep_rifle_removerecoil = mariolua_menu.Reference("weapon.rifle.removerecoil")
cfg.wep_rifle_delayshot = mariolua_menu.Reference("weapon.rifle.delayshot")
cfg.wep_rifle_quickstop = mariolua_menu.Reference("weapon.rifle.quickstop")
cfg.wep_rifle_quickstop_options = mariolua_menu.Reference("weapon.rifle.quickstop.options")
cfg.wep_rifle_bodyaim_lethal = mariolua_menu.Reference("weapon.rifle.bodyaim.lethal")
cfg.wep_rifle_bodyaim_prefer = mariolua_menu.Reference("weapon.rifle.bodyaim.prefer")
cfg.wep_rifle_bodyaim_disablers = mariolua_menu.Reference("weapon.rifle.bodyaim.disablers")
cfg.wep_rifle_doubletap = mariolua_menu.Reference("weapon.rifle.doubletap")
cfg.wep_rifle_doubletap_mode = mariolua_menu.Reference("weapon.rifle.doubletap.mode")
cfg.wep_rifle_doubletap_hitchance = mariolua_menu.Reference("weapon.rifle.doubletap.hitchance")
cfg.wep_rifle_doubletap_fakelaglimit = mariolua_menu.Reference("weapon.rifle.doubletap.fakelaglimit")
cfg.wep_rifle_doubletap_quickstop = mariolua_menu.Reference("weapon.rifle.doubletap.quickstop")
cfg.wep_scout_targethitbox = mariolua_menu.Reference("weapon.scout.targethitbox")
cfg.wep_scout_multipoint = mariolua_menu.Reference("weapon.scout.multipoint")
cfg.wep_scout_multipoint_scale = mariolua_menu.Reference("weapon.scout.multipoint.scale")
cfg.wep_scout_safepoint_prefer = mariolua_menu.Reference("weapon.scout.safepoint.prefer")
cfg.wep_scout_avoid = mariolua_menu.Reference("weapon.scout.avoid")
cfg.wep_scout_autofire = mariolua_menu.Reference("weapon.scout.autofire")
cfg.wep_scout_silentaim = mariolua_menu.Reference("weapon.scout.silentaim")
cfg.wep_scout_hitchance = mariolua_menu.Reference("weapon.scout.hitchance")
cfg.wep_scout_damage = mariolua_menu.Reference("weapon.scout.damage")
cfg.wep_scout_damage_override = mariolua_menu.Reference("weapon.scout.damage.override")
cfg.wep_scout_removerecoil = mariolua_menu.Reference("weapon.scout.removerecoil")
cfg.wep_scout_delayshot = mariolua_menu.Reference("weapon.scout.delayshot")
cfg.wep_scout_quickstop = mariolua_menu.Reference("weapon.scout.quickstop")
cfg.wep_scout_quickstop_options = mariolua_menu.Reference("weapon.scout.quickstop.options")
cfg.wep_scout_bodyaim_lethal = mariolua_menu.Reference("weapon.scout.bodyaim.lethal")
cfg.wep_scout_bodyaim_prefer = mariolua_menu.Reference("weapon.scout.bodyaim.prefer")
cfg.wep_scout_bodyaim_disablers = mariolua_menu.Reference("weapon.scout.bodyaim.disablers")
cfg.wep_scout_doubletap = mariolua_menu.Reference("weapon.scout.doubletap")
cfg.wep_scout_doubletap_mode = mariolua_menu.Reference("weapon.scout.doubletap.mode")
cfg.wep_scout_doubletap_hitchance = mariolua_menu.Reference("weapon.scout.doubletap.hitchance")
cfg.wep_scout_doubletap_fakelaglimit = mariolua_menu.Reference("weapon.scout.doubletap.fakelaglimit")
cfg.wep_scout_doubletap_quickstop = mariolua_menu.Reference("weapon.scout.doubletap.quickstop")
cfg.wep_awp_targethitbox = mariolua_menu.Reference("weapon.awp.targethitbox")
cfg.wep_awp_multipoint = mariolua_menu.Reference("weapon.awp.multipoint")
cfg.wep_awp_multipoint_scale = mariolua_menu.Reference("weapon.awp.multipoint.scale")
cfg.wep_awp_safepoint_prefer = mariolua_menu.Reference("weapon.awp.safepoint.prefer")
cfg.wep_awp_avoid = mariolua_menu.Reference("weapon.awp.avoid")
cfg.wep_awp_autofire = mariolua_menu.Reference("weapon.awp.autofire")
cfg.wep_awp_silentaim = mariolua_menu.Reference("weapon.awp.silentaim")
cfg.wep_awp_hitchance = mariolua_menu.Reference("weapon.awp.hitchance")
cfg.wep_awp_damage = mariolua_menu.Reference("weapon.awp.damage")
cfg.wep_awp_damage_override = mariolua_menu.Reference("weapon.awp.damage.override")
cfg.wep_awp_removerecoil = mariolua_menu.Reference("weapon.awp.removerecoil")
cfg.wep_awp_delayshot = mariolua_menu.Reference("weapon.awp.delayshot")
cfg.wep_awp_quickstop = mariolua_menu.Reference("weapon.awp.quickstop")
cfg.wep_awp_quickstop_options = mariolua_menu.Reference("weapon.awp.quickstop.options")
cfg.wep_awp_bodyaim_lethal = mariolua_menu.Reference("weapon.awp.bodyaim.lethal")
cfg.wep_awp_bodyaim_prefer = mariolua_menu.Reference("weapon.awp.bodyaim.prefer")
cfg.wep_awp_bodyaim_disablers = mariolua_menu.Reference("weapon.awp.bodyaim.disablers")
cfg.wep_awp_doubletap = mariolua_menu.Reference("weapon.awp.doubletap")
cfg.wep_awp_doubletap_mode = mariolua_menu.Reference("weapon.awp.doubletap.mode")
cfg.wep_awp_doubletap_hitchance = mariolua_menu.Reference("weapon.awp.doubletap.hitchance")
cfg.wep_awp_doubletap_fakelaglimit = mariolua_menu.Reference("weapon.awp.doubletap.fakelaglimit")
cfg.wep_awp_doubletap_quickstop = mariolua_menu.Reference("weapon.awp.doubletap.quickstop")
cfg.wep_asniper_targethitbox = mariolua_menu.Reference("weapon.asniper.targethitbox")
cfg.wep_asniper_multipoint = mariolua_menu.Reference("weapon.asniper.multipoint")
cfg.wep_asniper_multipoint_scale = mariolua_menu.Reference("weapon.asniper.multipoint.scale")
cfg.wep_asniper_safepoint_prefer = mariolua_menu.Reference("weapon.asniper.safepoint.prefer")
cfg.wep_asniper_avoid = mariolua_menu.Reference("weapon.asniper.avoid")
cfg.wep_asniper_autofire = mariolua_menu.Reference("weapon.asniper.autofire")
cfg.wep_asniper_silentaim = mariolua_menu.Reference("weapon.asniper.silentaim")
cfg.wep_asniper_hitchance = mariolua_menu.Reference("weapon.asniper.hitchance")
cfg.wep_asniper_damage = mariolua_menu.Reference("weapon.asniper.damage")
cfg.wep_asniper_damage_override = mariolua_menu.Reference("weapon.asniper.damage.override")
cfg.wep_asniper_removerecoil = mariolua_menu.Reference("weapon.asniper.removerecoil")
cfg.wep_asniper_delayshot = mariolua_menu.Reference("weapon.asniper.delayshot")
cfg.wep_asniper_quickstop = mariolua_menu.Reference("weapon.asniper.quickstop")
cfg.wep_asniper_quickstop_options = mariolua_menu.Reference("weapon.asniper.quickstop.options")
cfg.wep_asniper_bodyaim_lethal = mariolua_menu.Reference("weapon.asniper.bodyaim.lethal")
cfg.wep_asniper_bodyaim_prefer = mariolua_menu.Reference("weapon.asniper.bodyaim.prefer")
cfg.wep_asniper_bodyaim_disablers = mariolua_menu.Reference("weapon.asniper.bodyaim.disablers")
cfg.wep_asniper_doubletap = mariolua_menu.Reference("weapon.asniper.doubletap")
cfg.wep_asniper_doubletap_mode = mariolua_menu.Reference("weapon.asniper.doubletap.mode")
cfg.wep_asniper_doubletap_hitchance = mariolua_menu.Reference("weapon.asniper.doubletap.hitchance")
cfg.wep_asniper_doubletap_fakelaglimit = mariolua_menu.Reference("weapon.asniper.doubletap.fakelaglimit")
cfg.wep_asniper_doubletap_quickstop = mariolua_menu.Reference("weapon.asniper.doubletap.quickstop")
cfg.wep_lmg_targethitbox = mariolua_menu.Reference("weapon.lmg.targethitbox")
cfg.wep_lmg_multipoint = mariolua_menu.Reference("weapon.lmg.multipoint")
cfg.wep_lmg_multipoint_scale = mariolua_menu.Reference("weapon.lmg.multipoint.scale")
cfg.wep_lmg_safepoint_prefer = mariolua_menu.Reference("weapon.lmg.safepoint.prefer")
cfg.wep_lmg_avoid = mariolua_menu.Reference("weapon.lmg.avoid")
cfg.wep_lmg_autofire = mariolua_menu.Reference("weapon.lmg.autofire")
cfg.wep_lmg_silentaim = mariolua_menu.Reference("weapon.lmg.silentaim")
cfg.wep_lmg_hitchance = mariolua_menu.Reference("weapon.lmg.hitchance")
cfg.wep_lmg_damage = mariolua_menu.Reference("weapon.lmg.damage")
cfg.wep_lmg_damage_override = mariolua_menu.Reference("weapon.lmg.damage.override")
cfg.wep_lmg_removerecoil = mariolua_menu.Reference("weapon.lmg.removerecoil")
cfg.wep_lmg_delayshot = mariolua_menu.Reference("weapon.lmg.delayshot")
cfg.wep_lmg_quickstop = mariolua_menu.Reference("weapon.lmg.quickstop")
cfg.wep_lmg_quickstop_options = mariolua_menu.Reference("weapon.lmg.quickstop.options")
cfg.wep_lmg_bodyaim_lethal = mariolua_menu.Reference("weapon.lmg.bodyaim.lethal")
cfg.wep_lmg_bodyaim_prefer = mariolua_menu.Reference("weapon.lmg.bodyaim.prefer")
cfg.wep_lmg_bodyaim_disablers = mariolua_menu.Reference("weapon.lmg.bodyaim.disablers")
cfg.wep_lmg_doubletap = mariolua_menu.Reference("weapon.lmg.doubletap")
cfg.wep_lmg_doubletap_mode = mariolua_menu.Reference("weapon.lmg.doubletap.mode")
cfg.wep_lmg_doubletap_hitchance = mariolua_menu.Reference("weapon.lmg.doubletap.hitchance")
cfg.wep_lmg_doubletap_fakelaglimit = mariolua_menu.Reference("weapon.lmg.doubletap.fakelaglimit")
cfg.wep_lmg_doubletap_quickstop = mariolua_menu.Reference("weapon.lmg.doubletap.quickstop")
function util_functions.lock_indicators()
    local et
    if
        globalvars.mouse1_key_state and
            extended_math.inBounds(
                er.pos.x,
                er.pos.y - 24,
                er.pos.x + er.width + globalvars.gs_menu_w,
                er.pos.y + globalvars.gs_menu_h - 24,
                10
            ) or
            globalvars.mouse1_key_state and et
     then
        et = true
        globalvars.block_moving_indicator = true
    elseif
        globalvars.mouse1_key_state and
            not extended_math.inBounds(
                er.pos.x,
                er.pos.y - 24,
                er.pos.x + er.width + globalvars.gs_menu_w,
                er.pos.y + globalvars.gs_menu_h - 24,
                10
            ) and
            et
     then
        globalvars.block_moving_indicator = true
    elseif
        not globalvars.mouse1_key_state and
            extended_math.inBounds(
                er.pos.x,
                er.pos.y - 24,
                er.pos.x + er.width + globalvars.gs_menu_w,
                er.pos.y + globalvars.gs_menu_h - 24,
                10
            )
     then
        et = false
    end
    if not globalvars.mouse1_key_state then
        globalvars.block_moving_indicator = false
    end
end
globalvars.enabled_resolver_modules = {
    "Damage",
    "Condition",
    "Animlayer Side",
    "Animlayer Low Delta",
    "Module Accuracy",
    "Bruteforce",
    "Low Delta",
    "Desync Detection"
}
globalvars.all_resolver_modules = {
    "Edge",
    "Edge2",
    "Trace",
    "Trace2",
    "Fraction",
    "Fraction2",
    "Predict",
    "Damage",
    "Damage2",
    "Simple",
    "Position",
    "Lby",
    "Animlayer Side"
}
globalvars.resolver_performance = nil
globalvars.resolver_modules = {
    ["Low"] = {"Fraction2", "Animlayer Side"},
    ["Medium"] = {"Damage2", "Animlayer Side"},
    ["High"] = {"Edge2", "Damage2", "Animlayer Side"},
    ["Very High"] = {"Edge2", "Damage", "Damage2", "Animlayer Side"}
}
for I = 1, #globalvars.enabled_resolver_modules do
    cfg.resolver_modules:GetByName(globalvars.enabled_resolver_modules[I]):SetValue(true)
end
function util_functions.disable_all_resolver_modules()
    for I = 1, #globalvars.all_resolver_modules do
        cfg.resolver_modules:GetByName(globalvars.all_resolver_modules[I]):SetValue(false)
    end
end
cfg.resolver_prefer_lby:SetValue(false)
cfg.resolver_prefer_animlayer:SetValue(true)
cfg.resolver_performance:SetValue("Medium")
function util_functions.set_resolver_modules()
    local eu = cfg.resolver_performance:GetItemName()
    if globalvars.resolver_performance == eu then
        return
    end
    util_functions.disable_all_resolver_modules()
    for I = 1, #globalvars.resolver_modules[eu] do
        cfg.resolver_modules:GetByName(globalvars.resolver_modules[eu][I]):SetValue(true)
    end
    if resolver.data ~= nil then
        resolver.data = {}
    end
    globalvars.resolver_performance = eu
end
globalvars.set_debug_object_vis = nil
function util_functions.InvisibleObjects()
    if not cfg.settings_debugmode:IsEnabled("Show Debug Gui Objects") and globalvars.set_debug_object_vis then
        cfg.resolver_modules:SetInvisible(true)
        cfg.resolver_predict_local_ticks:SetInvisible(true)
        cfg.resolver_predict_enemy_ticks:SetInvisible(true)
        cfg.resolver_yawlimit:SetInvisible(true)
        cfg.resolver_debug_headline:SetInvisible(true)
        cfg.resolver_fdheadline:SetInvisible(true)
        cfg.resolver_fakeduck_ticks:SetInvisible(true)
        cfg.resolver_fakeduck_lby:SetInvisible(true)
        cfg.resolver_prefer_lby:SetInvisible(true)
        cfg.resolver_prefer_animlayer:SetInvisible(true)
        globalvars.set_debug_object_vis = false
    elseif cfg.settings_debugmode:IsEnabled("Show Debug Gui Objects") and not globalvars.set_debug_object_vis then
        cfg.resolver_modules:SetInvisible(false)
        cfg.resolver_predict_local_ticks:SetInvisible(false)
        cfg.resolver_predict_enemy_ticks:SetInvisible(false)
        cfg.resolver_yawlimit:SetInvisible(false)
        cfg.resolver_debug_headline:SetInvisible(false)
        cfg.resolver_fdheadline:SetInvisible(false)
        cfg.resolver_fakeduck_ticks:SetInvisible(false)
        cfg.resolver_fakeduck_lby:SetInvisible(false)
        cfg.resolver_prefer_lby:SetInvisible(false)
        cfg.resolver_prefer_animlayer:SetInvisible(false)
        globalvars.set_debug_object_vis = true
    end
end
globalvars.set_wep_tab = nil
function util_functions.handle_weapon_tabs()
    if ui.is_menu_open() and not globalvars.set_wep_tab then
        local ev = functions.GetWeapon(local_player)
        if entity.is_alive(local_player) and ev then
            for I = 1, #globalvars.wepcfg_wepgroups do
                if extended_table.Contains(globalvars.wepcfg_wepgroups[I], ev) then
                    globalvars.wep_tab = I
                    globalvars.current_wep_tab = globalvars.weapon_tabs[I]
                end
            end
        end
        globalvars.set_wep_tab = true
    elseif globalvars.set_wep_tab and not ui.is_menu_open() then
        globalvars.set_wep_tab = false
    end
end
function util_functions.handleWindow()
    mariolua.debug.draw["in_bounds"] = cfg.settings_debugmode:IsEnabled("Show In Bounds")
    mariolua.debug.draw["event_infos"] = cfg.settings_debugmode:IsEnabled("Event Infos")
    mariolua.debug.draw["player_infos"] = cfg.settings_debugmode:IsEnabled("Player Infos")
    mariolua.debug.draw["dmg_trace"] = cfg.settings_debugmode:IsEnabled("Side Tracing")
    mariolua.debug.draw["player_prediction"] = cfg.settings_debugmode:IsEnabled("Player Prediction")
    mariolua.debug.print = cfg.settings_debugmode:IsEnabled("Logs")
    if globalvars.used_Object == nil then
        globalvars.used_Object = ""
    end
    if not globalvars.mouse1_key_state and globalvars.used_Object ~= "" then
        mariolua.log(globalvars.used_Object .. " did not reset variable back!")
        globalvars.used_Object = ""
    end
    if globalvars.used_Object ~= "" and globalvars.used_Object ~= " " then
        mariolua.log("Current used object: " .. globalvars.used_Object)
    end
    if ui.is_menu_open() and not globalvars.is_window_hidden then
        if globalvars.used_Object == "" or globalvars.used_Object == " " then
            util_functions.lock_indicators()
        end
        window_w, window_h = er:GetWindowSize()
        if
            (window_w ~= globalvars.gs_menu_x - 510 or window_y ~= globalvars.gs_menu_y + 24) and
                not globalvars.moving_window
         then
            er:SetWindowPos(globalvars.gs_menu_x - 510, globalvars.gs_menu_y + 24)
        end
        if window_h ~= globalvars.gs_menu_h - 28 then
            er:SetWindowSize(window_w, globalvars.gs_menu_h - 28)
        end
        er:SetActive(true)
    elseif not globalvars.block_moving_indicator then
        globalvars.block_moving_indicator = true
        er:SetActive(false)
    end
    util_functions.InvisibleObjects()
end
local ew, ex = ui.reference("Visuals", "Effects", "Force third person (alive)")
local _, eF = ui.reference("AA", "Other", "Slow motion")
globalvars.indicator_text_font =
    extended_renderer.create_font(cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text, 25, 800, 0)
globalvars.indicator_arrow_font =
    extended_renderer.create_font(cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text, 20, 20, 0)
globalvars.IndicateBodyYaw_Font =
    extended_renderer.create_font(cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text, 40, 40, 0)
local indicator_table = {
    {
        name = "Rage Trigger",
        short_name = "RT",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "RT"
                else
                    eL = "Rage Trigger"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if
                    cfg.aimbot_enable:GetValue() and
                        (cfg.aimbot_mode:GetItemName() == "Toggle" and cfg.aimbot_key:IsKeyToggle() or
                            cfg.aimbot_mode:GetItemName() == "Hold" and cfg.aimbot_key:IsKeyDown())
                 then
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            eL
                        )
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                else
                    if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, eK, eL)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    eK,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                255,
                                255,
                                eK,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
            end
        end
    },
    {
        name = "Auto Wall",
        short_name = "AW",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "AW"
                else
                    eL = "Auto Wall"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if ui.get(globalvars.ref.rage.autowall) then
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            eL
                        )
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                else
                    if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, eK, eL)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    eK,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                255,
                                255,
                                eK,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
            end
        end
    },
    {
        name = "Force Body Aim",
        short_name = "FB",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "FB"
                else
                    eL = "Force Body Aim"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if cfg.forcebaim_enable:GetValue() and ui.get(globalvars.ref.rage.force_baim_key) then
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            eL
                        )
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                else
                    if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, eK, eL)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    eK,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                255,
                                255,
                                eK,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
            end
        end
    },
    {
        name = "Min Dmg Override",
        short_name = "MIN DMG",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "MIN DMG"
                else
                    eL = "Min Dmg Override"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if
                    cfg.dmg_override_enable:GetValue() and
                        (cfg.dmg_override_mode:GetItemName() == "Toggle" and cfg.dmg_override_key:IsKeyToggle() or
                            cfg.dmg_override_mode:GetItemName() == "Hold" and cfg.dmg_override_key:IsKeyDown())
                 then
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            eL
                        )
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                else
                    if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, eK, eL)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    eK,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                255,
                                255,
                                eK,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
            end
        end
    },
    {
        name = "Force On Shot",
        short_name = "FOS",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "FOS"
                else
                    eL = "Force On Shot"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if cfg.aimbot_wait_onshot:GetValue() and globalvars.wait_onshot_enabled then
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            eL
                        )
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                else
                    if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, eK, eL)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    eK,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                255,
                                255,
                                eK,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
            end
        end
    },
    {
        name = "Anti-Aim",
        short_name = "Anti-Aim",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL, eM, eN
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "AA"
                    eM = "F-AA"
                    eN = "M-AA"
                else
                    eL = "Anti-Aim"
                    eM = "Freestanding"
                    eN = "Manual"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if not ui.get(globalvars.ref.aa.enabled) or globalvars.antiaim.state == "off" then
                    local eO, eP =
                        extended_renderer.get_text_size(
                        globalvars.indicator_text_font,
                        eL .. " [Off]",
                        cfg.indicator_extras:IsEnabled("Draw Shadow")
                    )
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(255, 255, 255, eK, eL .. " [Off]")
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                eO + 22,
                                aw + 20,
                                eK,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            255,
                            255,
                            255,
                            eK,
                            globalvars.indicator_text_font,
                            eL .. " [Off]",
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                elseif cfg.antiaim_freestanding:GetItemName() ~= "Manual" then
                    if not (local_player == nil or entity.get_prop(local_player, "m_lifeState") ~= 0) then
                        local eQ = entity.get_prop(local_player, "m_flPoseParameter", 11)
                        if eQ ~= nil then
                            eQ = eQ * 120 - 60
                            if eQ >= 0 then
                                globalvars.antiaim.real = "right"
                            else
                                globalvars.antiaim.real = "left"
                            end
                        end
                    end
                    local eO, eP =
                        extended_renderer.get_text_size(
                        globalvars.indicator_text_font,
                        eM .. " [L]",
                        cfg.indicator_extras:IsEnabled("Draw Shadow")
                    )
                    if globalvars.antiaim.real == "left" then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(124, 195, 13, db, eM .. " [L]")
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    eO + 22,
                                    aw + 20,
                                    db,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                124,
                                195,
                                13,
                                db,
                                globalvars.indicator_text_font,
                                eM .. " [L]",
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    elseif globalvars.antiaim.real == "right" then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 0, 0, db, eM .. " [R]")
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    eO + 22,
                                    aw + 20,
                                    db,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                0,
                                0,
                                db,
                                globalvars.indicator_text_font,
                                eM .. " [R]",
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    else
                        local eO, eP = extended_renderer.get_text_size(globalvars.indicator_text_font, eM)
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, db, eM)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    eO + 22,
                                    aw + 20,
                                    eK,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(a9, aa, 255, 255, 255, db, globalvars.indicator_text_font, eM)
                        end
                    end
                elseif cfg.antiaim_freestanding:GetItemName() == "Manual" then
                    local eO, eP =
                        extended_renderer.get_text_size(
                        globalvars.indicator_text_font,
                        eN .. " [L]",
                        cfg.indicator_extras:IsEnabled("Draw Shadow")
                    )
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        if ui.get(globalvars.ref.aa.body_yaw[2]) < 0 then
                            renderer.indicator(124, 195, 13, db, eN .. " [L]")
                        else
                            renderer.indicator(255, 0, 0, db, eN .. " [R]")
                        end
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                eO + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        if ui.get(globalvars.ref.aa.body_yaw[2]) < 0 then
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                124,
                                195,
                                13,
                                db,
                                globalvars.indicator_text_font,
                                eN .. " [L]",
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        else
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                0,
                                0,
                                db,
                                globalvars.indicator_text_font,
                                eN .. " [R]",
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
            end
        end
    },
    {
        name = "Anti-Aim Arrows",
        short_name = "Anti-Aim Arrows",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK, eR, eS
                local eT =
                    extended_math.no_decimals(
                    cfg.indicator_arrow_spacing:GetValue() + cfg.indicator_arrow_size:GetValue() / 10
                )
                local eO, eP = extended_renderer.get_text_size(globalvars.indicator_arrow_font, eS)
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                local eU = cfg.indicator_arrow_style:GetItemName()
                if eU == ch["left_1"] .. " " .. ch["right_1"] then
                    eR = ch["left_1"]
                    eS = ch["right_1"]
                elseif eU == ch["left_2"] .. " " .. ch["right_2"] then
                    eR = ch["left_2"]
                    eS = ch["right_2"]
                elseif eU == ch["left_3"] .. " " .. ch["right_3"] then
                    eR = ch["left_3"]
                    eS = ch["right_3"]
                elseif eU == ch["4"] .. " " .. ch["4"] then
                    eR = ch["4"]
                    eS = ch["4"]
                end
                if not entity.is_alive(local_player) and not ui.is_menu_open() or globalvars.antiaim.state == "off" then
                    return
                end
                if ui.get(globalvars.ref.aa.enabled) and local_player ~= nil then
                    if not (local_player == nil or entity.get_prop(local_player, "m_lifeState") ~= 0) then
                        local eQ = entity.get_prop(local_player, "m_flPoseParameter", 11)
                        if eQ ~= nil then
                            eQ = eQ * 120 - 60
                            if eQ >= 0 then
                                globalvars.antiaim.real = "right"
                            else
                                globalvars.antiaim.real = "left"
                            end
                        end
                    end
                    if globalvars.antiaim.real == "left" then
                        extended_renderer.draw_text(
                            a9 - eT,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_arrow_font,
                            eR,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                        extended_renderer.draw_text(
                            a9 + eT,
                            aa,
                            200,
                            200,
                            200,
                            db / 4,
                            globalvars.indicator_arrow_font,
                            eS,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    elseif globalvars.antiaim.real == "right" then
                        extended_renderer.draw_text(
                            a9 + eT,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_arrow_font,
                            eS,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                        extended_renderer.draw_text(
                            a9 - eT,
                            aa,
                            200,
                            200,
                            200,
                            db / 4,
                            globalvars.indicator_arrow_font,
                            eR,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                elseif ui.is_menu_open() and local_player == nil then
                    extended_renderer.draw_text(
                        a9 - eT,
                        aa,
                        255,
                        255,
                        255,
                        db,
                        globalvars.indicator_arrow_font,
                        eR,
                        cfg.indicator_extras:IsEnabled("Draw Shadow")
                    )
                    extended_renderer.draw_text(
                        a9 + eT,
                        aa,
                        255,
                        255,
                        255,
                        db,
                        globalvars.indicator_arrow_font,
                        eS,
                        cfg.indicator_extras:IsEnabled("Draw Shadow")
                    )
                end
            end
        end
    },
    {
        name = "Slow Walk",
        short_name = "SW",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "SW"
                else
                    eL = "Slow Walk"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if ui.get(eF) then
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            eL
                        )
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                else
                    if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, eK, eL)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    eK,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                255,
                                255,
                                eK,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
            end
        end
    },
    {
        name = "Resolver",
        short_name = "R",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "R"
                else
                    eL = "Resolver"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if ui.get(globalvars.ref.rage.resolver_ref) then
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            eL
                        )
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                else
                    if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, db, eL)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    eK,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                255,
                                255,
                                db,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
            end
        end
    },
    {
        name = "Fake Duck",
        short_name = "FD",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "FD"
                else
                    eL = "Fake Duck"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 40
                end
                if ui.get(globalvars.ref.rage.fakeduck) then
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            eL
                        )
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw * 2 + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            globalvars.indicator_color[1],
                            globalvars.indicator_color[2],
                            globalvars.indicator_color[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                else
                    if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(255, 255, 255, eK, eL)
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    db,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                255,
                                255,
                                255,
                                eK,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    end
                end
                local eV, eW = renderer.measure_text("c", eL)
                local eX, eY, eZ, e_ = a9, aa + eW + 3, eV, eW
                local G = db
                if ui.get(globalvars.ref.rage.fakeduck) then
                    local f0 = globals.tickcount()
                    local f1, f2, f3, f4, f5
                    if f0 ~= f1 then
                        f2 = f3
                        f3 = f4
                        f4 = entity.get_prop(entity.get_local_player(), "m_flDuckAmount") or 0
                        f1 = f0
                    end
                    if f5 == nil or f4 > f5 then
                        f5 = f4
                    elseif f3 ~= nil and f2 ~= nil and f3 > f4 and f3 > f2 then
                        f5 = f3
                    elseif f3 == 0 and f4 == 0 and f2 == 0 then
                        f5 = 0
                    end
                    local f6, f7, f8 =
                        math.max(0, math.min(255, globalvars.indicator_color[1] + 4)),
                        math.max(0, math.min(255, globalvars.indicator_color[2] + 4)),
                        math.max(0, math.min(255, globalvars.indicator_color[3] + 4))
                    local f9, fa, fb =
                        math.max(0, math.min(255, globalvars.indicator_color[1] - 40)),
                        math.max(0, math.min(255, globalvars.indicator_color[2] - 40)),
                        math.max(0, math.min(255, globalvars.indicator_color[3] - 40))
                    if duckammount == nil then
                        return
                    end
                    mariolua.log("Duck amount percent: " .. extended_math.percentof(f4, 1))
                    local fc = math.floor(eZ * math.max(0, math.min(1, f5)))
                    renderer.rectangle(eX, eY, eZ, e_, 14, 14, 14, db, eJ)
                    renderer.gradient(eX + 1, eY + 1, fc - 2, e_ - 2, f6, f7, f8, G * 0.25, f9, fa, fb, G * 0.25, false)
                    renderer.gradient(eX + 1, eY + 1, eZ * f4 - 2, e_ - 2, f6, f7, f8, G, f9, fa, fb, G, false)
                    local fd, fe =
                        renderer.measure_text(nil, extended_math.round(extended_math.percentof(f4, 1), 0) .. "%")
                    renderer.text(
                        a9 + cJ / 2 - fd / 2,
                        eY,
                        globalvars.indicator_color[1],
                        globalvars.indicator_color[2],
                        globalvars.indicator_color[3],
                        db,
                        nil,
                        0,
                        extended_math.round(extended_math.percentof(f4, 1), 0) .. "%"
                    )
                    renderer.gradient(
                        eX + 1 + math.max(0, fc - 2),
                        eY + 1,
                        math.min(eZ - 2, eZ - fc),
                        e_ - 2,
                        52,
                        52,
                        52,
                        G,
                        68,
                        68,
                        68,
                        G,
                        false
                    )
                end
            end
        end
    },
    {
        name = "Fov",
        short_name = "FOV",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "FOV"
                else
                    eL = "FOV"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                local ff = ui.get(globalvars.ref.rage.rage_fov)
                local eO, eP =
                    extended_renderer.get_text_size(
                    globalvars.indicator_text_font,
                    eL .. " " .. ff,
                    cfg.indicator_extras:IsEnabled("Draw Shadow")
                )
                if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                    renderer.indicator(
                        globalvars.indicator_color[1],
                        globalvars.indicator_color[2],
                        globalvars.indicator_color[3],
                        db,
                        eL .. " " .. ff
                    )
                else
                    if cfg.indicator_extras:IsEnabled("Draw Container") then
                        globalvars.indicator.container(
                            a9 - 10,
                            aa - 10,
                            eO + 22,
                            eP + 20,
                            db,
                            cfg.indicator_extras:IsEnabled("Rainbow Header")
                        )
                    end
                    extended_renderer.draw_text(
                        a9,
                        aa,
                        globalvars.indicator_color[1],
                        globalvars.indicator_color[2],
                        globalvars.indicator_color[3],
                        db,
                        globalvars.indicator_text_font,
                        eL .. " " .. ff,
                        cfg.indicator_extras:IsEnabled("Draw Shadow")
                    )
                end
            end
        end
    },
    {
        name = "Minimum Damage",
        short_name = "DMG",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "DMG"
                else
                    eL = "Min Dmg"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                local dmg = ui.get(globalvars.ref.min_dmg)
                local eO, eP =
                    extended_renderer.get_text_size(
                    globalvars.indicator_text_font,
                    eL .. " " .. dmg,
                    cfg.indicator_extras:IsEnabled("Draw Shadow")
                )
                if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                    renderer.indicator(
                        globalvars.indicator_color[1],
                        globalvars.indicator_color[2],
                        globalvars.indicator_color[3],
                        db,
                        eL .. " " .. dmg
                    )
                else
                    if cfg.indicator_extras:IsEnabled("Draw Container") then
                        globalvars.indicator.container(
                            a9 - 10,
                            aa - 10,
                            eO + 22,
                            eP + 20,
                            db,
                            cfg.indicator_extras:IsEnabled("Rainbow Header")
                        )
                    end
                    extended_renderer.draw_text(
                        a9,
                        aa,
                        globalvars.indicator_color[1],
                        globalvars.indicator_color[2],
                        globalvars.indicator_color[3],
                        db,
                        globalvars.indicator_text_font,
                        eL .. " " .. dmg,
                        cfg.indicator_extras:IsEnabled("Draw Shadow")
                    )
                end
            end
        end
    },
    {
        name = "Fake",
        short_name = "FAKE",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "FAKE"
                else
                    eL = "Fake"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if ui.get(globalvars.ref.aa.enabled) and local_player ~= nil then
                    local bn = {
                        255 - globalvars.indicator.real_yaw * 2.29824561404,
                        globalvars.indicator.real_yaw * 3.42105263158,
                        globalvars.indicator.real_yaw * 0.22807017543
                    }
                    if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                        renderer.indicator(bn[1], bn[2], bn[3], db, eL)
                    else
                        if cfg.indicator_extras:IsEnabled("Draw Container") then
                            globalvars.indicator.container(
                                a9 - 10,
                                aa - 10,
                                cJ + 22,
                                aw + 20,
                                db,
                                cfg.indicator_extras:IsEnabled("Rainbow Header")
                            )
                        end
                        extended_renderer.draw_text(
                            a9,
                            aa,
                            bn[1],
                            bn[2],
                            bn[3],
                            db,
                            globalvars.indicator_text_font,
                            eL,
                            cfg.indicator_extras:IsEnabled("Draw Shadow")
                        )
                    end
                end
            end
        end
    },
    {
        name = "Dormant Aimbot",
        short_name = "DA",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "DA"
                else
                    eL = "Dormant Aim"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if util_functions.dormant_aim ~= nil and cfg.dormant_enable:GetValue() then
                    if util_functions.dormant_aim.is_key then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(
                                globalvars.indicator_color[1],
                                globalvars.indicator_color[2],
                                globalvars.indicator_color[3],
                                db,
                                eL
                            )
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    db,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                globalvars.indicator_color[1],
                                globalvars.indicator_color[2],
                                globalvars.indicator_color[3],
                                db,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    else
                        if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                            if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                                renderer.indicator(255, 255, 255, eK, eL)
                            else
                                if cfg.indicator_extras:IsEnabled("Draw Container") then
                                    globalvars.indicator.container(
                                        a9 - 10,
                                        aa - 10,
                                        cJ + 22,
                                        aw + 20,
                                        eK,
                                        cfg.indicator_extras:IsEnabled("Rainbow Header")
                                    )
                                end
                                extended_renderer.draw_text(
                                    a9,
                                    aa,
                                    255,
                                    255,
                                    255,
                                    eK,
                                    globalvars.indicator_text_font,
                                    eL,
                                    cfg.indicator_extras:IsEnabled("Draw Shadow")
                                )
                            end
                        end
                    end
                end
            end
        end
    },
    {
        name = "E-Peek",
        short_name = "EP",
        draw = function(a9, aa, cJ, aw, eI, db, eJ)
            if eJ then
                local eK
                local eL
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eL = "EP"
                else
                    eL = "E-Peek"
                end
                if er:IsActive() or local_player ~= nil then
                    eK = db
                else
                    eK = 10
                end
                if util_functions.e_peek ~= nil then
                    if cfg.antiaim_epeek:GetValue() and globalvars.epeek_key_input then
                        if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                            renderer.indicator(
                                globalvars.indicator_color[1],
                                globalvars.indicator_color[2],
                                globalvars.indicator_color[3],
                                db,
                                eL
                            )
                        else
                            if cfg.indicator_extras:IsEnabled("Draw Container") then
                                globalvars.indicator.container(
                                    a9 - 10,
                                    aa - 10,
                                    cJ + 22,
                                    aw + 20,
                                    db,
                                    cfg.indicator_extras:IsEnabled("Rainbow Header")
                                )
                            end
                            extended_renderer.draw_text(
                                a9,
                                aa,
                                globalvars.indicator_color[1],
                                globalvars.indicator_color[2],
                                globalvars.indicator_color[3],
                                db,
                                globalvars.indicator_text_font,
                                eL,
                                cfg.indicator_extras:IsEnabled("Draw Shadow")
                            )
                        end
                    else
                        if not cfg.indicator_extras:IsEnabled("Draw While Active") or ui.is_menu_open() then
                            if cfg.indicator_extras:IsEnabled("Use Skeet Indicators") then
                                renderer.indicator(255, 255, 255, eK, eL)
                            else
                                if cfg.indicator_extras:IsEnabled("Draw Container") then
                                    globalvars.indicator.container(
                                        a9 - 10,
                                        aa - 10,
                                        cJ + 22,
                                        aw + 20,
                                        eK,
                                        cfg.indicator_extras:IsEnabled("Rainbow Header")
                                    )
                                end
                                extended_renderer.draw_text(
                                    a9,
                                    aa,
                                    255,
                                    255,
                                    255,
                                    eK,
                                    globalvars.indicator_text_font,
                                    eL,
                                    cfg.indicator_extras:IsEnabled("Draw Shadow")
                                )
                            end
                        end
                    end
                end
            end
        end
    }
}

globalvars.font_flags = {
    ["Italic"] = 1,
    ["Underline"] = 2,
    ["Strikeout"] = 4,
    ["Symbol"] = 8,
    ["Antialias"] = 10,
    ["Gaussianblur"] = 20,
    ["Rotary"] = 40,
    ["Droshadow"] = 80,
    ["Additive"] = 100,
    ["Outline"] = 200,
    ["Custom"] = 400,
    ["Bitmap"] = 800
}
globalvars.font_byte_flags = {
    FONTFLAG_NONE = 0x000,
    FONTFLAG_ITALIC = 0x001,
    FONTFLAG_UNDERLINE = 0x002,
    FONTFLAG_STRIKEOUT = 0x004,
    FONTFLAG_SYMBOL = 0x008,
    FONTFLAG_ANTIALIAS = 0x010,
    FONTFLAG_GAUSSIANBLUR = 0x020,
    FONTFLAG_ROTARY = 0x040,
    FONTFLAG_DROPSHADOW = 0x080,
    FONTFLAG_ADDITIVE = 0x100,
    FONTFLAG_OUTLINE = 0x200,
    FONTFLAG_CUSTOM = 0x400,
    FONTFLAG_BITMAP = 0x800
}
local fg
globalvars.cur_font_flags = 0
globalvars.indicator_color = globalvars.color

function util_functions.handleIndicators()
    local fh, fi
    local fj = cfg.indicator_container:GetItemName()
    local fk = cfg.indicator_size:GetValue()
    local fl = cfg.indicator_alpha:GetValue()
    local fm = cfg.indicator_arrow_size:GetValue()
    local fn = cfg.indicator_arrow_alpha:GetValue()
    local fo = cfg.indicator_color:GetValue()
    globalvars.indicator_color = fo

    if (not entity.is_alive(local_player)) then
        return
    end

    if cfg.indicator_extras:IsEnabled("Hide Skeet Indicators") then
        for I = 1, 50 do
            renderer.indicator(0, 0, 0, 0, " ")
        end
    elseif extended_math.no_decimals(cfg.indicator_skeet_pos:GetValue()) > 0 then
        for I = 0, extended_math.no_decimals(cfg.indicator_skeet_pos:GetValue()) do
            renderer.indicator(0, 0, 0, 0, " ")
        end
    end
    if cfg.indicator_size == nil then
        fh = 14
    else
        fh = extended_math.no_decimals(fk)
    end
    if cfg.indicator_arrow_size == nil then
        fi = 20
    else
        fi = extended_math.no_decimals(cfg.indicator_arrow_size:GetValue())
    end
    if cfg.indicator_extras:IsEnabled("Antialias") and globalvars.cur_font_flags ~= {16, 128} then
        globalvars.cur_font_flags = {16, 128}
    elseif globalvars.cur_font_flags ~= 0 then
        globalvars.cur_font_flags = 0
    end
    if fj == "Gamesense" then
        globalvars.indicator.container = extended_renderer.gs_indicator_container
    elseif fj == "Aimware" then
        globalvars.indicator.container = extended_renderer.aimware_container
    elseif fj == "Shadow" then
        globalvars.indicator.container = extended_renderer.shadow_container
    elseif globalvars.indicator.container == nil then
        globalvars.indicator.container = extended_renderer.shadow_container
    end
    if
        cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text ~= nil and
            globalvars.indicator.old_font ~= cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text
     then
        globalvars.indicator.old_font = cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text
        globalvars.indicator_text_font =
            extended_renderer.create_font(
            cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text,
            fh,
            800,
            globalvars.cur_font_flags
        )
        globalvars.indicator_arrow_font =
            extended_renderer.create_font(
            cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text,
            fi,
            800,
            globalvars.cur_font_flags
        )
    end
    if fh ~= globalvars.indicator.old_size then
        globalvars.indicator.old_size = fh
        globalvars.indicator.old_font = cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text
        globalvars.indicator_text_font =
            extended_renderer.create_font(
            cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text,
            fh,
            800,
            globalvars.cur_font_flags
        )
    end
    if fi ~= globalvars.indicator.old_arrow_size then
        globalvars.indicator.old_arrow_size = fi
        globalvars.indicator.old_font = cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text
        globalvars.indicator_arrow_font =
            extended_renderer.create_font(
            cfg.indicator_fontitems.items[cfg.indicator_fontitems.enabledItem].text,
            fi,
            800,
            globalvars.cur_font_flags
        )
    end
    if fg ~= true then
        for I = 1, #indicator_table do
            if indicator_table[I] == nil then
                return
            end
            local dw = indicator_table[I]
            if I <= #indicator_table then
                cfg.indicator_itemlist:AddItem("indicator", dw.name)
                mariolua.log("Added Item '" .. dw.name .. "' to Multibox")
                local eO, eP
                if cfg.indicator_extras:IsEnabled("Short Text") then
                    eO, eP = extended_renderer.get_text_size(globalvars.indicator_text_font, dw.short_name)
                else
                    eO, eP = extended_renderer.get_text_size(globalvars.indicator_text_font, dw.name)
                end
                cfg.indicator:AddItem(dw.name, 50, 300 + eP + 50 * I, eO, eP)
                cfg.indicator.items[I].func = dw.draw
                if I == #indicator_table and fg ~= true then
                    fg = true
                end
            end
        end
    elseif fg ~= true then
        fg = true
    end
    for I = 1, #cfg.indicator.items do
        local dz = cfg.indicator.items[I]
        local fp = cfg.indicator_itemlist:GetByIndex(I)
        local dw = indicator_table[I]
        local eO, eP
        if cfg.indicator_extras:IsEnabled("Short Text") then
            eO, eP = extended_renderer.get_text_size(globalvars.indicator_text_font, dw.short_name)
        else
            eO, eP = extended_renderer.get_text_size(globalvars.indicator_text_font, dw.name)
        end
        if dz.func ~= nil and type(dz.func) == "function" then
            dz.width = eO
            dz.height = eP
            dz.pos.x = extended_math.clamp(dz.pos.x, 0, globalvars.screen_size.w - eO / 4)
            dz.pos.y = extended_math.clamp(dz.pos.y, 0, globalvars.screen_size.h - eP / 4)
            if dw.name == "Anti-Aim Arrows" then
                dz.width, dz.height = extended_renderer.get_text_size(globalvars.indicator_arrow_font, ch["left_3"])
                dz.func(dz.pos.x, dz.pos.y, dz.width, dz.height, fm, fn, fp)
            else
                dz.func(dz.pos.x, dz.pos.y, dz.width, dz.height, fk, fl, fp)
            end
            dz:Update()
        end
    end
end
function util_functions.center_arrow_indicator()
    local fq, fr = globalvars.screen_size.w / 2, globalvars.screen_size.h / 2
    local ai = mariolua_menu.Reference("indicator_item_Anti-AimArrows").height
    local ah = mariolua_menu.Reference("indicator_item_Anti-AimArrows").width
    mariolua_menu.Reference("indicator_item_Anti-AimArrows").pos.x = fq - ah / 2
    mariolua_menu.Reference("indicator_item_Anti-AimArrows").pos.y = fr - ai / 2
    return
end
function util_functions.SetWepCfg(fs)
    local ft = fs["target hitbox"]:GetValue()
    local fu = fs["multi-point"]:GetValue()
    local fv = fs["multi-point scale"]:GetValue()
    local fw = fs["prefer safe point"]:GetValue()
    local fx = fs["avoid unsafe hitboxes"]:GetValue()
    local fy = fs["automatic fire"]:GetValue()
    local fz = fs["silent aim"]:GetValue()
    local fA = fs["minimum hit chance"]:GetValue()
    local fB = fs["minimum damage"]:GetValue()
    local fC = fs["minimum damage override"]:GetValue()
    local fD = fs["remove recoil"]:GetValue()
    local fE = fs["automatic scope"]:GetValue()
    local fF = fs["delay shot"]:GetValue()
    local fG = fs["quick stop"]:GetValue()
    local fH = fs["quick stop options"]:GetValue()
    local fI = fs["body aim if lethal"]:GetValue()
    local fJ = fs["prefer body aim"]:GetValue()
    local fK = fs["prefer body aim disablers"]:GetValue()
    local fL = fs["double tap"]:GetValue()
    local fM = fs["double tap mode"]:GetItemName()
    local fN = fs["double tap hit chance"]:GetValue()
    local fO = fs["double tap fake lag limit"]:GetValue()
    local fP = fs["double tap quick stop"]:GetValue()
    if #ft > 0 and ft ~= nil then
        ui.set(globalvars.ref.targethitbox, ft)
    end
    if #fu > 0 and fu ~= nil then
        ui.set(globalvars.ref.multipoint, fu)
    end
    ui.set(globalvars.ref.multipoint_scale, fv)
    ui.set(globalvars.ref.prefer_safepoint, fw)
    ui.set(globalvars.ref.avoid_hitbox, fx)
    ui.set(globalvars.ref.automatic_fire, fy)
    ui.set(globalvars.ref.silentaim, fz)
    ui.set(globalvars.ref.hitchance, fA)
    if cfg.dmg_override_enable:GetValue() then
        if cfg.dmg_override_mode:GetItemName() == "Toggle" then
            if cfg.dmg_override_key:IsKeyToggle() then
                ui.set(globalvars.ref.min_dmg, fC)
            else
                ui.set(globalvars.ref.min_dmg, fB)
            end
        elseif cfg.dmg_override_mode:GetItemName() == "Hold" then
            if cfg.dmg_override_key:IsKeyDown() then
                ui.set(globalvars.ref.min_dmg, fC)
            elseif not cfg.dmg_override_key:IsKeyDown() then
                ui.set(globalvars.ref.min_dmg, fB)
            end
        end
    else
        ui.set(globalvars.ref.min_dmg, fB)
    end
    ui.set(globalvars.ref.removerecoil, fD)
    ui.set(globalvars.ref.automatic_scope, fE)
    ui.set(globalvars.ref.rage.delay_shot, fF)
    ui.set(globalvars.ref.quickstop, fG)
    if #fH > 0 and fH ~= nil then
        ui.set(globalvars.ref.quickstop_options, fH)
    end
    if util_functions.baim_lethal ~= nil then
        util_functions.baim_lethal.enabled = fI
    elseif fI then
        util_functions.baim_lethal = mariolua.load_module("bodyaim_if_lethal")
    end
    ui.set(globalvars.ref.prefer_bodyaim, fJ)
    if #fK > 0 and fK ~= nil then
        ui.set(globalvars.ref.prefer_bodyaim_disablers, fK)
    end
    ui.set(globalvars.ref.double_tap[1], fL)
    ui.set(globalvars.ref.double_tap_mode, fM)
    ui.set(globalvars.ref.double_tap_hitchance, fN)
    ui.set(globalvars.ref.double_tap_fakelaglimit, fO)
    if #fP > 0 and fP ~= nil then
        ui.set(globalvars.ref.double_tap_quick_stop, fP)
    end
end
function util_functions.AdaptiveWepCfg()
    if not cfg.settings_adaptivewep:GetValue() then
        return
    end
    if local_player == nil or not entity.is_alive(local_player) then
        return
    end
    local ev = functions.GetWeapon(local_player)
    local fQ = functions.GetWeaponGroup(local_player)
    if fQ == nil then
        return
    end
    local fR = cfg.adaptive_wep_cfg[fQ]
    util_functions.SetWepCfg(fR)
end
local fS = {
    scr = {w = globalvars.screen_size.w, h = globalvars.screen_size.h},
    widths = {
        ["MarioLua"] = 50,
        ["Watermark"] = 65,
        ["Username"] = 10,
        ["Time"] = 40,
        ["Velocity"] = 50,
        ["KDR"] = 50,
        ["FPS"] = 40,
        ["Latency"] = 35,
        ["Server tickrate"] = 45
    }
}
local fT = mariolua_menu.CObject:new("cobject")
fT:AddItem("watermark", fS.scr.w - 3, 9, 3, 18)
function util_functions.Watermark()
    local ah, ai = fT.items[1].width, fT.items[1].height
    local fU = cfg.visuals_watermark_items:GetValue()
    fU[0] = "MarioLua"
    local fV = entity.get_all("CCSPlayerResource")[1]
    local fW = math.min(999, client.latency() * 1000)
    local ce = globalvars.AccumulateFps()
    local ap, av, a5, G = globalvars.color[1], globalvars.color[2], globalvars.color[3], globalvars.color[4]
    local aU = mariolua.userdata.name or cvar.name:get_string()
    local bT, bU = entity.get_prop(local_player, "m_vecVelocity")
    local K, L, M, fX = client.system_time()
    local fY = "AM"
    local fZ
    local f_
    local g0 = {get_kills = 0, get_deaths = 0}
    if local_player ~= nil then
        g0 = {
            get_kills = entity.get_prop(fV, "m_iKills", local_player),
            get_deaths = entity.get_prop(fV, "m_iDeaths", local_player)
        }
    end
    if string.len(aU) > 20 then
        aU = string.sub(cvar.name:get_string(), 0, 21)
    end
    if K > 12 then
        K = K - 12
        fY = "PM"
    end
    if L < 10 then
        L = "0" .. L
    end
    g0.get_kills = g0.get_kills or 0
    g0.get_deaths = g0.get_deaths or 0
    if g0.get_deaths ~= 0 then
        local g1 = g0.get_kills / g0.get_deaths
        f_ = extended_math.round(g1, 2)
    elseif g0.get_kills ~= 0 then
        f_ = g0.get_kills
    else
        f_ = 0
    end
    fZ = bT and extended_math.toInteger(math.min(10000, math.sqrt(bT * bT + bU * bU))) or 0
    for I = 0, #fU do
        local g2 = fU[I]
        if fU[0] == "MarioLua" then
            fS.widths[g2] = renderer.measure_text("", "MarioLua") + 9
        end
        if g2 == "FPS" then
            fS.widths[g2] = renderer.measure_text("", ce) + renderer.measure_text("-", "  FPS") + 9
        end
        if g2 == "Latency" then
            fS.widths[g2] =
                renderer.measure_text("", extended_math.toInteger(fW)) + renderer.measure_text("-", "  PING") + 9
        end
        if g2 == "KDR" then
            fS.widths[g2] = renderer.measure_text("", f_) + renderer.measure_text("-", "  KDR") + 9
        end
        if g2 == "Velocity" then
            fS.widths[g2] = renderer.measure_text("", fZ) + renderer.measure_text("-", "  U/T") + 9
        end
        if g2 == "Username" then
            fS.widths[g2] = renderer.measure_text("", aU) + 9
        end
        if g2 == "Time" then
            fS.widths[g2] = renderer.measure_text("", K .. ":" .. L .. fY) + 9
        end
        if g2 == "Server tickrate" then
            fS.widths[g2] =
                renderer.measure_text("", 1 / globals.tickinterval()) + renderer.measure_text("-", "  TICKS") + 9
        end
        if fS.widths[g2] ~= nil then
            ah = ah + fS.widths[g2]
        end
    end
    local g3 = {x = fT.items[1].pos.x - ah, y = fT.items[1].pos.y + 3}
    if cfg.visuals_watermark:GetValue() then
        if cfg.visuals_watermark_style:GetItemName() == "Minimal" then
            extended_renderer.watermark_container(fT.items[1].pos.x - ah - 5, fT.items[1].pos.y, ah, ai)
        elseif cfg.visuals_watermark_style:GetItemName() == "Gamesense" then
            extended_renderer.gs_container(fT.items[1].pos.x - ah - 5, fT.items[1].pos.y, ah, ai, 255, true, true)
        elseif cfg.visuals_watermark_style:GetItemName() == "Aimware" then
            extended_renderer.aimware_container(fT.items[1].pos.x - ah - 5, fT.items[1].pos.y, ah, ai)
        end
        for I = 0, #fU do
            local g2 = fU[I]
            if g2 == "MarioLua" then
                local g4 = {ap, av, a5, a = 255}
                renderer.text(g3.x, g3.y, 66, 135, 245, 255, "", 0, "Mario")
                renderer.text(g3.x + renderer.measure_text("", "Mario"), g3.y, 255, 255, 255, 255, "", 0, "Lua")
            elseif g2 == "FPS" then
                local g5 = {ap, av, a5, a = 255}
                if ce < 1 / globals.tickinterval() then
                    g5.r = 255
                    g5.g = 50
                    g5.b = 50
                else
                    g5.r = ap
                    g5.g = av
                    g5.b = a5
                end
                renderer.text(g3.x, g3.y, 255, 255, 255, 255, "", 0, ce)
                renderer.text(g3.x + renderer.measure_text("", ce), g3.y + 3, g5.r, g5.g, g5.b, g5.a, "-", 0, "FPS")
            elseif g2 == "Username" then
                renderer.text(g3.x, g3.y, 255, 255, 255, 255, "", 0, aU)
            elseif g2 == "Watermark" then
                renderer.text(g3.x, g3.y - 1, 255, 255, 255, 255, "", 0, "game")
                renderer.text(g3.x + renderer.measure_text("", "game"), g3.y - 1, ap, av, a5, 255, "", 0, "sense")
            elseif g2 == "Latency" then
                local g6 = {ap, av, a5, a = 255}
                if fW > 60 then
                    g6.r = 255
                    g6.g = 50
                    g6.b = 50
                else
                    g6.r = ap
                    g6.g = av
                    g6.b = a5
                end
                renderer.text(g3.x, g3.y, 255, 255, 255, 255, "", 0, extended_math.toInteger(fW))
                renderer.text(
                    g3.x + renderer.measure_text("", extended_math.toInteger(fW)),
                    g3.y + 3,
                    g6.r,
                    g6.g,
                    g6.b,
                    g6.a,
                    "-",
                    0,
                    " PING"
                )
            elseif g2 == "KDR" then
                local g7 = {ap, av, a5, a = 255}
                g0.get_kills = g0.get_kills or 0
                g0.get_deaths = g0.get_deaths or 0
                if g0.get_kills < g0.get_deaths then
                    g7.r = 255
                    g7.g = 50
                    g7.b = 50
                else
                    g7.r = ap
                    g7.g = av
                    g7.b = a5
                end
                renderer.text(g3.x, g3.y, 255, 255, 255, 255, "", 0, f_)
                renderer.text(g3.x + renderer.measure_text("", f_), g3.y + 3, g7.r, g7.g, g7.b, g7.a, "-", 0, " KDR")
            elseif g2 == "Velocity" then
                renderer.text(g3.x, g3.y, 255, 255, 255, 255, "", 0, fZ)
                renderer.text(g3.x + renderer.measure_text("", fZ), g3.y + 3, ap, av, a5, 255, "-", 0, " U/T")
            elseif g2 == "Time" then
                renderer.text(g3.x, g3.y, 255, 255, 255, 255, "", 0, K)
                renderer.text(g3.x + renderer.measure_text("", K), g3.y, 255, 255, 255, 255, "", 0, ":")
                renderer.text(g3.x + renderer.measure_text("", K .. ":"), g3.y, 255, 255, 255, 255, "", 0, L)
                renderer.text(g3.x + renderer.measure_text("", K .. ":" .. L), g3.y + 3, ap, av, a5, 255, "-", 0, fY)
            elseif g2 == "Server tickrate" then
                renderer.text(g3.x, g3.y, 255, 255, 255, 255, "", 0, 1 / globals.tickinterval())
                renderer.text(
                    g3.x + renderer.measure_text("", 1 / globals.tickinterval()),
                    g3.y + 3,
                    ap,
                    av,
                    a5,
                    255,
                    "-",
                    0,
                    " TICKS"
                )
            end
            if fS.widths[g2] ~= nil then
                g3.x = g3.x + fS.widths[g2]
            end
            if #fU > I then
                renderer.text(g3.x - 10, g3.y - 1, 255, 255, 255, 255, "", 0, " | ")
            end
        end
        fT.items[1]:Update()
    end
    if #fU == 0 then
        return
    end
end

function util_functions.GetNearestEnemy()
    local g8 = entity.get_players(true)

    if #g8 ~= 0 then
        local g9, ga, gb = client.eye_position()
        local gc, gd = client.camera_angles()
        globalvars.closest_enemy = nil
        globalvars.closest_distance = 999999999

        for I = 1, #g8 do
            local ge = g8[I]
            local gf, gg, gh = entity.hitbox_position(ge, 0)
            local a9 = gf - g9
            local aa = gg - ga
            local b3 = gh - gb
            local gi = math.atan2(aa, a9) * 180 / math.pi
            local gj = -(math.atan2(b3, math.sqrt(math.pow(a9, 2) + math.pow(aa, 2))) * 180 / math.pi)
            local gk = math.abs(gd % 360 - gi % 360) % 360
            local gl = math.abs(gc - gj) % 360

            if gk > 180 then
                gk = 360 - gk
            end

            local gm = math.sqrt(math.pow(gk, 2) + math.pow(gl, 2))

            if globalvars.closest_distance > gm then
                globalvars.closest_distance = gm
                globalvars.closest_enemy = ge
            end
        end

        if globalvars.closest_enemy ~= nil then
            return globalvars.closest_enemy, globalvars.closest_distance
        end
    end

    return nil, nil
end

function util_functions.DynamicFOV_Draw()
    if cfg.dyn_fov_mode:GetItemName() ~= "Off" and cfg.dyn_fov_draw:GetValue() then
        local ap, av, a5, G = 255, 255, 255, 150
        local gn = ui.get(globalvars.ref.rage.rage_fov) * 10
        renderer.circle(globalvars.screen_size.w / 2, globalvars.screen_size.h / 2, ap, av, a5, G, gn, 270, 1.0)
    end
end

function util_functions.DynamicFOV()
    if cfg.dyn_fov_mode:GetItemName() ~= "Off" then
        local go = ui.get(globalvars.ref.rage.rage_fov)
        local gp = cfg.dyn_fov_min:GetValue()
        local gq = cfg.dyn_fov_max:GetValue()
        local g9, ga, gb = client.eye_position()
        globalvars.closest_enemy, globalvars.closest_distance = util_functions.GetNearestEnemy()

        if globalvars.closest_enemy ~= nil then
            local gr, gs, gt = entity.hitbox_position(globalvars.closest_enemy, 0)
            local gu = math.sqrt(math.pow(g9 - gr, 2) + math.pow(ga - gs, 2) + math.pow(gb - gt, 2))

            if cfg.dyn_fov_mode:GetItemName() == "Static" then
                globalvars.dynamicfov_new_fov = gq - (gq - gp) * (gu - 250) / 1000
            elseif cfg.dyn_fov_mode:GetItemName() == "Automatic" then
                globalvars.dynamicfov_new_fov = 3800 / gu * cfg.dyn_fov_factor:GetValue() * 0.01
            end

            if globalvars.dynamicfov_new_fov > gq then
                globalvars.dynamicfov_new_fov = gq
            elseif globalvars.dynamicfov_new_fov < gp then
                globalvars.dynamicfov_new_fov = gp
            end
        else
            globalvars.dynamicfov_new_fov = gp
        end

        globalvars.dynamicfov_new_fov = math.floor(globalvars.dynamicfov_new_fov)

        if globalvars.dynamicfov_new_fov ~= go then
            ui.set(globalvars.ref.rage.rage_fov, globalvars.dynamicfov_new_fov)
        end
    end
end

globalvars.name_backup = cvar.name:get_string()
globalvars.namespam_names = mariolua_menu.SaveVar:new("misc.namespam.names", {"MarioLua > All", "MarioLua > Luigi"})

function util_functions.LoadNameSpamItems()
    cfg.misc_namespam_name.items = {}
    cfg.misc_namespam_name:SetItems(globalvars.namespam_names:GetValue())
    mariolua.log("Loaded Name Spam List")
end

function util_functions.NameSpamAdd()
    globalvars.should_addnamespam = false

    if extended_table.Contains(cfg.misc_namespam_name.item_list, globalvars.clipboard) then
        return
    end

    cfg.misc_namespam_name:AddItem(globalvars.clipboard)
    globalvars.namespam_names:SetValue(extended_table.table_to_string3(cfg.misc_namespam_name:GetItems()))
    mariolua.log("Added " .. globalvars.clipboard .. " to Name Spam List")
end

function util_functions.NameSpamRemove()
    globalvars.should_removenamespam = false

    for I = 1, #cfg.misc_namespam_name.items do
        if cfg.misc_namespam_name.items[I] == nil then
            return
        end

        local gv = cfg.misc_namespam_name:GetItemName()

        if cfg.misc_namespam_name.items[I].text == gv then
            table.remove(cfg.misc_namespam_name.items, I)
            table.remove(cfg.misc_namespam_name.item_list, I)
            globalvars.namespam_names:SetValue(extended_table.table_to_string3(cfg.misc_namespam_name:GetItems()))
            mariolua.log("Removed " .. gv .. " from Name Spam List")
        end
    end
end

globalvars.spam_counter = 0
globalvars.should_namespam = false

client.set_event_callback(
    "paint",
    function()
        local gw = cvar.name:get_string()
        local gx = cfg.misc_namespam_name:GetItemName()

        if type(cfg.misc_namespam_name:GetItemName()) == "boolean" then
            return
        end

        if #gx < 2 then
            return
        end

        local gy = cfg.misc_namespam_restore:GetValue()

        if globalvars.should_namespam then
            if globalvars.spam_counter >= 0 and globalvars.spam_counter < 20 then
                cvar.name:invoke_callback()
                cvar.name:set_string(gx)
                globalvars.spam_counter = globalvars.spam_counter + 1
            elseif globalvars.spam_counter >= 20 and globalvars.spam_counter < 40 then
                cvar.name:invoke_callback()
                cvar.name:set_string(gx .. " \x00")
                globalvars.spam_counter = globalvars.spam_counter + 1
            elseif globalvars.spam_counter >= 40 and globalvars.spam_counter < 60 then
                cvar.name:invoke_callback()
                cvar.name:set_string(gx)
                globalvars.spam_counter = globalvars.spam_counter + 1
            elseif globalvars.spam_counter >= 60 and globalvars.spam_counter < 80 then
                cvar.name:invoke_callback()
                cvar.name:set_string(gx .. " \x00")
                globalvars.spam_counter = globalvars.spam_counter + 1
            elseif globalvars.spam_counter >= 80 and globalvars.spam_counter < 100 then
                cvar.name:invoke_callback()
                cvar.name:set_string(gy and gw or gx)
                globalvars.spam_counter = globalvars.spam_counter + 1
            end
        end
    end
)

globalvars.clantag = {
    off = nil,
    tag = "",
    tag_index = 0,
    tag_length = 0,
    tag_reverse = 0,
    tag_last_index = 0
}

function util_functions.clantag_static()
    local e = entity.get_local_player()
    local gz = entity.get_prop(entity.get_player_resource(), "m_szClan", e)

    if globalvars.clantag.tag:len() == 0 and gz:len() ~= 0 then
        client.set_clan_tag("\0")
    elseif gz ~= tag then
        client.set_clan_tag(tag)
    end
end

function util_functions.clantag_time()
    local aw, aA, a1 = client.system_time()
    local gA = string.format("%d:%02d:%02d", aw, aA, a1)
    client.set_clan_tag(gA)
end

function util_functions.clantag_default()
    if globalvars.clantag.tag_index == globalvars.clantag.tag_last_index then
        return
    end

    client.set_clan_tag(
        globalvars.clantag.tag_index == 0 and "\0" or globalvars.clantag.tag:sub(1, globalvars.clantag.tag_index)
    )
end

function util_functions.clantag_reverse()
    if globalvars.clantag.tag_index == globalvars.clantag.tag_last_index then
        return
    end

    if globalvars.clantag.tag_reverse <= globalvars.clantag.tag_length then
        client.set_clan_tag(globalvars.clantag.tag:sub(1, globalvars.clantag.tag_index))
    else
        client.set_clan_tag(
            globalvars.clantag.tag_length - globalvars.clantag.tag_index == 0 and "\0" or
                globalvars.clantag.tag:sub(1, globalvars.clantag.tag_length - globalvars.clantag.tag_index)
        )
    end
end

function util_functions.clantag_loop()
    if globalvars.clantag.tag_index == globalvars.clantag.tag_last_index then
        return
    end

    local gB = globalvars.clantag.tag

    for _ = 1, globalvars.clantag.tag_index do
        gB = gB .. gB:sub(1, 1)
        gB = gB:sub(2, gB:len())
    end

    client.set_clan_tag(gB)
end

globalvars.clantag.animations = {
    ["Static"] = util_functions.clantag_static,
    ["Time"] = util_functions.clantag_time,
    ["Default"] = util_functions.clantag_default,
    ["Reverse"] = util_functions.clantag_reverse,
    ["Classic"] = util_functions.clantag_loop,
    ["Loop"] = util_functions.clantag_loop
}

function util_functions.clantag_handle_custom()
    local e = entity.get_local_player()
    local gC = cfg.clantag_anim:GetItemName()
    globalvars.clantag.tag = cfg.clantag_custom:GetValue()
    globalvars.clantag.tag = gC == "Loop" and globalvars.clantag.tag .. " " or globalvars.clantag.tag
    globalvars.clantag.tag =
        gC == "Classic" and globalvars.clantag.tag .. string.rep(" ", math.floor(globalvars.clantag.tag:len() * 2)) or
        globalvars.clantag.tag
    globalvars.clantag.tag_length = globalvars.clantag.tag:len()
    globalvars.clantag.tag_index =
        math.floor(globals.curtime() * cfg.clantag_speed:GetValue() / 10 % globalvars.clantag.tag_length + 1)
    globalvars.clantag.tag_reverse =
        math.floor(globals.curtime() * cfg.clantag_speed:GetValue() / 10 % (globalvars.clantag.tag_length * 2) + 1)
    local gD = globalvars.clantag.animations[gC]
    gD()
    globalvars.clantag.tag_last_index = globalvars.clantag.tag_index
end

local gE = {
    clantag = {
        ["mariolua"] = {
            "MarioLua",
            "MarioLua",
            "arioLua",
            "rioLua",
            "ioLua",
            "oLua",
            "Lua",
            "ua",
            "",
            "a",
            "ua",
            "Lua",
            "oLua",
            "ioLua",
            "rioLua",
            "arioLua",
            "MarioLua",
            "MarioLua"
        },
        ["mariolua2"] = {
            "MarioLua",
            "arioLua M",
            "rioLua Ma",
            "ioLua Mar",
            "oLua Mari",
            "Lua Mario",
            "ua MarioL",
            "a MarioLu",
            " MarioLua",
            "MarioLua"
        },
        ["aimware"] = {
            "AIMWARE.net ",
            "IMWARE.net A",
            "MWARE.net AI",
            "WARE.net AIM",
            "ARE.net AIMW",
            "RE.net AIMWA",
            "E.net AIMWAR",
            "net AIMWARE.",
            "et AIMWARE.n",
            " AIMWARE.ne",
            "AIMWARE.net "
        },
        ["cooking"] = {"(\\^.^)\\", "/(^.^)\\", "/(^.^/)", "(\\^.^)\\", "(/^.^)/", "\\(^.^)/", "\\(^.^\\) "},
        ["pphud"] = {
            "p",
            "pp",
            "pph",
            "pphu",
            "pphud",
            "pphud ",
            "*phud b",
            "p*hud be",
            "pp*ud bet",
            "pph*d beta",
            "pphu* beta",
            "pphud*beta",
            "pphud *eta",
            "pphud b*ta",
            "pphud be*a",
            "pphud bet*",
            "/phud beta",
            "//hud beta",
            "///ud beta",
            "////d beta",
            "///// beta",
            "///// //ta",
            "///// ////",
            " "
        },
        ["iniuria"] = {
            "I         ",
            "IN        ",
            "INI       ",
            "INIU      ",
            "INIUR     ",
            "INIURI    ",
            "INIURIA   ",
            "INIURIA.  ",
            "INIURIA.U ",
            "INIURIA.US",
            "INIURIA.US",
            " NIURIA.US",
            "  IURIA.US",
            "   URIA.US",
            "    RIA.US",
            "     IA.US",
            "      A.US",
            "       .US",
            "        US",
            "         S",
            "          "
        },
        ["neverlose"] = {
            " ",
            "N ",
            "N3 ",
            "Ne ",
            "Ne\\ ",
            "Ne\\/ ",
            "Nev ",
            "Nev3 ",
            "Neve ",
            "Neve| ",
            "Neve|2 ",
            "Never| ",
            "Never|_ ",
            "Neverl ",
            "Neverl0 ",
            "Neverlo ",
            "Neverlo5 ",
            "Neverlos ",
            "Neverlos3 ",
            "Neverlose ",
            "Neverlose. ",
            "Neverlose.< ",
            "Neverlose.c< ",
            "Neverlose.cc ",
            "Neverlose.c< ",
            "Neverlose.< ",
            "Neverlose. ",
            "Neverlose ",
            "Neverlos3 ",
            "Neverlos ",
            "Neverlo_ ",
            "Neverlo5 ",
            "Neverlo ",
            "Neverl_ ",
            "Never|0 ",
            "Never| ",
            "Neve|2 ",
            "Neve| ",
            "Neve ",
            "Nev3 ",
            "Nev ",
            "Ne\\/ ",
            "Ne\\ ",
            "Ne ",
            "N3 ",
            "N ",
            "|\\| "
        },
        ["dicksuck"] = {
            "SCHWANZ LUTSCHER",
            "SCHWANZ LUTSCHER",
            "CHWANZ LUTSCHER",
            "HWANZ LUTSCHER",
            "ANZ LUTSCHER",
            "NZ LUTSCHER",
            " LUTSCHER",
            "LUTSCHER",
            "UTSCHER",
            "TSCHER",
            "SCHER",
            "CHER",
            "HER",
            "ER",
            "R",
            "ER",
            "HER",
            "CHER",
            "SCHER",
            "TSCHER",
            "UTSCHER",
            "LUTSCHER",
            " LUTSCHER",
            "NZ LUTSCHER",
            "ANZ LUTSCHER",
            "HWANZ LUTSCHER",
            "CHWANZ LUTSCHER",
            "SCHWANZ LUTSCHER",
            "SCHWANZ LUTSCHER"
        }
    },
    index = {
        ["mariolua"] = nil,
        ["aimware"] = nil,
        ["cooking"] = nil,
        ["pphud"] = nil,
        ["iniuria"] = nil,
        ["neverlose"] = nil
    },
    previous = {
        ["mariolua"] = nil,
        ["aimware"] = nil,
        ["cooking"] = nil,
        ["pphud"] = nil,
        ["iniuria"] = nil,
        ["neverlose"] = nil
    }
}

function util_functions.AnimatedClanTag()
    if not cfg.clantag_enabled:GetValue() then
        if not globalvars.clantag.off then
            globalvars.clantag.off = true
            client.set_clan_tag("\0")
        end
        return
    elseif cfg.clantag_enabled:GetValue() and (globalvars.clantag.off or globalvars.clantag.off ~= nil) then
        globalvars.clantag.off = false
    end
    if cfg.clantag_tag:GetItemName() == "Custom" then
        util_functions.clantag_handle_custom()
        return
    end
    local gF = 24
    gE.index["mariolua"] = math.floor(globals.curtime() * 25 / 10 % table.getn(gE.clantag["mariolua"]) + 1)
    gE.index["mariolua2"] = math.floor(globals.curtime() * 25 / 10 % table.getn(gE.clantag["mariolua2"]) + 1)
    gE.index["aimware"] = math.floor(globals.curtime() * gF / 10 % table.getn(gE.clantag["aimware"]) + 1)
    gE.index["cooking"] = math.floor(globals.curtime() * gF / 10 % table.getn(gE.clantag["cooking"]) + 1)
    gE.index["pphud"] = math.floor(globals.curtime() * gF / 10 % table.getn(gE.clantag["pphud"]) + 1)
    gE.index["iniuria"] = math.floor(globals.curtime() * gF / 10 % table.getn(gE.clantag["iniuria"]) + 1)
    gE.index["neverlose"] = math.floor(globals.curtime() * gF / 7 % table.getn(gE.clantag["neverlose"]) + 1)
    gE.index["dicksuck"] = math.floor(globals.curtime() * gF / 10 % table.getn(gE.clantag["dicksuck"]) + 1)
    if cfg.clantag_tag:GetItemName() == "MarioLua" then
        for I = 1, gE.index["mariolua"] do
            local gG = gE.clantag["mariolua"][gE.index["mariolua"] + 1]
            if gG ~= gE.previous["mariolua"] then
                if gG == nil then
                    gG = "MarioLua"
                end
                gE.previous["mariolua"] = gG
                client.set_clan_tag(gG)
            end
        end
    end
    if cfg.clantag_tag:GetItemName() == "MarioLua 2" then
        for I = 1, gE.index["mariolua2"] do
            local gG = gE.clantag["mariolua2"][gE.index["mariolua2"] + 1]
            if gG ~= gE.previous["mariolua2"] then
                if gG == nil then
                    gG = "MarioLua"
                end
                gE.previous["mariolua2"] = gG
                client.set_clan_tag(gG)
            end
        end
    end
    if cfg.clantag_tag:GetItemName() == "Aimware" then
        for I = 1, gE.index["aimware"] do
            local gG = gE.clantag["aimware"][gE.index["aimware"] + 1]
            if gG ~= gE.previous["aimware"] then
                if gG == nil then
                    gG = "AIMWARE.net "
                end
                gE.previous["aimware"] = gG
                client.set_clan_tag(gG)
            end
        end
    end
    if cfg.clantag_tag:GetItemName() == "Cooking" then
        for I = 1, gE.index["cooking"] do
            local gH = gE.clantag["cooking"][gE.index["cooking"] + 1]
            if gH ~= gE.previous["cooking"] then
                if gH == nil then
                    gH = "(\\^.^)\\"
                end
                gE.previous["cooking"] = gH
                client.set_clan_tag(gH)
            end
        end
    end
    if cfg.clantag_tag:GetItemName() == "pphud" then
        for I = 1, gE.index["pphud"] do
            local gI = gE.clantag["pphud"][gE.index["pphud"] + 1]
            if gI ~= gE.previous["pphud"] then
                if gI == nil then
                    gI = "p"
                end
                gE.previous["pphud"] = gI
                client.set_clan_tag(gI)
            end
        end
    end
    if cfg.clantag_tag:GetItemName() == "Iniuria" then
        for I = 1, gE.index["iniuria"] do
            local gI = gE.clantag["iniuria"][gE.index["iniuria"] + 1]
            if gI ~= gE.previous["iniuria"] then
                if gI == nil then
                    gI = "INIURIA.US"
                end
                gE.previous["iniuria"] = gI
                client.set_clan_tag(gI)
            end
        end
    end
    if cfg.clantag_tag:GetItemName() == "Neverlose" then
        for I = 1, gE.index["neverlose"] do
            local gI = gE.clantag["neverlose"][gE.index["neverlose"] + 1]
            if gI ~= gE.previous["neverlose"] then
                if gI == nil then
                    gI = ""
                end
                gE.previous["neverlose"] = gI
                client.set_clan_tag(gI)
            end
        end
    end
    if cfg.clantag_tag:GetItemName() == "SCHWANZ LUTSCHER" then
        for I = 1, gE.index["dicksuck"] do
            local gI = gE.clantag["dicksuck"][gE.index["dicksuck"] + 1]
            if gI ~= gE.previous["dicksuck"] then
                if gI == nil then
                    gI = "SCHWANZ LUTSCHER"
                end
                gE.previous["dicksuck"] = gI
                client.set_clan_tag(gI)
            end
        end
    end
end
globalvars.sk_table = {}
function util_functions.Skeleton()
    if not cfg.visuals_skeleton:GetValue() then
        return
    end
    local gJ = entity.get_players(true)
    for I = 1, #gJ do
        local ent = gJ[I]
        util_functions.draw_Skeleton(ent)
    end
end
globalvars.pl = {}
globalvars.pl.fl_fakeclient = 0x200
globalvars.pl.score = cvar.score
local gK = mariolua_menu.CObject:new("player_list_object")
gK:AddItem("player_list", 350, 25, 200, 25)
globalvars.pl.wnd = {
    x = gK.items[1].pos.x,
    y = gK.items[1].pos.y,
    w = gK.items[1].width,
    h = gK.items[1].height,
    dragging = false,
    resize = false,
    rx = 0
}
function globalvars.pl.intersect(a9, aa, cJ, aw, gL)
    local ad, ae = ui.mouse_position()
    gL = gL or false
    if gL then
        renderer.rectangle(a9, aa, cJ, aw, 255, 0, 0, 50)
    end
    return ad >= a9 and ad <= a9 + cJ and ae >= aa and ae <= aa + aw
end
function util_functions.RenderExtrapolation(ent, b_, gM)
    functions.extrapolated_ent_data = functions.extrapolated_ent_data or {}
    if not mariolua.debug.draw["player_prediction"] then
        return
    end
    for I = 1, #functions.extrapolated_ent_data do
        local b_ = functions.extrapolated_ent_data[I]
        util_functions.draw_Skeleton(I, b_)
    end
end
function functions.ExtrapolatePosition(gN, gO, gP, b_, ent)
    b_ = b_ or 0
    if b_ == 0 then
        return gN, gO, gP
    end
    local gQ = globals.tickinterval()
    local a9, aa, b3 = entity.get_prop(ent, "m_vecVelocity")
    if a9 ~= nil and math.sqrt(a9 * a9 + aa * aa + b3 * b3) > 1.01 then
        for I = 0, b_ do
            gN = gN + a9 * gQ
            gO = gO + aa * gQ
            gP = gP + b3 * gQ
        end
    end
    functions.extrapolated_ent_data = functions.extrapolated_ent_data or {}
    functions.extrapolated_ent_data[ent] = b_
    return gN, gO, gP
end
function util_functions.hitgroup_id(gR)
    return globalvars.hitgroups[gR:lower()]
end
function util_functions.get_hitbox_hitgorup(gS)
    for I = 1, #globalvars.hitscan2 do
        if extended_table.Contains(globalvars.hitscan2[I], gS) then
            return I
        end
    end
end
globalvars.visible_hitboxes = {}
function util_functions.get_visible_hitboxes(_, b_)
    b_ = b_ or 0
    local gV = {}

    for I = 0, 18 do
        local gW = Vector3(entity.hitbox_position(ent_end, I))

        if b_ > 0 then
            ---@diagnostic disable-next-line: redundant-parameter
            gW = Vector3(functions.ExtrapolatePosition(gW.x, gW.y, gW.z, b_, ent_end))
        end

        if client.visible(gW.x, gW.y, gW.z) then
            table.insert(gV, I)
        end
    end

    return gV
end

globalvars.vis_hitboxes = {}

function util_functions.isVisible(ent)
    local b_ = cfg.tr_legitaw_extrapolated:GetValue()
    local gT = Vector3(entity.get_origin(local_player))
    local gU = Vector3(client.eye_position())
    if globalvars.vis_hitboxes[ent] == nil then
        globalvars.vis_hitboxes[ent] = {}
    end
    for I = 1, #cfg.tr_legitaw_hitbox.enabled_items do
        local gR = cfg.tr_legitaw_hitbox.enabled_items[I]
        local gX = util_functions.hitgroup_id(gR) + 1
        for _, F in pairs(globalvars.hitscan2[gX]) do
            local gW
            if b_ > 0 then
                ---@diagnostic disable-next-line: redundant-parameter
                vec_pos = Vector3(entity.hitbox_position(ent, F - 1))
                ---@diagnostic disable-next-line: redundant-parameter
                gW = Vector3(functions.ExtrapolatePosition(vec_pos.x, vec_pos.y, vec_pos.z, b_, ent))
            else
                ---@diagnostic disable-next-line: redundant-parameter
                gW = Vector3(entity.hitbox_position(ent, F - 1))
            end

            _, entindex = client.trace_line(local_player, gT.x, gT.y, gU.z, gW.x, gW.y, gW.z)
            if entindex == ent or client.visible(gW.x, gW.y, gW.z) then
                if not extended_table.Contains(globalvars.vis_hitboxes[ent], F - 1) then
                    table.insert(globalvars.vis_hitboxes[ent], F - 1)
                end
                return true
            elseif extended_table.Contains(globalvars.vis_hitboxes[ent], F - 1) then
                table.remove(globalvars.vis_hitboxes[ent], F - 1)
            end
        end
    end
    return false
end
function util_functions.GetClosestCrosshair()
    local g8 = entity.get_players(true)
    local gj, gi = client.camera_angles()
    ---@diagnostic disable-next-line: redundant-parameter
    local gY = Vector3(entity.get_prop(entity.get_local_player(), "m_vecAbsOrigin"))
    ---@diagnostic disable-next-line: redundant-parameter
    local gZ = Vector3(gj, gi, 0)
    globalvars.nearest_player = nil
    globalvars.nearest_player_fov = math.huge

    for I = 1, #g8 do
        local g_ = g8[I]
        ---@diagnostic disable-next-line: redundant-parameter
        local h0 = Vector3(entity.get_prop(g_, "m_vecOrigin"))
        local h1 = B.get_FOV(gZ, gY, h0)

        if h1 <= globalvars.nearest_player_fov then
            globalvars.nearest_player = g_
            globalvars.nearest_player_fov = h1
        end
    end

    return globalvars.nearest_player, globalvars.nearest_player_fov
end

function util_functions.LegitAutowall()
    if
        cfg.autowall_enable:GetValue() and (cfg.autowall_key:IsKeyToggle() or cfg.autowall_key:IsKeyDown()) or
            not cfg.tr_legitaw_enabled:GetValue() or
            not entity.is_alive(local_player)
     then
        return
    end
    local h2 = ui.get(globalvars.ref.rage.rage_fov)
    local gT = Vector3(entity.get_prop(local_player, "m_vecOrigin"))
    local gU = Vector3(client.eye_position())
    local h3, h4 = util_functions.GetClosestCrosshair()
    local h5 = Vector3(entity.get_prop(local_player, "m_vecViewOffset[2]"))
    local lz = lz + h5
    if not (cfg.autowall_enable:GetValue() and (cfg.autowall_key:IsKeyToggle() or cfg.autowall_key:IsKeyDown())) then
        if h3 ~= nil and h4 <= h2 then
            ui.set(globalvars.ref.rage.autowall, util_functions.isVisible(h3))
        elseif ui.get(globalvars.ref.rage.autowall) then
            ui.set(globalvars.ref.rage.autowall, false)
        end
    end
end
local h6 = {values = {ForceBodyYaw = {}, ForceBodyYawCheckbox = {}, CorrectionActive = {}}}
function h6.GetForceBodyYawCheckbox(ent)
    if ent == nil then
        return
    end
    return plist.get(ent, "Force body yaw")
end
function h6.SetForceBodyYawCheckbox(ent, H)
    if ent == nil or h6.values.ForceBodyYawCheckbox[ent] == H then
        return
    end
    plist.set(ent, "Force body yaw", H)
    h6.values.ForceBodyYawCheckbox[ent] = H
end
function h6.GetPlayerBodyYaw(ent)
    if ent == nil then
        return
    end
    return plist.get(ent, "Force body yaw value")
end
function h6.SetPlayerBodyYaw(ent, H)
    if ent == nil or h6.values.ForceBodyYaw[ent] == H then
        return
    end
    plist.set(ent, "Force body yaw value", H)
    h6.values.ForceBodyYaw[ent] = H
end
function h6.IsCorrectionActive(ent)
    if ent == nil then
        return
    end
    return plist.get(ent, "Correction active")
end
function h6.SetCorrectionActive(ent, H)
    if ent == nil or h6.values.CorrectionActive[ent] == H then
        return
    end
    plist.set(ent, "Correction active", H)
    h6.values.CorrectionActive[ent] = H
end
function h6.force_correction()
    if globalvars.resolver_t == nil then
        return
    end
    if not resolver.IsActive() or not cfg.resolver_modules:IsEnabled("Desync Detection") then
        return
    end
    local gJ = entity.get_players(true)
    for I = 1, #gJ do
        local ent = gJ[I]
        plist.set(ent, "Correction active", h6.values.CorrectionActive[ent])
    end
end
local function h7(a6)
    local a8 = ""
    if type(a6) == "string" then
        return a6
    end
    for Q, F in pairs(a6) do
        if type(Q) == "string" then
            a8 = a8 .. "\n'" .. Q .. "'" .. " = "
        end
        if type(F) == "table" then
            a8 = a8 .. h7(F)
        elseif type(F) == "boolean" then
            a8 = a8 .. "" .. tostring(F)
        elseif type(F) == "string" then
            a8 = a8 .. "\n'" .. F .. "'"
        elseif type(F) == "function" then
            return
        else
            a8 = a8 .. F
        end
        a8 = a8 .. " | "
    end
    if a8 ~= "" then
        a8 = a8:sub(1, a8:len() - 1)
    end
    return tostring(a8)
end
globalvars.t_last_layer_info = {}
function functions.get_animlayer(ent, I)
    I = I or 12
    local br = functions.get_client_entity(functions.ientitylist, ent)
    local bs = functions.get_client_networkable(functions.ientitylist, ent)
    local by = ffi.cast(ffi_utils.interface_ptr, br)
    local bz = ffi.cast("char*", by) + 0x3914
    local bA = ffi.cast("struct c_animstate_mariolua**", bz)[0]
    local h8 = {}
    if br == nil or bs == nil or bA == nil then
        return
    end
    if type(I) == "table" then
        for c7, du in ipairs(I) do
            h8[du] = functions.get_anim_layer(br, du)
        end
    else
        h8 = functions.get_anim_layer(br, I)
    end
    local h9 = functions.get_sequence_activity(br, bs, h8.m_nSequence)
    return h8, h9
end
function functions.update_client_animation(ent)
    local ha = functions.animstate(ent)
    ha.m_iLastClientSideAnimationUpdateFramecount =
        ha.m_iLastClientSideAnimationUpdateFramecount >= globals.framecount() and globals.framecount() - 1 or
        ha.m_iLastClientSideAnimationUpdateFramecount
end
globalvars.debug_font = extended_renderer.create_font("Verdana", 13, 600, 0)
function globalvars.draw_ply_info(hb, a9, aa)
    for I = 1, #hb do
        for cl = 1, #hb[I] do
            extended_renderer.draw_text(
                a9 + 50 + I - 1 * 100,
                aa + cl * 15 + (I - 1) * 220,
                globalvars.color[1],
                globalvars.color[2],
                globalvars.color[3],
                globalvars.color[4],
                globalvars.debug_font,
                hb[I][cl]
            )
        end
    end
end
function external_modules.get_max_feet_yaw(bK)
    local ha = functions.animstate(bK)
    local duckammount = ha.m_fDuckAmount
    local hc = math.max(0, math.min(ha.m_flFeetSpeedForwardsOrSideWays, 1.))
    local hd = math.max(0, math.max(1, ha.m_flFeetSpeedUnknownForwardOrSideways))
    local he = (ha.m_flStopToFullRunningFraction * -0.30000001 - 0.19999999) * hc
    local hf = he + 1
    if duckammount > 0 then
        hf = hf + duckammount * hd * (0.5 - hf)
    end
    return ha.m_flMaxYaw * hf
end

function functions.GetPlayerMaxFeetYaw(ent)
    local ha = functions.animstate(ent)
    local hg = ha.m_fDuckAmount
    local hh = math.max(0, math.min(1, ha.m_flFeetSpeedForwardsOrSideWays))
    local hi = math.max(1, ha.m_flFeetSpeedUnknownForwardOrSideways)
    local hj = (ha.m_flStopToFullRunningFraction * -0.30000001 - 0.19999999) * hh + 1

    if hg > 0 then
        hj = hj + hg * hi * (0.5 - hj)
    end

    local hk = ha.m_flMaxYaw * hj

    return hk < 60 and hk >= 0 and hk or 0
end

function B.multiplyvalues(a9, aa, b3, H)
    a9 = a9 * H
    aa = aa * H
    b3 = b3 * H
    return a9, aa, b3
end

function B.angle_vector(a9, aa)
    local bQ, ae, hl, hm = math.rad(aa), math.rad(aa), math.rad(a9), math.rad(a9)
    return math.cos(hm) * math.cos(ae), math.cos(hm) * math.sin(bQ), -math.sin(hl)
end

function util_functions.CalcFov(ent, hn, ho, lz, hp, hq, hr)
    ---@diagnostic disable-next-line: redundant-parameter
    local hs = Vector3(entity.get_prop(ent, "m_vecOrigin"))
    local ht = B.normalize_angle(hs.x - hn, hs.y - ho, lz - lz)
    ---@diagnostic disable-next-line: redundant-parameter
    local hu = ht:dot(Vector3(hp, hq, hr))
    local hv = math.acos(hu)
    return extended_math.deg2rad * hv
end

function util_functions.get_closest_player(hn, ho, lz, gj, gi)
    local hp, hq, hr = B.to_angle(gj, gi)
    local g8 = entity.get_players(true)
    local h3 = nil
    local h4 = math.huge

    for I = 1, #g8 do
        local g_ = g8[I]
        local h1 = util_functions.CalcFov(g_, hn, ho, lz, hp, hq, hr)

        if h1 <= h4 then
            h3 = g_
            h4 = h1
        end
    end

    return h3, h4
end

globalvars.HitGroupDisplay = {
    "Generic",
    "Head",
    "Chest",
    "Stomach",
    "Left arm",
    "Right arm",
    "Left leg",
    "Right leg",
    "Neck"
}

globalvars.HitGroupNames = {
    "HITGROUP_GENERIC",
    "HITGROUP_HEAD",
    "HITGROUP_CHEST",
    "HITGROUP_STOMACH",
    "HITGROUP_LEFTARM",
    "HITGROUP_RIGHTARM",
    "HITGROUP_LEFTLEG",
    "HITGROUP_RIGHTLEG",
    "HITGROUP_GEAR"
}

globalvars.HitGroupToHitBox = {
    2,
    0,
    4,
    2,
    13,
    14,
    7,
    8,
    1
}

globalvars.CubeOutlineConnections = {
    {1, 2},
    {1, 3},
    {1, 5},
    {8, 7},
    {8, 6},
    {8, 4},
    {6, 3},
    {4, 3},
    {4, 2},
    {2, 7},
    {7, 5},
    {6, 5}
}

globalvars.CubeFillConnections = {
    {1, 2, 3},
    {1, 2, 5},
    {1, 3, 5},
    {8, 7, 6},
    {8, 7, 4},
    {8, 6, 4},
    {3, 6, 5},
    {3, 4, 6},
    {2, 4, 7},
    {2, 7, 5},
    {3, 4, 2},
    {6, 5, 7}
}

globalvars.HitBoxConnections = {
    {0, 1},
    {1, 2},
    {2, 7},
    {7, 6},
    {6, 5},
    {5, 4},
    {4, 3},
    {3, 9},
    {3, 8},
    {9, 11},
    {8, 10},
    {11, 13},
    {10, 12},
    {7, 18},
    {18, 19},
    {19, 15},
    {7, 16},
    {16, 17},
    {17, 14}
}

function util_functions.RenderEnemyAngles()
    if
        local_player == nil or
            not (cfg.visuals_angle:IsEnabled("Real") or cfg.visuals_angle:IsEnabled("Fake") or
                cfg.visuals_angle:IsEnabled("LBY"))
     then
        return
    end

    local bJ = entity.get_players(true)

    if bJ == nil then
        return
    end

    for I = 1, #bJ, 1 do
        local ent = bJ[I]

        if not (entity.is_alive(ent) or entity.is_enemy(ent) or entity.is_alive(entity.get_local_player())) then
            return
        end

        ---@diagnostic disable-next-line: redundant-parameter
        local hw = Vector3(entity.get_origin(ent))
        local _, hx = entity.get_prop(ent, "m_angAbsRotation")
        local hy = entity.get_prop(ent, "m_angEyeAngles[1]")
        local hB = {
            degrees = 50,
            start_at = hx,
            start_at2 = hy,
            start_at3 = hz
        }
        local hC = 10
        local hD, hE, hG = {255, 255, 255, 255}, {255, 0, 0, 255}, {0, 150, 255, 255}

        if cfg.visuals_angle:IsEnabled("Real") then
            extended_renderer.circle_3d(
                hw.x,
                hw.y,
                hw.z,
                hC + 10 * I,
                hB.degrees,
                hB.start_at,
                hD[1],
                hD[2],
                hD[3],
                hD[4],
                3,
                5
            )
        end

        if cfg.visuals_angle:IsEnabled("Fake") then
            extended_renderer.circle_3d(
                hw.x,
                hw.y,
                hw.z,
                hC + 10 * I,
                hB.degrees,
                hB.start_at2,
                hE[1],
                hE[2],
                hE[3],
                hE[4],
                3,
                5
            )
        end

        if cfg.visuals_angle:IsEnabled("LBY") then
            extended_renderer.circle_3d(
                hw.x,
                hw.y,
                hw.z,
                hC + 10 * I,
                hB.degrees,
                hB.start_at3,
                hG[1],
                hG[2],
                hG[3],
                hG[4],
                3,
                5
            )
        end
    end
end

function util_functions.player_infos()
    globalvars.closest_enemy, globalvars.closest_distance = util_functions.GetNearestEnemy()

    if globalvars.closest_enemy == nil then
        return
    end

    local _, _, bN = entity.hitbox_position(globalvars.closest_enemy, 1)
    local hH, hI, hJ = entity.get_origin(globalvars.closest_enemy)
    bN = hJ or bN
    local a9, aa = renderer.world_to_screen(hH, hI, bN)

    if a9 and aa and resolver.data then
        resolver.get_info_for_player(globalvars.closest_enemy, a9, aa)
        resolver.record_animlayers(globalvars.closest_enemy, a9, aa)
    end
end

function lerp_pos(ab, ac, hK, b8, b9, hL, hM)
    local a9 = (b8 - ab) * hM + ab
    local aa = (b9 - ac) * hM + ac
    local b3 = (hL - hK) * hM + hK

    return a9, aa, b3
end

function util_functions.AntiResolver(cmd)
    if cfg.antiaim_lbyextras_mode == nil then
        return
    elseif cfg.antiaim_lbyextras_mode:GetItemName() == "Off" then
        return
    end

    local hN = cfg.antiaim_lbyextras_mode:GetItemName()

    if cmd.chokedcommands == 0 then
        globalvars.antiresolver.updates = globalvars.antiresolver.updates + 1
        globalvars.antiresolver.targeted = globalvars.antiresolver.targeted + 1
    end

    if globalvars.antiresolver.targeted >= ui.get(globalvars.ref.fakelag_limit) then
        globalvars.antiresolver.targeted = 0
    end

    if cmd.in_forward == 0 and cmd.in_back == 0 and cmd.in_moveleft == 0 and cmd.in_moveright == 0 then
        cmd.allow_send_packet = false

        if hN == "Fake Twist" then
            if
                cmd.chokedcommands %
                    (globalvars.antiresolver.updates % 2 == 0 and ui.get(globalvars.ref.fakelag_limit) / 2 or 0) ==
                    0
             then
                cmd.forwardmove = 1.01
            end

            if cmd.chokedcommands % 2 ~= 0 and cmd.chokedcommands % globalvars.antiresolver.targeted == 0 then
                cmd.forwardmove = 1.01
            end
        elseif hN == "Fake Max" then
            if cmd.chokedcommands % globalvars.antiresolver.targeted then
                cmd.forwardmove = 1.01
            end
        elseif hN == "Cradle" then
            if cmd.chokedcommands % globalvars.antiresolver.targeted == 0 then
                cmd.forwardmove = 1.01
            end
        elseif hN == "Shake" then
            if cmd.chokedcommands % 3 == 0 or cmd.chokedcommands % globalvars.antiresolver.targeted / 2 == 0 then
                cmd.forwardmove = 1.01
            end
        end
    end
end

function util_functions.Slidewalk(cmd)
    local hO = entity.get_prop(entity.get_local_player(), "m_MoveType")
    if hO == 9 then
        return
    end

    if cmd.in_use == 1 then
        return
    end

    if cfg.misc_features:IsEnabled("Slide Walk") then
        if cmd.forwardmove > 0 then
            cmd.in_back = 1
            cmd.in_forward = 0
        end

        if cmd.forwardmove < 0 then
            cmd.in_forward = 1
            cmd.in_back = 0
        end

        if cmd.sidemove < 0 then
            cmd.in_moveright = 1
            cmd.in_moveleft = 0
        end

        if cmd.sidemove > 0 then
            cmd.in_moveleft = 1
            cmd.in_moveright = 0
        end
    end
end

function extended_math.world2screen(hS, hT)
    if hS == 0 and hT == 0 then
        return 0
    end

    return math.deg(math.atan2(hT, hS))
end

local hU = ui.reference("AA", "Fake lag", "Enabled")
local hV, hW, hX = 0, 0, 0
local hY = ui.reference("RAGE", "Aimbot", "Minimum Damage")

function util_functions.is_auto_vis(e, hn, ho, lz, bL, bM, bN)
    entindex, dmg = client.trace_bullet(ent, hn, ho, lz, bL, bM, bN)
    if entindex == nil then
        return false
    end

    if entindex == e then
        return false
    end

    if not entity.is_enemy(entindex) then
        return false
    end

    if dmg > hY then
        return true
    else
        return false
    end
end

function util_functions.trace_positions(bL, bM, bN, hZ, h_, i0, i1, i2, i3, hV, hW, hX)
    if util_functions.is_auto_vis(local_player, hV, hW, hX, bL, bM, bN) then
        return true
    end

    if util_functions.is_auto_vis(local_player, hV, hW, hX, hZ, h_, i0) then
        return true
    end

    if util_functions.is_auto_vis(local_player, hV, hW, hX, i1, i2, i3) then
        return true
    end

    return false
end

function util_functions.disable_fakelag()
    ui.set(hU, false)
end

function util_functions.enable_fakelag()
    ui.set(hU, true)
end

function util_functions.drawException()
    if not (cfg.misc_fakelagonpeek:GetValue() and cfg.misc_fakelagonpeek_drawprediction:GetValue()) then
        return
    end
    if local_player == nil then
        return
    end
    if not entity.is_alive(local_player) then
        return
    end
    local bJ = entity.get_players(true)
    if bJ == nil then
        return
    end
    local wx, wy = client.world_to_screen(ctx, hV, hW, hX)
    if wx == nil then
        return
    end
    renderer.circle(wx, wy, 17, 17, 17, 255, 4, 0, 1)
end
function util_functions.FakelagOnPeek(cmd)
    if not cfg.misc_fakelagonpeek:GetValue() then
        return
    end
    local bJ = entity.get_players()
    if bJ == nil then
        return
    end
    local hn, ho, lz = client.eye_position()
    if hn == nil then
        return
    end
    for I = 1, #bJ do
        local i4 = bJ[I]
        if not entity.is_enemy(i4) then
            if not i4 == local_player then
                return
            end
        end
        local bL, bM, bN = entity.hitbox_position(i4, 0)
        local hZ, h_, i0 = entity.hitbox_position(i4, 4)
        local i1, i2, i3 = entity.hitbox_position(i4, 2)
        if bL == nil then
            return
        end
        if functions.is_moving(local_player) then
            hV, hW, hX =
                functions.ExtrapolatePosition(hn, ho, lz, cfg.misc_fakelagonpeek_prediction:GetValue(), local_player)
        else
            hV, hW, hX = hn, ho, lz
        end
        if hV == nil then
            return
        end
        if util_functions.trace_positions(bL, bM, bN, hZ, h_, i0, i1, i2, i3, hV, hW, hX) then
            util_functions.enable_fakelag()
            client.delay_call(cfg.misc_fakelagonpeek_time:GetValue() / 1000, util_functions.disable_fakelag)
        end
    end
end
local i5 = {ui.reference("VISUALS", "Effects", "Force third person (alive)")}
function util_functions.local_info()
    local ent = local_player
    if entity.is_alive(ent) and mariolua.debug.draw["player_infos"] then
        for bv = 0, 13 do
            renderer.text(
                10 + 200 * bv,
                100 + 15 * bv,
                globalvars.color[1],
                globalvars.color[2],
                globalvars.color[3],
                220,
                "c",
                0,
                "Layer: " .. bv
            )
            for I, F in pairs(globalvars.t_last_layer_info) do
                wx, wy = 10 + 200 * I, 250 + 15 * I
                if wx == nil or wy == nil then
                    return
                end
                renderer.text(wx, wy, globalvars.color[1], globalvars.color[2], globalvars.color[3], 220, "c", 0, h7(F))
            end
        end
    end
end
local i6 = {choked = 0, limit = 0, sendpacket = false}
function util_functions.FakelagChams(ar)
    if ui.get(i5[1]) and ui.get(i5[2]) and cfg.visuals_fakelagchams:GetValue() then
        local i7 = ar.chokedcommands
        if i6.choked ~= 0 and i7 == 0 then
            i6.limit = i6.choked
        end
        i6.sendpacket = i7 == i6.limit
        i6.choked = i7
        local i8 = false
        local i9 = false
        i8 = i6.sendpacket
        i9 = i6.choked == 0
        if i8 then
            client.draw_hitboxes(local_player, globals.tickinterval() * (i6.limit + 2), 19, 60, 147, 250, 255)
        end
        if i9 then
            client.draw_hitboxes(local_player, globals.tickinterval() * (i6.limit + 2), 19, 250, 50, 60, 255)
        end
    end
end
globalvars.antiaim.exception_met = true
local ia = {aa = false, limit = 14, auto_inf_duck = false}
function util_functions.FakeDuckFix()
    if cfg.misc_features:IsEnabled("Legit Fake Duck Fix") then
        if ui.get(globalvars.ref.rage.fakeduck) then
            if ia.aa == false then
                ia.limit = ui.get(globalvars.ref.fakelag_limit)
            end
            if not ui.get(globalvars.ref.misc.infinite_duck) and ia.auto_inf_duck == false then
                ui.set(globalvars.ref.misc.infinite_duck, true)
                ia.auto_inf_duck = true
            end
            ui.set(globalvars.ref.fakelag_limit, 6)
            globalvars.antiaim.exception_met = false
            ia.aa = true
        elseif ia.aa then
            if ia.auto_inf_duck then
                ui.set(globalvars.ref.misc.infinite_duck, false)
                ia.auto_inf_duck = false
            end
            ui.set(globalvars.ref.fakelag_limit, ia.limit)
            ia.aa = false
            globalvars.antiaim.exception_met = true
        end
    end
end
local ib = {
    IndicesNoteam = {[0] = "kick", [1] = "changelevel", [3] = "scrambleteams", [4] = "swapteams"},
    IndicesTeam = {[1] = "starttimeout", [2] = "surrender"},
    Descriptions = {
        changelevel = "change the map",
        scrambleteams = "scramble the teams",
        starttimeout = "start a timeout",
        surrender = "surrender",
        kick = "kick"
    },
    OnGoingVotes = {},
    VoteOptions = {}
}
function util_functions.OnVoteReset()
    if cfg.misc_features:IsEnabled("Vote Revealer") then
        for ic, id in pairs(ib.OnGoingVotes) do
            if entity.get_prop(id.controller, "m_iActiveIssueIndex") ~= id.IssueIndex then
                ib.OnGoingVotes[ic] = nil
            end
        end
    end
end
function util_functions.on_vote_options(au)
    if cfg.misc_features:IsEnabled("Vote Revealer") then
        ib.VoteOptions = {au.option1, au.option2, au.option3, au.option4, au.option5}
        for I = #ib.VoteOptions, 1, -1 do
            if ib.VoteOptions[I] == "" then
                table.remove(ib.VoteOptions, I)
            end
        end
    end
end
function util_functions.on_vote_cast(au)
    if cfg.misc_features:IsEnabled("Vote Revealer") then
        client.delay_call(
            0.3,
            function()
                local ic = au.team
                local ie = ib
                if ib.VoteOptions then
                    local ig
                    local ih = entity.get_all("CVoteController")
                    for I = 1, #ih do
                        if entity.get_prop(ih[I], "m_iOnlyTeamToVote") == ic then
                            ig = ih[I]
                            break
                        end
                    end
                    if ig then
                        local ii = {
                            team = ic,
                            options = ib.VoteOptions,
                            controller = ig,
                            IssueIndex = entity.get_prop(ig, "m_iActiveIssueIndex"),
                            votes = {}
                        }
                        for I = 1, #ib.VoteOptions do
                            ii.votes[ib.VoteOptions[I]] = {}
                        end
                        ii.type = ib.IndicesNoteam[ii.IssueIndex]
                        if ic ~= -1 and ib.IndicesTeam[ii.IssueIndex] then
                            ii.type = ib.IndicesTeam[ii.IssueIndex]
                        end
                        ib.OnGoingVotes.team = ii
                    end
                    ib.VoteOptions = nil
                end
                local ii = ib.OnGoingVotes.team
                if ii then
                    local bK = au.entityid
                    local ij = ii.options[au.vote_option + 1]
                    table.insert(ii.votes[ij], bK)
                    if ij == "Yes" and ii.caller == nil then
                        ii.caller = bK
                        if ii.type ~= "kick" then
                            local bp = entity.get_player_name(bK) .. " called a vote to: " .. ib.Descriptions[ii.type]
                            mariolua.print(bp)
                            mariolua.PrintInChat(
                                entity.get_player_name(bK) .. " called a vote to: " .. ib.Descriptions[ii.type]
                            )
                        end
                    end
                    if ii.type == "kick" then
                        if ij == "No" then
                            if ii.target == nil then
                                ii.target = bK
                                local ik = ic == 3 and "CT's" or "T's"
                                local bp =
                                    ik ..
                                    " called a vote to " ..
                                        ib.Descriptions[ii.type] .. ": " .. entity.get_player_name(ii.target)
                                mariolua.print(bp)
                                mariolua.PrintInChat(
                                    ik ..
                                        " called a vote to " ..
                                            ib.Descriptions[ii.type] .. ": " .. entity.get_player_name(ii.target)
                                )
                            end
                        end
                    end
                    local bp = entity.get_player_name(bK) .. " voted: " .. ij
                    mariolua.print(bp)
                    mariolua.PrintInChat(entity.get_player_name(bK) .. " voted: " .. ij)
                end
            end
        )
    end
end
function util_functions.hold_bot(au)
    if cfg.misc_features:IsEnabled("Auto Hold Bot") then
        local bJ = entity.get_players()
        if bJ == nil then
            return
        end
        for I = 1, #bJ do
            local entindex = bJ[I]
            if not entity.is_enemy(entindex) then
                local be = entity.get_prop(entindex, "m_fFlags")
                if be and bit.band(be, 0x200) == 0x200 then
                    client.exec("holdpos")
                    return
                end
            end
        end
    end
end
function util_functions.ReportEnemy()
    if globalvars.report.should_report then
        local il = globals.curtime()
        if entity.get_steam64(globalvars.report.idx) ~= 0 and entity.is_enemy(globalvars.report.idx) then
            if globalvars.report.next_report_time > il then
                return
            end
            local im = entity.get_player_name(globalvars.report.idx)
            if
                not pcall(
                    function()
                        globalvars.report.total_reports = globalvars.report.total_reports + 1
                        local io = GameStateAPI.GetPlayerXuidStringFromEntIndex(globalvars.report.idx)
                        GameStateAPI.SubmitPlayerReport(io, "textabuse,voiceabuse,grief,aimbot,wallhack,speedhack")
                        mariolua.PrintInChat(
                            "Submitting report for player " .. im .. "(Report " .. globalvars.report.idx .. "#)"
                        )
                        if cfg.report_write_chat:GetValue() then
                            client.exec(
                                "say ",
                                "Submitting report for player " .. im .. "(Report " .. globalvars.report.idx .. "#)"
                            )
                        end
                        GameStateAPI.ToggleMute(io)
                    end
                )
             then
                mariolua.PrintInChat("Error encountered while submitting report!")
                mariolua.notify(5, "Error encountered while submitting report!")
            end
            globalvars.report.next_report_time = il + 1
        end
        if globalvars.report.idx >= globals.maxplayers() then
            globalvars.report.total_reports = 0
            globalvars.report.should_report = false
            globalvars.report.idx = 1
            mariolua.PrintInChat("Finished reporting")
            mariolua.notify(5, "Finished reporting")
        else
            globalvars.report.idx = globalvars.report.idx + 1
        end
    end
end
local ip =
    panorama.loadstring(
    [[
	if ( typeof i_ForceHvHion == 'undefined' ) {
		i_ForceHvHion = {};
		i_ForceHvHion.status = true;
		i_ForceHvHion.last = 0;

		i_ForceHvHion.EventFunc = (shouldShow, playersReadyCount, numTotalClientsInReservation)=>{
			let PossibleAutoAccepts = playersReadyCount - i_ForceHvHion.last;
			i_PossibleAutoAccepts = PossibleAutoAccepts;
			i_ForceHvHion.last = playersReadyCount;
			i_playersReadyCount = playersReadyCount;
			return {
				autoaccept: PossibleAutoAccepts,
				accepts: playersReadyCount,
			}
		};
	}
]]
)()
local iq =
    panorama.loadstring(
    [[
	i_playersReadyCount = 0;
	i_PossibleAutoAccepts = 0;

	if ( typeof i_ForceHvHion == 'undefined' ) {
		i_ForceHvHion = {};
		i_ForceHvHion.status = true;
		i_ForceHvHion.last = 0;

		i_ForceHvHion.EventFunc = (shouldShow, playersReadyCount, numTotalClientsInReservation)=>{
			let PossibleAutoAccepts = playersReadyCount - i_ForceHvHion.last;
			i_PossibleAutoAccepts = PossibleAutoAccepts;
			i_ForceHvHion.last = playersReadyCount;
			i_playersReadyCount = playersReadyCount;
			if ( PossibleAutoAccepts < 8 ) {






	LobbyAPI.StopMatchmaking();
				LobbyAPI.StartMatchmaking( '', '', '', '' );
			}
		};
	}
	return {
		i_ForceHvHion: {
			toggle: (status)=>{
				if ( status && !i_ForceHvHion.handle ) {
					i_ForceHvHion.handle = $.RegisterForUnhandledEvent( 'PanoramaComponent_Lobby_ReadyUpForMatch', i_ForceHvHion.EventFunc);
				} else {
					if ( i_ForceHvHion.handle ) {
						$.UnregisterForUnhandledEvent( 'PanoramaComponent_Lobby_ReadyUpForMatch', i_ForceHvHion.handle);
						i_ForceHvHion.handle = false;
					}
				}
			},
		},
	}
]]
)()
iq.i_ForceHvHion.toggle(false)
function util_functions.ForceHvH_Init()
    if cfg.misc_forcehvh == nil then
        return
    end
    if cfg.misc_forcehvh:GetValue() then
        iq.i_ForceHvHion.toggle(true)
    end
end
util_functions.ForceHvH_Init()
function util_functions.ForceHvH()
    if cfg.misc_forcehvh == nil then
        return
    end
    if cfg.misc_forcehvh:GetValue() then
        mariolua.log("ForceHvH: On")
        iq.i_ForceHvHion.toggle(true)
        print(tostring(ip.autoaccept))
        print(tostring(ip.accepts))
    end
end
globalvars.epeek_key_input = false
function util_functions.handle_epeek()
    if not cfg.antiaim_epeek:GetValue() then
        return
    end
    local ir = cfg.antiaim_epeek_method:GetItemName() == "Automatic" and true or false
    if cfg.antiaim_epeek_mode:GetItemName() == "Hold" then
        if cfg.antiaim_epeek_key:IsKeyDown() then
            globalvars.epeek_key_input = true
        elseif not cfg.antiaim_epeek_key:IsKeyDown() then
            globalvars.epeek_key_input = false
        end
    elseif cfg.antiaim_epeek_mode:GetItemName() == "Toggle" then
        globalvars.epeek_key_input = cfg.antiaim_epeek_key:IsKeyToggle()
    elseif cfg.antiaim_epeek_mode:GetItemName() == "Always" then
        globalvars.epeek_key_input = true
    end
    util_functions.e_peek.onhandle(globalvars.epeek_key_input, ir)
end
function util_functions.ThirdpersonDistance()
    if cfg.thirdperson_dist:GetValue() ~= globalvars.thirdperson.dist then
        client.exec("cam_idealdist " .. cfg.thirdperson_dist:GetValue())
    end
end
globalvars.thirdperson.collison = nil
function util_functions.ThirdpersonCollision()
    if cfg.thirdperson_collision:GetValue() and globalvars.thirdperson.collison ~= false then
        client.exec("cam_collision 0")
        globalvars.thirdperson.collison = false
    elseif not cfg.thirdperson_collision:GetValue() and globalvars.thirdperson.collison ~= true then
        client.exec("cam_collision 1")
        globalvars.thirdperson.collison = true
    end
end
client.register_esp_flag(
    "RESOLVING",
    225,
    40,
    40,
    function(ent)
        if not ui.is_menu_open() and cfg.esp_flags:IsEnabled("Resolving") then
            return ui.get(globalvars.ref.correction_active_ref)
        end
    end
)
client.register_esp_flag(
    "LETHAL",
    240,
    0,
    0,
    function(ent)
        if not ui.is_menu_open() and cfg.esp_flags:IsEnabled("Lethal") then
            local is = {entity.hitbox_position(ent, "pelvis")}
            if #is == 3 then
                local _, dmg = client.trace_bullet(local_player, is[1], is[2], is[3], is[1], is[2], is[3])
                return entity.get_prop(ent, "m_iHealth") <= dmg
            end
        end
    end
)
client.register_esp_flag(
    "LOW-DELTA",
    225,
    40,
    40,
    function(ent)
        if globalvars.resolver_t ~= nil then
            if resolver.info[ent] then
                return resolver.IsActive() and cfg.esp_flags:IsEnabled("Low-Delta") and
                    resolver.info[ent].low_delta["active"] or
                    false
            else
                return false
            end
        else
            return false
        end
    end
)
client.register_esp_flag(
    "SW",
    200,
    40,
    40,
    function(ent)
        if globalvars.resolver_t ~= nil then
            if resolver.data[ent] then
                return resolver.IsActive() and cfg.esp_flags:IsEnabled("Slow-walk") and
                    resolver.data[ent].props["is_slowwalking"] or
                    false
            else
                return false
            end
        else
            return false
        end
    end
)
globalvars.antiaim.switch_side_toggle = false
function util_functions.handleKeybinds()
    if cfg.aimbot_enable:GetValue() then
        if cfg.aimbot_mode:GetItemName() == "Hold" then
            if cfg.aimbot_key:IsKeyDown() then
                ui.set(globalvars.ref.rage.aimbot_enabled[1], true)
                ui.set(globalvars.ref.automatic_fire, true)
                ui.set(globalvars.ref.rage.aimbot_enabled[2], "Always on")
            elseif not cfg.aimbot_key:IsKeyDown() then
                ui.set(globalvars.ref.rage.aimbot_enabled[2], "On hotkey")
            end
        elseif cfg.aimbot_mode:GetItemName() == "Toggle" then
            if cfg.aimbot_key:IsKeyToggle() then
                ui.set(globalvars.ref.rage.aimbot_enabled[1], true)
                ui.set(globalvars.ref.automatic_fire, true)
                ui.set(globalvars.ref.rage.aimbot_enabled[2], "Always on")
            else
                ui.set(globalvars.ref.rage.aimbot_enabled[2], "On hotkey")
            end
        elseif cfg.aimbot_mode:GetItemName() == "Always" then
            ui.set(globalvars.ref.rage.aimbot_enabled[1], true)
            ui.set(globalvars.ref.automatic_fire, true)
            ui.set(globalvars.ref.rage.aimbot_enabled[2], "Always on")
        end
    end
    if cfg.autowall_enable:GetValue() then
        if cfg.autowall_mode:GetItemName() == "Hold" then
            if cfg.autowall_key:IsKeyDown() then
                ui.set(globalvars.ref.rage.autowall, true)
            elseif not cfg.autowall_key:IsKeyDown() then
                ui.set(globalvars.ref.rage.autowall, false)
            end
        elseif cfg.autowall_mode:GetItemName() == "Toggle" then
            ui.set(globalvars.ref.rage.autowall, cfg.autowall_key:IsKeyToggle())
        elseif cfg.autowall_mode:GetItemName() == "Always" then
            ui.set(globalvars.ref.rage.autowall, true)
        end
    end
    if cfg.forcebaim_enable:GetValue() then
        if cfg.forcebaim_mode:GetItemName() == "Hold" then
            if cfg.forcebaim_key:IsKeyDown() then
                ui.set(globalvars.ref.rage.force_baim_key, "Always on")
            elseif not cfg.forcebaim_key:IsKeyDown() then
                ui.set(globalvars.ref.rage.force_baim_key, "On hotkey")
            end
        elseif cfg.forcebaim_mode:GetItemName() == "Toggle" then
            if cfg.forcebaim_key:IsKeyToggle() then
                ui.set(globalvars.ref.rage.force_baim_key, "Always on")
            else
                ui.set(globalvars.ref.rage.force_baim_key, "On hotkey")
            end
        elseif cfg.forcebaim_mode:GetItemName() == "Always" then
            ui.set(globalvars.ref.rage.force_baim_key, "Always on")
        end
    end
    if util_functions.dormant_aim ~= nil then
        if cfg.dormant_enable:GetValue() then
            util_functions.dormant_aim.is_enabled = true
            if cfg.dormant_mode:GetItemName() == "Hold" then
                if cfg.dormant_key:IsKeyDown() then
                    util_functions.dormant_aim.is_key = true
                elseif not cfg.dormant_key:IsKeyDown() then
                    util_functions.dormant_aim.is_key = false
                end
            elseif cfg.dormant_mode:GetItemName() == "Toggle" then
                util_functions.dormant_aim.is_key = cfg.dormant_key:IsKeyToggle()
            elseif cfg.dormant_mode:GetItemName() == "Always" then
                util_functions.dormant_aim.is_key = true
            end
        elseif util_functions.dormant_aim.is_enabled then
            util_functions.dormant_aim.is_enabled = false
            util_functions.dormant_aim.is_key = false
        elseif not cfg.dormant_enable:GetValue() then
            util_functions.dormant_aim = nil
            mariolua.unload_module("dormant_aim" .. (debug and "_dev" or ""))
        end
    elseif cfg.dormant_enable:GetValue() then
        util_functions.dormant_aim = mariolua.load_module("dormant_aim" .. (debug and "_dev" or ""))
    elseif not cfg.dormant_enable:GetValue() and util_functions.dormant_aim ~= nil then
        util_functions.dormant_aim = nil
        mariolua.unload_module("dormant_aim" .. (debug and "_dev" or ""))
    end
    if util_functions.wait_onshot ~= nil then
        if cfg.aimbot_wait_onshot:GetValue() then
            if cfg.aimbot_wait_onshot_mode:GetItemName() == "Hold" then
                if cfg.aimbot_wait_onshot_key:IsKeyDown() then
                    globalvars.wait_onshot_enabled = true
                    util_functions.wait_onshot.on_enable()
                elseif not cfg.aimbot_wait_onshot_key:IsKeyDown() then
                    globalvars.wait_onshot_enabled = false
                    util_functions.wait_onshot.on_disable()
                end
            elseif cfg.aimbot_wait_onshot_mode:GetItemName() == "Toggle" then
                if cfg.aimbot_wait_onshot_key:IsKeyToggle() then
                    globalvars.wait_onshot_enabled = true
                    util_functions.wait_onshot.on_enable()
                else
                    globalvars.wait_onshot_enabled = false
                    util_functions.wait_onshot.on_disable()
                end
            elseif cfg.aimbot_wait_onshot_mode:GetItemName() == "Always" then
                globalvars.wait_onshot_enabled = true
                util_functions.wait_onshot.on_enable()
            end
        elseif globalvars.wait_onshot_enabled then
            globalvars.wait_onshot_enabled = false
            util_functions.wait_onshot.on_disable()
        elseif not cfg.aimbot_wait_onshot:GetValue() then
            util_functions.wait_onshot = nil
            mariolua.unload_module("force_onshot")
        end
    elseif cfg.aimbot_wait_onshot:GetValue() then
        util_functions.wait_onshot = mariolua.load_module("force_onshot")
    elseif not cfg.aimbot_wait_onshot:GetValue() and util_functions.wait_onshot ~= nil then
        util_functions.wait_onshot = nil
        mariolua.unload_module("force_onshot")
    end
    if cfg.thirdperson_key:IsKeyToggle() then
        ui.set(ex, "Always on")
        ui.set(ew, true)
        ui.set(globalvars.ref.thirdperson_death_ref, true)
    else
        ui.set(ex, "On hotkey")
        ui.set(ew, false)
        ui.set(globalvars.ref.thirdperson_death_ref, false)
    end
end

globalvars.config_str = ""
function util_functions.cfg_on_load()
    mariolua.userdata.backup = database.read("mariolua_db_backup")
    if not mariolua.userdata.backup then
        return
    end
    globalvars.config_str = config.export()
end
function util_functions.save_cfg()
    mariolua.userdata.backup = database.read("mariolua_db_backup")
    if not mariolua.userdata.backup then
        return
    end
    local it = client.unix_time()
    writefile("config_backup_" .. it .. ".mariolua", globalvars.config_str)
    mariolua.print("Config backup created!")
    mariolua.PrintInChat("Config backup created!")
end
globalvars.double_tap_speed = "Off"
function util_functions.ExecuteCommand()
    local iu = cfg.double_tap_speed:GetItemName()
    local iv = entity.get_prop(entity.get_game_rules(), "m_bIsValveDS")
    if iv == 1 then
        if globalvars.double_tap_speed ~= "Off" then
            ui.set(globalvars.ref.misc.sv_maxusrcmdprocessticks, 16)
            cvar.cl_clock_correction:set_int(1)
            globalvars.double_tap_speed = "Off"
            mariolua.log("Double tab speed set to " .. globalvars.double_tap_speed)
        end
    elseif iu == "Off" and globalvars.double_tap_speed ~= "Off" then
        ui.set(globalvars.ref.misc.sv_maxusrcmdprocessticks, 16)
        cvar.sv_maxusrcmdprocessticks:set_int(16)
        cvar.cl_clock_correction:set_int(1)
        globalvars.double_tap_speed = "Off"
        mariolua.log("Double tab speed set to " .. globalvars.double_tap_speed)
    elseif iu == "Fast" and globalvars.double_tap_speed ~= "Fast" then
        ui.set(globalvars.ref.misc.sv_maxusrcmdprocessticks, 17)
        cvar.sv_maxusrcmdprocessticks:set_int(17)
        cvar.cl_clock_correction:set_int(0)
        globalvars.double_tap_speed = "Fast"
        mariolua.log("Double tab speed set to " .. globalvars.double_tap_speed)
    elseif iu == "+Fast" and globalvars.double_tap_speed ~= "+Fast" then
        ui.set(globalvars.ref.misc.sv_maxusrcmdprocessticks, 18)
        cvar.sv_maxusrcmdprocessticks:set_int(18)
        cvar.cl_clock_correction:set_int(0)
        globalvars.double_tap_speed = "+Fast"
        mariolua.log("Double tab speed set to " .. globalvars.double_tap_speed)
    elseif iu == "Max" and globalvars.double_tap_speed ~= "Max" then
        ui.set(globalvars.ref.misc.sv_maxusrcmdprocessticks, 20)
        cvar.sv_maxusrcmdprocessticks:set_int(20)
        cvar.cl_clock_correction:set_int(0)
        globalvars.double_tap_speed = "Max"
        mariolua.log("Double tab speed set to " .. globalvars.double_tap_speed)
    end
    if cfg.misc_features:IsEnabled("Disable Engine Sleep") and globalvars.engine_no_focus_sleep ~= 0 then
        globalvars.engine_no_focus_sleep = 0
        client.exec("engine_no_focus_sleep ", "0")
    elseif not cfg.misc_features:IsEnabled("Disable Engine Sleep") and globalvars.engine_no_focus_sleep then
        globalvars.engine_no_focus_sleep = 50
        client.exec("engine_no_focus_sleep ", "50")
    end
end
function util_functions.can_defuse_c4()
    local iw = entity.get_all("CPlantedC4")[1]
    if iw == nil then
        return
    end
    local ix, iy, iz = entity.get_prop(iw, "m_vecOrigin")
    local iA, iB, iC = entity.get_prop(local_player, "m_vecOrigin")
    local c1 = B.distance_3d(Vector3(ix, iy, iz), Vector3(iA, iB, iC))
    if c1 < 62 then
        return true
    end
    return false
end
function util_functions.can_rescure_hostage()
    local iD = entity.get_all("CHostage")[1]
    if iD == nil then
        return
    end
    local iE, iF, iG = entity.get_prop(iD, "m_vecOrigin")
    local iA, iB, iC = entity.get_prop(local_player, "m_vecOrigin")
    local c1 = B.distance_3d(Vector3(iE, iF, iG), Vector3(iA, iB, iC))
    if c1 < 62 then
        return true
    end
    return false
end
function util_functions.e_peek_fix()
    if cfg.antiaim_epeek:GetValue() and cfg.antiaim_epeek_key:GetKeyName() == "E" then
        if util_functions.can_rescure_hostage() then
            if globalvars.epeek_key_input and cfg.antiaim_epeek_key:IsKeyDown() then
                client.exec("+use")
                client.exec("unbind e")
            else
                client.exec("-use")
                client.exec("bind e +use")
            end
        elseif util_functions.can_defuse_c4() and cfg.antiaim_epeek_key:IsKeyDown() then
            if globalvars.epeek_key_input then
                client.exec("+use")
                client.exec("unbind e")
            else
                client.exec("-use")
                client.exec("bind e +use")
            end
        elseif globalvars.epeek_key_input and not entity.get_classname(entity.get_player_weapon(local_player)) == "CC4" then
            client.exec("-use")
            client.exec("unbind e")
        elseif globalvars.epeek_key_input and entity.get_classname(entity.get_player_weapon(local_player)) == "CC4" then
            client.exec("+use")
            client.exec("unbind e")
        else
            client.exec("-use")
            client.exec("bind e +use")
        end
    end
end
function util_functions.render_intersect()
    if cfg.settings_debugmode:IsEnabled("Show In Bounds") then
        for _, F in pairs(globalvars.intersect_t) do
            renderer.rectangle(F[1], F[2], F[3], F[4], 255, 0, 0, F[5])
            globalvars.intersect_t[F[1] .. F[2] .. F[3] .. F[4]] = nil
        end
    end
end
function util_functions.lock_input(cmd)
    local_player = entity.get_local_player()
    if cmd then
        globalvars.is_input_blocked = false
        ffi_utils.enable_input(ffi_utils.inputsystem, true)
        if ui.is_menu_open() then
            cmd.in_attack = 0
            cmd.in_attack2 = 0
        end
        if globalvars.lock_input["move"] then
            cmd.in_jump = 0
            cmd.in_reload = 0
            cmd.weaponselect = 0
            cmd.in_attack3 = 0
            cmd.forwardmove = 0
            cmd.sidemove = 0
        end
    elseif not local_player then
        globalvars.lock_input["attack"] = false
        globalvars.lock_input["move"] = false
        globalvars.is_input_blocked = ui.is_menu_open()
        ffi_utils.enable_input(ffi_utils.inputsystem, not globalvars.is_input_blocked)
    end
end
function util_functions.get_aa_condition()
    if not entity.is_alive(local_player) then
        return
    end
    local iH = ui.get(globalvars.ref.aa.slow_motion[1]) and ui.get(globalvars.ref.aa.slow_motion[2])
    local iI = functions.is_moving(local_player)
    local cn
    if not iH then
        cn = iI and "moving" or "standing"
    else
        cn = "slowmotion"
    end
    cfg.antiaim_enable = mariolua_menu.Reference("antiaim." .. cn .. ".enable")
    cfg.antiaim_state = mariolua_menu.Reference("antiaim." .. cn .. ".state")
    cfg.antiaim_fake = mariolua_menu.Reference("antiaim." .. cn .. ".fake")
    cfg.antiaim_fake_jitter = mariolua_menu.Reference("antiaim." .. cn .. ".fake.jitter")
    cfg.antiaim_fake_jitter_enable = mariolua_menu.Reference("antiaim." .. cn .. ".fake.jitter.enable")
    cfg.antiaim_fake_jitter_speed = mariolua_menu.Reference("antiaim." .. cn .. ".fake.jitter.speed")
    cfg.antiaim_body = mariolua_menu.Reference("antiaim." .. cn .. ".body")
    cfg.antiaim_body_mode = mariolua_menu.Reference("antiaim." .. cn .. ".body.mode")
    cfg.antiaim_lbyextras_mode = mariolua_menu.Reference("antiaim." .. cn .. ".lbyextras")
    cfg.antiaim_freestanding = mariolua_menu.Reference("antiaim." .. cn .. ".mode")
    cfg.antiaim_lby_mode = mariolua_menu.Reference("antiaim." .. cn .. ".lby")
    cfg.antiaim_slowwalk = mariolua_menu.Reference("antiaim." .. cn .. ".slowwalk.enabled")
    cfg.antiaim_slowwalk_speed = mariolua_menu.Reference("antiaim." .. cn .. ".slowwalk.speed")
    cfg.antiaim_enable_anti_bruteforce = mariolua_menu.Reference("antiaim." .. cn .. ".antibruteforce.enable")
    cfg.antiaim_anti_bruteforce_timeout = mariolua_menu.Reference("antiaim." .. cn .. ".antibruteforce.timeout")
    cfg.antiaim_default_yaw_offset = mariolua_menu.Reference("antiaim." .. cn .. ".antibruteforce.yaw.default")
    cfg.antiaim_max_yaw_delta = mariolua_menu.Reference("antiaim." .. cn .. ".antibruteforce.yaw.max")
    cfg.antiaim_yaw_step = mariolua_menu.Reference("antiaim." .. cn .. ".antibruteforce.yaw.step")
    cfg.antiaim_default_body_yaw_offset = mariolua_menu.Reference("antiaim." .. cn .. ".antibruteforce.bodyyaw.default")
    cfg.antiaim_max_body_yaw_delta = mariolua_menu.Reference("antiaim." .. cn .. ".antibruteforce.bodyyaw.max")
    cfg.antiaim_body_yaw_step = mariolua_menu.Reference("antiaim." .. cn .. ".antibruteforce.bodyyaw.step")
end
function globalvars.antiaim.LoadConfigList()
    cfg.antiaim_config_list.items = {}
    config_db = database.read("mariolua_db_aa_config") or {}
    local items = {}
    for Q, F in pairs(config_db) do
        table.insert(items, Q)
    end
    cfg.antiaim_config_list:SetItems(items)
end
function globalvars.antiaim.SaveConfig()
    local iJ = cfg.antiaim_config_input:GetValue()
    local iK = cfg.antiaim_config_list:GetItemName()
    local iL = iJ == "" and iK or iJ
    local a8 = "{\n"
    for I = 1, #globalvars.reference do
        local cy = globalvars.reference[globalvars.reference[I]]
        if
            cy.reference:match("antiaim.") and cy:GetValue() ~= nil and
                (cy.type ~= "text" or cy.type == "text" and cy.should_save) and
                (cy.type ~= "tab" or cy.type == "tab" and cy.should_save) and
                cy.type ~= "button" and
                cy.type ~= "groupbox" and
                cy.type ~= "editbox" and
                cy.type ~= "multibox_item"
         then
            if type(cy:GetValue()) == "table" or cy.type == "save_var" or cy.type == "multibox" then
                a8 =
                    a8 ..
                    "['" ..
                        globalvars.reference[I] ..
                            "']" .. " = " .. extended_table.table_to_string3(cy:GetValue()) .. ",\n"
            elseif type(cy:GetValue()) == "string" then
                a8 = a8 .. "['" .. globalvars.reference[I] .. "']" .. " = " .. cy:GetValue() .. ",\n"
            elseif type(cy:GetValue()) == "boolean" or "number" then
                a8 = a8 .. "['" .. globalvars.reference[I] .. "']" .. " = " .. tostring(cy:GetValue()) .. ",\n"
            end
        end
    end
    a8 = a8 .. "}"
    config_db = database.read("mariolua_db_aa_config") or {}
    config_db[iL] = a8
    if extended_table.Contains(cfg.antiaim_config_list.item_list, iL) then
        return mariolua.notify(5, 'Anti-Aim config "' .. iL .. '" saved')
    end
    cfg.antiaim_config_list:AddItem(cfg.antiaim_config_input:GetValue())
    mariolua.log("Added " .. iL .. " to Config List")
    mariolua.notify(5, 'Anti-Aim config "' .. iL .. '" added and saved')
end
function globalvars.antiaim.LoadConfig()
    local gv = cfg.antiaim_config_list:GetItemName()
    config_db = database.read("mariolua_db_aa_config") or {}
    mariolua_menu.LoadConfig(config_db[gv])
    mariolua.notify(5, 'Anti-Aim config "' .. gv .. '" loaded')
    mariolua.log("Loaded " .. gv .. " from Config List")
end
function globalvars.antiaim.RemoveConfig()
    config_db = database.read("mariolua_db_aa_config") or {}
    for I = 1, #cfg.antiaim_config_list.items do
        if cfg.antiaim_config_list.items[I] == nil then
            return
        end
        local gv = cfg.antiaim_config_list:GetItemName()
        if cfg.antiaim_config_list.items[I].text == gv then
            config_db[gv] = nil
            table.remove(cfg.antiaim_config_list.items, I)
            table.remove(cfg.antiaim_config_list.item_list, I)
            database.write("mariolua_db_aa_config", config_db)
            mariolua.notify(5, 'Anti-Aim config "' .. gv .. '" removed')
            mariolua.log("Removed " .. gv .. " from Config List")
        end
    end
end
util_functions.debug_event_infos()
function util_functions.handle_sv_pure_fix()
    if util_functions.sv_pure_fix ~= nil and cfg.misc_features:IsEnabled("Bypass sv_pure Fix") then
        util_functions.sv_pure_fix.on_paint_ui()
    elseif cfg.misc_features:IsEnabled("Bypass sv_pure Fix") then
        util_functions.sv_pure_fix = mariolua.load_module("sv_pure_fix")
    elseif not cfg.misc_features:IsEnabled("Bypass sv_pure Fix") and util_functions.sv_pure_fix ~= nil then
        util_functions.sv_pure_fix = nil
        mariolua.unload_module("sv_pure_fix")
    end
end
function util_functions.handle_clipboard()
    local iM = ffi_utils.gcpbs(ffi_utils.isystem)
    local iN = ffi.new("char[?]", iM)
    ffi_utils.gcpbt(ffi_utils.isystem, 0, iN, iM * ffi.sizeof("char[?]", iM))
    local iO = ffi.string(iN)
    if iO ~= globalvars.clipboard and iO ~= nil then
        globalvars.clipboard = iO
        if string.len(iO) > 60 then
            cfg.settings_clipboardtext.label =
                "Clipboard: " ..
                string.sub(iO, extended_math.clamp(string.len(iO), 0, -30)) ..
                    "' ... '" .. string.sub(iO, extended_math.clamp(string.len(iO) * -1, -30, 0))
        else
            cfg.settings_clipboardtext.label = "Clipboard: " .. iO
        end
    end
end
function util_functions.handle_resovler()
    if cfg.resolver_enable:GetValue() and globalvars.resolver_t == nil then
        globalvars.resolver_t = mariolua.load_module("resolver" .. (debug and "_dev" or ""))
        if globalvars.resolver_t ~= nil then
            resolver = globalvars.resolver_t.resolver
            local iP = globalvars.resolver_t.resolver.get_vars().cache
            globalvars.resolver_t.resolver.set_vars(
                {
                    mariolua = mariolua,
                    cache = extended_table.merge(iP, globalvars),
                    bindings = ffi_utils,
                    refs = cfg,
                    cmath = extended_math,
                    centity = functions,
                    cstring = base64,
                    crender = extended_renderer,
                    cangle = external_modules,
                    Angle = z,
                    Vector3 = Vector3,
                    vector = B,
                    plylist = h6,
                    cweapon = y
                }
            )
            resolver = globalvars.resolver_t.resolver.get_vars().resolver
        end
    elseif cfg.resolver_enable:GetValue() then
        resolver.handle()
    elseif not cfg.resolver_enable:GetValue() and (globalvars.resolver_t ~= nil or resolver ~= nil) then
        globalvars.resolver_t = nil
        mariolua.unload_module("resolver" .. (debug and "_dev" or ""))
    end
end
function util_functions.handle_aa()
    if cfg.antiaim_enable:GetValue() and globalvars.aa_t == nil then
        globalvars.aa_t = mariolua.load_module("antiaim" .. (debug and "_dev" or ""))
        if globalvars.aa_t ~= nil then
            globalvars.antiaim = extended_table.merge(globalvars.aa_t.antiaim, globalvars.antiaim)
            util_functions.anti_aim = globalvars.antiaim.handle
            globalvars.aa_t.antiaim.set_vars(
                {
                    mariolua = mariolua,
                    cache = globalvars,
                    bindings = ffi_utils,
                    refs = cfg,
                    cmath = extended_math,
                    centity = functions,
                    ctable = extended_table,
                    cstring = base64,
                    crender = extended_renderer,
                    cangle = external_modules,
                    Angle = z,
                    Vector3 = Vector3,
                    vector = B
                }
            )
            globalvars.aa_t = {}
            globalvars.antiaim.LoadConfigList()
        end
    elseif cfg.antiaim_enable:GetValue() and util_functions.anti_aim ~= nil then
        util_functions.anti_aim()
    end
end
function util_functions.handle_dormant_aim()
    if util_functions.dormant_aim ~= nil then
        if util_functions.dormant_aim.is_enabled and cfg.dormant_enable:GetValue() then
            util_functions.dormant_aim.on_run_command(cmd)
            util_functions.dormant_aim.chat_print = mariolua.PrintInChat
            util_functions.dormant_aim.log = mariolua.log
        end
        if mariolua.userdata.permissions == "Developer" or mariolua.userdata.permissions == "Moderator" then
            util_functions.dormant_aim.set_data(
                cfg.dormant_hitchance:GetValue(),
                cfg.dormant_mindmg:GetValue(),
                cfg.dormant_extra:IsEnabled("Quick Stop"),
                cfg.dormant_extra:IsEnabled("Force Baim"),
                cfg.dormant_extra:IsEnabled("Automatic Scope")
            )
        else
            util_functions.dormant_aim.set_data(
                cfg.dormant_hitchance:GetValue(),
                cfg.dormant_mindmg:GetValue(),
                cfg.dormant_extra:IsEnabled("Quick Stop"),
                cfg.dormant_extra:IsEnabled("Force Baim"),
                cfg.dormant_extra:IsEnabled("Automatic Scope")
            )
        end
    end
end
function util_functions.handle_antibrute()
    if util_functions.antibrute ~= nil and cfg.antiaim_enable_anti_bruteforce:GetValue() then
        util_functions.antibrute.enable_anti_bruteforce = false
        util_functions.antibrute.anti_bruteforce_timeout = cfg.antiaim_anti_bruteforce_timeout:GetValue()
        util_functions.antibrute.default_yaw_offset = cfg.antiaim_default_yaw_offset:GetValue()
        util_functions.antibrute.max_yaw_delta = cfg.antiaim_max_yaw_delta:GetValue()
        util_functions.antibrute.yaw_step = cfg.antiaim_yaw_step:GetValue()
        util_functions.antibrute.default_body_yaw_offset = cfg.antiaim_default_body_yaw_offset:GetValue()
        util_functions.antibrute.max_body_yaw_delta = cfg.antiaim_max_body_yaw_delta:GetValue()
        util_functions.antibrute.body_yaw_step = cfg.antiaim_body_yaw_step:GetValue()
        util_functions.antibrute.vector = Vector3
        util_functions.antibrute.log = mariolua.log
        util_functions.antibrute.on_run_command()
    elseif cfg.antiaim_enable_anti_bruteforce:GetValue() then
        util_functions.antibrute = mariolua.load_module("antibruteforce")
    end
end
function util_functions.handle_pre_render()
    globalvars.mouse1_key_state = b4(client.key_state(0x01))
    globalvars.mouse_x, globalvars.mouse_y = ui.mouse_position()
    globalvars.gs_menu_x, globalvars.gs_menu_y = ui.menu_position()
    globalvars.gs_menu_w, globalvars.gs_menu_h = ui.menu_size()
    globalvars.menu.w, globalvars.menu.h = er:GetWindowSize()
    globalvars.menu.x = er.pos.x or globalvars.menu.x
    globalvars.menu.y = er.pos.y or globalvars.menu.y
end
client.set_event_callback(
    "paint_ui",
    function(ctx)
        local_player = entity.get_local_player()
        util_functions.debug_event_infos()
        util_functions.lock_input()
        if ui.is_menu_open() then
            er:Render()
        end
        if not util_functions.IsEventActive("run_command") then
            if not ffi_utils.console_is_visible(ffi_utils.engine_client) then
                util_functions.handleKeybinds()
                if globalvars.resolver_t ~= nil then
                    resolver.ResolverToggle()
                end
            end
            util_functions.AdaptiveWepCfg()
            if ui.is_menu_open() then
                util_functions.handle_clipboard()
            end
        end
        util_functions.handleIndicators()
        util_functions.ReportEnemy()
        util_functions.DynamicFOV_Draw()
        util_functions.Watermark()
        util_functions.RenderEnemyAngles()
        util_functions.drawException()
        util_functions.RenderExtrapolation()
        util_functions.Skeleton()
        util_functions.handle_sv_pure_fix()
        if globalvars.resolver_t ~= nil then
            resolver.render_resolver_indicator()
        end
        if mariolua.debug.draw["player_infos"] then
            util_functions.player_infos()
        end
        if cfg.visuals_angle_circle:GetValue() and util_functions.angle_indicator ~= nil then
            util_functions.angle_indicator.on_render()
        elseif cfg.visuals_angle_circle:GetValue() then
            util_functions.angle_indicator = mariolua.load_module("angle_indicator")
        elseif not cfg.visuals_angle_circle:GetValue() and util_functions.angle_indicator ~= nil then
            util_functions.angle_indicator = nil
            mariolua.unload_module("angle_indicator")
        end
        if cfg.visuals_penetration_damage:GetValue() and util_functions.penetration_damage ~= nil then
            util_functions.penetration_damage.on_render()
        elseif cfg.visuals_penetration_damage:GetValue() then
            util_functions.penetration_damage = mariolua.load_module("penetration_damage")
        elseif not cfg.visuals_penetration_damage:GetValue() and util_functions.penetration_damage ~= nil then
            util_functions.penetration_damage = nil
            mariolua.unload_module("penetration_damage")
        end
        if util_functions.dormant_aim ~= nil and cfg.dormant_enable:GetValue() then
            if util_functions.dormant_aim.is_enabled then
                util_functions.dormant_aim.on_paint()
            end
        end
        util_functions.render_intersect()
    end
)
client.set_event_callback(
    "pre_render",
    function()
        util_functions.handle_pre_render()
        er:Update()
        util_functions.handleWindow()
    end
)
client.set_event_callback(
    "run_command",
    function(cmd)
        util_functions.AdaptiveWepCfg()
        util_functions.DynamicFOV()
        util_functions.LegitAutowall()
        util_functions.FakeDuckFix()
        util_functions.OnVoteReset()
        util_functions.e_peek_fix()
        util_functions.handle_weapon_tabs()
        util_functions.handle_resovler()
        util_functions.handle_aa()
        util_functions.handle_dormant_aim()
        util_functions.handle_antibrute()
        h6.force_correction()
        util_functions.get_aa_condition()
        if not ffi_utils.console_is_visible(ffi_utils.engine_client) then
            util_functions.handleKeybinds()
            if globalvars.resolver_t ~= nil then
                resolver.ResolverToggle()
            end
        end
        if ui.is_menu_open() then
            util_functions.handle_clipboard()
        end
        if cfg.visuals_angle_circle:GetValue() and util_functions.angle_indicator ~= nil then
            util_functions.angle_indicator.on_run_command()
        end
        if util_functions.e_peek ~= nil then
            util_functions.handle_epeek()
        elseif cfg.antiaim_epeek:GetValue() then
            util_functions.e_peek = mariolua.load_module("e_peek" .. (debug and "_dev" or ""))
        elseif not cfg.antiaim_epeek:GetValue() and util_functions.e_peek ~= nil then
            util_functions.e_peek = nil
            mariolua.unload_module("e_peek" .. (debug and "_dev" or ""))
        end
        if util_functions.baim_lethal ~= nil then
            if util_functions.baim_lethal.enabled then
                util_functions.baim_lethal.on_run_command()
            end
        end
        if util_functions.penetration_damage ~= nil and cfg.visuals_penetration_damage:GetValue() then
            util_functions.penetration_damage.on_run_command()
        end
    end
)
globalvars.restore_callback = false
function util_functions.delay_call()
    if not mariolua.is_library then
        client.delay_call(1, util_functions.delay_call)
        return
    elseif not globalvars.restore_callback then
        client.set_event_callback = globalvars.old_client_set_event_callback
        client.unset_event_callback = globalvars.old_client_unset_event_callback
        ui.set_callback = globalvars.old_ui_set_callback
        globalvars.restore_callback = true
    end
    globalvars.color[1], globalvars.color[2], globalvars.color[3], globalvars.color[4] =
        ui.get(ui.reference("MISC", "Settings", "Menu color"))
    util_functions.ExecuteCommand()
    util_functions.ThirdpersonDistance()
    util_functions.ThirdpersonCollision()
    util_functions.set_resolver_modules()
    client.delay_call(0.5, util_functions.delay_call)
end
util_functions.delay_call()
client.set_event_callback(
    "net_update_start",
    function(cmd)
        util_functions.AnimatedClanTag()
    end
)
client.set_event_callback(
    "setup_command",
    function(cmd)
        util_functions.Slidewalk(cmd)
        util_functions.FakelagChams(cmd)
        util_functions.AntiResolver(cmd)
        util_functions.FakelagOnPeek(cmd)
        util_functions.lock_input(cmd)

        if util_functions.dormant_aim ~= nil then
            if util_functions.dormant_aim.is_key then
                util_functions.dormant_aim.on_setup_command(cmd)
            end
        end
        if util_functions.freezetime_antiaim ~= nil and cfg.antiaim_freezetime:GetValue() then
            if util_functions.freezetime_antiaim.on_setup_command then
                util_functions.freezetime_antiaim.on_setup_command(cmd)
            end
        elseif cfg.antiaim_freezetime:GetValue() and util_functions.freezetime_antiaim == nil then
            util_functions.freezetime_antiaim = util_functions.freezetime_antiaim or {}
            util_functions.freezetime_antiaim.on_setup_command =
                util_functions.freezetime_antiaim.on_setup_command or false
            if not util_functions.freezetime_antiaim.on_setup_command then
                util_functions.freezetime_antiaim =
                    mariolua.load_module("freezetime_antiaim" .. (debug and "_dev" or ""))
            end
        elseif not cfg.antiaim_freezetime:GetValue() and util_functions.freezetime_antiaim ~= nil then
            util_functions.freezetime_antiaim = nil
            mariolua.unload_module("freezetime_antiaim" .. (debug and "_dev" or ""))
        end
        if cmd.chokedcommands == 0 then
            globalvars.indicator.real_yaw =
                math.min(60, math.abs(entity.get_prop(local_player, "m_flPoseParameter", 11) * 120 - 60))
        elseif cmd.chokedcommands == 1 then
            globalvars.indicator.fake_yaw = cmd.yaw
        end
    end
)

client.set_event_callback(
    "predict_command",
    function(iQ)
        if util_functions.wait_onshot ~= nil then
            util_functions.wait_onshot.handle(iQ)
        end
    end
)

client.set_event_callback(
    "weapon_fire",
    function(au)
        local e = entity.get_local_player()
        local ent = client.userid_to_entindex(au.userid)

        if not entity.is_alive(e) or ent == e then
            return
        end

        if globalvars.resolver_t ~= nil then
            resolver.on_weapon_fire(ent)
        end
    end
)

client.set_event_callback(
    "bullet_impact",
    function(ar)
        if util_functions.antibrute ~= nil and cfg.antiaim_enable_anti_bruteforce:GetValue() then
            util_functions.antibrute.on_bullet_impact(ar)
        end
    end
)

client.set_event_callback(
    "game_start",
    function(au)
        if globalvars.resolver_t ~= nil then
            resolver.on_game_start()
        end
    end
)

client.set_event_callback(
    "game_end",
    function(au)
        if globalvars.resolver_t ~= nil then
            resolver.on_game_end()
        end
    end
)

client.set_event_callback(
    "cs_game_disconnected",
    function(au)
        if globalvars.resolver_t ~= nil then
            resolver.on_cs_game_disconnected()
        end

        globalvars.player_list = {}
        globalvars.should_namespam = false
        globalvars.spam_counter = 0
    end
)

client.set_event_callback(
    "player_connect_full",
    function(au)
        ffi_utils.initHudChat()
        globalvars.report.total_reports = 0
        globalvars.report.should_report = false
        globalvars.report.idx = 1
        globalvars.report.next_report_time = 0
        globalvars.stored_weakest_hitbox = {}
    end
)

function util_functions.on_shutdown()
    --- LOL
end

client.set_event_callback(
    "shutdown",
    function()
        cvar.name:set_string(globalvars.name_backup)

        if globalvars.resolver_t ~= nil then
            resolver.onshot_shutdown()
            ui.set(globalvars.ref.plist_resetall_ref, true)
            resolver.full_reset()
        end

        globalvars.lock_input["attack"] = false
        globalvars.lock_input["move"] = false
        ffi_utils.enable_input(ffi_utils.inputsystem, true)
        util_functions.on_shutdown()
        globalvars.player_list = {}
    end
)

client.set_event_callback(
    "vote_options",
    function(au)
        util_functions.on_vote_options(au)
    end
)

client.set_event_callback(
    "vote_cast",
    function(au)
        util_functions.on_vote_cast(au)
    end
)

client.set_event_callback(
    "round_freeze_end",
    function(au)
        util_functions.hold_bot(au)
    end
)

client.set_event_callback(
    "player_death",
    function(au)
        local iR = au.userid
        local ent = client.userid_to_entindex(iR)
        local iS = client.userid_to_entindex(iR)
        local iT = client.userid_to_entindex(au.attacker)
        local iU = entity.get_local_player()

        if globalvars.resolver_t ~= nil then
            resolver.on_player_death(ent)
        end

        functions.extrapolated_ent_data = functions.extrapolated_ent_data or {}
        functions.extrapolated_ent_data[ent] = {}
    end
)

client.set_event_callback(
    "round_start",
    function()
        if globalvars.resolver_t ~= nil then
            resolver.on_round_start()
        end

        globalvars.visible_hitboxes = {}
        functions.extrapolated_ent_data = {}
    end
)

client.set_event_callback(
    "round_end",
    function()
        if globalvars.resolver_t ~= nil then
            resolver.on_round_end()
        end
    end
)

local iV = ui.reference("CONFIG", "Presets", "Load")
local iW = ui.reference("CONFIG", "Presets", "Save")
local iX = ui.reference("CONFIG", "Presets", "Import from clipboard")
local iY = ui.reference("CONFIG", "Presets", "Export to clipboard")
local iZ = ui.reference("CONFIG", "Lua", "Reload active scripts")

ui.set_callback(
    iW,
    function()
        mariolua_menu.SaveConfig()
    end
)

ui.set_callback(
    iY,
    function()
        mariolua_menu.SaveConfig()
    end
)

ui.set_callback(
    iX,
    function()
        mariolua_menu.LoadConfig()
    end
)

client.set_event_callback(
    "pre_config_save",
    function()
        util_functions.save_cfg()
        mariolua_menu.SaveConfig()
    end
)

client.set_event_callback(
    "post_config_save",
    function()
        mariolua_menu.SaveConfig()

        mariolua.print("Config saved")
        mariolua.notify(5, "Config saved")
    end
)

client.set_event_callback(
    "post_config_load",
    function()
        util_functions.cfg_on_load()
        mariolua_menu.LoadConfig()
        globalvars.antiaim.LoadConfigList()
        util_functions.LoadNameSpamItems()

        mariolua.print("Config loaded")
        mariolua.notify(5, "Config loaded")
    end
)

mariolua_menu.LoadConfig()
globalvars.antiaim.LoadConfigList()
util_functions.LoadNameSpamItems()

mariolua.print("MarioLua loaded!")
mariolua.print(string.format("Welcome %s %s!", mariolua.userdata.permissions, mariolua.userdata.name))
